({
	doInit : function(component, event, helper) {
     
        alert('recordId' + component.get("v.recordId"));
        helper.getCaseData(component,event,helper);
		helper.getAppointments(component, event, helper);
    },
    
    cancelAppointment : function(component,event,helper){
        alert('Cancel');
    },
    
    reScheduleAppointment : function(component,event,helper){
        var selectedAppointment = component.get("v.appointmentDetailInfo"); 
        if(selectedAppointment == null || selectedAppointment == undefined){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type": "Error",
                "message": 'Please select atleast one slot'
            });
            toastEvent.fire();
            return;
        }
        alert('ReSchedule Appointment for ' + JSON.stringify(selectedAppointment));
    },

    scheduleAppointment : function(component,event,helper){
        var selectedAppointment = component.get("v.appointmentDetailInfo");
        if(selectedAppointment == null || selectedAppointment == undefined){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type": "Error",
                "message": 'Please select atleast one slot'
            });
            toastEvent.fire();
            return;
        }
        helper.scheduleAppoint(component,event,helper);
    },

    selectSlot : function(component,event,helper){
      var selectedSlot = event.getSource().get("v.label");
     var dataName = event.getSource().get("v.name");
        var appointData = component.get("v.appointmentData");
        var allTypeData = appointData.offerData;
        for(var i=0;i<allTypeData.length;i++){
            var offrData = allTypeData[i].availOffers;
            for(var k=0;k<offrData.length;k++){
                offrData[k].isOfferSelected = false;
            }
            if(allTypeData[i].offerName == dataName){
                var slotDataList = allTypeData[i].availOffers;
               for(var j=0;j<slotDataList.length;j++){
                   var availDate = slotDataList[j].timeWindowStart+' '+ slotDataList[j].timeWindowCd + '-' + slotDataList[j].timeWindowEnd+' '+ slotDataList[j].timeWindowCd;
                    if(availDate == selectedSlot){
                        slotDataList[j].isOfferSelected = true;
                    }else{
                        slotDataList[j].isOfferSelected = false;
                    }
               }
            }
        }
        component.set("v.appointmentData",appointData);
    },
})