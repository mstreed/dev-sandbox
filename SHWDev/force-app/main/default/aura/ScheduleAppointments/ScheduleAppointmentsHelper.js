({
    getCaseData : function(component,event,helper){
        
        var action = component.get("c.getCaseRecord");
        action.setParams({
            "recordId" : component.get("v.recordId") 
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();
            if (state === "SUCCESS") {
                if(result != null && result != undefined){
                    component.set("v.caseRecord",result);
                }
            }else if(state == "ERROR"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "Error",
                    "message": response.getError()[0].message
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    getAppointments : function(component,event,helper) {
        component.set("v.spinner",true);
        var action = component.get("c.getAllAppointments");
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();
            if (state === "SUCCESS") {
                if(result != null){
                    if(result.statusCode == 'Success'){
                        if(result.availWrapper.responseMessage == 'SUCCESS'){
                        component.set("v.appointmentData",result);
                        component.set("v.spinner",false);
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type": "Success",
                            "message": result.availWrapper.responseMessage
                        });
                        toastEvent.fire();
                   }
                    else{
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type": "Error",
                            "title": result.availWrapper.responseMessage,
                            "message": JSON.stringify(result.availWrapper.messages) + ' Response Code : ' + result.availWrapper.responseCode
                        });
                        toastEvent.fire();
                       component.set("v.spinner",false);
                    }
                    }else{
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type": "Error",
                            "message": result.statusMessage
                        });
                        toastEvent.fire();
                        component.set("v.spinner",false);
                    }
                    
                }
            }else if(state == "ERROR"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "Error",
                    "message": response.getError()[0].message
                });
                toastEvent.fire();
                component.set("v.spinner",false);
            }
        });
        $A.enqueueAction(action);
	},
    
    scheduleAppoint : function(component,event,helper){
        component.set("v.spinner",true);
        var action = component.get("c.scheduleAnAppointment");
        action.setParams({
            "availWrapData" : JSON.stringify(component.get("v.appointmentData")),
            "selectedOfferData" : JSON.stringify(component.get("v.appointmentDetailInfo")),
            "caseData" : JSON.stringify(component.get("v.caseRecord"))
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();
            if (state === "SUCCESS") {
                if(result != null){
                    if(result.statusCode == 'Success'){
                        if(result.orderSWrapper.ResponseMessage == 'SUCCESS'){
                        component.set("v.spinner",false);
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type": "Success",
                            "message": result.orderSWrapper.ResponseMessage
                        });
                        toastEvent.fire();
                   }
                    else{
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type": "Error",
                            "title": result.orderSWrapper.responseMessage,
                            "message": JSON.stringify(result.orderSWrapper.messages) + ' Response Code : ' + result.orderSWrapper.ResponseCode
                        });
                        toastEvent.fire();
                       component.set("v.spinner",false);
                    }
                    }else{
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type": "Error",
                            "message": result.statusMessage
                        });
                        toastEvent.fire();
                        component.set("v.spinner",false);
                    }
                    
                }
            }else if(state == "ERROR"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "Error",
                    "message": response.getError()[0].message
                });
                toastEvent.fire();
                component.set("v.spinner",false);
            }
        });
        $A.enqueueAction(action);
        
    },
})