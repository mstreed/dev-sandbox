public with sharing class AccountDetailController {
   
    public Account thisAcc {set; get;}
    public String accountId {set; get;}
    public String errorMsg {set; get;}
    
    public AccountDetailController( ApexPages.StandardController controller ){
        errorMsg  = '';
         // get account record
        accountId = ApexPages.currentPage().getParameters().get('id');
        if ( String.isBlank( accountId ) ) {
            // must have accountId , otherwise bail out
            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, 'No account ID provided.' ) );
            return;
        }
        thisAcc = [
            SELECT
                Id,
                Name,
                Priority__c,
                PersonEmail,
                Phone,
                OwnerId,
                PersonMobilePhone,
                Account_Status__c,
                Spend__c,
                Account_Type__c,
                Type,
                Owner.Name,
                Description,
                Admin_Contract_Application_Status__c,
                CreatedBy.Name,
                LastModifiedBy.Name,
                BillingPostalCode,
                BillingCountry,
                BillingState,
                BillingCity,
                BillingStreet,
                CreatedById,
                CreatedDate,
                LastModifiedById,
                LastModifiedDate,
                Priority_Visual__c
            FROM Account
            WHERE Id = :accountId 
        ];
    }
    public PageReference deleteRecord(){
        try{
           //delete account record
           delete thisAcc ;
           Schema.DescribeSObjectResult result = Account.SObjectType.getDescribe(); 
           PageReference pageRef = new PageReference('/' + result.getKeyPrefix()+'/o'); 
           pageRef.setRedirect(true); 
           return pageRef; 
        }catch(exception ex ){
            if( String.isNotBlank( ex.getMessage()) && ex.getMessage().contains('error:') ) {
                errorMsg = ex.getMessage().split('error:')[1].split(':')[0] + '.';
            }
            else {
               errorMsg = ex.getMessage();     
            }
            return NULL;
        }
    }   
}