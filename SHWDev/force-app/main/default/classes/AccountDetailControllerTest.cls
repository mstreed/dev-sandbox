/*

    @Description    AccountDetailController Class 
    @createdate     19 May 2018

*/

@isTest
private class AccountDetailControllerTest {
    private static DataFactory df = new DataFactory();

    @isTest 
    static void controllerTest() {
        df.createGlobalSetting();
        df.createPolicy();

        System.assertNotEquals(df.acc,NULL );     
        Test.StartTest();
        PageReference pageRef = Page.AccountDetail;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put( 'id',df.acc.id );
        ApexPages.StandardController controller = new ApexPages.StandardController(df.acc);
        AccountDetailController extension = new AccountDetailController(controller);
        System.assertEquals( df.acc.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        System.assertEquals( extension.thisAcc.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        Test.StopTest();
    }
    @isTest static void deleteRecord() {
        df.createGlobalSetting();
        df.createPolicy();
        
        System.assertNotEquals( df.acc,NULL );
        Test.StartTest();
            PageReference pageRef = Page.AccountDetail;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put( 'id',NULL );
            System.assertEquals( NULL, Apexpages.currentpage().getparameters().get( 'id' ) );
            ApexPages.StandardController controller = new ApexPages.StandardController( df.acc );
            AccountDetailController extension = new AccountDetailController(controller);
            extension.thisAcc = df.acc ;
            extension.deleteRecord();
            List<Account> acList = [SELECT Id,Name FROM Account WHERE Name = 'testAccount'];
            System.assertEquals( 0, acList.size() );
            extension.thisAcc = NULL;
            extension.deleteRecord();
        Test.StopTest();
    }
}