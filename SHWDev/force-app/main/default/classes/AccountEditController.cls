public class AccountEditController {
    
    public Account thisAcc {get; set;}
    private Id lastModifiedBy;
    private DateTime lastModifiedDate;
    public Contact[] thisCon {get; set;}
    public User[] partnerUsr {get; set;}
    public Account conQuickEmail {get; set;}
    public String affLink {get; set;}
    public Boolean editContractors {get; set;}
    public Boolean editAffiliates {get; set;}
    public String currentRecordId {get; set;}
    public String errorMsg {get; set;}
    public String successMsg {get;set;}
    public String btnAction {get;set;}
    public String recordtype {get;set;}
    public Id affliatePortalId {get;set;}
    //public List<Case> caseList {get;set;}
    public List<WrapHistory> historyList {get;set;}
    public List<WrapRating> ratingList {get;set;}
    public List<Task> emailList{get;set;}
    public List<Contractor_Insurance__c> insuranceList{get;set;}
    
    public Note thisNote {get;set;} 
    public List<Note> notesList{get;set;}
    
    public List<Account> existingContractors {set; get;}
    
    public List<View__c> viewList{get;set;}
    
    public Global_Setting__c gs {get;set;}
    
    public AccountEditController(ApexPages.StandardController stdController){
        this.thisAcc = (Account)stdController.getRecord();
        affLink = 'https://quote.selecthomewarranty.com/affiliate/'+thisAcc.Affiliate_ID__c;
        errorMsg ='';
        // get account record
        currentRecordId = ApexPages.currentPage().getParameters().get('id');
        thisCon = new List<Contact>();
        if(String.isBlank(currentRecordId)) {
            thisAcc.RecordTypeId = ApexPages.currentPage().getParameters().get('RecordType');
            thisCon.add(new Contact());
        } else {
            thisCon = [Select Id, FirstName, LastName FROM Contact WHERE AccountId = :currentRecordId];
            if (thisCon.size()>0) {
                partnerUsr = [Select Id, IsActive FROM User WHERE ContactId = :thisCon[0].Id];
                //isPartner = partnerUsr[0].IsActive;
            } else {
                thisCon.add(new Contact());
            }
        }
        affliatePortalId = [SELECT Id,Name FROM Network WHERE Name = 'SHW Affiliate Portal'].Id;
        
        relatedRecords();
        checkForDupe();
        gs = [Select Id, System_Administrator_User_ID__c, Affiliate_Lead_Access_Start_Date__c FROM Global_Setting__c LIMIT 1];
        system.debug('thisAcc.RecordTypeId: ' + thisAcc.RecordTypeId);
        recordtype = [SELECT Id, Name, DeveloperName FROM RecordType WHERE Id = :thisAcc.RecordTypeId LIMIT 1].DeveloperName;
        
        if (recordtype == 'Affiliate_Account' && thisAcc.Affiliate_ID__c == NULL && String.isBlank(currentRecordId)) {
            Blob blobKey = crypto.generateAesKey(128);
            String key = EncodingUtil.convertToHex(blobKey);
            thisAcc.Affiliate_ID__c = key.left(6);
            thisAcc.Affiliate_API_Token__c = key.left(40);
        }
        
        thisNote = new Note();
        conQuickEmail = new Account(); //this is just for contractor quick email
        
        
        //setup access flags
        User currentUser = [SELECT Id, Name, Profile.Name, Set_Claims_Call_Backs_Status__c, Disable_Keyword_Alerts__c, Edit_Contractor_Reimbursement_NewCPA__c, Dispatch_Approver__c, Email2Case__c, Bypass_Claim_De_Dupe__c
                       FROM User 
                       WHERE Id = :UserInfo.getUserId() LIMIT 1];
        if (currentUser.Profile.Name.contains('Administrator')) {
            editContractors = TRUE;
            editAffiliates = TRUE;
        } else {
            editContractors = FALSE;
            editAffiliates = FALSE;
            
            for (PermissionSetAssignment psa : [SELECT Id, PermissionSet.Name,AssigneeId 
                                                FROM PermissionSetAssignment
                                                WHERE AssigneeId = :Userinfo.getUserId()]) {
                                                    System.debug('##psa.PermissionSet.Name' + psa.PermissionSet.Name);
                                                    if (psa.PermissionSet.Name == 'Edit_Create_Contractors') {
                                                        editContractors = TRUE;
                                                    } else if (psa.PermissionSet.Name == 'Edit_Create_Affiliates') {
                                                        editAffiliates = TRUE;
                                                    } 
                                                }
        }
    }
    public void relatedRecords() {
        system.debug('relatedRecords');

        //caseList =  new List<Case>();
        historyList =  new List<WrapHistory>();
        emailList = new List<Task>();
        insuranceList = new List<Contractor_Insurance__c>();
        ratingList = new List<WrapRating>();
        notesList = new List<Note>();

        if( String.isNotBlank( currentRecordId ) ) {
            /*caseList= [
                SELECT
                Id,
                AccountId,
                Subject,
                Priority,
                Status,
                CaseNumber,
                Policy__c,
                LastModifiedDate,
                CreatedDate,
                Contractor_Authorized_Amount__c,
                Service_Call_Fee__c,
                Invoice_Received_Date__c,
                Invoice_Status__c,
                Policy_Custom_Link__c,
                Property_Address__c,
                Last_Name__c
                FROM Case
                WHERE Contractor__c = :currentRecordId
                ORDER BY Status, CaseNumber
                LIMIT 1000 
            ];*/
            
            for (AccountHistory acctHist : [SELECT 
                                            Id, 
                                            AccountId, 
                                            CreatedById, 
                                            CreatedBy.Name, 
                                            CreatedDate, 
                                            OldValue, 
                                            NewValue, 
                                            Field
                                            FROM AccountHistory  
                                            WHERE AccountId = :currentRecordId 
                                            ORDER BY CreatedDate DESC
                                            LIMIT 1000]) {
                                                historyList.add(new WrapHistory(acctHist, NULL));
                                                
                                            }
            
            if (thisCon != NULL) {
                if (thisCon.size()>0) {
                    for (ContactHistory conHist : [SELECT 
                                                   Id, 
                                                   ContactId, 
                                                   CreatedById, 
                                                   CreatedBy.Name, 
                                                   CreatedDate, 
                                                   OldValue, 
                                                   NewValue, 
                                                   Field
                                                   FROM ContactHistory  
                                                   WHERE ContactId = :thisCon[0].Id 
                                                   ORDER BY CreatedDate DESC
                                                   LIMIT 1000]) {
                                                       historyList.add(new WrapHistory(NULL, conHist));
                                                       
                                                   }
                }
            }            
            
            historyList.sort();
            
            emailList = [SELECT 
                         Id,
                         Subject,
                         Status,
                         TaskSubtype,
                         Description,
                         CreatedById,
                         CreatedBy.Name,
                         CreatedDate,
                         ActivityDate,
                         WhoId,
                         WhatId
                         FROM Task 
                         WHERE Status = 'Completed' AND TaskSubtype = 'Email' AND WhatId = :currentRecordId
                         ORDER BY ActivityDate DESC
                         LIMIT 1000];
            
            insuranceList = [
                SELECT
                Id,
                Contractor__c,
                External_ID__c,
                Policy__c,
                Expiration__c
                FROM Contractor_Insurance__c
                WHERE Contractor__c = :currentRecordId
                ORDER BY Policy__c ASC, Expiration__c DESC
                LIMIT 1000 
            ];
            
            viewList = [SELECT Id, Viewed_By__r.Name, Viewed_On__c
                        FROM View__c
                        WHERE Contractor__c = :currentRecordId 
                        ORDER BY CreatedDate DESC
                        LIMIT 1000];
            
            for (Account rec : [SELECT LastModifiedById, LastModifiedDate FROM Account WHERE Id = :currentRecordId LIMIT 1]) {
                lastModifiedBy = rec.LastModifiedById;
                lastModifiedDate = rec.LastModifiedDate;
            }
            
            for (dispconn__Job__c job : [SELECT ID, dispconn__Service_Provider__r.Ext_Service_Provider__c, dispconn__Rating__c, dispconn__Rating_Message__c, Ext_Job__c, Ext_Job__r.Rating_Date_Time__c, Ext_Job__r.Name
                                         FROM dispconn__Job__c 
                                         WHERE dispconn__Rating__c != NULL AND dispconn__Service_Provider__r.Ext_Service_Provider__c = :currentRecordId ORDER BY Ext_Job__r.Rating_Date_Time__c DESC]) {
                                             ratingList.add(new WrapRating(job));
                                         }
            
            getNotes();
        }
    }
    
    public void getNotes(){
        system.debug('getNotes');
        
        notesList = new List<Note>();
        notesList = [
            SELECT
            Id,
            CreatedById,
            CreatedBy.Name,
            Title,
            Body,
            ParentId,
            CreatedDate
            FROM Note
            WHERE ParentId = :currentRecordId
            ORDER BY CreatedDate DESC
            LIMIT 10000     
        ];
    }
    public PageReference updateRecord(){
        system.debug('updateRecord');
        
        errorMsg ='';
        successMsg ='';
        btnAction =  '';
        try {
            /*if (currentRecordId != NULL &&
                Util.updateOkay(currentRecordId, lastModifiedBy, lastModifiedDate) == FALSE) {
                errorMsg = 'There was a previous update to this record. Please refresh and perform your updates again.';
                return NULL;
            } else {*/
            if(String.isNotBlank(currentRecordId)) {
                btnAction = 'update';
                system.debug('btnAction: ' + btnAction);
                if (recordType == 'Contractor_Account') {
                    system.debug('thisAcc.Contractor_Quick_Email__c: ' + thisAcc.Contractor_Quick_Email__c);

                    if(thisAcc.Contractor_Quick_Email__c != NULL){
                        sendContractorQuickEmail();
                        thisAcc.Contractor_Quick_Email__c = NULL;
                    }
                    if (thisAcc.Contractor_Name__c == NULL) {
                        thisCon[0].LastName = thisAcc.Name.left(80);
                    } else {
                        thisCon[0].FirstName = thisAcc.Contractor_Name__c.left(thisAcc.Contractor_Name__c.indexOf(' ')).left(80);
                        thisCon[0].LastName = thisAcc.Contractor_Name__c.mid(thisAcc.Contractor_Name__c.indexOf(' '), 80);
                    }
                } else {
                    system.debug('thisAcc.Affiliate_Quick_Email__c: ' + thisAcc.Affiliate_Quick_Email__c);

                    if(thisAcc.Affiliate_Quick_Email__c != NULL){
                        sendAffiliateQuickEmail();
                        thisAcc.Affiliate_Quick_Email__c = NULL;
                    }
                    thisCon[0].FirstName = thisAcc.Affiliate_First_Name__c;
                    thisCon[0].LastName = thisAcc.Affiliate_Last_Name__c;
                }
                UPSERT thisCon;
                UPDATE thisAcc;
                relatedRecords();
            } else {
                btnAction = 'create';
                system.debug('btnAction: ' + btnAction);
                INSERT thisAcc;
                if (recordType == 'Contractor_Account') {
                    Contact newCon = new Contact(
                        AccountId = thisAcc.Id,
                        Company__c = thisAcc.Name,
                        Email = thisAcc.Business_Email__c,
                        MailingStreet = thisAcc.ShippingStreet,
                        MailingCity = thisAcc.ShippingCity,
                        MailingStateCode = thisAcc.ShippingStateCode,
                        MailingPostalCode = thisAcc.ShippingPostalCode,
                        MailingCountryCode = thisAcc.ShippingCountryCode,
                        MobilePhone = thisAcc.Pager_number__c
                    );
                    if (thisAcc.Contractor_Name__c == NULL) {
                        newCon.LastName = thisAcc.Name.left(80);
                    } else {
                        newCon.FirstName = thisAcc.Contractor_Name__c.left(thisAcc.Contractor_Name__c.indexOf(' ')).left(80);
                        newCon.LastName = thisAcc.Contractor_Name__c.mid(thisAcc.Contractor_Name__c.indexOf(' '), 80);
                    }
                    INSERT newCon;
                } else {
                    thisCon[0].AccountId = thisAcc.Id;
                    thisCon[0].FirstName = thisAcc.Affiliate_First_Name__c;
                    thisCon[0].LastName = thisAcc.Affiliate_Last_Name__c;
                    UPSERT thisCon;
                }
                
                currentRecordId = thisAcc.id;
            }
            affLink = 'https://quote.selecthomewarranty.com/affiliate/'+thisAcc.Affiliate_ID__c;
        } catch ( Exception err ) {
            if ( String.isNotBlank( err.getMessage()) && err.getMessage().contains( 'error:' ) )
                errorMsg = err.getMessage().split('error:')[1].split(':')[0] + '.';
            else
                errorMsg = err.getMessage();  
        }
        return NULL;
    }
    
    public void checkForDupe() {
        system.debug('checkForDupe');
        existingContractors = new List<Account>();
        
        if (thisAcc.Business_Email__c != NULL) {
            existingContractors = [SELECT Id, Phone, Business_Email__c, (Select Id FROM Contacts)
                                   FROM Account 
                                   WHERE Business_Email__c = :thisAcc.Business_Email__c AND Id != :thisAcc.Id ORDER BY CreatedDate LIMIT 1];
        } else if (thisAcc.Phone != NULL && existingContractors.isEmpty()) {
            existingContractors = [SELECT Id, Phone, Business_Email__c, (Select Id FROM Contacts)
                                   FROM Account 
                                   WHERE Phone = :thisAcc.Phone AND Id != :thisAcc.Id ORDER BY CreatedDate LIMIT 1];            
        }
        system.debug('existingContractors: ' + existingContractors);
    }
    
    public PageReference mergeToContractor() {
        system.debug('mergeToContractor');
        
        //migrate all tasks
        List<Task> taskList = [SELECT Id,Status,TaskSubtype,WhoId FROM Task WHERE WhatId = :thisAcc.Id];
        for(Task tsk : taskList){
            for (Contact con : existingContractors[0].Contacts) {
                tsk.WhoId = con.Id;
                break;
            }
            if (thisCon.size()>0) {
                if (tsk.WhoId == thisCon[0].Id) {
                    tsk.WhoId = NULL;
                }
            }
            tsk.WhatId = existingContractors[0].id;
        }
        if(!taskList.isEmpty() ) {
            Database.update(taskList);
        }
        
        //migrate all cases
        List<Case> caseList = [SELECT Id, Suggested_Contractor__c, Contractor__c FROM Case WHERE Contractor__c = :thisAcc.Id OR Suggested_Contractor__c = :thisAcc.Id];
        for(Case cs : caseList){
            if (cs.Contractor__c == thisAcc.Id) {
                cs.Contractor__c = existingContractors[0].Id;
            }
            if (cs.Suggested_Contractor__c == thisAcc.Id) {
                cs.Suggested_Contractor__c = existingContractors[0].Id;
            }
        }
        if(!caseList.isEmpty() ) {
            Database.update(caseList);
        }
        
        //migrate all work orders
        List<Work_Order__c> woList = [SELECT Id, Contractor__c, Merge_Date__c FROM Work_Order__c WHERE Contractor__c = :thisAcc.Id];
        for(Work_Order__c wo : woList){
            wo.Contractor__c = existingContractors[0].Id;
            wo.Merge_Date__c = DateTime.now();
        }
        if(!woList.isEmpty() ) {
            Database.update(woList);
        }
        
        //migrate all auth forms
        List<Authorization_Form__c> afList = [SELECT Id,Contractor__c FROM Authorization_Form__c WHERE Contractor__c = :thisAcc.Id];
        for(Authorization_Form__c af : afList){
            af.Contractor__c = existingContractors[0].Id;
        }
        if(!afList.isEmpty() ) {
            Database.update(afList);
        }
        delete thisCon;
        delete thisAcc;
        
        PageReference redirect = new PageReference('/'+existingContractors[0].Id);
        redirect.setRedirect(true);
        return redirect;    
    }
    public void sendContractorQuickEmail(){
        system.debug('sendContractorQuickEmail');
        
        Boolean sendEmail;
        if (thisCon.size()>0) {
            system.debug('sendEmail-qualify1');
            sendEmail = Util.sendEmail(thisCon[0].Id, String.valueOf(thisAcc.Business_Email__c), thisAcc.Contractor_Quick_Email__c, 'Contractor Quick Email', currentRecordId, 'SHW Claims', false);
        } else {
            system.debug('sendEmail-qualify2');
            Id randConId = [Select Id FROM Contact LIMIT 1].Id;
            sendEmail = Util.sendEmail(randConId, String.valueOf(thisAcc.Business_Email__c), thisAcc.Contractor_Quick_Email__c, 'Contractor Quick Email', currentRecordId, 'SHW Claims', true);
        }
        
        system.debug('sendEmail-sendContractorQuickEmail: ' + sendEmail);
        if (sendEmail == true) {
            successMsg = 'Quick email sent.';
        } else {
            errorMsg = 'Quick email not sent.';
        }
    }
    
    public void sendAffiliateQuickEmail(){
        system.debug('sendAffiliateQuickEmail');
        
        Boolean sendEmail;
        if (thisCon.size()>0) {
            system.debug('sendEmail-qualify1');
            sendEmail = Util.sendEmail(thisCon[0].Id, String.valueOf(thisAcc.Business_Email__c), thisAcc.Affiliate_Quick_Email__c, 'Affiliate Quick Email', currentRecordId, 'SHW Info', false);
        } else {
            system.debug('sendEmail-qualify2');
            Id randConId = [Select Id FROM Contact LIMIT 1].Id;
            sendEmail = Util.sendEmail(randConId, String.valueOf(thisAcc.Business_Email__c), thisAcc.Affiliate_Quick_Email__c, 'Affiliate Quick Email', currentRecordId, 'SHW Info', true);
        }
        
        system.debug('sendEmail-sendAffiliateQuickEmail: ' + sendEmail);
        if (sendEmail == true) {
            successMsg = 'Quick email sent.';
        } else {
            errorMsg = 'Quick email not sent.';
        }
    }
    
    public void addNoteRecord(){
        system.debug('addNoteRecord');
        thisNote.Title = 'Note-' + DateTime.now().format('MM/dd/yyyy HH:mm a');
        thisNote.ParentId = currentRecordId ;
        insert thisNote;
        getNotes();
        thisNote = new Note();
    }
    
    public void logView() {
        Util.logView(Schema.getGlobalDescribe().get('Account').getDescribe().getKeyPrefix(), currentRecordId);
    }
    
    public void enablePartner() {
        thisAcc.IsPartner = true;
        UPDATE thisAcc;
        thisCon[0].FirstName = thisAcc.Affiliate_First_Name__c;
        thisCon[0].LastName = thisAcc.Affiliate_Last_Name__c;
        UPSERT thisCon[0];
        
        User newPartner = new User();
        newPartner.FirstName = thisAcc.Affiliate_First_Name__c;
        newPartner.LastName = thisAcc.Affiliate_Last_Name__c;
        newPartner.Alias = thisAcc.Affiliate_First_Name__c.left(1)+thisAcc.Affiliate_Last_Name__c.left(4);
        newPartner.Email = thisAcc.Business_Email__c;
        newPartner.Username = thisAcc.Business_Email__c+'.shwAffiliate';
        newPartner.CommunityNickname = thisAcc.Business_Email__c.SubStringBefore('@') + Integer.valueof((Math.random() * 100));
        newPartner.ContactId = thisCon[0].Id;
        newPartner.ProfileId = [SELECT Id, Name From Profile WHERE Name = 'SHW Affiliate' LIMIT 1].Id;
        newPartner.Phone = thisAcc.Phone;
        newPartner.Fax = thisAcc.Fax;
        newPartner.MobilePhone = thisAcc.Pager_number__c;
        newPartner.CountryCode = thisAcc.ShippingCountryCode;
        newPartner.Street = thisAcc.ShippingStreet;
        newPartner.City = thisAcc.ShippingCity;
        newPartner.StateCode = thisAcc.ShippingStateCode;
        newPartner.PostalCode = thisAcc.ShippingStateCode;
        newPartner.LocaleSidKey = 'en_US';
        newPartner.LanguageLocaleKey = 'en_US';
        newPartner.EmailEncodingKey = 'ISO-8859-1';
        newPartner.TimeZoneSidKey = 'America/New_York';
        newPartner.IsActive = true;
        insert newPartner;
        partnerUsr = [Select Id, IsActive FROM User WHERE ContactId = :thisCon[0].Id];
        
        thisAcc.Partner_Login__c = newPartner.Id;
        update thisAcc;
        
        leadOwnership_batch lob = new leadOwnership_batch(thisAcc.Id, newPartner.Id, thisAcc.View_Duplicates__c); 
        database.executeBatch(lob,50);
    }
    
    public void disablePartner() {
        thisAcc.IsPartner = false;
        update thisAcc;
        
        leadOwnership_batch lob = new leadOwnership_batch(thisAcc.Id, NULL, thisAcc.View_Duplicates__c); 
        database.executeBatch(lob,50);
    }
    
    public PageReference CoverageItemManager() {
        system.debug('CoverageItemManager');
        PageReference redirect = Page.CoverageItem;
        redirect.setRedirect(true);
        redirect.getParameters().put('id',thisAcc.Id);
        return redirect;
    }
    
    public PageReference CoverageAreaManager() {
        system.debug('CoverageAreaManager');
        PageReference redirect = Page.CoverageAreaManager;
        redirect.setRedirect(true);
        redirect.getParameters().put('id',thisAcc.Id);
        return redirect;
    }
    
    public class WrapHistory implements Comparable{
        public DateTime dt {get; set;} //this is just used for sorting
        public AccountHistory acctHist {get; set;}
        public ContactHistory conHist {get; set;}
        
        public WrapHistory(AccountHistory acctHist, ContactHistory conHist) {            
            if (acctHist != NULL) {
                this.dt = acctHist.CreatedDate;
                this.acctHist = acctHist;
                
            } else { 
                this.dt = conHist.CreatedDate;
                this.conHist = conHist;
            }
        }
        
        public Integer compareTo(Object compareTo) {
            WrapHistory compareToHist = (WrapHistory)compareTo;
            if (this.dt < compareToHist.dt) return 1;
            if (this.dt > compareToHist.dt) return -1;
            return 0;
        }
    }

    public class WrapRating{
        public Work_Order__c wo {get;set;}
        public String woNumber {get;set;}
        
        public WrapRating(dispconn__Job__c job) { 
            wo = new Work_Order__c();
            wo.Id = job.Ext_Job__c;
            wo.Rating__c = job.dispconn__Rating__c;
            wo.Rating_Message__c = job.dispconn__Rating_Message__c;
            wo.Rating_Date_Time__c = job.Ext_Job__r.Rating_Date_Time__c;
            
            woNumber = job.Ext_Job__r.Name;
        }
    }
}