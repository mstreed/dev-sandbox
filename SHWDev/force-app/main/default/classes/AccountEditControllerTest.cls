/*

    @Description    AccountEditController Class 
    @createdate     19 May 2018

*/
@isTest
private class AccountEditControllerTest {
    private static DataFactory df = new DataFactory();
    
    @isTest static void newContractor() {
        df.createGlobalSetting();
        
        Test.StartTest();
        PageReference pageRef = Page.AccountEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('RecordType',df.contractor.recordTypeId);
        Account newAccount = new Account();
        ApexPages.StandardController controller = new ApexPages.StandardController(newAccount);
        AccountEditController extension = new AccountEditController(controller);            
        extension.thisAcc.Name='Test';
        extension.thisAcc.ShippingStreet = '120 2nd Ave';
        extension.thisAcc.ShippingCity = 'New York';
        extension.thisAcc.ShippingStateCode = 'NY';
        extension.thisAcc.ShippingCountryCode = 'US';
        extension.thisAcc.ShippingPostalCode = '10022';
        extension.thisAcc.Business_Email__c = 'test@test.com';
        extension.updateRecord();
        Test.StopTest();
    }
    
    @isTest static void newAffiliate() {
        df.createGlobalSetting();
        df.createLead();
        String recordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Affiliate_Account' LIMIT 1].Id;

        Test.StartTest();
        PageReference pageRef = Page.AccountEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('RecordType',recordTypeId);
        Account newAccount = new Account();
        ApexPages.StandardController controller = new ApexPages.StandardController(newAccount);
        AccountEditController extension = new AccountEditController(controller);
        extension.thisAcc.Name='Test';
        extension.thisAcc.ShippingStreet = '120 2nd Ave';
        extension.thisAcc.ShippingCity = 'New York';
        extension.thisAcc.ShippingStateCode = 'NY';
        extension.thisAcc.ShippingCountryCode = 'US';
        extension.thisAcc.ShippingPostalCode = '10022';
        extension.thisAcc.Business_Email__c = 'test@test.com';
        extension.thisAcc.Endpoint_Active__c = TRUE;
        extension.thisAcc.Request_Type__c = 'POST';
        extension.thisAcc.Endpoint_URL__c = 'https://test.com/convert?custId=#{custId}&subId=#{subId}&subId2=#{subId2}&dtTime=#{dateTimeNow}';
        extension.thisAcc.Affiliate_First_Name__c = 'John';
        extension.thisAcc.Affiliate_Last_Name__c = 'Smith';
        extension.thisAcc.Batch_Pixel_Start_Date__c = DateTime.now().addDays(-1);
        extension.thisAcc.Batch_Pixel_End_Date__c = DateTime.now().addDays(1);
        extension.updateRecord();
        
        extension.enablePartner();
        
        df.newLead.Affiliate_Account__c = extension.thisAcc.Id;
        df.newLead.OwnerId = UserInfo.getUserId();
        df.newLead.Status = 'New';
        update df.newLead;
                    
        RecursiveTriggerHandler.isFirstTimeAccount = TRUE;
        
        Lead newLead = new Lead(
            FirstName ='test123',
            LastName = 'testLast123',
            Email = 'test123@test.com',
            Email_with_Multiple_Address__c = 'test123@test.com',
            MobilePhone = '3242',
            CountryCode = 'US',
            Street = 'testStreet',
            City = 'testCity',
            StateCode = 'DE',
            PostalCode = '32423',
            Status = 'New',
            Total_Price_Manual__c = 1,
            Customer_Id__c = '123456',
            recordTypeId = df.newLead.recordTypeId,
            Affiliate_Account__c = extension.thisAcc.Id,
            Sub_ID__c = '123',
            Sub_ID2__c = '245'
        );
        insert newLead;
        
        RecursiveTriggerHandler.isFirstTimeAccount = TRUE;
        
        extension.thisAcc.Batch_Pixel_Trigger__c = TRUE;
        extension.updateRecord();
        
        extension.disablePartner();
        Test.StopTest();
    }
    
    @isTest static void updateContractorTest() {
        df.createGlobalSetting();
        
        Test.StartTest();
        Contact conContact = new Contact();
        conContact.firstName = 'test';
        conContact.LastName = 'test';
        conContact.AccountId = df.contractor.Id;
        insert conContact;
        
        //df.contractor.Name = 'test test';
        df.contractor.Contractor_Name__c = 'test test';
        df.contractor.Business_Email__c = 'test1@test.com';
        update df.contractor;
        
        AccountHistory actHistory = new AccountHistory();
        actHistory.AccountId =  df.contractor.Id;
        actHistory.Field = 'Business_Email__c';
        insert actHistory;
        
        PageReference pageRef = Page.AccountEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',df.contractor.id);
        ApexPages.StandardController controller = new ApexPages.StandardController( df.contractor );
        AccountEditController extension = new AccountEditController(controller);
        System.assertEquals( df.contractor.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        extension.thisAcc.Contractor_Quick_Email__c = 'Dispatch Request';
        extension.updateRecord();

        extension.thisNote.Body = 'Test';
        extension.addNoteRecord();
        Test.StopTest();
    }
    
    @isTest static void relatedRecords() {
        df.createGlobalSetting();

        Test.StartTest();
        PageReference pageRef = Page.AccountEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',NULL );
        ApexPages.currentPage().getParameters().put('RecordType',df.contractor.recordTypeId);
        ApexPages.StandardController controller = new ApexPages.StandardController( df.contractor );
        AccountEditController extension = new AccountEditController(controller);
        System.assertEquals( NULL, Apexpages.currentpage().getparameters().get( 'id' ) ); 
        extension.currentRecordId = df.contractor.id;
        extension.relatedRecords();
        Test.StopTest();
    }
    @isTest static void updateRecord() {
        df.createGlobalSetting();

        Test.StartTest();
        PageReference pageRef = Page.AccountEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',df.contractor.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(df.contractor);
        AccountEditController extension = new AccountEditController(controller);
        System.assertEquals( df.contractor.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        extension.logView();
        extension.updateRecord();
        Test.StopTest();
    }
    
    @isTest
    static void testDupeContractor() {
        Test.StartTest();
        df.createGlobalSetting();
        
        Account dupeCon = df.contractor.clone(false, true, false, false);
        insert dupeCon;
        
        ApexPages.currentPage().getParameters().put('id',df.contractor.id );
        System.assertEquals( df.contractor.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.contractor );
        AccountEditController extension = new AccountEditController(controller);
        extension.mergeToContractor();
        
        Test.StopTest();
    }
}