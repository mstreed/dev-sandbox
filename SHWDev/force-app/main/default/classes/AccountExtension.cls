//extension for the header panel of the affiliate/contractor pages
public class AccountExtension {
    
    public Account thisAcc {get; set;}
    public Contact[] thisCon {get; set;}
    public User[] partnerUsr {get; set;}
    public Id partnerId {set; get;}

    public Boolean editContractors {get; set;}
    public Boolean editAffiliates {get; set;}
    public String currentRecordId {get; set;}
    public String recordtype {get;set;}
    public Id affliatePortalId {get;set;}
    public List<Account> existingContractors {set; get;}
    public Id mergeId {set; get;}

    public List<WrapRating> ratingList {get;set;}
    
    public AccountExtension(ApexPages.StandardController stdController){
        //Add fields to SOQL
        if (!Test.isRunningTest()) {
            stdController.addFields(new String[]{
                'Affiliate_First_Name__c',
                    'Affiliate_Last_Name__c',
                    'Business_Email__c',
                    'Phone',
                    'Fax',
                    'Pager_Number__c',
                    'ShippingCountryCode',
                    'ShippingStreet',
                    'ShippingCity',
                    'ShippingStateCode',
                    'ShippingStateCode',
                    'View_Duplicates__c',
                    'Record_Type_Name__c'
                    });
        }
        
        this.thisAcc = (Account)stdController.getRecord();
        // get account record
        currentRecordId = thisAcc.Id;
        
        //get related contact
        thisCon = [Select Id, FirstName, LastName FROM Contact WHERE AccountId = :currentRecordId AND IsDeleted = FALSE];
        system.debug('thisCon: ' + thisCon);
        if (thisCon.size()>0) { //get partner info if possible
            partnerUsr = [Select Id, IsActive FROM User WHERE ContactId = :thisCon[0].Id];
            system.debug('partnerUsr: ' + partnerUsr);

            if (!partnerUsr.isEmpty()) {
                partnerId = partnerUsr[0].Id;
            } else {
                partnerId = NULL;
            }
        }
        
        affliatePortalId = [SELECT Id,Name FROM Network WHERE Name = 'SHW Affiliate Portal'].Id;
        
        //check for dupes if contractor
        if (thisAcc.Record_Type_Name__c == 'Contractor Account') {
            checkForDupe();
        }
        
        recordtype = [SELECT Id, Name, DeveloperName FROM RecordType WHERE Id = :thisAcc.RecordTypeId LIMIT 1].DeveloperName;
        
        //setup access flags
        User currentUser = [SELECT Id, Name, Profile.Name, Set_Claims_Call_Backs_Status__c, Disable_Keyword_Alerts__c, Edit_Contractor_Reimbursement_NewCPA__c, Dispatch_Approver__c, Email2Case__c, Bypass_Claim_De_Dupe__c
                       FROM User 
                       WHERE Id = :UserInfo.getUserId() LIMIT 1];
        if (currentUser.Profile.Name.contains('Administrator')) {
            editContractors = TRUE;
            editAffiliates = TRUE;
        } else {
            editContractors = FALSE;
            editAffiliates = FALSE;
            
            for (PermissionSetAssignment psa : [SELECT Id, PermissionSet.Name,AssigneeId 
                                                FROM PermissionSetAssignment
                                                WHERE AssigneeId = :Userinfo.getUserId()]) {
                                                    System.debug('##psa.PermissionSet.Name' + psa.PermissionSet.Name);
                                                    if (psa.PermissionSet.Name == 'Edit_Create_Contractors') {
                                                        editContractors = TRUE;
                                                    } else if (psa.PermissionSet.Name == 'Edit_Create_Affiliates') {
                                                        editAffiliates = TRUE;
                                                    } 
                                                }
        }
        
        ratingList = new List<WrapRating>();
        for (dispconn__Job__c job : [SELECT ID, dispconn__Service_Provider__r.Ext_Service_Provider__c, dispconn__Rating__c, dispconn__Rating_Message__c, Ext_Job__c, Ext_Job__r.Rating_Date_Time__c, Ext_Job__r.Name
                                     FROM dispconn__Job__c 
                                     WHERE dispconn__Rating__c != NULL AND dispconn__Service_Provider__r.Ext_Service_Provider__c = :currentRecordId ORDER BY Ext_Job__r.Rating_Date_Time__c DESC]) {
                                         ratingList.add(new WrapRating(job));
                                     }
    }
    
    public void checkForDupe() {
        system.debug('checkForDupe');
        existingContractors = new List<Account>();
        
        if (thisAcc.Business_Email__c != NULL) {
            existingContractors = [SELECT Id, Phone, Business_Email__c, (Select Id FROM Contacts)
                                   FROM Account 
                                   WHERE Business_Email__c = :thisAcc.Business_Email__c AND Id != :thisAcc.Id AND Record_Type_Name__c = 'Contractor Account' ORDER BY CreatedDate LIMIT 1];
        } else if (thisAcc.Phone != NULL && existingContractors.isEmpty()) {
            existingContractors = [SELECT Id, Phone, Business_Email__c, (Select Id FROM Contacts)
                                   FROM Account 
                                   WHERE Phone = :thisAcc.Phone AND Id != :thisAcc.Id AND Record_Type_Name__c = 'Contractor Account' ORDER BY CreatedDate LIMIT 1];            
        }
        
        if (!existingContractors.isEmpty()) {
            mergeId = existingContractors[0].Id;
        } else {
            mergeId = NULL;
        }
    }
    
    public void mergeToContractor() {
        system.debug('mergeToContractor');
        
        //migrate all tasks
        List<Task> taskList = [SELECT Id,Status,TaskSubtype,WhoId FROM Task WHERE WhatId = :thisAcc.Id];
        for(Task tsk : taskList){
            for (Contact con : existingContractors[0].Contacts) {
                tsk.WhoId = con.Id;
                break;
            }
            if (thisCon.size()>0) {
                if (tsk.WhoId == thisCon[0].Id) {
                    tsk.WhoId = NULL;
                }
            }
            tsk.WhatId = existingContractors[0].id;
        }
        if(!taskList.isEmpty() ) {
            Database.update(taskList);
        }
        
        //migrate all cases
        List<Case> caseList = [SELECT Id, Suggested_Contractor__c, Contractor__c FROM Case WHERE Contractor__c = :thisAcc.Id OR Suggested_Contractor__c = :thisAcc.Id];
        for(Case cs : caseList){
            if (cs.Contractor__c == thisAcc.Id) {
                cs.Contractor__c = existingContractors[0].Id;
            }
            if (cs.Suggested_Contractor__c == thisAcc.Id) {
                cs.Suggested_Contractor__c = existingContractors[0].Id;
            }
        }
        if(!caseList.isEmpty() ) {
            Database.update(caseList);
        }
        
        //migrate all work orders
        List<Work_Order__c> woList = [SELECT Id, Contractor__c, Merge_Date__c FROM Work_Order__c WHERE Contractor__c = :thisAcc.Id];
        for(Work_Order__c wo : woList){
            wo.Contractor__c = existingContractors[0].Id;
            wo.Merge_Date__c = DateTime.now();
        }
        if(!woList.isEmpty() ) {
            Database.update(woList);
        }
        
        //migrate all auth forms
        List<Authorization_Form__c> afList = [SELECT Id,Contractor__c FROM Authorization_Form__c WHERE Contractor__c = :thisAcc.Id];
        for(Authorization_Form__c af : afList){
            af.Contractor__c = existingContractors[0].Id;
        }
        if(!afList.isEmpty() ) {
            Database.update(afList);
        }
        delete thisCon;
        delete thisAcc;
    }
    
    public void enablePartner() {
        system.debug('enablePartner'); //for SHW
        thisAcc.IsPartner = true;
        UPDATE thisAcc;
        
        User newPartner = new User();
        newPartner.FirstName = thisAcc.Affiliate_First_Name__c;
        newPartner.LastName = thisAcc.Affiliate_Last_Name__c;
        newPartner.Alias = thisAcc.Affiliate_First_Name__c.left(1)+thisAcc.Affiliate_Last_Name__c.left(4);
        newPartner.Email = thisAcc.Business_Email__c;
        newPartner.Username = thisAcc.Business_Email__c+'.shwAffiliate';
        newPartner.CommunityNickname = thisAcc.Business_Email__c.SubStringBefore('@') + Integer.valueof((Math.random() * 100));
        newPartner.ContactId = thisCon[0].Id;
        newPartner.ProfileId = [SELECT Id, Name From Profile WHERE Name = 'SHW Affiliate' LIMIT 1].Id;
        newPartner.Phone = thisAcc.Phone;
        newPartner.Fax = thisAcc.Fax;
        newPartner.MobilePhone = thisAcc.Pager_number__c;
        newPartner.CountryCode = thisAcc.ShippingCountryCode;
        newPartner.Street = thisAcc.ShippingStreet;
        newPartner.City = thisAcc.ShippingCity;
        newPartner.StateCode = thisAcc.ShippingStateCode;
        newPartner.PostalCode = thisAcc.ShippingStateCode;
        newPartner.LocaleSidKey = 'en_US';
        newPartner.LanguageLocaleKey = 'en_US';
        newPartner.EmailEncodingKey = 'ISO-8859-1';
        newPartner.TimeZoneSidKey = 'America/New_York';
        newPartner.IsActive = true;
        INSERT newPartner;
        partnerUsr = [Select Id, IsActive FROM User WHERE ContactId = :thisCon[0].Id];
        
        thisAcc.Partner_Login__c = newPartner.Id;
        UPDATE thisAcc;
        
        //reassign leads to affiliate
        leadOwnership_batch lob = new leadOwnership_batch(thisAcc.Id, newPartner.Id, thisAcc.View_Duplicates__c); 
        database.executeBatch(lob,50);
    }
    
    public void disablePartner() {
        thisAcc.IsPartner = false;
        update thisAcc;
        
        //remove lead ownership from affiliates
        leadOwnership_batch lob = new leadOwnership_batch(thisAcc.Id, NULL, thisAcc.View_Duplicates__c); 
        database.executeBatch(lob,50);
    }
    
    
    public class WrapRating{
        public Work_Order__c wo {get;set;}
        public String woNumber {get;set;}
        
        public WrapRating(dispconn__Job__c job) { 
            wo = new Work_Order__c();
            wo.Id = job.Ext_Job__c;
            wo.Rating__c = job.dispconn__Rating__c;
            wo.Rating_Message__c = job.dispconn__Rating_Message__c;
            wo.Rating_Date_Time__c = job.Ext_Job__r.Rating_Date_Time__c;
            
            woNumber = job.Ext_Job__r.Name;
        }
    }
}