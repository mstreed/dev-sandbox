@isTest
private class AccountExtensionTest {
    private static DataFactory df = new DataFactory();
    
    @isTest static void affiliate() {
        df.createGlobalSetting();
        df.createAffiliate();
        Contact con = new Contact();
        con.firstName = 'test';
        con.LastName = 'test';
        con.AccountId = df.affiliate.Id;
        INSERT con;
        
        Test.StartTest();
        PageReference pageRef = Page.AccountHeaderPanel;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController controller = new ApexPages.StandardController(df.affiliate);
        AccountExtension extension = new AccountExtension(controller);
        
        extension.enablePartner();
        RecursiveTriggerHandler.isFirstTimeAccount = TRUE;

        extension.disablePartner();
        Test.StopTest();
    }
    
    @isTest
    static void contractor() {
        Test.StartTest();
        df.createGlobalSetting();
        Account dupeCon = df.contractor.clone(false, true, false, false);
        INSERT dupeCon;
        
        PageReference pageRef = Page.AccountHeaderPanel;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController controller = new ApexPages.StandardController(df.contractor);
        AccountExtension extension = new AccountExtension(controller);
        extension.checkForDupe();
        extension.mergeToContractor();
        
        Test.StopTest();
    }
}