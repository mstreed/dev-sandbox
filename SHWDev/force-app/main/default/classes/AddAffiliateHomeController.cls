public class AddAffiliateHomeController{
    public List<SelectOption> filterOptionList{get;set;}
    public String listId{get;set;}
    public Boolean eListState{get;set;}
    public List<Account> accountList{get;set;}
    public AddAffiliateHomeController(){
          filterOptionList = new List<SelectOption>();
          accountList = new List<Account>();
          eListState = true;
          String tempListId;
          for(ListView lW : [SELECT Id,Name FROM listview WHERE sobjecttype='account' and Name LIKE 'Affiliates%']){
            tempListId = String.valueOf(lw.id).substring(0,15);
              filterOptionList.add(new SelectOption(tempListId,lW.Name));
              if(lW.Name=='Affiliates'){
                  listId = tempListId;
              }
          }
    }
    public PageReference newAffiliate(){
      PageReference pg;
        try {
            User currentUser = [Select Id, Name, Profile.Name FROM User where Id = :userinfo.getUserId()];
            RecordType rt = [SELECT Id, Name, DeveloperName FROM RecordType WHERE DeveloperName = 'Affiliate_Account' LIMIT 1];
            /*if(currentUser.Name == 'Select Home Warrantyyyy') {
                pg = new PageReference('/001/e?ent=Account&RecordType='+rt.Id);
            } else {
                pg = new PageReference('/apex/AccountEdit?RecordType='+rt.Id);
            }*/
            pg = new PageReference('/001/e?ent=Account&RecordType='+rt.Id);
            return pg;   
        } catch(Exception e) {
            System.debug(e.getMessage());
        }
        
        return null;
    }
}