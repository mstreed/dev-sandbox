/*

    @Description    AddContractorHomeController Class 
    @createdate     19 May 2018

*/
@isTest
private class AddContractorHomeControllerTest {
    private static DataFactory df = new DataFactory();

	@isTest static void controllerTest() {
        df.createGlobalSetting();

		AddContractorHomeController controller = new AddContractorHomeController();
		controller.newContract();
		
		Id RecordTypeIdContact = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Contractor Account').getRecordTypeId();
		Account ac = new Account(Name='test',recordTypeId=RecordTypeIdContact);
		insert ac;
		List<Account> acList = [SELECT Id,Name,recordType.Name,recordTypeId  FROM Account WHERE recordType.Name = 'Contractor Account'];
		System.assertEquals(2,acList.size());
		controller.newContract();

	}
}