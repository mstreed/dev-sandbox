public class AdditionalAddressLookupController {
    
    public List<SelectOption> userOptions;
    List<String> contactsEmail;
    List<String> additonalToEmail;  
    
    public AdditionalAddressLookupController(){
        userOptions = new List<SelectOption>(); 
        contactsEmail = new List<String>();
        additonalToEmail = new List<String>(); 
    }
    
    public void setcontactsEmail(List<String> contactsEmail) {
        //If multiselect is false, contactsEmail must be of type String
        this.contactsEmail = contactsEmail;
    }
    
    public List<String> getcontactsEmail() {
        return contactsEmail;
    }
    
    public void setadditonalToEmail(List<String> additonalToEmail) {
        //If multiselect is false, additonalToEmail must be of type String
        this.additonalToEmail = additonalToEmail;
    }
    
    public List<String> getadditonalToEmail() {
        return additonalToEmail;
    }    
    
    public List<SelectOption> getUsers() {
        LIST<user> userLst  = [SELECT name, email FROM user where isActive= True];
        userLst.sort();
        for(user userObj : userLst){
            userOptions.add(new SelectOption(userObj.email,userObj.name));
            
        }
        return userOptions; 
    }
    
    public List<SelectOption> getAdditonalTo() {
        LIST<SelectOption> options = new List<SelectOption>();
        if(contactsEmail.isEmpty()){
            options.add(new SelectOption('','--None--'));        
        }
        return options;
    }
    
    public string addToAdditonalTo(){
        system.debug(contactsEmail);
        return null;
        
    }
}