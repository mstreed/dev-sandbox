/*

    @Description    AdditionalAddressLookupController Class 
    @createdate     23 Jan 2018

*/
@isTest
private class AdditionalAddressLookupControllerTest {
     
     @isTest
     static void testMethod1() {

        Test.StartTest();
        
        AdditionalAddressLookupController controller = new AdditionalAddressLookupController ();
        System.assertNotEquals( controller, NULL );
        System.assertEquals( emailList.size(), 1 );
        controller.setcontactsEmail( emailList );
        controller.getadditonalToEmail();
        
        controller.setadditonalToEmail( emailList );
        
        List<String> emailList1 = new List<String>();
        controller.setadditonalToEmail( emailList1 );
        System.assertEquals( emailList1.size(), 0 );

        controller.getUsers();
        controller.getAdditonalTo();
        controller.addToAdditonalTo();
        controller.getContactsEmail();
        
        Test.StopTest();    
     }
      /***************************************************************************************************
                           Initialize Test Data
     **************************************************************************************************/
    
     private static List<String> emailList {
        get {
            if ( emailList == NULL ) {
                emailList = new List<String> {'test@test.com'};
            }
            return emailList;
        }
        private set;
    }
}