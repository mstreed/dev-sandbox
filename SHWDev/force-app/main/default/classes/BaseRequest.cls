public virtual class BaseRequest {
    
    // Input unique request name; gather necessary details from mdt; return HttpRequest so that 
    // code that extends this method can further manipulate the request. (e.g. set Body)
    public virtual HttpRequest buildHttpRequest(String uniqueRequestName){
        HttpRequest req = new HttpRequest();
        try{
            System.debug('BaseRequest|buildHttpRequest|Start');
            // TODO Query mdt for info
            Web_Service_Setting__mdt mdtRec = [SELECT Endpoint__c, Request_Method__c, Client_Id__c, User_Id__c, 
                                                Service_Name__c, Service_Version__c, Bearer__c, Is_Auth__c
                                               FROM Web_Service_Setting__mdt 
                                               WHERE Unique_Request_Name__c = :uniqueRequestName AND Active__c = True
                                               LIMIT 1];
            if(mdtRec == null){
                throw new BaseRequestException('Could not find Custom Metadata Record: ' + uniqueRequestName);
            }
            req.setMethod(mdtRec.Request_Method__c);
            req.setEndpoint(mdtRec.Endpoint__c);
            req.setHeader('Accept', 'application/json');                                    
            if(mdtRec.Is_Auth__c){
                if(mdtRec.Bearer__c == null){
                    throw new BaseRequestException('Invalid Bearer Token for the Auth call');
                }else{
                    req.setHeader('Authorization', 'Bearer ' + mdtRec.Bearer__c);
                }
            }
            if(mdtRec.Client_Id__c != null){
                req.setHeader('clientId', mdtRec.Client_Id__c);
            }
            if(mdtRec.User_Id__c != null){
                req.setHeader('userId', mdtRec.User_Id__c);
            }
            if(mdtRec.Service_Name__c != null){
                req.setHeader('servicename', mdtRec.Service_Name__c);
            }

            if(mdtRec.Service_Version__c != null){
                req.setHeader('serviceversion', mdtRec.Service_Version__c);
            }
            System.debug('BaseRequest|buildHttpRequest|End');
        } catch(System.Exception e) {
            System.debug('BaseRequest|buildHttpRequest|Exception:  ' + e.getLineNumber() + ' | ' + e.getCause() + ' | ' + e.getMessage());
        }
        return req;
    }
    
    // This method must be overwritten when used, because Http request headers must be custom defined.
    public virtual HttpRequest addHeaders(HttpRequest req){
        return req;
    }
    
    // This method will only be used if the request requires a body.
    public virtual HttpRequest addBody(HttpRequest req, String jsonBody){
        try{
            System.debug('BaseRequest|addBody|Start');
            req.setBody(jsonBody);
            System.debug('BaseRequest|addBody|End');
        } catch(System.Exception e) {
            System.debug('BaseRequest|addBody|Exception:  ' + e.getLineNumber() + ' | ' + e.getCause() + ' | ' + e.getMessage());
        }
        return req;
    }
    
    // Input HttpRequest, sends request, returns response.
    public virtual HttpResponse sendHttpRequest(HttpRequest req){
        Http http = new Http();
        HttpResponse res = new HttpResponse();
        try{
            System.debug('BaseRequest|sendHttpRequest|Start');
            res = http.send(req);
            System.debug('BaseRequest|sendHttpRequest|End');
        } catch(System.Exception e) {
            System.debug('BaseRequest|sendHttpRequest|Exception:  ' + e.getLineNumber() + ' | ' + e.getCause() + ' | ' + e.getMessage());
        }
        return res;
    }
    
    
    public class BaseRequestException extends Exception{}
    
    
}