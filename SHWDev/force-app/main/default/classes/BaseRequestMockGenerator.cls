// Requires Web_Service_Setting__mdt record with Unique_Request_Name__c 'Test'
@isTest
global class BaseRequestMockGenerator implements HttpCalloutMock {
    
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        String testName = 'Test';
        String endpoint = '';
        try {
            System.debug('BaseRequestMockGenerator|respond|Start');
            // Make the endpoint somewhat dynamic - Only necessary setup data is mdt record with 'Test' Unique_Request_Name__c
            Web_Service_Setting__mdt mdtRec = [SELECT Endpoint__c, Request_Method__c
                                               FROM Web_Service_Setting__mdt 
                                               WHERE Unique_Request_Name__c = :testName AND Active__c = True
                                               LIMIT 1];
            endpoint = mdtRec.Endpoint__c;
            System.debug('BaseRequestMockGenerator|respond|endpoint: ' + endpoint);
            
            if (req.getEndpoint() == endpoint) {
                // TODO replace
                res.setHeader('Content-Type', 'application/json');
                res.setBody('{"example":"test"}');
                res.setStatusCode(200);
            }
            System.debug('BaseRequestMockGenerator|respond|res: ' + res);
            System.debug('BaseRequestMockGenerator|respond|End');
        } catch(System.Exception e) {
            System.debug('BaseRequestMockGenerator|respond|Exception:  ' + e.getLineNumber() + ' | ' + e.getCause() + ' | ' + e.getMessage());
        }
        
        return res;
    }
}