@isTest
public class BaseRequestTest {
    // Requires Web_Service_Setting__mdt record with Unique_Request_Name__c 'Test'
    @isTest static void buildHttpRequestTest(){
        try {
            System.debug('BaseRequestTest|buildHttpRequestTest|Start');
            HttpRequest req = new HttpRequest();
            String testName = 'Test';
            BaseRequest baseReqObj = new BaseRequest();
            
            Web_Service_Setting__mdt mdtRec = [SELECT Endpoint__c, Request_Method__c
                                               FROM Web_Service_Setting__mdt 
                                               WHERE Unique_Request_Name__c = :testName AND Active__c = True
                                               LIMIT 1];
            
            req = baseReqObj.buildHttpRequest('Test');
            System.assert(req.getEndpoint() == mdtRec.Endpoint__c);
            System.assert(req.getMethod() == mdtRec.Request_Method__c);
            System.debug('BaseRequestTest|buildHttpRequestTest|End');
        } catch(System.Exception e) {
            System.debug('BaseRequestTest|buildHttpRequestTest|Exception:  ' + e.getLineNumber() + ' | ' + e.getCause() + ' | ' + e.getMessage());
        }
    }
    
    @isTest static void addHeadersTest(){
        try{
            System.debug('BaseRequestTest|addHeadersTest|Start');
            HttpRequest req = new HttpRequest();
            BaseRequest baseReqObj = new BaseRequest();
            req = baseReqObj.addHeaders(req);
            System.debug('BaseRequestTest|addHeadersTest|End');
        } catch(System.Exception e) {
            System.debug('BaseRequestTest|addHeadersTest|Exception:  ' + e.getLineNumber() + ' | ' + e.getCause() + ' | ' + e.getMessage());
        }
    }
    
    @isTest static void addBodyTest(){
        try{
            System.debug('BaseRequestTest|addBodyTest|Start');
            HttpRequest req = new HttpRequest();
            BaseRequest baseReqObj = new BaseRequest();
            String testJsonBody = '{"test": "body}';
            req = baseReqObj.addBody(req, testJsonBody);
            System.assert(req.getBody() == testJsonBody);
            System.debug('BaseRequestTest|addBodyTest|End');
        } catch(System.Exception e) {
            System.debug('BaseRequestTest|addBodyTest|Exception:  ' + e.getLineNumber() + ' | ' + e.getCause() + ' | ' + e.getMessage());
        }
    }
    
    @isTest static void sendHttpRequestTest(){
        try{
            System.debug('BaseRequestTest|sendHttpRequestTest|Start');
            Test.setMock(HttpCalloutMock.class, new BaseRequestMockGenerator());
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            BaseRequest baseReqObj = new BaseRequest();
            res = baseReqObj.sendHttpRequest(req);
            System.debug('BaseRequestTest|sendHttpRequestTest|res: ' + res);
            System.debug('BaseRequestTest|sendHttpRequestTest|End');
        } catch(System.Exception e) {
            System.debug('BaseRequestTest|sendHttpRequestTest|Exception:  ' + e.getLineNumber() + ' | ' + e.getCause() + ' | ' + e.getMessage());
        }
    }
        
    
    
}