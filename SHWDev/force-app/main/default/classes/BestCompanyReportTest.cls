@isTest
private class BestCompanyReportTest {
    private static DataFactory df = new DataFactory();

    @isTest
    static void testBatchAndSchedule() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        df.createWorkOrder();
        
        df.workOrder.Rating_Date_Time__c = DateTime.now().addDays(-1);
        update df.workOrder;
        system.debug('df.workOrder: ' + df.workOrder);
        
        Test.StartTest();
        BestCompanyReport_schedule sh1 = new BestCompanyReport_schedule();
        String sch = '0 0 23 * * ?'; system.schedule('Test', sch, sh1);
        BestCompanyReport_batch batch = new BestCompanyReport_batch();
        ID batchprocessid = Database.executeBatch(batch);
        Test.StopTest();
    }
}