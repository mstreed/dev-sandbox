global class BestCompanyReport_batch implements Database.Batchable<SObject> {
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id, Claim__c, Claim_Number__c, Claim__r.Policy__r.Account.PersonContact.FirstName, Claim__r.Policy__r.Account.PersonContact.LastName, Customer_Email__c, Customer_Phone__c FROM Work_Order__c WHERE Rating__c >= 4 AND Rated_Yesterday__c = TRUE';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Work_Order__c> scope) {   
        List<Messaging.EmailFileAttachment> csvAttcs = new List<Messaging.EmailFileAttachment>();
        
        String reportDateFormat = Datetime.newInstance(Date.today().addDays(-1), Time.newInstance(12, 0, 0, 0)).format('yyyy-MM-dd',  UserInfo.getTimeZone().toString());
        
        Messaging.EmailFileAttachment csv = new Messaging.EmailFileAttachment();
        String csvname = 'SHW_BestCompany_Report.csv';
        String header = 'CLAIM,CUSTOMER FIRST NAME,CUSTOMER LAST NAME,CUSTOMER EMAIL,CUSTOMER PHONE\n';
        String finalstr = header;
        
        for (Work_Order__c wo : scope) {
            String recordString = '"'+wo.Claim_Number__c+'","'+wo.Claim__r.Policy__r.Account.PersonContact.FirstName+'","'+wo.Claim__r.Policy__r.Account.PersonContact.LastName+'","'+wo.Customer_Email__c+'","'+wo.Customer_Phone__c+'"\n';
            finalstr = finalstr + recordString;
        }
        
        Blob csvBlob = Blob.valueOf(finalstr);
        
        csv.setFileName(csvname);
        csv.setBody(csvBlob);
        
        csvAttcs.add(csv);
        
        String[] toAddresses = new String[] {'reporting@bestcompany.com'};
            OrgWideEmailAddress owa = [SELECT Id, DisplayName, Address FROM OrgWideEmailAddress WHERE DisplayName = 'SHW No Reply' LIMIT 1];
        Messaging.SingleEmailMessage eml = new Messaging.SingleEmailMessage();
        eml.setSubject('SHW Best Company Report');
        eml.setPlainTextBody('SHW Best Company Report for'+' - '+reportDateFormat);
        eml.setOrgWideEmailAddressId(owa.id);
        eml.setToAddresses(toAddresses);
        eml.setFileAttachments(csvAttcs);
        eml.setSaveAsActivity(false); //need to set to false to store attachment
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {eml});
        
        /*Task emlTask = new Task(
OwnerId = UserInfo.getUserId(),
Subject = 'Email: ' + eml.getSubject(),
WhatId = eml.getWhatId(),
ActivityDate = Date.today(),
Description = eml.getPlainTextBody(),
Status = 'Completed',
TaskSubtype = 'Email'
);
insert emlTask;

Attachment attachment = new Attachment();
attachment.Body = csvBlob;
attachment.Name = csvname;
attachment.ParentId = emlTask.Id; 
insert attachment;*/
    }   

    global void finish(Database.BatchableContext BC) {
    }
}