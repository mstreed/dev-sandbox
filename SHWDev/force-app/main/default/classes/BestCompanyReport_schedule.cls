global class BestCompanyReport_schedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        BestCompanyReport_batch batchRun = new BestCompanyReport_batch(); 
        ID batchId = Database.executeBatch(batchRun);
    }
}