global class CalloutUtil {
    global static Boolean isSandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
    global static Global_Setting__c gs = [Select AES256_Key__c, EZ_Texting_Username__c, EZ_Texing_Password_Encrypted__c, Website_Rebuild_Endpoint_PROD__c, Website_Rebuild_Endpoint_SANDBOX__c
                                          FROM Global_Setting__c LIMIT 1];
    
    /*public static string getPardotAccessToken(){
        system.debug('gs-getPardotAccessToken: ' + gs);
        
        String accessToken = '';
        
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        
        /TO REENCRYPT PW
        Blob key = EncodingUtil.base64Decode(gs.AES256_Key__c);
        Blob data = Blob.valueOf('PasswordString');
        Blob encrypted = Crypto.encryptWithManagedIV('AES256', key, data);
        system.debug(EncodingUtil.base64Encode(encrypted));/ 
        
        req = new HttpRequest();
        req.setEndpoint('https://login.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG9g9rbsTkKnAUtr.QYEfXlNngOCeWnuoMwD_z8FLNXvzzvNjHD9f.nTPjuAi85IU6lCc4wz6Hw85AZMuSH&client_secret=74726440F94E37B28CE028A38D037014078CA79B77DAEB2DFB3B2AE8A5C8A699&username=integration@selecthomewarranty.com&password=XXX');
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        
        System.debug('res-getPardotAccessToken: ' + res );
        System.debug('res.getBody()-getPardotAccessToken: ' + res.getBody() );
        
        if (!Test.isRunningTest()) {
            res = new Http().send( req );
            
            System.debug('res-getPardotAccessToken: ' + res );
            System.debug('res.getStatus()-getPardotAccessToken: ' + res.getStatus() );
            System.debug('res.getBody()-getPardotAccessToken: ' + res.getBody());
            
            String response = res.getBody();
            Map<String, String> values = (Map<String, String>)
                JSON.deserialize(response, Map<String, String>.class);
            
            accessToken = values.get('access_token');

            System.debug( accessToken );
        }
        
        return accessToken;
    }*/
    
    public static void optInPardot(String prospectId){
        system.debug('gs-optInPardot: ' + gs);
                
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
                
        req = new HttpRequest();
        req.setEndpoint( 'https://pi.pardot.com/api/prospect/version/4/do/update/id/' + prospectId);
        req.setMethod( 'POST' );
        req.setBody( 'is_do_not_email=0');
        req.setHeader('Authorization', 'Bearer '+UserInfo.getSessionID());
        req.setHeader('Pardot-Business-Unit-Id', '0Uv1I000000XZQPSA4');
        System.debug('res-optInPardot: ' + res );
        System.debug('res.getBody()-optInPardot: ' + res.getBody() );
        
        if (!Test.isRunningTest()) {
            res = new Http().send( req );
            
            System.debug('res-optInPardot: ' + res );
            System.debug('res.getStatus()-optInPardot: ' + res.getStatus() );
            System.debug('res.getBody()-optInPardot: ' + res.getBody());
        }
    }
    
    @future(callout=true)
    global static void syncPardotVisitor(String visitorId, String prospectId){
        system.debug('syncPardotVisitor');
        
        system.debug('visitorId: ' + visitorId);
        system.debug('prospectId: ' + prospectId);
        
        //**Start update API Key**//
        //gs.Pardot_API_Key__c = updatePardotAccessToken();
        //gs.Pardot_API_Key_Last_Request__c = DateTime.now();
        //**End update API Key**//

        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        
        optInPardot(prospectId);
        system.debug('gs-syncPardotVisitor: ' + gs);
        
        req = new HttpRequest();
        req.setEndpoint( 'https://pi.pardot.com/api/visitor/version/4/do/assign/id/' + visitorId);
        req.setMethod( 'POST' );
        req.setBody( 'prospect_id=' + prospectId);
        
        req.setHeader('Authorization', 'Bearer '+UserInfo.getSessionID());
        req.setHeader('Pardot-Business-Unit-Id', '0Uv1I000000XZQPSA4');
        System.debug('req-syncPardotVisitor: ' + req );
        System.debug('reqBody-syncPardotVisitor: ' + req.getBody());
        
        if (!Test.isRunningTest()) {
            res = new Http().send( req );
            
            System.debug('res-syncPardotVisitor: ' + res );
            System.debug('res.getStatus()-syncPardotVisitor: ' + res.getStatus() );
            System.debug('res.getBody()-syncPardotVisitor: ' + res.getBody());
        }
    }
    
    @future(callout=true)
    global static void sendPardotEmail(String prospectId, String pardotTemplateId, Boolean velocify, String velocifyOwnName, String velocifyOwnEmail){
        system.debug('sendPardotEmail');
        system.debug('prospectId: ' + prospectId);
        system.debug('pardotTemplateId: ' + pardotTemplateId);
        system.debug('velocify: ' + velocify);
        system.debug('velocifyOwnName: ' + velocifyOwnName);
        system.debug('velocifyOwnEmail: ' + velocifyOwnEmail);
        
        //**Start update API Key**//
        //gs.Pardot_API_Key__c = updatePardotAccessToken();
        //gs.Pardot_API_Key_Last_Request__c = DateTime.now();
        //**End update API Key**//
        
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        
        system.debug('gs-sendPardotEmail: ' + gs);
        
        req = new HttpRequest();
        req.setEndpoint( 'https://pi.pardot.com/api/email/version/4/do/send/');
        req.setMethod( 'POST' );
        String body = 'prospect_id=' + prospectId + '&campaign_id=85687&email_template_id=' + pardotTemplateId;
        if (velocify) {
            body += '&from_name=' + velocifyOwnName + '&from_email=' + velocifyOwnEmail + '&replyto_email=' + velocifyOwnEmail;
        } else {
            optInPardot(prospectId); //opt in realtor
        }
        req.setBody(body);
        
        //req.setHeader('Authorization', 'Bearer '+UserInfo.getSessionID());
        req.setHeader('Authorization', 'Bearer '+UserInfo.getSessionID());
        req.setHeader('Pardot-Business-Unit-Id', '0Uv1I000000XZQPSA4');
        System.debug('req-sendPardotEmail: ' + req );
        System.debug('reqBody-sendPardotEmail: ' + req.getBody());
        
        if (!Test.isRunningTest()) {
            res = new Http().send( req );
            
            System.debug('res-sendPardotEmail: ' + res );
            System.debug('res.getStatus()-sendPardotEmail: ' + res.getStatus());
            System.debug('res.getBody()-sendPardotEmail: ' + res.getBody());
            
            /*Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
            mail.setToAddresses(new String[] {'lnguyen@cloudna.io'});
            mail.setSubject('sendPardotRealtorEmail - ' + res.getStatus());
            mail.setBccSender(false);
            mail.setPlainTextBody('resbody:' + String.valueOf(res.getBody()) + '\n\n res:' + String.valueOf(res) + '\n\n resStatus:' + String.valueOf(res.getStatus()) + '\n\n resBody:' + String.valueOf(res.getBody()) + '\n\n UserInfo.getSessionID():' + UserInfo.getSessionID());
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });*/
        }
    }
    
    @future(callout=true)
    global static void sendLeadPixel(String endpoint, String reqType, String custId, String subId, String subId2){
        system.debug('sendLeadPixel');
        system.debug('endpoint: ' + endpoint);
        system.debug('reqType: ' + reqType);
        system.debug('custId: ' + custId);
        system.debug('subId: ' + subId);
        system.debug('subId2: ' + subId2);
        
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        if (custId != NULL) {
            endpoint = endpoint.replace('#{custId}', custId);
        }
        if (subId != NULL) {
            endpoint = endpoint.replace('#{subId}', subId);
        }
        if (subId2 != NULL) {
            endpoint = endpoint.replace('#{subId2}', subId2);
        }
        endpoint = endpoint.replace('#{dateTimeNow}', DateTime.now().format('yyyy-MM-dd HH:mm:ss', UserInfo.getTimeZone().toString()));
        system.debug('endpoint: ' + endpoint);
        
        req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod(reqType);
        System.debug('req-sendLeadPixel: ' + req );
        
        if (!Test.isRunningTest()) {
            res = new Http().send( req );
            
            System.debug('res-sendLeadPixel: ' + res );
            System.debug('res.getStatus()-sendLeadPixel: ' + res.getStatus());
            System.debug('res.getBody()-sendLeadPixel: ' + res.getBody());
        }
    }
    
    @future(callout=true)
    global static void rebuildWebsite(){
        system.debug('rebuildWebsite');
        system.debug('gs.Website_Rebuild_Endpoint_PROD__c: ' + gs.Website_Rebuild_Endpoint_PROD__c);
        system.debug('gs.Website_Rebuild_Endpoint_PROD__c: ' + gs.Website_Rebuild_Endpoint_SANDBOX__c);
        
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        req = new HttpRequest();
        if (!isSandbox) {
            req.setEndpoint(gs.Website_Rebuild_Endpoint_PROD__c);
        } else {
            req.setEndpoint(gs.Website_Rebuild_Endpoint_SANDBOX__c);
        }
        req.setMethod('POST');
        req.setHeader('Content-Type','application/x-www-form-urlencoded');
        req.setHeader('Content-Length','0');
        System.debug('req-rebuildWebsite: ' + req );
        
        if (!Test.isRunningTest()) {
            res = new Http().send( req );
            
            System.debug('res-rebuildWebsite: ' + res );
            System.debug('res.getStatus()-rebuildWebsite: ' + res.getStatus());
            System.debug('res.getBody()-rebuildWebsite: ' + res.getBody());
        }
    }
    
    @future(callout=true)
    global static void EzTextCampaign(Id recordId, String campaign, Boolean addToCampaign){
        system.debug('EzTextCampaign');
        system.debug('recordId: ' + recordId);
        system.debug('campaign: ' + campaign);
        
        Lead ld;
        //Contract pol;
        String cleanFirstName;
        String cleanLastName;
        String cleanEmail;
        String cleanPhone;
        String exTextId;
        
        if (String.valueOf(recordId).left(3) == '00Q') { //Lead
            ld = [SELECT Id, FirstName, LastName, Email, Phone, EZ_Texting_ID__c, EZ_Texting_Groups__c, EZ_Texting_Queue_Groups__c FROM Lead WHERE Id = :recordId];
            cleanFirstName = ld.FirstName.substring(0, Math.min(ld.FirstName.length(), 10));
            cleanLastName = ld.LastName.substring(0, Math.min(ld.LastName.length(), 10));
            cleanEmail = ld.Email.substring(0, Math.min(ld.Email.length(), 40));
            cleanPhone = ld.Phone.replaceAll('\\D','');
            cleanPhone = cleanPhone.substring(0, Math.min(cleanPhone.length(), 10));
            exTextId = ld.EZ_Texting_ID__c;
        } /*else if (String.valueOf(recordId).left(3) == '800') { //Policy
pol = [SELECT Id, AccountId, Account.FirstName, Account.LastName, Email__c, Home_Phone__c, EZ_Texting_ID__c, EZ_Texting_Groups__c FROM Contract WHERE Id = :recordId];
cleanFirstName = pol.Account.FirstName.substring(0, Math.min(pol.Account.FirstName.length(), 10));
cleanLastName = pol.Account.LastName.substring(0, Math.min(pol.Account.LastName.length(), 10));
cleanEmail = pol.Email__c.substring(0, Math.min(pol.Email__c.length(), 40));
cleanPhone = pol.Home_Phone__c.replaceAll('\\D','');
cleanPhone = cleanPhone.substring(0, Math.min(cleanPhone.length(), 10));
exTextId = pol.EZ_Texting_ID__c;
}*/
        
        system.debug('cleanPhone: ' + cleanPhone);
        
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
                
        String password = Crypto.decryptWithManagedIV('AES256', EncodingUtil.base64Decode(gs.AES256_Key__c), EncodingUtil.base64Decode(gs.EZ_Texing_Password_Encrypted__c)).toString();
        
        //Find Lead/Contract via Id
        if (exTextId != NULL) {
            req = new HttpRequest();
            req.setEndpoint( 'https://app.eztexting.com/contacts/'+exTextId+'?format=json&User='+gs.EZ_Texting_Username__c+'&Password='+password);
            req.setMethod( 'GET' );
            System.debug('req-addToEzText-viaId: ' + req );
            System.debug('reqBody-addToEzText-viaId: ' + req.getBody());
        } else { //Find Lead/Contract via Phone
            if (cleanPhone != NULL) {
                req = new HttpRequest();
                req.setEndpoint( 'https://app.eztexting.com/contacts?format=json&User='+gs.EZ_Texting_Username__c+'&Password='+password+'&query='+cleanPhone);
                req.setMethod( 'GET' );
                System.debug('req-addToEzText-viaPhone: ' + req );
                System.debug('reqBody-addToEzText-viaPhone: ' + req.getBody());
            }
        }
        
        EZTextingJSON.entryWrapper ezTextContact = new EZTextingJSON.entryWrapper();
        
        if (req != NULL) {
            req.setTimeout(50000);
            try {
            res = new Http().send( req );
            } catch( Exception ex) {
                system.debug('->>>'+ ex.getMessage());
            }
            System.debug('res-addToEzText-findRequest: ' + res );
            System.debug('res.getStatus()-addToEzText-findRequest: ' + res.getStatus());
            System.debug('res.getBody()-addToEzText-findRequest: ' + res.getBody());
            
            EZTextingJSON ezText = EZTextingJSON.parse(res.getBody());
            system.debug('ezText: '+ ezText);
            
            if (ezText.Response.Entry != NULL) { //id search
                ezTextContact = ezText.Response.Entry;
            } else if (ezText.Response.Entries != NULL && !ezText.Response.Entries.isEmpty()) { //query
                for (EZTextingJSON.entryWrapper entry : ezText.Response.Entries) {
                    system.debug('entry: '+ entry);
                    if (entry.PhoneNumber == cleanPhone) {
                        ezTextContact = entry;
                        Break;
                    }
                }
            } else { //no match! create contact if adding to campaign, otherwise, skip
                if (addToCampaign) {
                    req = new HttpRequest();
                    req.setEndpoint( 'https://app.eztexting.com/contacts?format=json&User='+gs.EZ_Texting_Username__c+'&Password='+password);
                    req.setMethod('POST');    
                    req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
                    String payload = 'PhoneNumber='+cleanPhone;
                    System.debug(payload);
                    req.setBody(payload);
                    req.setTimeout(50000);
                    try {
                        res = new Http().send( req );
                        System.debug('res-addToEzText-createRequest: ' + res );
                        System.debug('res.getStatus()-addToEzText-createRequest: ' + res.getStatus());
                        System.debug('res.getBody()-addToEzText-createRequest: ' + res.getBody());
                        
                        ezText = EZTextingJSON.parse(res.getBody());
                        system.debug('ezText: '+ ezText);
                        
                        //assign contact
                        ezTextContact = ezText.Response.Entry;
                    } catch( Exception ex) {
                        system.debug('->>>'+ ex.getMessage());
                    }
                }
            }
        }
        
        if (ezTextContact != NULL) {
            //set groups
            String groupsPayload = '';
            String groups = '';
            
            Integer i = 0;
            if (addToCampaign) { //adding to groups
                system.debug('add to groups');
                groups = campaign;
                groupsPayload = groupsPayload + '&Groups[' + i + ']=' + campaign;
                i++;
                if (ezTextContact.Groups != NULL) {
                    for (String grp : ezTextContact.Groups) {
                        if (grp == 'No Campaigns' || grp == campaign) {
                            Continue;
                        }
                        groupsPayload = groupsPayload + '&Groups[' + i + ']=' + grp;
                        groups = groups + ', ' + grp;
                        i++;
                    }
                }
            } else { //removing from groups
                system.debug('remove from groups');
                if (ezTextContact.Groups != NULL) {
                    for (String grp : ezTextContact.Groups) {
                        if (grp == 'No Campaigns' || grp == campaign) {
                            Continue;
                        }
                        groupsPayload = groupsPayload + '&Groups[' + i + ']=' + grp;
                        if (groups == '') {
                            groups = grp;
                        } else {
                            groups = groups + ', ' + grp;
                        }
                        i++;
                    }
                }
            }
            
            //if there are no groups, set to default 'No Campaigns'
            if (groupsPayload == '') {
                groupsPayload = '&Groups=No Campaigns';
                groups = 'No Campaigns';
            } 
            
            system.debug('groups: ' + groups);
            
            //update contact groups and details
            req = new HttpRequest();
            req.setEndpoint( 'https://app.eztexting.com/contacts/'+ezTextContact.Id+'?format=json&User='+gs.EZ_Texting_Username__c+'&Password='+password);
            req.setMethod('POST');    
            req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            String payload = 'PhoneNumber='+cleanPhone+'&FirstName='+cleanFirstName+'&LastName='+cleanLastName+'&Email='+cleanEmail+'&Custom1='+String.valueOf(recordId).substring(0, 10)+'&Custom2='+String.valueOf(recordId).substring(10, 18)+groupsPayload;
            
            System.debug(payload);
            req.setBody(payload);
            req.setTimeout(50000);
            try {
                res = new Http().send( req );
                System.debug('res-addToEzText-updateRequest: ' + res );
                System.debug('res.getStatus()-addToEzText-updateRequest: ' + res.getStatus());
                System.debug('res.getBody()-addToEzText-updateRequest: ' + res.getBody());
                
            } catch( Exception ex) {
                system.debug('->>>'+ ex.getMessage());
            }
            if (String.valueOf(recordId).left(3) == '00Q') { //Lead
                ld.EZ_Texting_ID__c = ezTextContact.Id;
                ld.EZ_Texting_Groups__c = groups;
            } /*else if (String.valueOf(recordId).left(3) == '800') { //Policy
                pol.EZ_Texting_ID__c = ezTextContact.Id;
                pol.EZ_Texting_Groups__c = groups;
                update pol;
                }*/
            
        }
        
        if (String.valueOf(recordId).left(3) == '00Q') { //Lead
            if (!addToCampaign && ld.EZ_Texting_Queue_Groups__c != NULL) {
                if (ld.EZ_Texting_Queue_Groups__c.contains('Buy It Now')) {
                    List<String> groupQueueList = ld.EZ_Texting_Queue_Groups__c.split(', ');
                    ld.EZ_Texting_Queue_Groups__c = NULL;
                    for (String grp : groupQueueList) {
                        if (grp == 'Buy It Now') {
                            continue;
                        } else {
                            if (ld.EZ_Texting_Queue_Groups__c == NULL) {
                                ld.EZ_Texting_Queue_Groups__c = grp;
                            } else {
                                ld.EZ_Texting_Queue_Groups__c = ld.EZ_Texting_Queue_Groups__c + ', ' + grp;
                            }
                        }
                    }
                }
            }
            
            system.debug('ld-final: ' + ld);
            try {
                update ld;        
            } catch( Exception ex) {
                system.debug('->>>'+ ex.getMessage());
            }
        }
    }
    
    @future(callout=true)
    public static void deleteRecs(Set<Id> objectsIdsToDelete) {   
        DELETE [SELECT Id FROM Case WHERE Id IN :objectsIdsToDelete];
    }
    
    @future(callout=true)
    public static void createRemoteSiteSettings(String remoteSiteURL)
    {   
        system.debug('remoteSiteURL: ' + remoteSiteURL);
        String url = '';
        url = remoteSiteURL.split('/')[2];     
        if (remoteSiteURL.startsWith('https')) {
            url = 'https://' + url;
        } else {
            url = 'http://' + url;
        }
        String name = 'Affiliate_' + url.replaceAll('[^a-zA-Z0-9\\s+]', '').left(30);

        system.debug('url: ' + url);
        system.debug('name: ' + name);
        MetadataService.MetadataPort service = createService();
        MetadataService.RemoteSiteSetting remoteSiteSettings = new MetadataService.RemoteSiteSetting();
        remoteSiteSettings.fullName = name;
        remoteSiteSettings.url = remoteSiteURL;
        //remoteSiteSettings.description=’ewf’;
        remoteSiteSettings.isActive=true;
        remoteSiteSettings.disableProtocolSecurity=false;
        MetadataService.AsyncResult[] results = service.create(new List<MetadataService.Metadata> { remoteSiteSettings });
        MetadataService.AsyncResult[] checkResults = service.checkStatus(new List<string> {string.ValueOf(results[0].Id)});
        system.debug('createRemoteSiteSettings-chk' + checkResults );
    }
    
    public static MetadataService.MetadataPort createService()
    {
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = UserInfo.getSessionId();
        return service;
    }
}