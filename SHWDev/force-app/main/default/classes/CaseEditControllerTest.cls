/*

    @Description    CaseEditController Class 
    @createdate     19 May 2018

*/
@isTest
private class CaseEditControllerTest {
    private static DataFactory df = new DataFactory();

    @isTest 
    static void controllerTest() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        
        Test.StartTest();
        PageReference pageRef = Page.CaseEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',df.newCase.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newCase);
        CaseEditController extension = new CaseEditController(controller);
        system.debug(extension.parentRecordId);
        system.debug(extension.domainURL);
        System.assertEquals(df.newCase.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        Test.stopTest();
    }
    @isTest 
    static void inquiry() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createInquiry();
        
        Test.StartTest();
        PageReference pageRef = Page.CaseEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',df.newInquiry.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newInquiry);
        CaseEditController extension = new CaseEditController(controller);
        extension.convertCase.Reason = 'Additional AC Unit(each)';
        extension.convertCase.Issue_Detail__c = 'Humidifier is leaking water';
        extension.convertToClaim();
        Test.stopTest();
    }
    @isTest 
    static void redirectPolicy() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        
        Test.StartTest();
        PageReference pageRef = Page.CaseEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',df.newCase.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newCase);
        CaseEditController extension = new CaseEditController(controller);
        System.assertEquals( df.newCase.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        extension.redirectPolicy();
        Test.stopTest();
    }
    @isTest 
    static void addNoteRecord() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        df.newPolicy.BillingStateCode = 'CA';
        update df.newPolicy;
       
        Test.StartTest();
        User randUsr = [Select Id FROM User WHERE (NOT Profile.Name Like '%Administrator%') AND IsActive = TRUE AND Profile.Name = 'SHW Claims' LIMIT 1];        
        System.runAs(randUsr) {
            PageReference pageRef = Page.CaseEdit;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id',df.newCase.id);
            ApexPages.StandardController controller = new ApexPages.StandardController(df.newCase);
            CaseEditController extension = new CaseEditController(controller);
            System.assertEquals( df.newCase.id, Apexpages.currentpage().getparameters().get( 'id' ) );
            extension.thisCaseComment.CommentBody = 'bbb test';
            extension.addNoteRecord();
            extension.thisCaseComment.CommentBody = 'bbbaaa test {';
            extension.addNoteRecord();
            System.assertEquals( df.newCase.id,extension.currentRecordId);        
            extension.currentRecordId = 'Test';
            extension.addNoteRecord();       
            System.assertNotEquals(df.newCase.id,extension.currentRecordId);
            Test.stopTest();
        }
    }
    @isTest 
    static void updateNote() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        
        Test.StartTest();
        CaseComment caseComment = new CaseComment(
            ParentId = df.newCase.id
        );
        insert caseComment;
        
        PageReference pageRef = Page.CaseEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',df.newCase.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newCase);
        CaseEditController extension = new CaseEditController(controller);
        System.assertEquals( df.newCase.id, Apexpages.currentpage().getparameters().get( 'id' ) );      
        extension.updateNote();
        System.assertEquals(extension.noteRecordUpdateId,null);
        extension.thisCaseComment = new CaseComment();
        extension.noteRecordUpdateId = caseComment.id;
        extension.thisCaseComment.CommentBody = 'test';
        extension.updateNote();
        Test.stopTest();
    }
    @isTest 
    static void deleteRecord() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        df.createWorkOrder();
        
        Test.StartTest();
        CaseComment caseComment = new CaseComment(
            ParentId = df.newCase.id
        );
        insert caseComment;
        
        Authorization_Form__c authoForm = new Authorization_Form__c(
            claim__c = df.newCase.Id
        );
        insert authoForm;
        
        PageReference pageRef = Page.CaseEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',df.newCase.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newCase);
        CaseEditController extension = new CaseEditController(controller);
        System.assertEquals( df.newCase.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        extension.ObjectName = 'CaseComment';
        extension.deleteRelatedRecordId = CaseComment.id;
        extension.deleteRecord();
        List<CaseComment> caseCommentList = [SELECT Id,ParentId FROM CaseComment WHERE ParentId =: df.newCase.id];
        System.assertEquals(0,caseCommentList.size());  
        extension.ObjectName = 'Work_Order__c';
        extension.deleteRelatedRecordId = df.workOrder.id;
        extension.deleteRecord();
        List<Work_Order__c> workOrderList = [SELECT Id FROM Work_Order__c];
        System.assertEquals(0,workOrderList.size());
        extension.ObjectName = 'Authorization_Form__c';
        extension.deleteRelatedRecordId = authoForm.id;
        extension.deleteRecord();
        List<Authorization_Form__c> authList = [SELECT Id FROM Authorization_Form__c];
        System.assertEquals(0,authList.size());
        extension.ObjectName = 'Authorization_Form__c';
        extension.deleteRelatedRecordId = 'Test';
        extension.deleteRecord();
        extension.dateTimeConverter('10/10/2018');
        Test.stopTest();
    }
    @isTest 
    static void updateCase1() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        
        Test.StartTest();
        PageReference pageRef = Page.CaseEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',df.newCase.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newCase);
        CaseEditController extension = new CaseEditController(controller);
        extension.thisCase.Invoice_Status__c = 'NewCPA';
        RecursiveTriggerHandler.isFirstTime = true;
        extension.updateRecord();
        Test.stopTest();
    }
    
    @isTest 
    static void updateCase2() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        
        Test.StartTest();
        PageReference pageRef = Page.CaseEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',df.newCase.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newCase);
        CaseEditController extension = new CaseEditController(controller);
        System.assertEquals( df.newCase.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        extension.approveForDispatch();
        RecursiveTriggerHandler.isFirstTime = true;

        extension.sendTripCharge();
        extension.setConInvStatusRec();
        extension.setResolution();
        extension.setPolicySection();
        extension.logView();
        Test.stopTest();
    }
        
    @isTest 
    static void sendQuickEmails() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        df.createWorkOrder();
        
        Test.StartTest();
        PageReference pageRef = Page.CaseEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',df.newCase.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newCase);
        CaseEditController extension = new CaseEditController(controller);
        System.assertEquals( df.newCase.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        extension.thisCase.Quick_Email__c = 'BBB Review';
        extension.thisCase.Contractor_Quick_Email__c = 'Diagnosis Request';
        extension.updateRecord();
        Test.stopTest();
    }
    @isTest 
    static void quickDispatch() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        df.newCase.Suggested_Contractor__c = df.contractor.Id;
        df.newCase.Status = 'Ready to Dispatch';
        UPDATE df.newCase;
        df.createWorkOrder();
        
        Test.StartTest();
        PageReference pageRef = Page.CaseEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',df.newCase.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newCase);
        CaseEditController extension = new CaseEditController(controller);
        System.assertEquals( df.newCase.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        extension.approveForDispatch();
        RecursiveTriggerHandler.isFirstTime = true;
        extension.CusLat = '35.728004';
        extension.CusLong = '-73.987649';
        extension.updateRecord();
        extension.quickDispatch();
        extension.setRedispatchReason();
        Test.stopTest();
    }
    @isTest 
    static void createAuthForm1() {
        Test.StartTest();

        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        
        PageReference pageRef = Page.CaseEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',df.newCase.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newCase);
        CaseEditController extension = new CaseEditController(controller);
        System.assertEquals( df.newCase.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        extension.af.Contractor__c = df.contractor.Id;
        extension.af.Email_Contractor__c = true; 
        extension.thisCase.Contractor_Authorized_Amount__c = 123;
        extension.af.Approval_Reason__c = 'Approved for a courtesy amount';
        extension.af.Part_1_Price__c = 1;
        extension.af.Part_2_Price__c = 1;
        extension.af.Part_3_Price__c = 1;
        extension.af.Part_4_Price__c = 1;
        extension.af.Part_5_Price__c = 1;
        extension.af.Part_6_Price__c = 1;
        extension.af.Part_1_Wholesale_Price__c = 1;
        extension.af.Part_2_Wholesale_Price__c = 1;
        extension.af.Part_3_Wholesale_Price__c = 1;
        extension.af.Part_4_Wholesale_Price__c = 1;
        extension.af.Part_5_Wholesale_Price__c = 1;
        extension.af.Part_6_Wholesale_Price__c = 1;
        extension.af.Part_1_Included_In_Pricing__c = TRUE;
        extension.af.Part_2_Included_In_Pricing__c = TRUE;
        extension.af.Part_3_Included_In_Pricing__c = TRUE;
        extension.af.Part_4_Included_In_Pricing__c = TRUE;
        extension.af.Part_5_Included_In_Pricing__c = TRUE;
        extension.af.Part_6_Included_In_Pricing__c = TRUE;
        extension.af.Total_Cost_of_Labor__c = 1;
        extension.af.Labor_Included_In_Pricing__c = TRUE;
        extension.af.Diagnosis_Fee__c = 1;
        extension.af.Diagnosis_Fee_Included_In_Pricing__c = TRUE;
        extension.af.Service_Call_Fee_Included_In_Pricing__c = TRUE;
        extension.af.Tax__c = 1;
        extension.calcAfCosts();
        extension.getNTE();
        extension.approveAuthForm();
        extension.af.Authorization_Number__c = '123';
        extension.approveAuthForm();
        //extension.thisAfRfId = [SELECT Id FROM Authorization_Form__c LIMIT 1].Id;
        Test.stopTest();
    }
    
    @isTest 
    static void createAuthForm2() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        
        PageReference pageRef = Page.CaseEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',df.newCase.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newCase);
        CaseEditController extension = new CaseEditController(controller);
        System.assertEquals( df.newCase.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        extension.af.Contractor__c = df.contractor.Id;
        extension.af.Email_Contractor__c = true; 
        extension.thisCase.Contractor_Authorized_Amount__c = 123;
        extension.af.Approval_Reason__c = 'Approved for a courtesy amount';
        extension.af.Part_1_Price__c = 1;
        extension.af.Part_2_Price__c = 1;
        extension.af.Part_3_Price__c = 1;
        extension.af.Part_4_Price__c = 1;
        extension.af.Part_5_Price__c = 1;
        extension.af.Part_6_Price__c = 1;
        extension.af.Part_1_Wholesale_Price__c = 1;
        extension.af.Part_2_Wholesale_Price__c = 1;
        extension.af.Part_3_Wholesale_Price__c = 1;
        extension.af.Part_4_Wholesale_Price__c = 1;
        extension.af.Part_5_Wholesale_Price__c = 1;
        extension.af.Part_6_Wholesale_Price__c = 1;
        extension.af.Part_1_Included_In_Pricing__c = TRUE;
        extension.af.Part_2_Included_In_Pricing__c = TRUE;
        extension.af.Part_3_Included_In_Pricing__c = TRUE;
        extension.af.Part_4_Included_In_Pricing__c = TRUE;
        extension.af.Part_5_Included_In_Pricing__c = TRUE;
        extension.af.Part_6_Included_In_Pricing__c = TRUE;
        extension.af.Total_Cost_of_Labor__c = 1;
        extension.af.Labor_Included_In_Pricing__c = TRUE;
        extension.af.Diagnosis_Fee__c = 1;
        extension.af.Diagnosis_Fee_Included_In_Pricing__c = TRUE;
        extension.af.Service_Call_Fee_Included_In_Pricing__c = TRUE;
        extension.af.Tax__c = 1;
        extension.approveAuthForm();
        Test.StartTest();
        extension.thisAfRfId = [SELECT Id FROM Authorization_Form__c LIMIT 1].Id;
        extension.resendAuthFormEmail();
        Test.stopTest();
    }
    
    @isTest 
    static void createAuthForm3() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        
        PageReference pageRef = Page.CaseEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',df.newCase.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newCase);
        CaseEditController extension = new CaseEditController(controller);
        System.assertEquals( df.newCase.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        extension.af.Contractor__c = df.contractor.Id;
        extension.af.Email_Contractor__c = true; 
        extension.thisCase.Contractor_Authorized_Amount__c = 123;
        extension.af.Approval_Reason__c = 'Approved for a courtesy amount';
        extension.af.Part_1_Price__c = 1;
        extension.af.Part_2_Price__c = 1;
        extension.af.Part_3_Price__c = 1;
        extension.af.Part_4_Price__c = 1;
        extension.af.Part_5_Price__c = 1;
        extension.af.Part_6_Price__c = 1;
        extension.af.Part_1_Wholesale_Price__c = 1;
        extension.af.Part_2_Wholesale_Price__c = 1;
        extension.af.Part_3_Wholesale_Price__c = 1;
        extension.af.Part_4_Wholesale_Price__c = 1;
        extension.af.Part_5_Wholesale_Price__c = 1;
        extension.af.Part_6_Wholesale_Price__c = 1;
        extension.af.Part_1_Included_In_Pricing__c = TRUE;
        extension.af.Part_2_Included_In_Pricing__c = TRUE;
        extension.af.Part_3_Included_In_Pricing__c = TRUE;
        extension.af.Part_4_Included_In_Pricing__c = TRUE;
        extension.af.Part_5_Included_In_Pricing__c = TRUE;
        extension.af.Part_6_Included_In_Pricing__c = TRUE;
        extension.af.Total_Cost_of_Labor__c = 1;
        extension.af.Labor_Included_In_Pricing__c = TRUE;
        extension.af.Diagnosis_Fee__c = 1;
        extension.af.Diagnosis_Fee_Included_In_Pricing__c = TRUE;
        extension.af.Service_Call_Fee_Included_In_Pricing__c = TRUE;
        extension.af.Tax__c = 1;
        extension.approveAuthForm();
        extension.af.Authorization_Number__c = '123';
        
        Test.StartTest();
        extension.thisAfRfId = [SELECT Id FROM Authorization_Form__c LIMIT 1].Id;
        extension.revokeUnrevokeAuthReteForm();
        Test.stopTest();
    }
    @isTest 
    static void sendReimbursementAgreement() {
        
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        
        Test.StartTest();
        PageReference pageRef = Page.CaseEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',df.newCase.Id );
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newCase);
        CaseEditController extension = new CaseEditController(controller); 
        extension.sendReimbursementAgreement();
        
        pageRef = Page.ReimbursementAgreement;
        Test.setCurrentPage(pageRef);
        Id raId = [SELECT Id FROM RA_Consent__c LIMIT 1].Id;
        ApexPages.currentPage().getParameters().put('token',raId);
        ReimbursementAgreementController extension2 = new ReimbursementAgreementController();
        extension2.setViewed();
        extension2.saveAndTY();
        
        extension.thisRAConsentId = raId;
        extension.resendReimbursementAgreement();

        Test.stopTest();
    }
    
    @isTest 
    static void sendRefund1() {
        
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        df.createRenewal();
        df.policyRenewal.Renewal_Start_Date__c = Date.today().addDays(-150);
		update df.policyRenewal;
        
        Test.StartTest();
        PageReference pageRef = Page.CaseEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',df.newCase.Id );
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newCase);
        CaseEditController extension = new CaseEditController(controller); 
        extension.generateRefundNote = TRUE;
        extension.thisCase.Cancellation_Date__c = Date.today();
        extension.updateRecord();
        Test.stopTest();
    }
    @isTest 
    static void sendRefund2() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
                
        PageReference pageRef = Page.CaseEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',df.newCase.Id );
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newCase);
        CaseEditController extension = new CaseEditController(controller); 
        extension.generateRefundNote = TRUE;
        extension.sendRefundEmail();
        extension.calcRefundFields();
        Test.stopTest();
    }
    
    @isTest 
    static void logACall() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        
        PageReference pageRef = Page.CaseEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',df.newCase.Id );
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newCase);
        CaseEditController extension = new CaseEditController(controller); 
        extension.taskSubject = 'test';
        extension.followUp = true;
        extension.followUpSubject = 'test';
        extension.logACall();
        extension.taskSelectedQuickComment = 'Other';
        extension.setTaskSelectedQuickComment();
        extension.followUpSelectedQuickComment = 'Other';
        extension.setFollowUpSelectedQuickComment();
        extension.taskSelectedQuickComment = '';
        extension.setTaskSelectedQuickComment();
        extension.followUpSelectedQuickComment = '';
        extension.setFollowUpSelectedQuickComment();
        Test.stopTest();
    }
}