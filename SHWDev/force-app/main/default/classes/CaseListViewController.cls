public with sharing class CaseListViewController {
    public String typeId {get;set;}
    public List<SelectOption> caseOptsList {get;set;}
    public List<CaseRecordListViewWrapper> caseWrapperRecords {get;set;}
    private static Map<String, String> nameColorMap {get;set;}
    public CaseListViewController() {      
        if (nameColorMap == NULL ) {
        User currentUser = [SELECT Id, Profile.Name, Case_List_Retention_Buyers_Remorse__c, Case_List_Parts__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];

            /*Boolean carrAccess = false;
            for (PermissionSetAssignment psa : [SELECT Id, PermissionSet.Name,AssigneeId 
                                                FROM PermissionSetAssignment
                                                WHERE AssigneeId = :Userinfo.getUserId()]) {
                                                    if (psa.PermissionSet.Name == 'Edit_Create_Case_Assignment_Round_Robin') {
                                                        carrAccess = true;
                                                        break;
                                                    }
                                                }*/
            
            nameColorMap = new Map<String, String>();
            if (currentUser.Profile.Name.contains('Administrator')) {
                nameColorMap.put('All Open Cases and Claims','#ffffff');
            }
            nameColorMap.put('My Open Cases and Claims','#ffffff');
            nameColorMap.put('Not Set','#555555');
            nameColorMap.put('App Email','#ff5c33');
            nameColorMap.put('App Email Sent','#ff809f');
            nameColorMap.put('Approved','#ccffdd');
            nameColorMap.put('Autho Review','#3366ff');
            nameColorMap.put('Billing','#b3d9ff');
            nameColorMap.put('C.A.','#E5A07E');
            nameColorMap.put('Case Escalation','#ff5c33');
            nameColorMap.put('Check Mailed','#ff80ff');
            if (currentUser.Profile.Name.contains('Administrator')) {
                nameColorMap.put('Check Status','#33cc33');
            }
            nameColorMap.put('Claims Call Backs','#ffgge6');
            nameColorMap.put('Closed','#ff8080');
            nameColorMap.put('Contractor Accepted Appointment Set','#ff0000');
            nameColorMap.put('Contractor Accepted','#00cc00');
            nameColorMap.put('Contractor Reported','#ffe6ff');
            nameColorMap.put('Customer Contacted','#e6ffe6');
            nameColorMap.put('Della','#ffcc66');
            nameColorMap.put('Denied','#ffb3d9');
            if (currentUser.Profile.Name.contains('Administrator')) {
                nameColorMap.put('Do Numbers','#ff80bf');
            }
            nameColorMap.put('Farjana','#cc66ff');
            nameColorMap.put('George Authorization','#ffff80');
            nameColorMap.put('In Progress','#ffffe6');
            nameColorMap.put('Jenica','#D697DE');
            nameColorMap.put('Jermel','#ffff80');
            nameColorMap.put('Jonelle','#df80ff');
            nameColorMap.put('Mail Policy','#ffff66');
            nameColorMap.put('Maria','#DC97E8');
            //nameColorMap.put('Need More Info','#ffffcc');
            nameColorMap.put('New Web Claim','#80bfff');
            if (currentUser.Profile.Name.contains('Administrator')) {
                nameColorMap.put('NewCPA','#80ff80');
            }
            nameColorMap.put('Paid PayPal','#80dfff');
            if (currentUser.Profile.Name.contains('Administrator') || currentUser.Case_List_Parts__c == TRUE) {
                nameColorMap.put('Parts Research','#B2F6AF');
                nameColorMap.put('Parts Approved for Purchase','#70F56A');
            }
            nameColorMap.put('Pics and Recs Received','#ffeed9');
            nameColorMap.put('Placed by Agent/Converted from Inquiry','#ffdd99');
            nameColorMap.put('RA Received','#ff00bf');
            nameColorMap.put('RA Sent','#00ff80');
            nameColorMap.put('Raysa','#d279d2');
            nameColorMap.put('Ready to Dispatch','#ffb3d1');
            nameColorMap.put('Ready to Dispatch Second Request','#ff6666');
            if (currentUser.Profile.Name.contains('Administrator')) {
                nameColorMap.put('Refund Check','#ffbf80');
            }
            if (currentUser.Profile.Name.contains('Administrator') || currentUser.Case_List_Retention_Buyers_Remorse__c == TRUE) {
                nameColorMap.put('Retention Buyers Remorse','#ff9900');
            }
            nameColorMap.put('Retention','#80bfff');
            nameColorMap.put('Secure Your Own','#8899ff');
            if (currentUser.Profile.Name.contains('Administrator')) {
                nameColorMap.put('Send Check','#808080');
            }
            nameColorMap.put('Send Release','#ffe6ff');
            nameColorMap.put('Using Own Tech','#0099ff');
            nameColorMap.put('Waiting for Payment','#4BA9E1');
            nameColorMap.put('Waiting for Pics','#808080');
            nameColorMap.put('Warranty Check','#cc66cc');
        }    
        caseWrapperRecords= new List<CaseRecordListViewWrapper >();
        caseOptsList = new List<SelectOption>();
        Map<String,String> nameIdMap = new Map<String,String>();
        Integer flag = 0;
        for ( ListView lw  : [SELECT Id, Name FROM ListView WHERE SObjectType = 'Case' AND (NOT Name LIKE '%Inquiries%') ORDER BY Name]) {
            caseOptsList.add( new SelectOption( String.valueOf(lw.id).substring(0,15),lw.Name) );
            nameIdMap.put( lw.Name, lw.Id );
        }
        for(String name : nameColorMap.keySet()) {
            if( flag == 0 ) {
                typeId = String.valueOf(nameIdMap.get(name)).substring(0,15);
                flag = 1;
            }
            if(nameColorMap != NULL && nameColorMap.containsKey(name) && nameIdMap.containsKey(name)) {
                caseWrapperRecords.add(new CaseRecordListViewWrapper( nameIdMap.get(name), name, nameColorMap.get(name) ));
            }
        }
    }
    public void changeView() {
        typeId = Apexpages.currentpage().getparameters().get('fcf');
        if( typeId != NULL ) {
            typeId = typeId.substring(0,15);
            System.debug(typeId);
        }
        Apexpages.currentpage().getparameters().put('fcf','');
    }
    public class CaseRecordListViewWrapper {
        public String viewId     {get;set;}
        public String viewName   {get;set;}
        public String viewColor  {get;set;}
        public CaseRecordListViewWrapper( String viewId, String viewName, String viewColor ) {
            this.viewId = viewId;
            this.viewName = viewName;
            this.viewColor = viewColor;
        }
    }
}