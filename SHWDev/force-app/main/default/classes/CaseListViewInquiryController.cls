public with sharing class CaseListViewInquiryController {
    public String typeId {get;set;}
    public List<SelectOption> caseOptsList {get;set;}
    public List<CaseRecordListViewWrapper> caseWrapperRecords {get;set;}
    private static Map<String, String> nameColorMap {get;set;}
    public CaseListViewInquiryController() {      
        if (nameColorMap == NULL ) {
            String currentUserProfile = [SELECT Id, Name FROM PROFILE WHERE Id = :UserInfo.getProfileId() LIMIT 1].Name;
            
            nameColorMap = new Map<String, String>();
            nameColorMap.put('Open Inquiries','#4BA9E1');
            nameColorMap.put('In Progress Inquiries','#ffffe6');
            nameColorMap.put('Closed Inquiries','#ff8080');
        }    
        caseWrapperRecords= new List<CaseRecordListViewWrapper >();
        caseOptsList = new List<SelectOption>();
        Map<String,String> nameIdMap = new Map<String,String>();
        Integer flag = 0;
        for ( ListView lw  : [SELECT Id, Name FROM ListView WHERE SObjectType = 'Case' AND Name LIKE '%Inquiries%' ORDER BY Name DESC]) {
            caseOptsList.add( new SelectOption( String.valueOf(lw.id).substring(0,15),lw.Name) );
            nameIdMap.put( lw.Name, lw.Id );
        }
        for(String name : nameColorMap.keySet()) {
            if( flag == 0 ) {
                typeId = String.valueOf(nameIdMap.get(name)).substring(0,15);
                flag = 1;
            }
            if(nameColorMap != NULL && nameColorMap.containsKey(name) && nameIdMap.containsKey(name)) {
                caseWrapperRecords.add(new CaseRecordListViewWrapper( nameIdMap.get(name), name, nameColorMap.get(name) ));
            }
        }
    }
    public void changeView() {
        typeId = Apexpages.currentpage().getparameters().get('fcf');
        if( typeId != NULL ) {
            typeId = typeId.substring(0,15);
            System.debug(typeId);
        }
        Apexpages.currentpage().getparameters().put('fcf','');
    }
    public class CaseRecordListViewWrapper {
        public String viewId     {get;set;}
        public String viewName   {get;set;}
        public String viewColor  {get;set;}
        public CaseRecordListViewWrapper( String viewId, String viewName, String viewColor ) {
            this.viewId = viewId;
            this.viewName = viewName;
            this.viewColor = viewColor;
        }
    }
}