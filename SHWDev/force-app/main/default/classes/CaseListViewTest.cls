@isTest
public with sharing class CaseListViewTest {
    @isTest
    private static void testController(){
        Test.setCurrentPage( Page.caseListView );
        Test.startTest();
        Apexpages.currentpage().getparameters().put('fcf','testtesttesttesttest');
        System.assertEquals( 'testtesttesttesttest', Apexpages.currentpage().getparameters().get( 'fcf' ) );
        CaseListViewController con = new CaseListViewController();
        System.assertNotEquals( con, NULL );
        con.changeView();
        System.assertEquals( con.typeId, 'testtesttesttes' );
        Test.stopTest();
    }
    
    @isTest
    private static void testController2(){
        Test.setCurrentPage( Page.caseListViewInquiry );
        Test.startTest();
        Apexpages.currentpage().getparameters().put('fcf','testtesttesttesttest');
        System.assertEquals( 'testtesttesttesttest', Apexpages.currentpage().getparameters().get( 'fcf' ) );
        CaseListViewInquiryController con = new CaseListViewInquiryController();
        System.assertNotEquals( con, NULL );
        con.changeView();
        System.assertEquals( con.typeId, 'testtesttesttes' );
        Test.stopTest();
    }
}