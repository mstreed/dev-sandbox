public class CaseNewController{
    public Case thisCase {set; get;}
    public Boolean hide {get;set;}
    public Boolean nonClaim {get;set;}
    public Boolean closeWindow {get;set;}
    public String polStatus {get;set;}
    public String polZip {get;set;}
    public String polPriority {get;set;}
    public String currentRecordId {get;set;}
    public String accountRecordId {get;set;}
    public String contactRecordId {get;set;}
    public String contactRecordEmail {get;set;}
    public Boolean appliance30Days {get;set;}
    public Boolean sameReason365Days {get;set;}
    public Boolean raAppliance365Days {get;set;}
    private Date polStartDate;
    private Date polEndDate;
    private String polContractTerm;
    public String errorMsg{set;get;} 
    public String parentRecordId {get;set;}
    public List<Case> existingClaims {get;set;}
    public String bypass {get;set;}
    //public List<SelectOption> caseReasonOptsList {get;set;}
    //public String selectedCaseReason {get;set;}
    public Global_Setting__c gs {get;set;}
    public Set<String> geZips = new Set<String>();
    public Map<String,Case_Reason_Coverage_Mapping__mdt> crCovMap = new Map<String,Case_Reason_Coverage_Mapping__mdt>();
    public Boolean geClaim {get;set;}

    public CaseNewController(ApexPages.StandardController stdController) {
        this.thisCase = (Case)stdController.getRecord();

        //selectedCaseReason ='';
        // get policy record
        parentRecordId   = ApexPages.currentPage().getParameters().get('parentId');
        bypass = ApexPages.currentPage().getParameters().get('bypass');
        if ( String.isBlank( parentRecordId) ) {
            // must have parentId , otherwise bail out
            errorMsg = 'No Policy ID provided.';
            //apexpages.addMessage(new ApexPages.message(Apexpages.Severity.WARNING,'No Policy ID provided.'));
            return;
        }
        //caseReasonOptsList  = new List<SelectOption>();
        //caseReasonOptsList.add( new SelectOption ( '', '--None--' ));
        nonClaim = false;
        geClaim = false;
        for( Contract c:  [
            SELECT
            Id,
            Status,
            AccountId,
            Contact_ID__c,
            Account.PersonEmail,
            Package__c,
            Central_Vacuum__c,
            Second_Refrigerator__c,
            Sump_Pump__c,
            Well_Pump__c,
            Roof_Leak__c,
            Stand_Alone_Freezer__c,
            Lawn_Sprinkler_System__c,
            Septic_System__c,
            Ice_Maker_In_Refrigerator__c,
            Lighting_Fixtures_Plumbing_Fixtures__c,
            Additional_AC_Unit_each__c,
            Additional_Heat_Furnace_each__c,
            Additional_Water_Heater_each__c,
            Spa__c,
            Pool__c,
            Priority_Value__c,
            No_Cancellation_Fee__c,
            BillingStateCode,
            Manager_Total_Discounted_Price_Override__c,
            StartdateAndGracePeriod__c,
            Original_Policy_End_Date__c,
            Current_Contract_Term__c,
            BillingPostalCode
            FROM Contract
            WHERE Id = :parentRecordId  
        ]) {
            
            accountRecordId = c.AccountId;
            contactRecordId = c.Contact_ID__c;
            contactRecordEmail = c.Account.PersonEmail;
            polStatus = c.Status;
            polPriority = c.Priority_Value__c;
            polStartDate = c.StartdateAndGracePeriod__c;
            polEndDate = c.Original_Policy_End_Date__c;
            polContractTerm = c.Current_Contract_Term__c;
            polZip = c.BillingPostalCode;
            hide = false;
            
            if (polStatus == 'Expired' || polStatus == 'Lapsed' || polStatus == 'Canceled' || polStatus == 'Payment Declined' || polStatus == 'Refuse SCF' || polStatus == 'Refuse SCF NSA') {
                hide = true;
                nonClaim = true;
                if (bypass == 'true') {
                    hide = false;
                }
            } /*else {
                if( c.Pool__c ) caseReasonOptsList.add( new SelectOption ( 'Pool', 'Pool' ));  
                if( c.Central_Vacuum__c ) caseReasonOptsList.add( new SelectOption ( 'Central Vacuum', 'Central Vacuum' ));  
                if( c.Second_Refrigerator__c ) caseReasonOptsList.add( new SelectOption ( 'Second Refrigerator', 'Second Refrigerator' ));  
                if( c.Sump_Pump__c ) caseReasonOptsList.add( new SelectOption ( 'Sump Pump', 'Sump Pump' ));  
                if( c.Well_Pump__c ) caseReasonOptsList.add( new SelectOption ( 'Well Pump', 'Well Pump' ));  
                if( c.Roof_Leak__c ) caseReasonOptsList.add( new SelectOption ( 'Roof Leak', 'Roof Leak' ));  
                if( c.Stand_Alone_Freezer__c ) caseReasonOptsList.add( new SelectOption ( 'Stand Alone Freezer', 'Stand Alone Freezer' ));  
                if( c.Lawn_Sprinkler_System__c ) caseReasonOptsList.add( new SelectOption ( 'Lawn Sprinkler System', 'Lawn Sprinkler System' ));  
                if( c.Septic_System__c ) caseReasonOptsList.add( new SelectOption ( 'Septic System', 'Septic System' ));  
                if( c.Ice_Maker_In_Refrigerator__c) caseReasonOptsList.add( new SelectOption ( 'Ice Maker (In Refrigerator)', 'Ice Maker (In Refrigerator)' ));  
                if( c.Lighting_Fixtures_Plumbing_Fixtures__c ) caseReasonOptsList.add( new SelectOption ( 'Lighting Fixtures/Plumbing Fixtures', 'Lighting Fixtures/Plumbing Fixtures' ));  
                if( c.Additional_AC_Unit_each__c != NULL ) caseReasonOptsList.add( new SelectOption ( 'Additional AC Unit(each)', 'Additional AC Unit(each)' ));
                if( c.Additional_Water_Heater_each__c != NULL ) caseReasonOptsList.add( new SelectOption ( 'Additional Water Heater (each)', 'Additional Water Heater (each)' ));  
                if( c.Spa__c ) caseReasonOptsList.add( new SelectOption ( 'Spa', 'Spa' ));  
                
                if( c.Package__c ==  'Platinum Care' ) {
                    caseReasonOptsList.add( new SelectOption ('A/C, Cooling', 'A/C, Cooling'));
                    caseReasonOptsList.add( new SelectOption ('Ceiling Fan', 'Ceiling Fan'));
                    caseReasonOptsList.add( new SelectOption ('Clothes Dryer', 'Clothes Dryer'));
                    caseReasonOptsList.add( new SelectOption ('Clothes Washer', 'Clothes Washer'));
                    caseReasonOptsList.add( new SelectOption ('Cooktop', 'Cooktop'));
                    caseReasonOptsList.add( new SelectOption ('Dishwasher', 'Dishwasher'));
                    caseReasonOptsList.add( new SelectOption ('Ductwork', 'Ductwork'));
                    caseReasonOptsList.add( new SelectOption ('Electrical System', 'Electrical System'));
                    caseReasonOptsList.add( new SelectOption ('Garage Door Opener', 'Garage Door Opener'));
                    caseReasonOptsList.add( new SelectOption ('Garbage Disposal', 'Garbage Disposal'));
                    caseReasonOptsList.add( new SelectOption ('Heating System', 'Heating System'));
                    caseReasonOptsList.add( new SelectOption ('Microwave Oven (Built In)', 'Microwave Oven (Built In)'));
                    caseReasonOptsList.add( new SelectOption ('Plumbing Stoppage', 'Plumbing Stoppage'));
                    caseReasonOptsList.add( new SelectOption ('Plumbing System', 'Plumbing System'));
                    caseReasonOptsList.add( new SelectOption ('Refrigerator', 'Refrigerator'));
                    caseReasonOptsList.add( new SelectOption ('Stove/Oven', 'Stove/Oven'));
                    caseReasonOptsList.add( new SelectOption ('Water Heater', 'Water Heater'));  
                }
                if( c.Package__c ==  'Gold Care' ) {
                    thisCase.Reason = 'A/C, Cooling';
                    caseReasonOptsList.add( new SelectOption ('A/C, Cooling', 'A/C, Cooling'));
                    caseReasonOptsList.add( new SelectOption ('Ductwork', 'Ductwork'));
                    caseReasonOptsList.add( new SelectOption ('Electrical System', 'Electrical System'));
                    caseReasonOptsList.add( new SelectOption ('Heating System', 'Heating System'));
                    caseReasonOptsList.add( new SelectOption ('Plumbing System', 'Plumbing System'));
                    caseReasonOptsList.add( new SelectOption ('Water Heater', 'Water Heater'));
                }
                if( c.Package__c ==  'Bronze Care' ) {
                    caseReasonOptsList.add( new SelectOption ('Clothes Dryer', 'Clothes Dryer'));
                    caseReasonOptsList.add( new SelectOption ('Clothes Washer', 'Clothes Washer'));
                    caseReasonOptsList.add( new SelectOption ('Cooktop', 'Cooktop'));
                    caseReasonOptsList.add( new SelectOption ('Dishwasher', 'Dishwasher'));
                    caseReasonOptsList.add( new SelectOption ('Garbage Disposal', 'Garbage Disposal'));
                    caseReasonOptsList.add( new SelectOption ('Microwave Oven (Built In)', 'Microwave Oven (Built In)'));
                    caseReasonOptsList.add( new SelectOption ('Refrigerator', 'Refrigerator'));
                    caseReasonOptsList.add( new SelectOption ('Stove/Oven', 'Stove/Oven'));
                }
                caseReasonOptsList.sort();
            }
            caseReasonOptsList.add( new SelectOption ('Billing Ticket', 'Billing Ticket'));
            caseReasonOptsList.add( new SelectOption ('Retention Ticket', 'Retention Ticket'));*/
            gs = [Select Id, System_Administrator_User_ID__c, GE_Contractor_Id__c FROM Global_Setting__c LIMIT 1];
            for (Coverage_Area__c ca :[SELECT Zip_Code__r.Name 
                                       FROM Coverage_Area__c 
                                       WHERE Active__c = TRUE AND Contractor__c = :gs.GE_Contractor_ID__c AND Trade__c = 'Appliances']) {
                geZips.add(ca.Zip_Code__r.Name);
            }
            for (Case_Reason_Coverage_Mapping__mdt rec : [SELECT MasterLabel, DEveloperName, Coverage_Trades__c, Coverage_Items__c FROM Case_Reason_Coverage_Mapping__mdt ORDER BY MasterLabel]) {
                crCovMap.put(rec.MasterLabel, rec);
            }
    
            //Set cancellation fee
            Double tenPercent = 0;
            if (c.Manager_Total_Discounted_Price_Override__c != NULL) {
                tenPercent = c.Manager_Total_Discounted_Price_Override__c * 0.10;
            }
            if (c.No_Cancellation_Fee__c == TRUE) {
                thisCase.Cancellation_Fee__c = 0;
            } else if (c.BillingStateCode == 'AL') {
                thisCase.Cancellation_Fee__c = 25;
            } else if (c.BillingStateCode == 'OK') {
                thisCase.Cancellation_Fee__c = 50;
            } else if (c.BillingStateCode == 'AZ' || c.BillingStateCode == 'IL') {
                if (tenPercent < 50) {
                    thisCase.Cancellation_Fee__c = tenPercent;
                } else {
                    thisCase.Cancellation_Fee__c = 50;
                }
            } else if (c.BillingStateCode == 'GA') {
                if (tenPercent < 75) {
                    thisCase.Cancellation_Fee__c = tenPercent;
                } else {
                    thisCase.Cancellation_Fee__c = 75;
                }            
            } else if (c.BillingStateCode == 'CA') {
                thisCase.Cancellation_Fee__c = 0;
            }
            
            existingClaims = new List<Case>();
            appliance30Days = FALSE;
            sameReason365Days = FALSE;
            raAppliance365Days = FALSE;
        }
        
        //doChange();
    }
    
    public void updateReasonValue() {
        //thisCase.Reason = selectedCaseReason;
        //if (selectedCaseReason == 'Billing Ticket' || selectedCaseReason == 'Retention Ticket') {
        if (thisCase.Reason == 'Billing Ticket' || thisCase.Reason == 'Retention Ticket') {
            nonClaim = true;
        } else {
            nonClaim = false;
        }
    }
    
    public void checkExistingClaim() {
        existingClaims = new List<Case>();
        if (thisCase.Reason != NULL && thisCase.Reason != 'Billing Ticket' && thisCase.Reason != 'Retention Ticket') {
            
            if (Util.duplicateClaimReason(thisCase.Reason)) { //check valid appliance reason
                for (Case ec :[SELECT Id, CaseNumber, Reason, Issue_Detail__c, Status, CreatedById, CreatedDate, (SELECT Id FROM RA_Consents__r) FROM Case WHERE Policy__c = :parentRecordId AND Reason = :thisCase.Reason AND CreatedDate = LAST_N_DAYS:365 ORDER BY CreatedDate DESC]) {
                    if ((ec.Status == 'RA Sent' || ec.Status == 'RA Received' || ec.Status == 'Sent Check' || ec.Status == 'Check Status') && ec.RA_Consents__r.size()>0) {//Check appliances Last 365 Days AND RA status AND RA Form
                        existingClaims.add(ec);
                        raAppliance365Days = TRUE;
                    } else if (Date.newInstance(ec.CreatedDate.year(), ec.CreatedDate.month(), ec.CreatedDate.day()).daysBetween(Date.today())<=30) { //Check appliances Last 30 Days
                        existingClaims.add(ec);
                        appliance30Days = TRUE;
                    } 
                }
            }
            
            //Check Same Reason Within Last 365 Days PERIOD of policy START DATE for ceiling fan/garbage disposal/microwave oven
            Date qryStrDate = polStartDate;
            Integer days = qryStrDate.daysBetween(Date.today());
            if (days > 0 && (thisCase.Reason == 'Ceiling Fan' || thisCase.Reason == 'Garbage Disposal' || thisCase.Reason == 'Microwave Over (Built In)')) {
                while (days > 365) {
                    days-= 365;
                    qryStrDate = qryStrDate.addDays(365);
                }
                system.debug(qryStrDate);
                system.debug(days);
                for (Case ec :[SELECT Id, CaseNumber, Reason, Issue_Detail__c, Status, CreatedById, CreatedDate FROM Case WHERE Policy__c = :parentRecordId AND Reason = :thisCase.Reason AND CreatedDate > :qryStrDate AND ID NOT IN :existingClaims ORDER BY CreatedDate DESC]) {
                    existingClaims.add(ec);
                    sameReason365Days = TRUE;
                }
            }
        }
        
        //set trade
        if (thisCase.Reason != NULL && crCovMap.get(thisCase.Reason) != NULL) {
            thisCase.Coverage_Trades__c = crCovMap.get(thisCase.Reason).Coverage_Trades__c;
            thisCase.Coverage_Items__c = crCovMap.get(thisCase.Reason).Coverage_Items__c;
        } else {
            thisCase.Coverage_Trades__c = NULL;
            thisCase.Coverage_Items__c = NULL;
        }
        
        //check if appliances and GE Zip
        if (thisCase.Coverage_Trades__c != NULL && thisCase.Coverage_Trades__c.contains('Appliances') && geZips.contains(polZip)) {
            geClaim = TRUE;
        } else {
            geClaim = FALSE;
        }
        
        system.debug('geClaim: ' + geClaim);
        system.debug('geZips: ' + geZips);
        system.debug('thisCase.Coverage_Trades__c: ' + thisCase.Coverage_Trades__c);
    }
    
    public PageReference saveRecord(){
        errorMsg ='';
        try {
            // insert Case fields
            if( errorMsg == '') {
                //assign parent policy id 
                if( String.isNotBlank(parentRecordId) )
                    thisCase.Policy__c = parentRecordId;
                
                thisCase.AccountId = accountRecordId;
                thisCase.ContactId = contactRecordId;
                
                //assign caseReason 
                //thisCase.Reason = selectedCaseReason;   
                thisCase.Status = 'Placed by Agent';
                
                if (existingClaims.size()>0) {
                    existingClaims.sort();
                    thisCase.Recall_Parent__c = existingClaims[0].Id;
                }
                
                insert thisCase;
                //To assign new case Id
                currentRecordId = thisCase.Id;
            }
        } catch ( Exception err ) {
            if ( String.isNotBlank( err.getMessage()) && err.getMessage().contains( 'error:' ) )
                errorMsg = err.getMessage().split('error:')[1].split(':')[0] + '.';
            else
                errorMsg = err.getMessage();  
        }
        return NULL;
    }
    
    public PageReference allowBypass(){
        PageReference newPage = new PageReference('/apex/CaseNew?bypass=true&parentId=' + parentRecordId);
        newPage.setRedirect(true);
        
        return newPage;
    }
}