/*

    @Description    CaseNewController Class 
    @createdate     19 May 2018

*/
@isTest
private class CaseNewControllerTest {
    private static DataFactory df = new DataFactory();

	@isTest static void controllerTest1() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        df.newPolicy.No_Cancellation_Fee__c = TRUE;
        update df.newPolicy;
        df.newCase.Reason = 'Billing Ticket';
        update df.newCase;

        PageReference pageRef = Page.CaseNew;
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('id',df.newCase.id);
        ApexPages.currentPage().getParameters().put('parentId',df.newPolicy.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newCase);
        CaseNewController extension = new CaseNewController(controller);
        extension.updateReasonValue();
        System.assertEquals( df.newCase.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        System.debug(extension.closeWindow);
        System.debug(extension.currentRecordId);

        Test.stopTest();
	}
    
    @isTest static void controllerTest2() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        df.newPolicy.Status = 'Expired';
        df.newPolicy.BillingStateCode = 'AL';
        update df.newPolicy;

        PageReference pageRef = Page.CaseNew;
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('id',df.newCase.id);
        ApexPages.currentPage().getParameters().put('parentId',df.newPolicy.id);
		ApexPages.currentPage().getParameters().put('bypass','true');
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newCase);
        CaseNewController extension = new CaseNewController(controller);
        System.assertEquals( df.newCase.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        extension.updateReasonValue();
		extension.allowBypass();
        
        Test.stopTest();
	}
    
     @isTest static void controllerTest3() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        df.newPolicy.BillingStateCode = 'AZ';        
        df.newPolicy.Package__c = 'Platinum Care';
        update df.newPolicy;

        PageReference pageRef = Page.CaseNew;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',df.newCase.id);
        ApexPages.currentPage().getParameters().put('parentId',df.newPolicy.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newCase);
        CaseNewController extension = new CaseNewController(controller);
        System.assertEquals( df.newCase.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'parentId' ) );

        Test.stopTest();
    }
    
    @isTest static void controllerTest4() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        df.newPolicy.BillingStateCode = 'GA';  
        update df.newPolicy;

        PageReference pageRef = Page.CaseNew;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',df.newCase.id);
        ApexPages.currentPage().getParameters().put('parentId',df.newPolicy.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newCase);
        CaseNewController extension = new CaseNewController(controller);

        Test.stopTest();
    }
    
     @isTest static void controllerTest5() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        df.newPolicy.BillingStateCode = 'CA';  
        update df.newPolicy;

        PageReference pageRef = Page.CaseNew;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',df.newCase.id);
        ApexPages.currentPage().getParameters().put('parentId',df.newPolicy.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newCase);
        CaseNewController extension = new CaseNewController(controller);

        Test.stopTest();
    }
    
	@isTest static void saveRecord() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        df.newPolicy.BillingStateCode = 'OK';
        update df.newPolicy;
        
        PageReference pageRef = Page.CaseNew;
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('id',df.newCase.id);
        ApexPages.currentPage().getParameters().put('parentId',df.newPolicy.Id);
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newCase);
        CaseNewController extension = new CaseNewController(controller);
        df.newPolicy.Package__c = 'Gold Care';
        update df.newPolicy;
        
        CaseNewController controller1 = new CaseNewController(controller);
        System.assertEquals( df.newCase.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        System.assertEquals( df.newPolicy.Id, Apexpages.currentpage().getparameters().get( 'parentId' ) );

        df.newPolicy.Package__c = 'Bronze Care';
        update df.newPolicy;
        CaseNewController controller2 = new CaseNewController(controller);
        controller1.thisCase.Reason = 'Additional AC Unit(each)';
        controller1.thisCase.Issue_Detail__c = 'Humidifier is leaking water';
        controller1.saveRecord();

        Test.stopTest();
	}
}