/*
@Name           CaseTriggerTest 
@Description    CaseTrigger test class 
@createdate     Aug 2021

*/

@isTest
public class CaseTriggerTest {
    private static DataFactory df = new DataFactory();
    
    @isTest
    public static void testCaseTrigger(){
        df.createGlobalSetting();
        df.createPolicy();
        df.createInquiry();
        
        Case_Assignment_Round_Robin__c roundRobin = new Case_Assignment_Round_Robin__c();
        roundRobin.Status__c = 'Autho Review';
        roundRobin.User__c = UserInfo.getUserId();
        roundRobin.Away__c = false;
        INSERT roundRobin;
        
        Test.startTest();
        Case cseRecord = new Case();
        cseRecord.AccountId = df.acc.Id;
        cseRecord.Subject='subject';
        cseRecord.Status = 'Not Set';
        cseRecord.Description='description';
        cseRecord.Type='Claim Submission';
        cseRecord.Issue_Started__c = Date.today();
        cseRecord.Contractor_Authorized_Amount__c=1100;
        cseRecord.Customer_Invoice_Received_Date__c = Date.today();
        cseRecord.Customer_Authorized_Amount__c=1200;
        cseRecord.Customer_Reimbursement_Amount__c=1200;
        cseRecord.Invoice_Due_Date__c = Date.today();
        cseRecord.Invoice_Received_Date__c = Date.today();
        cseRecord.Contractor__c = df.contractor.id;
        cseRecord.Reason = 'Additional AC Unit(each)';
        cseRecord.Issue_Detail__c = 'Humidifier is leaking water';
        cseRecord.Cancellation_Date__c = Date.today();
        cseRecord.Cancellation_Fee__c = 0;
        cseRecord.Plan_Amount__c = 0;
        cseRecord.Claims_Paid__c = 0;
        cseRecord.Customer_Paid__c = 0;
        cseRecord.Invoice_Status__c = 'NewCPA';
        cseRecord.Origin = 'Web';
        cseRecord.Web_Policy__c = 'SHW7654321';
        INSERT cseRecord;
        
        cseRecord.Policy__c = df.newPolicy.id;
        cseRecord.Customer_Cheque_Date__c = System.today().addDays(2);
        cseRecord.Origin = 'Email';
        cseRecord.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        UPDATE cseRecord;
        
        cseRecord.App_Email_Triggered__c = true;
        cseRecord.App_Email_Complete__c = false;
        cseRecord.Status = 'App Email Sent';
        cseRecord.IsCovered__c = true;
        cseRecord.Status = 'Autho Review';
        cseRecord.Trigger_Contractor_Acc_App_Set_Email__c = '7 Days';
        UPDATE cseRecord;
        
        cseRecord.Trigger_Contractor_Acc_App_Set_Email__c = '2 Days';
        UPDATE cseRecord;
        
        Case updatedCase = [SELECT Id,Trigger_Contractor_Acc_App_Set_Email__c FROM Case WHERE Id =: cseRecord.Id LIMIT 1];
        System.assertEquals('2 Days', updatedCase.Trigger_Contractor_Acc_App_Set_Email__c);
        Test.stopTest();
    }
    @isTest
    public static void testDuplicate(){
        df.createGlobalSetting();
        df.createPolicy();
        df.createInquiry();
        
        Test.startTest();
        Case cseRecord = new Case();
        cseRecord.Policy__c = df.newPolicy.id;
        cseRecord.AccountId = df.acc.Id;
        cseRecord.Subject='subject';
        cseRecord.Status = 'Not Set';
        cseRecord.Description='description' + Integer.valueOf(Math.random() * 100);
        cseRecord.Type='Claim Submission';
        cseRecord.Issue_Started__c = Date.today();
        cseRecord.Contractor_Authorized_Amount__c=1100;
        cseRecord.Customer_Invoice_Received_Date__c = Date.today();
        cseRecord.Customer_Authorized_Amount__c=1200;
        cseRecord.Customer_Reimbursement_Amount__c=1200;
        cseRecord.Invoice_Due_Date__c = Date.today();
        cseRecord.Invoice_Received_Date__c = Date.today();
        cseRecord.Contractor__c = df.contractor.id;
        cseRecord.Reason = 'Billing Ticket';
        cseRecord.Issue_Detail__c = 'Humidifier is leaking water';
        cseRecord.Cancellation_Date__c = Date.today();
        cseRecord.Cancellation_Fee__c = 0;
        cseRecord.Plan_Amount__c = 0;
        cseRecord.Claims_Paid__c = 0;
        cseRecord.Customer_Paid__c = 0;
        cseRecord.Invoice_Status__c = 'NewCPA';
        cseRecord.Origin = 'Web';
        cseRecord.Web_Policy__c = 'SHW7654321';
        cseRecord.Recall_Parent__c = df.newInquiry.Id;
        cseRecord.Flag_Delete__c = true;
        cseRecord.Web_Street_Address__c = '9111 NW 37TH PLACE';
        cseRecord.Web_City__c = 'Altamonte Springs';
        cseRecord.Web_State__c = 'AS';
        cseRecord.Web_Zip__c = '32701';
        cseRecord.Brand__c = 'A.O. Smith';
        cseRecord.Location_Details__c = 'Attic';
        cseRecord.Model__c = 'New Model' + Integer.valueOf(Math.random() * 100);
        cseRecord.Serial_Number__c = '44654654989';
        cseRecord.Age__c = '5';
        cseRecord.Prior_Issue_Repaired_Before__c = 'Yes';
        cseRecord.SuppliedName = 'New Case ' + Integer.valueOf(Math.random() * 100);
        cseRecord.SuppliedEmail = 'home.warranty@gmail.com';
        INSERT cseRecord;
        
        Case updatedCase = [SELECT Id,Origin FROM Case WHERE Id =: cseRecord.Id LIMIT 1];
        System.assertEquals('Web', updatedCase.Origin);
        Test.stopTest();
    }
}