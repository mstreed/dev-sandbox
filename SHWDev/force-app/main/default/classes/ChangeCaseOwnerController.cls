public class ChangeCaseOwnerController {
    PageReference cancel;
    Case[] records;
    Public Case newOwner {get;set;}
    public ChangeCaseOwnerController (ApexPages.StandardSetController c) {
        records = (Case[])c.getSelected();
        cancel = c.cancel();
        newOwner = new Case();
    }
    public PageReference updateCases() {
        for(Case record: records) {
            record.OwnerId = newOwner.OwnerId;
        }
        update records;
        return cancel.setRedirect(true);
    }
}