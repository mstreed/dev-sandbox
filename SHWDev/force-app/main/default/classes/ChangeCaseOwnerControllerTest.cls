@isTest
private class ChangeCaseOwnerControllerTest {
    private static DataFactory df = new DataFactory();

    @isTest static void controllerTest() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        
        List<Case> cases = [Select Id FROM Case];
            
        Test.setCurrentPage(Page.ChangeCaseOwner);
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(cases);
        stdSetController.setSelected(cases);
        ChangeCaseOwnerController ext = new ChangeCaseOwnerController(stdSetController);
        ext.newOwner.OwnerId = UserInfo.getUserId();
        ext.updateCases();
        
        Test.stopTest();
    }
}