@isTest
private class CommissionsReportControllerTest {
    private static DataFactory df = new DataFactory();

    @isTest
    static void testController() {
        Test.startTest();

        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        df.createRenewal();
        
        User usr = [Select Id, Daily_Spiff_Type__c, Daily_Spiff_Percent__c FROM User WHERE Id = :UserInfo.getUserId()];
        usr.Daily_Spiff_Type__c = 'Years';
        usr.Daily_Spiff_Percent__c = '1.5';
        usr.Year_Commissions_Type__c = '$20';
        update usr;
        
        CommissionsReportController customCon = new CommissionsReportController();
        customCon.getCommsData(true);
        //customCon.emailCSV();

        usr.Daily_Spiff_Type__c = 'Amount Billed';
        usr.Year_Commissions_Type__c = '$15';
        update usr;
        customCon.getCommsData(true);
        
        df.newPolicy.Payment_Terms__c = 'Single Payment';
        df.policyRenewal.of_Payments__c = 'Single Payment';
        df.policyRenewal.Service_Call_fee_DD__c = '100';
        df.policyRenewal.Renewal_Amount__c = 14000;
        update df.newPolicy;
        update df.policyRenewal;
        customCon.getCommsData(true);
        
        df.newPolicy.Payment_Terms__c = '2 Payments';
        df.policyRenewal.of_Payments__c = '2 Payments';
        df.policyRenewal.Service_Call_fee_DD__c = '125';
        df.policyRenewal.Renewal_Amount__c = 20000;
        update df.newPolicy;
        update df.policyRenewal;
        customCon.getCommsData(true);
        
        Test.stopTest();
    }
    
    @isTest
    static void testController2() {
        Test.startTest();

        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        df.createRenewal();
                
        User usr = [Select Id, Daily_Spiff_Type__c, Daily_Spiff_Percent__c FROM User WHERE Id = :UserInfo.getUserId()];
        usr.Daily_Spiff_Type__c = 'Years';
        usr.Daily_Spiff_Percent__c = '1.5';
        usr.Year_Commissions_Type__c = '$20';
        update usr;
        
        CommissionsReportController customCon = new CommissionsReportController();
        customCon.getCommsData(true);
        //customCon.emailCSV();

        usr.Daily_Spiff_Type__c = 'Amount Billed';
        usr.Year_Commissions_Type__c = '$15';
        update usr;
        customCon.getCommsData(true);
        
        df.newPolicy.Payment_Terms__c = '3 Payments';
        df.policyRenewal.of_Payments__c = '3 Payments';
        df.policyRenewal.Renewal_Amount__c = 30000;
        update df.newPolicy;
        update df.policyRenewal;
        customCon.getCommsData(true);
        
        df.newPolicy.Payment_Terms__c = '4 Payments';
        df.policyRenewal.of_Payments__c = '4 Payments';
        df.policyRenewal.Renewal_Amount__c = 40000;
        update df.newPolicy;
        update df.policyRenewal;
        customCon.getCommsData(true);  
        
        customCon.dateToUpdate = Date.today();
        customCon.dailySpiffsMap.get(usr.Id).get(Date.today()).dailySpiffOverride.Daily_Spiff_Amount_Override__c  = 1500;
        customCon.updateDailySpiffOverride();

        /*customCon.recIdToUpdate = df.policyRenewal.Id;
        customCon.commissionsList[0].dateOverride.Commission_Date_Override__c = Date.today();
        customCon.updateDateOverride();*/
        
        customCon.recIdToUpdate = df.policyRenewal.Id;
        customCon.publicCommsMap.get(usr.Id)[0].typeOverrideSelected = '3x';

        customCon.updateTypeOverride();
        
        customCon.getCommsData(false);
        Test.stopTest();
    }
    
    @isTest
    static void testController3() {
        Test.startTest();

        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        df.createRenewal();
                
        User usr = [Select Id, Daily_Spiff_Type__c, Daily_Spiff_Percent__c FROM User WHERE Id = :UserInfo.getUserId()];
        usr.Daily_Spiff_Type__c = 'Years';
        usr.Daily_Spiff_Percent__c = '1.5';
        usr.Year_Commissions_Type__c = '$20';
        usr.AKA__c = 'TEST';
        update usr;
        
        CommissionsReportController customCon = new CommissionsReportController();
        df.newPolicy.Status = 'Canceled';
        df.newPolicy.Cancelled_Date__c = Date.today();
        df.newPolicy.Cancellation_Refund_Years__c = '1';
        df.newPolicy.Cancellation_Refund_Amount__c = 10;
        df.newPolicy.Cancellation_Reason__c = 'Buyers Remorse After 30 Days';
        update df.newPolicy;
        df.policyRenewal.Renewal_Status__c = 'Canceled';
        df.policyRenewal.Cancellation_Date__c = Date.today();
        df.policyRenewal.Cancellation_Refund_Years__c = '1';
        df.policyRenewal.Cancellation_Refund_Amount__c = 10;
        df.policyRenewal.Cancellation_Reason__c = 'Buyers Remorse After 30 Days';
        update df.policyRenewal;
        customCon.getCommsData(true);
        
        List<SelectOption> commsOverrideList = customCon.commsOverrideList;
        
        Test.stopTest();
    }
}