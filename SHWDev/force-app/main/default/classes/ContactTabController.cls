public class ContactTabController{
    public Contact thisCon {set; get;}
    public User currentUser {set;get;}
    public String currentRecordId {get;set;}
    
    public ContactTabController( ApexPages.StandardController stdController){
        currentRecordId = ApexPages.currentPage().getParameters().get('id');
        
        currentUser = [Select Id, Name, Profile.Name FROM User where Id = :userinfo.getUserId()];
        
        thisCon = [Select Id, AccountId, Account.Policy__c FROM Contact WHERE Id = :currentRecordId];
    }
    
    public PageReference redirect() {
        //PageReference returnURL;
        
       /* if(currentUser.Name == 'Select Home Warranty') { 
            returnURL = new PageReference('/' + currentRecordId);
            returnURL.getParameters().put('nooverride', '1');
        } else {*/
            if (thisCon.Account.Policy__c!=NULL) {
                return new PageReference('/' + thisCon.Account.Policy__c);
            } else {
                return new PageReference('/apex/InsufficientAccess');
            }
        //} 
        
        //returnURL.setRedirect(true);
        //return returnURL; 
    }
}