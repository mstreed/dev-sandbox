@isTest
private class ContactTabControllerTest {
    private static DataFactory df = new DataFactory();

    @isTest static void controllerTest() {
        df.createGlobalSetting();
        df.createPolicy();
        Contact con = new Contact(Id = df.conId);
        
        Test.StartTest();
        PageReference pageRef = Page.ContactTab;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',df.conId);
        ApexPages.StandardController controller = new ApexPages.StandardController(con);
        ContactTabController extension = new ContactTabController(controller);
        extension.redirect();
        Test.stopTest();
    }
}