global class ContractorRatingRanking_batch implements Database.Batchable<sObject>, Database.Stateful {
    Integer i = 1;
    private String pf;
    private String rf;
    private String ord;
    private String s;
    
    public ContractorRatingRanking_batch(String pointField, String rankingField, String order, String service) {
        pf = pointField;
        rf = rankingField;
        ord = order;
        s = service;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id, '+pf+', '+rf+' FROM Contractor_Ranking__c WHERE Service__c = \''+s+'\' ORDER BY '+ord;
        system.debug('query: '+ query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Contractor_Ranking__c> scope) {
        
        for (Contractor_Ranking__c rankRec : scope) {
            if (rf == 'Cost_Rank__c') {
                rankRec.Cost_Rank__c = i;
            } else if (rf == 'Quality_Rank__c') {
                rankRec.Quality_Rank__c = i;
            } else if (rf == 'Buyout_Rank__c') {
                rankRec.Buyout_Rank__c = i;
            }
            i++;
        }
        
        update scope;
    }   

    global void finish(Database.BatchableContext BC) {
        //after cost rank batch, run quality batch
        if (rf == 'Cost_Rank__c') {
            ContractorRatingRanking_batch rankQuality = new ContractorRatingRanking_batch('Quality_Point_Value__c','Quality_Rank__c','Quality_Point_Value__c DESC, Average_Rating__c DESC, of_Ratings__c DESC',s); 
            database.executeBatch(rankQuality,50);
        } else if (rf == 'Quality_Rank__c') { //after quality batch, run buyout batch
            ContractorRatingRanking_batch rankBuyout = new ContractorRatingRanking_batch('Buyout_Point_Value__c','Buyout_Rank__c', 'Buyout_Point_Value__c DESC, Buyout_Percent__c ASC',s); 
            database.executeBatch(rankBuyout,50);
        }
    }
}