global class ContractorRatingScoring_batch implements Database.Batchable<SObject>, Database.Stateful {
    Set<String> qualifiedRankingKeys = new Set<String>();
    Map<String,Decimal> mostEconomicalMap = new Map<String,Decimal>();
    Map<String,Decimal> highestRatingMap = new Map<String,Decimal>();
    Set<String> services = new Set<String>();
    global static Global_Setting__c gs = [Select Id, NSA_Contractor_ID__c FROM Global_Setting__c LIMIT 1];

    //this batch is to set the avg cost, rating and bop
    global static Contractor_Rating__c cr = [SELECT Id, Most_Economical_Appliance__c, Most_Economical_Electrical__c, Most_Economical_Garage_Door_Tech__c, Most_Economical_Hvac__c, Most_Economical_Lawn_Sprinkler_Tech__c, Most_Economical_Plumber__c, Most_Economical_Pool__c, Most_Economical_Pool_Tech__c, Most_Economical_Roofer__c, Most_Economical_Septic_Tech__c, Highest_Rating_Appliance__c, Highest_Rating_Electrical__c, 
                                             Highest_Rating_Garage_Door_Tech__c, Highest_Rating_Hvac__c, Highest_Rating_Lawn_Sprinkler_Tech__c, Highest_Rating_Plumber__c, Highest_Rating_Pool__c, Highest_Rating_Pool_Tech__c, Highest_Rating_Roofer__c, Highest_Rating_Septic_Tech__c 
                                             FROM Contractor_Rating__c LIMIT 1];

    global Database.QueryLocator start(Database.BatchableContext Bc){
        //return Database.getQueryLocator([SELECT Id, Name, Avg_Covered_Cost__c, of_Ratings_6_Months__c, Average_Rating_6_Months__c, Buyout_Percent__c, Cost_Rank__c, Quality_Rank__c, Buyout_Rank__c FROM Account WHERE ID IN (SELECT Contractor__c FROM Case WHERE Approved_Claim__c = TRUE AND Dispatched_Last_6_Months__c = TRUE) AND RecordType.DeveloperName = 'Contractor_Account']);
        return Database.getQueryLocator([SELECT Id, Name FROM Account WHERE ID IN (SELECT Contractor__c FROM Case WHERE Approved_Claim__c = TRUE AND Dispatched_Last_6_Months__c = TRUE AND Service_Provided__c != NULL) AND RecordType.DeveloperName = 'Contractor_Account']);
    } //Avg_Covered_Cost__c, of_Ratings_6_Months__c, Average_Rating_6_Months__c, Buyout_Percent__c, Cost_Rank__c, Quality_Rank__c, Buyout_Rank__c 

    global void execute(Database.BatchableContext Bc, List<Account> scope){
        Savepoint sp = Database.setSavepoint();
        
        //set beginning data
        if (services.size()==0) {
            services.add('Appliance');
            services.add('Electrical');
            services.add('Garage Door Tech');
            services.add('Hvac');
            services.add('Lawn Sprinkler Tech');
            services.add('Plumber');
            services.add('Pool');
            services.add('Pool Tech');
            services.add('Roofer');
            services.add('Septic Tech');
            
            for (String s :services) {
                mostEconomicalMap.put(s, NULL);
                highestRatingMap.put(s, 0);
            }
            
            setContractorRatingSettings();
        }
        
        try {
            Map<String,Contractor_Ranking__c> contractorRankings = new Map<String,Contractor_Ranking__c>();
            List<Contractor_Ranking__c> qualifiedRankings = new List<Contractor_Ranking__c>();
            
            for (Account acc :scope) {
                //calculate avg cost and ratings and bop
                for (Case cs : [SELECT Id, Contractor__c, Reason, Service_Provided__c, Customer_Reimbursement_Amount__c, Approved_Cost__c, Work_Order_Rating__c, Approved_Claim__c, Dispatched_Last_6_Months__c 
                                FROM Case 
                                WHERE Contractor__c = :acc.Id AND Approved_Claim__c = TRUE AND Dispatched_Last_6_Months__c = TRUE AND Service_Provided__c != NULL ORDER BY Service_Provided__c]) {
                                    system.debug('cs: ' + cs);

                                    String service = cs.Service_Provided__c;
                                    if (cs.Reason == 'Garbage Disposal' || cs.Reason == 'Lighting Fixtures/Plumbing Fixtures' || cs.Reason == 'Septic System' || cs.Reason == 'Water Heater') {
                                        service = 'Plumber';
                                    }

                                    if (cs.Approved_Claim__c == TRUE && cs.Dispatched_Last_6_Months__c == TRUE) {
                                        Contractor_Ranking__c rankRec = new Contractor_Ranking__c();
                                        if (contractorRankings.get(cs.Contractor__c+service) == NULL) {
                                            rankRec.Contractor__c = cs.Contractor__c;
                                            rankRec.Service__c = service;
                                            rankRec.of_Claims__c = 0;
                                            rankRec.Total_Covered_Cost__c = 0;
                                            rankRec.of_Ratings__c = 0;
                                            rankRec.Sum_Ratings__c = 0;
                                            rankRec.of_Claims_w_Customer_Reimbursement__c = 0;
                                            rankRec.Ranking_ID__c = rankRec.Contractor__c + rankRec.Service__c;
                                            contractorRankings.put(cs.Contractor__c+service, rankRec);
                                        }
                                        
                                        contractorRankings.get(cs.Contractor__c+service).of_Claims__c++;
                                        
                                        //cost
                                        contractorRankings.get(cs.Contractor__c+service).Total_Covered_Cost__c+= cs.Approved_Cost__c;
                                        
                                        //rating
                                        if (cs.Work_Order_Rating__c != NULL) {
                                            contractorRankings.get(cs.Contractor__c+service).of_Ratings__c++;
                                            contractorRankings.get(cs.Contractor__c+service).Sum_Ratings__c+= cs.Work_Order_Rating__c;
                                        }
                                        
                                        //buyout
                                        if (cs.Customer_Reimbursement_Amount__c > 0) {
                                            contractorRankings.get(cs.Contractor__c+service).of_Claims_w_Customer_Reimbursement__c++;
                                        }
                                    }
                                }
                
                system.debug('contractorRankings.size(): ' + contractorRankings.size());
                system.debug('contractorRankings: ' + contractorRankings);
                
                for (Contractor_Ranking__c rankRec : contractorRankings.values()) {
                    system.debug('rankRec: ' + rankRec);
                    
                    if (rankRec.of_Claims__c>=3 || rankRec.Contractor__c == gs.NSA_Contractor_ID__c) {
                        rankRec.Average_Covered_Cost__c = rankRec.Total_Covered_Cost__c/rankRec.of_Claims__c;
                        rankRec.Average_Rating__c = 0;
                        if (rankRec.of_Ratings__c > 0) {
                            rankRec.Average_Rating__c = rankRec.Sum_Ratings__c/rankRec.of_Ratings__c;
                        }
                        
                        rankRec.Cost_Rank__c = 0;
                        rankRec.Quality_Rank__c = 0;
                        rankRec.Buyout_Rank__c = 0;
                        
                        if (rankRec.of_Claims__c>=3) {
                            Decimal mostEconomical = mostEconomicalMap.get(rankRec.Service__c);
                            Decimal highestRating = highestRatingMap.get(rankRec.Service__c);
                            
                            //set most economical
                            if (mostEconomical == NULL || rankRec.Average_Covered_Cost__c <= mostEconomical) {
                                mostEconomical = rankRec.Average_Covered_Cost__c;
                            }
                            
                            //set highest rating
                            if (highestRating == NULL || rankRec.Average_Rating__c >= highestRating) {
                                highestRating = rankRec.Average_Rating__c;
                            }
                            
                            mostEconomicalMap.put(rankRec.Service__c, mostEconomical);
                            highestRatingMap.put(rankRec.Service__c, highestRating);
                        }
                        
                        qualifiedRankings.add(rankRec);
                        qualifiedRankingKeys.add(rankRec.Ranking_ID__c);
                    } else {
                        continue;
                    }
                }
                
            }
            
            if (!qualifiedRankings.isEmpty()) {
                system.debug('qualifiedRankings: ' + qualifiedRankings);
                                
                Schema.SObjectField ftoken = Contractor_Ranking__c.Fields.Ranking_ID__c;
                Database.UpsertResult[] srList = Database.upsert(qualifiedRankings,ftoken,false);
                for (Database.UpsertResult sr : srList) {
                    if (sr.isSuccess()) {
                        // Operation was successful
                    } else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : sr.getErrors()) {
                            System.debug('error has occurred.' + err.getStatusCode() + ': ' + err.getMessage());                    
                            System.debug('fields that affected this error: ' + err.getFields());
                            
                        }
                    }
                } 
            }
            
            
            /*
             List<Account> qualifiedContractors = new List<Account>();

             for (Account acc :scope) {
                //only contractors with 3 or more approved claims qualify
                Decimal totalCost = 0;
                Decimal totalRating = 0;
                Integer numOfRatings = 0;
                Decimal countCRA = 0;
                Integer countCases = 0;
                
                //calculate avg cost and ratings and bop
                for (Case cs : [SELECT Id, Contractor__c, Reason, Service_Provided__c, Customer_Reimbursement_Amount__c, Approved_Cost__c, Work_Order_Rating__c, Approved_Claim__c, Dispatched_Last_6_Months__c 
                                FROM Case 
                                WHERE Contractor__c = :acc.Id AND Approved_Claim__c = TRUE AND Dispatched_Last_6_Months__c = TRUE]) {
                    if (cs.Approved_Claim__c == TRUE && cs.Dispatched_Last_6_Months__c == TRUE) {
                        countCases++;
                        
                        //cost
                        totalCost+= cs.Approved_Cost__c;
                        
                        //rating
                        if (cs.Work_Order_Rating__c != NULL) {
                            numOfRatings++;
                            totalRating+= cs.Work_Order_Rating__c;
                        }
                        
                        //buyout
                        if (cs.Customer_Reimbursement_Amount__c > 0) {
                            countCRA++;
                        }
                    }
                }
                
                if (countCases>=3) {
                    acc.Avg_Covered_Cost__c = totalCost/countCases;
                    acc.of_Ratings_6_Months__c = numOfRatings;
                    if (acc.of_Ratings_6_Months__c > 0) {
                        acc.Average_Rating_6_Months__c = totalRating/acc.of_Ratings_6_Months__c;
                    } else {
                        acc.Average_Rating_6_Months__c = 0;
                    }
                    acc.Buyout_Percent__c = (countCRA/countCases)*100;
                        
                    //set most economical
                    if (mostEconomical == NULL || acc.Avg_Covered_Cost__c <= mostEconomical) {
                        mostEconomical = acc.Avg_Covered_Cost__c;
                    }
                    
                    //set highest rating
                    if (highestRating == NULL || acc.Average_Rating_6_Months__c >= highestRating) {
                        highestRating = acc.Average_Rating_6_Months__c;
                    }
                    
                    acc.Cost_Rank__c = 0;
                    acc.Quality_Rank__c = 0;
                    acc.Buyout_Rank__c = 0;
                    
                    qualifiedContractors.add(acc);
                    rankedContractors.add(acc.Id);
                    
                } else {
                    continue;
                }
                
                /Account a = (Account)acc; //Grab the Account sObject - needed to avoid query locator error
                Work_Order__c[] woList = a.getSObjects('Work_Orders__r'); //grab all the related Work_Orders__r records
                a.of_Ratings__c = 0;
                Double total = 0;
                if (woList != NULL) {
                    for (Work_Order__c wo : woList) {
                        if (wo.Rating__c != NULL) {
                            a.of_Ratings__c++;
                            total+= wo.Rating__c;
                        }
                    }
                }
                if (a.of_Ratings__c == 0) {
                    a.Average_Rating__c = NULL;
                } else {
                    a.Average_Rating__c = total/a.of_Ratings__c;
                }
                contractorsToUpdate.add(a);/
                
            }
            
            if (!qualifiedContractors.isEmpty()) {
                system.debug('qualifiedContractors: ' + qualifiedContractors);
                UPDATE qualifiedContractors;
            }*/
            
        } catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
            Database.rollback(sp);
        }
    }
    
    global void finish(Database.BatchableContext Bc){
        if (!qualifiedRankingKeys.isEmpty()) {
            //Delete other rankings
            DELETE [SELECT Id FROM Contractor_Ranking__c WHERE Ranking_ID__c NOT IN :qualifiedRankingKeys];
        }
        
        system.debug('mostEconomicalMap: ' + mostEconomicalMap);
        system.debug('highestRatingMap: ' + highestRatingMap);
        //set most economical and highestRating
        setContractorRatingSettings();
        UPDATE cr;
        
        //run cost rank batch
        for (String s :services) {
            ContractorRatingRanking_batch rankCost = new ContractorRatingRanking_batch('Cost_Point_Value__c','Cost_Rank__c', 'Cost_Point_Value__c DESC, Average_Covered_Cost__c ASC',s); 
            database.executeBatch(rankCost,50);
        }
        
        /*//update unqualified contractors
        List<Account> unqualifiedContractors = new List<Account>();

        for (Account uq : [SELECT Id FROM Account WHERE Id NOT IN :rankedContractors AND RecordType.DeveloperName = 'Contractor_Account' AND (Cost_Rank__c != NULL OR Quality_Rank__c != NULL OR Buyout_Rank__c != NULL)]) {
            uq.Cost_Rank__c = NULL;
            uq.Avg_Covered_Cost__c = NULL;
            
            uq.Quality_Rank__c = NULL;
            uq.Average_Rating_6_Months__c = NULL;
            uq.of_Ratings_6_Months__c = NULL;
            
            uq.Buyout_Rank__c = NULL;
            uq.Buyout_Percent__c = NULL;
            
            unqualifiedContractors.add(uq);
        }
        
        if (!unqualifiedContractors.isEmpty()) {
            UPDATE unqualifiedContractors;
        }
        
        //set most economical and highestRating
        cr.Cost_Most_Economical__c = mostEconomical;
        cr.Quality_Highest_Rating__c = highestRating;
        UPDATE cr;
        
        //run cost rank batch
        ContractorRatingRanking_batch rankCost = new ContractorRatingRanking_batch('Cost_Point_Value__c','Cost_Rank__c', 'Cost_Point_Value__c DESC, Avg_Covered_Cost__c ASC'); 
        database.executeBatch(rankCost,50);*/
    }

    
    private void setContractorRatingSettings() {
        for (String s :services) {
            if (s == 'Appliance') {
                cr.Most_Economical_Appliance__c = mostEconomicalMap.get(s);
                cr.Highest_Rating_Appliance__c = highestRatingMap.get(s);
            } else if (s == 'Electrical') {
                cr.Most_Economical_Electrical__c = mostEconomicalMap.get(s);
                cr.Highest_Rating_Electrical__c = highestRatingMap.get(s);
            } else if (s == 'Garage Door Tech') {
                cr.Most_Economical_Garage_Door_Tech__c = mostEconomicalMap.get(s);
                cr.Highest_Rating_Garage_Door_Tech__c = highestRatingMap.get(s);
            } else if (s == 'Hvac') {
                cr.Most_Economical_Hvac__c = mostEconomicalMap.get(s);
                cr.Highest_Rating_Hvac__c = highestRatingMap.get(s);
            } else if (s == 'Lawn Sprinkler Tech') {
                cr.Most_Economical_Lawn_Sprinkler_Tech__c = mostEconomicalMap.get(s);
                cr.Highest_Rating_Lawn_Sprinkler_Tech__c = highestRatingMap.get(s);
            } else if (s == 'Plumber') {
                cr.Most_Economical_Plumber__c = mostEconomicalMap.get(s);
                cr.Highest_Rating_Plumber__c = highestRatingMap.get(s);
            } else if (s == 'Pool') {
                cr.Most_Economical_Pool__c = mostEconomicalMap.get(s);
                cr.Highest_Rating_Pool__c = highestRatingMap.get(s);
            } else if (s == 'Pool Tech') {
                cr.Most_Economical_Pool_Tech__c = mostEconomicalMap.get(s);
                cr.Highest_Rating_Pool_Tech__c = highestRatingMap.get(s);
            } else if (s == 'Roofer') {
                cr.Most_Economical_Roofer__c = mostEconomicalMap.get(s);
                cr.Highest_Rating_Roofer__c = highestRatingMap.get(s);
            } else if (s == 'Septic Tech') {
                cr.Most_Economical_Septic_Tech__c = mostEconomicalMap.get(s);
                cr.Highest_Rating_Septic_Tech__c = highestRatingMap.get(s);
            }
        }
    }
    /*public class AccountWrapper implements comparable {
        public Account acc {get;set;}
        public Decimal pointVal {get;set;}
        public AccountWrapper (Account acc, Decimal pointVal){
            this.acc = acc;
            this.pointVal = pointVal;
        }
        public Integer compareTo (Object compareTo) {
            AccountWrapper aw = (AccountWrapper)compareTo;
            Integer returnValue = 0;
            If (pointVal > aw.pointVal) {
                returnValue = 1;
            } else if (pointVal < aw.pointVal) {
                returnValue = -1;
            }
            Return returnValue; 
        }
    }*/

}