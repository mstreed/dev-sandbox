@isTest
private class ContractorRatingTest {
    private static DataFactory df = new DataFactory();

    @isTest 
    static void tests() {
        df.createGlobalSetting();
        df.createContractorRating();
        df.createPolicy();
        df.createCase();
        df.createWorkOrder();
        df.workOrder.Contractor__c = df.contractor.Id;
        df.workOrder.Dispatched_Date__c = Date.today();
        df.workOrder.Date_Scheduled__c = Date.today();
        df.workOrder.Date_Rescheduled__c = Date.today();
        df.workOrder.Rating__c = 5;
        UPDATE df.workOrder;
        
        /*df.contractor.Cost_Rank__c = 0;
        df.contractor.Quality_Rank__c = 0;
        df.contractor.Buyout_Rank__c = 0;
		UPDATE df.contractor;

        Account cloneCon = df.contractor.clone(false, true, false, false);
		INSERT cloneCon;*/
        
        List<Case> csList = new List<Case>();
        csList.add(df.newCase.clone(false, true, false, false));
        csList.add(df.newCase.clone(false, true, false, false));
        csList.add(df.newCase.clone(false, true, false, false));
        Case plumb = df.newCase.clone(false, true, false, false);
        plumb.Reason = 'Garbage Disposal';
        csList.add(plumb);
        csList.add(plumb.clone(false, true, false, false));
        csList.add(plumb.clone(false, true, false, false));
        Case cenVac = df.newCase.clone(false, true, false, false);
        cenVac.Reason = 'Central Vacuum';
        csList.add(cenVac);
        csList.add(cenVac.clone(false, true, false, false));
        csList.add(cenVac.clone(false, true, false, false));
        INSERT csList;
        
        List<Work_Order__c> woList = new List<Work_Order__c>();
        Work_Order__c wo1 = df.workOrder.clone(false, true, false, false);
        wo1.Claim__c = csList[0].Id;
        woList.add(wo1);
        Work_Order__c wo2 = df.workOrder.clone(false, true, false, false);
        wo2.Claim__c = csList[1].Id;
        woList.add(wo2);
        Work_Order__c wo3 = df.workOrder.clone(false, true, false, false);
        wo3.Claim__c = csList[2].Id;
        woList.add(wo3);
        Work_Order__c wo4 = df.workOrder.clone(false, true, false, false);
        wo4.Claim__c = csList[3].Id;
        woList.add(wo4);
        Work_Order__c wo5 = df.workOrder.clone(false, true, false, false);
        wo5.Claim__c = csList[4].Id;
        woList.add(wo5);
        Work_Order__c wo6 = df.workOrder.clone(false, true, false, false);
        wo6.Claim__c = csList[5].Id;
        woList.add(wo6);
        Work_Order__c wo7 = df.workOrder.clone(false, true, false, false);
        wo7.Claim__c = csList[6].Id;
        woList.add(wo7);
        Work_Order__c wo8 = df.workOrder.clone(false, true, false, false);
        wo8.Claim__c = csList[7].Id;
        woList.add(wo8);
        INSERT woList;
        
        Test.StartTest();
        
        Database.executeBatch(new ContractorRatingScoring_batch(),50);
        
        ContractorRating_schedule sh1 = new ContractorRating_schedule();
        String sch = '0 0 23 * * ?'; system.schedule('Test', sch, sh1);
        Test.stopTest();
    }
}