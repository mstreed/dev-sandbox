global class ContractorRating_schedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        ContractorRatingScoring_batch batchRun = new ContractorRatingScoring_batch(); 
        ID batchId = Database.executeBatch(batchRun,1);
    }
}