public class CoverageAreaManagerExtension {
    public String errorMsg {set;get;}
    public String successMsg {set;get;}
    public String step {set;get;}
    
    private final Account con {get;set;}

    public Boolean filterApplied {get;set;}
    public String filterZip {get;set;}
    public String filterTravelFee {get;set;}
    public String filterCity {get;set;}
    public String filterCounty {get;set;}
    public String filterState {get;set;}
    public Coverage_Area__c filterCreatedDate {get;set;}
    public Coverage_Area__c filterLastModifiedDate {get;set;}
    public Coverage_Area__c filterSetExpiryDate {get;set;}
        
    public String searchState {get;set;}
    public String searchCounty {get;set;}
    public String searchCity {get;set;}
    public String searchRadius {get;set;}
    public List<SelectOption> getRadiusOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('0','0'));
        options.add(new SelectOption('5','5'));
        options.add(new SelectOption('10','10'));
        options.add(new SelectOption('15','15'));
        options.add(new SelectOption('20','20'));
        options.add(new SelectOption('25','25'));
        options.add(new SelectOption('30','30'));
        options.add(new SelectOption('35','35'));
        options.add(new SelectOption('40','40'));
        options.add(new SelectOption('45','45'));
        options.add(new SelectOption('50','50'));
        return options;
    }
    public String searchZip {get;set;}
    
    public String copyTradeFrom {get;set;}
    public String copyTradeTo {get;set;}
    
    public List<SelectOption> getExpireOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('By Zip Code','By Zip Code'));
        options.add(new SelectOption('By Zip Code Range','By Zip Code Range'));
        options.add(new SelectOption('By County','By County'));
        return options;
    }
    Transient Map<String,Map<String,Coverage_Area__c>> tradeCoverageMap;
    public List<Coverage_Area__c> tradeCoverageArea {get;set;}

    //public Set<String> selectedIds {get;set;}
    public String selectedTrade {get;set;}
    private String lastSelectedTrade;
    public String[] selectedTrades {get;set;}
    private Set<String> avlTrades = new Set<String>();
    public List<SelectOption> availableTradeOptions {get;set;}
    //public List<SelectOption> tradeOptions {get;set;}
    
    public List<CovAreaStagingWrapper> casWrapList {get;set;}
    private Map<String,Set<String>> existingZipsToTrades = new Map<String,Set<String>>();
    //public List<ZipCodeWrapper> zcWrapList {get;set;}
    
    public String emailType {get;set;}
    public String btn;
    
    private Integer counter=0;  //keeps track of the offset
    public Integer list_size {get;set;} //sets the page size or number of rows
    public Integer total_size; //used to show user the total size of the list
    public decimal distanceFirst = 0;
    public decimal distanceLast = 0;
    
    public Global_Setting__c gs {get;set;}

    public CoverageAreaManagerExtension(ApexPages.StandardController stdController) {
        if (!Test.isRunningTest()) {
            stdController.addFields(new List<String>{'ShippingLatitude'});
            stdController.addFields(new List<String>{'ShippingLongitude'});
            stdController.addFields(new List<String>{'ShippingStateCode'});
            stdController.addFields(new List<String>{'Business_Email__c'});
        }
        this.con = (Account)stdController.getRecord();
        gs = [Select Id, System_Administrator_User_ID__c FROM Global_Setting__c LIMIT 1];
        list_size = 300;
        
        availableTradeOptions = new List<SelectOption>();
        for (Coverage_Item__c ci : [SELECT Id, Trade__c FROM Coverage_Item__c  WHERE Contractor__c = :con.Id ORDER BY Trade__c]) {
            avlTrades.add(ci.Trade__c);
        }
        
        for (String trade : avlTrades) {
            availableTradeOptions.add(new SelectOption(trade,trade));
        }
        selectedTrade = availableTradeOptions[0].getLabel();
        lastSelectedTrade = availableTradeOptions[0].getLabel();

        filterZip = '';
        filterTravelFee = '';
        filterCity = '';
        filterCounty = '';
        filterState = '';
        filterCreatedDate = new Coverage_Area__c();
        filterLastModifiedDate = new Coverage_Area__c();
        filterSetExpiryDate = new Coverage_Area__c();
        
        step = 'main';
        
        getCoverageAreaListNoFilter();
    }
    
    public void pageSwitcher() {
        system.debug('pageSwitcher');
        system.debug('step: ' + step);
        
        successMsg = '';
        errorMsg = '';
        
        searchState = '';
        searchCounty = '';
        searchCity = '';
        searchRadius = '50';
        searchZip = '';
        
        selectedTrades = new String[]{};
            
            counter=0;
        total_size = 0;
        distanceLast = 0;
        
        if (step == 'expireZips') {
            clearZips();
        }

        if (step == 'addZips' || step == 'expireZips') {
            getCASList();
            filterSetExpiryDate = new Coverage_Area__c();
        } else if (step == 'copyZips') {
            //total_size = 0;
            copyTradeFrom = availableTradeOptions[0].getLabel();
            copyTradeTo = availableTradeOptions[0].getLabel();
        }
        
    }
    
    public PageReference backToMain() {
        system.debug('goToMain');
        successMsg = '';
        errorMsg = '';
        if (step == 'addZips') {
            updateCAS();
        } else { //expire
            clearZips();
        }
        step = 'main';
        
        PageReference mainPage = Page.CoverageAreaManager;
        mainPage.setRedirect(true);
        mainPage.getParameters().put('id',con.Id);
        return mainPage;
    }
    
    public void searchZips() {
        system.debug('searchZips');
        successMsg = '';
        errorMsg = '';
        
        try {
            system.debug('step: ' + step);
            
            String searchQry = 'SELECT Id, Name, City__c, County__c, State__c, UID__c, DISTANCE(Geolocation__c, GEOLOCATION('+con.ShippingLatitude+','+con.ShippingLongitude+'), \'mi\') distance FROM Zip_Code__c WHERE ';
            searchState = searchState.trim();
            searchCounty = searchCounty.trim();
            searchCity = searchCity.trim();
            //searchRadius = searchRadius.trim();
            searchZip = searchZip.trim();
            
            if (searchState == '' && searchCounty == '' && searchCity == '' && searchZip == '') {
                errorMsg = 'Please enter a State, County, City or Zip for searching';
                return;
            }
                        
            if (searchState != '') {
                searchQry+= 'State__c = \'' + searchState + '\' AND ';
            }
            
            if (searchCounty != '') {
                searchQry+= 'County__c = \'' + searchCounty + '\' AND ';
            }
            
            if (searchCity != '') {
                searchQry+= 'City__c = \'' + searchCity + '\' AND ';
            }
            
            if (searchZip != '') {
                Zip_Code__c[] zipCodeRadius = [SELECT Id, Name, Geolocation__c, Geolocation__Latitude__s, Geolocation__Longitude__s FROM Zip_Code__c WHERE Name = :searchZip LIMIT 1];
                if (zipCodeRadius.isEmpty()) {
                    return;
                }
                searchQry+= '(Name = \'' + searchZip + '\' OR DISTANCE(Geolocation__c, GEOLOCATION('+zipCodeRadius[0].Geolocation__Latitude__s+','+zipCodeRadius[0].Geolocation__Longitude__s+'), \'mi\') < '+searchRadius + ') AND ';
            }
                        
            if (step == 'expireZips') {
                searchQry+= 'Name IN:zipSet AND ';
            }
            searchQry = searchQry.left(searchQry.length()-4);
            searchQry+= 'ORDER BY DISTANCE(Geolocation__c, GEOLOCATION('+con.ShippingLatitude+','+con.ShippingLongitude+'), \'mi\') ASC';
            
            system.debug('searchQry: ' + searchQry);
            
            Set<String> zipSet = existingZipsToTrades.keySet();
            //check against existing Coverage_Area_Staging__c
            List<Coverage_Area_Staging__c> casList = new List<Coverage_Area_Staging__c>();
            for (Zip_Code__c zc : Database.query(searchQry)) {
                Coverage_Area_Staging__c cas = new Coverage_Area_Staging__c(
                    Contractor__c = con.Id, 
                    Zip_Code__c = zc.Id, 
                    UID__c = con.Id + String.valueOf(zc.Id)+step,
                    Type__c = step
                );
                casList.add(cas);
            }
            if (!casList.isEmpty()) {
                UPSERT casList UID__c;
                total_size = NULL;
                distanceLast = 0;
                getCASList();
            } else {
                errorMsg = 'No Results';
            }
            
        } catch (Exception ex) {
            if(ex.getMessage().contains('CANNOT_INSERT_UPDATE_ACTIVATE_ENTITY')) {
                errorMsg ='An error occurred while recording to Database your Transaction data. Please contact your Administrator';
            } else {
                if ( String.isNotBlank( ex.getMessage()) && ex.getMessage().contains( 'error:' ) ) {
                    errorMsg = ex.getMessage().split('error:')[1].split(':')[0] + '.';
                } else {
                    errorMsg = ex.getMessage();  
                }
            }
        }
    }
    
    public void addZipsToTrades() {
        system.debug('addZipsToTrades');
        successMsg = '';
        errorMsg = '';
        
        try {            
            if (selectedTrades == NULL || selectedTrades.isEmpty()) {
                system.debug('selectedTrades: ' + selectedTrades);
                system.debug('selectedTrades.isEmpty(): ' + selectedTrades.isEmpty());
                errorMsg = 'Please select at least one Trade.';
                return;
            }
            
            List<Coverage_Area__c> caList = new List<Coverage_Area__c>();
            List<Coverage_Area_Staging__c> casList = new List<Coverage_Area_Staging__c>();
            for (CovAreaStagingWrapper caw : casWrapList) {
                if (caw.selected == TRUE) {
                    for (String trade : selectedTrades) {
                        Coverage_Area__c ca = new Coverage_Area__c(
                            Contractor__c = caw.cas.Contractor__c, 
                            Zip_Code__c = caw.cas.Zip_Code__c,
                            Travel_Fee__c = caw.cas.Travel_Fee__c,
                            Expiry_Date__c = caw.cas.Expiry_Date__c,
                            Trade__c = trade
                        );
                        ca.UID__c = String.valueOf(ca.Zip_Code__c) + ca.Contractor__c + ca.Trade__c;
                        
                        if (ca.Expiry_Date__c > Date.today() || ca.Expiry_Date__c == NULL) {
                            ca.Active__c = TRUE;
                        } else {
                            ca.Active__c = FALSE;
                        }
                        
                        caList.add(ca);
                    }
                    casList.add(caw.cas);
                }
            }
            
            if (!caList.isEmpty()) {
                UPSERT caList UID__c;
                DELETE casList;
                successMsg = 'Zip Codes added and saved to Contractor.';
                step = 'main';
                selectedTrade = availableTradeOptions[0].getLabel();
                getCoverageAreaListNoFilter();
            } else {
            	errorMsg = 'No Zip Codes selected. Please select at least one Zip Code.';
                return;
            }
            
        } catch (Exception ex) {
            if(ex.getMessage().contains('CANNOT_INSERT_UPDATE_ACTIVATE_ENTITY')) {
                errorMsg ='An error occurred while recording to Database your Transaction data. Please contact your Administrator';
            } else {
                if ( String.isNotBlank( ex.getMessage()) && ex.getMessage().contains( 'error:' ) ) {
                    errorMsg = ex.getMessage().split('error:')[1].split(':')[0] + '.';
                } else {
                    errorMsg = ex.getMessage();  
                }
            }
        }
    }
    
    public void expireZipsToTrades() {
        system.debug('expireZipsToTrades');
        system.debug('filterSetExpiryDate.Expiry_Date__c: ' + filterSetExpiryDate.Expiry_Date__c);
        
        successMsg = '';
        errorMsg = '';
        
        try {
            
            if (selectedTrades == NULL || selectedTrades.isEmpty()) {
                errorMsg = 'Please select at least one Trade.';
                return;
            }

            Set<String> selZips = new Set<String>();
            /*for (ZipCodeWrapper zcw : zcWrapList) {
                if (zcw.selected) {
                    selZips.add(zcw.zc.Name);
                }                
            }*/
            
            for (CovAreaStagingWrapper caw : casWrapList) {
                if (caw.selected == TRUE) {
                    selZips.add(caw.cas.Zip_Code__r.Name);
                }
            }
            
            if (selZips.isEmpty()) {
                errorMsg = 'Please select at least one Zip Code.';
                return;
            }

            String searchQry = 'SELECT Id, Contractor__c, Trade__c, Expiry_Date__c, Distance_Miles__c, UID__c, Zip_Code__r.Name, Zip_Code__r.City__c, Zip_Code__r.County__c, Zip_Code__r.State__c FROM Coverage_Area__c WHERE Contractor__c = \''+con.Id+'\' AND ';
            searchQry+= ' Trade__c IN :selectedTrades AND Zip_Code__r.Name IN :selZips ORDER BY Zip_Code__r.Name';

            List<Coverage_Area__c> caList = Database.query(searchQry);
            for (Coverage_Area__c ca : caList) {
                ca.Expiry_Date__c = filterSetExpiryDate.Expiry_Date__c;
                if (ca.Expiry_Date__c > Date.today() || ca.Expiry_Date__c == NULL) {
                    ca.Active__c = TRUE;
                } else {
                    ca.Active__c = FALSE;
                }
            }
            
            if (!caList.isEmpty()) {
                system.debug('upsert expired zipes');
                system.debug('caList: ' + caList);
                UPSERT caList UID__c;
                successMsg = 'Zip Codes Expired for Trades where exists.';
                clearZips();
                step = 'main';
                selectedTrade = availableTradeOptions[0].getLabel();
                getCoverageAreaListNoFilter();
            } else {
                errorMsg = 'No matching Zip Codes found for Trades selected to expire.';
            }
            
        } catch (Exception ex) {
            if(ex.getMessage().contains('CANNOT_INSERT_UPDATE_ACTIVATE_ENTITY')) {
                errorMsg ='An error occurred while recording to Database your Transaction data. Please contact your Administrator';
            } else {
                if ( String.isNotBlank( ex.getMessage()) && ex.getMessage().contains( 'error:' ) ) {
                    errorMsg = ex.getMessage().split('error:')[1].split(':')[0] + '.';
                } else {
                    errorMsg = ex.getMessage();  
                }
            }
        }
    }
    
    public void copyZipsToTrades() {
        system.debug('copyZipsToTrades');
        successMsg = '';
        errorMsg = '';
        
        try {            
            if (String.isBlank(copyTradeFrom) || String.isBlank(copyTradeTo)) {
                errorMsg = 'Please fill out all fields.';
            } else if (copyTradeFrom == copyTradeTo) {
                errorMsg = 'Selected trades cannot be the same.';
            } 
            
            if (errorMsg != '') {
                return;
            }
            
            List<Coverage_Area__c> caList = new List<Coverage_Area__c>();
            for (Coverage_Area__c ca : [SELECT Id, Contractor__c, Zip_Code__c, Trade__c, Travel_Fee__c, Expiry_Date__c FROM Coverage_Area__c WHERE Contractor__c = :con.Id AND Trade__c = :copyTradeFrom]) {
                Coverage_Area__c caNew = new Coverage_Area__c(
                    Contractor__c = ca.Contractor__c,
                    Zip_Code__c = ca.Zip_Code__c,
                    Trade__c = copyTradeTo,
                    Travel_Fee__c = ca.Travel_Fee__c,
                    Expiry_Date__c = ca.Expiry_Date__c
                );
                caNew.UID__c = String.valueOf(caNew.Zip_Code__c) + caNew.Contractor__c + caNew.Trade__c;
                
                if (caNew.Expiry_Date__c > Date.today()) {
                    caNew.Active__c = TRUE;
                } else {
                    caNew.Active__c = FALSE;
                }
                
                caList.add(caNew);
            }
            
            if (!caList.isEmpty()) {
                UPSERT caList UID__c;
                successMsg = 'Zip Codes copied from '+ copyTradeFrom + ' to ' +copyTradeTo +'.';
                step = 'main';
                getCoverageAreaListNoFilter();
            } else {
                errorMsg = 'No matching Zip Codes found for Trades selected.';
            }
            
        } catch (Exception ex) {
            if(ex.getMessage().contains('CANNOT_INSERT_UPDATE_ACTIVATE_ENTITY')) {
                errorMsg ='An error occurred while recording to Database your Transaction data. Please contact your Administrator';
            } else {
                if ( String.isNotBlank( ex.getMessage()) && ex.getMessage().contains( 'error:' ) ) {
                    errorMsg = ex.getMessage().split('error:')[1].split(':')[0] + '.';
                } else {
                    errorMsg = ex.getMessage();  
                }
            }
        }
    }
    
    public void clearZips() {
        system.debug('clearZips');
        successMsg = '';
        errorMsg = '';
        
        DELETE [SELECT Id FROM Coverage_Area_Staging__c WHERE Contractor__c = :con.Id AND Type__c = :step];

        total_size = NULL;
        /*if (step == 'addZips') {
            getCASList();
        } */    
        
        getCASList();

    }
    
    public void selectAll() {
        system.debug('selectAll');
        successMsg = '';
        errorMsg = '';
        
        for (CovAreaStagingWrapper caw : casWrapList) {
            caw.selected = TRUE;
        }
    }
    
    public void selectNone() {
        system.debug('selectNone');
        successMsg = '';
        errorMsg = '';
        
        for (CovAreaStagingWrapper caw : casWrapList) {
            caw.selected = FALSE;
        }
    }
    
    public PageReference saveData() {
        system.debug('saveData');
        successMsg = '';
        errorMsg = '';
        
        try {
            List<Coverage_Area__c> caList = new List<Coverage_Area__c>();
            for (Coverage_Area__c ca : tradeCoverageArea) {
                if (ca.Expiry_Date__c > Date.today() || ca.Expiry_Date__c == NULL) {
                    ca.Active__c = TRUE;
                } else {
                    ca.Active__c = FALSE;
                }
                caList.add(ca);
            }
                        
            UPDATE caList;
            successMsg = 'Zip Codes updated.';
            
            /*PageReference redirect = new PageReference('/'+con.Id);
            redirect.setRedirect(true);
            return redirect;*/
        } catch (Exception ex) {
            if(ex.getMessage().contains('CANNOT_INSERT_UPDATE_ACTIVATE_ENTITY')) {
                errorMsg ='An error occurred while recording to Database your Transaction data. Please contact your Administrator';
            } else {
                if ( String.isNotBlank( ex.getMessage()) && ex.getMessage().contains( 'error:' ) ) {
                    errorMsg = ex.getMessage().split('error:')[1].split(':')[0] + '.';
                } else {
                    errorMsg = ex.getMessage();  
                }
            }
        }
        
        return NULL;
    }
    
    public void updateCAS() {
        system.debug('updateCAS');
        successMsg = '';
        errorMsg = '';
        
        try {
            List<Coverage_Area_Staging__c> casList = new List<Coverage_Area_Staging__c>();
            for (CovAreaStagingWrapper caw : casWrapList) {
                casList.add(caw.cas);
            }
            UPSERT casList UID__c;
        } catch (Exception ex) {
            if(ex.getMessage().contains('CANNOT_INSERT_UPDATE_ACTIVATE_ENTITY')) {
                errorMsg ='An error occurred while recording to Database your Transaction data. Please contact your Administrator';
            } else {
                if ( String.isNotBlank( ex.getMessage()) && ex.getMessage().contains( 'error:' ) ) {
                    errorMsg = ex.getMessage().split('error:')[1].split(':')[0] + '.';
                } else {
                    errorMsg = ex.getMessage();  
                }
            }
        }
    }
    
    public PageReference casPDF() {
        system.debug('casPDF');
        successMsg = '';
        errorMsg = '';
        
        updateCAS();
        PageReference redirect = Page.CoverageAreaPDF;
        redirect.setRedirect(true);
        redirect.getParameters().put('id',con.Id);
        redirect.getParameters().put('cas','Y');
        return redirect;
    }
    
    public void getCoverageAreaListFilter() {
        filterApplied = TRUE;
        counter = 0;
        distanceLast = 0;
        getCoverageAreaList();
    }
    
    public void getCoverageAreaListNoFilter() {
        filterApplied = FALSE;
        counter = 0;
        distanceLast = 0;
        filterZip = '';
        filterTravelFee = '';
        filterCity = '';
        filterCounty = '';
        filterState = '';
        filterCreatedDate = new Coverage_Area__c();
        filterLastModifiedDate = new Coverage_Area__c();
        filterSetExpiryDate = new Coverage_Area__c();
        getCoverageAreaList();
    }
    
    private void getCoverageAreaList() {
        system.debug('getCoverageAreaList');
        getCAList();
            
        if (tradeCoverageArea != NULL) {
            for (Coverage_Area__c ca : tradeCoverageArea) {
                tradeCoverageMap.get(lastSelectedTrade).put(ca.UID__c, ca);
            }
        }
       
        List<Coverage_Area__c> caList = new List<Coverage_Area__c>();
        
        if (filterApplied) {
            for (Coverage_Area__c ca : tradeCoverageMap.get(selectedTrade).values()) {
				Boolean pass = FALSE;
                
                if (filterZip != '') {
                    system.debug('filterZip: ' + filterZip);
                    if (filterZip == ca.Zip_Code__r.Name) {
                        pass = TRUE;
                    } else {
                        pass = FALSE;
                        continue;
                    }
                }
                
                if (filterTravelFee != '') {
                    system.debug('filterTravelFee: ' + filterTravelFee);
                    if (Decimal.valueOf(filterTravelFee) == ca.Travel_Fee__c) {
                        pass = TRUE;
                    } else {
                        pass = FALSE;
                        continue;
                    }
                }
                
                if (filterCity != '') {
                    system.debug('filterCity: ' + filterCity);
                    if (filterCity == ca.Zip_Code__r.City__c) {
                        pass = TRUE;
                    } else {
                        pass = FALSE;
                        continue;
                    }
                }
                
                if (filterCounty != '') {
                    system.debug('filterCounty: ' + filterCounty);
                    if (filterCounty == ca.Zip_Code__r.County__c) {
                        pass = TRUE;
                    } else {
                        pass = FALSE;
                        continue;
                    }
                }
                
                if (filterState != '') {
                    system.debug('filterState: ' + filterState);
                    if (filterState == ca.Zip_Code__r.State__c) {
                        pass = TRUE;
                    } else {
                        pass = FALSE;
                        continue;
                    }
                }
                
                if (filterCreatedDate.Expiry_Date__c != NULL) {
                    system.debug('filterCreatedDate.Expiry_Date__c: ' + filterCreatedDate.Expiry_Date__c);
                    Date cd = ca.CreatedDate.date();
                    if (filterCreatedDate.Expiry_Date__c == cd) {
                        pass = TRUE;
                    } else {
                        pass = FALSE;
                        continue;
                    }
                }
                
                if (filterLastModifiedDate.Expiry_Date__c != NULL) {
                    system.debug('filterLastModifiedDate.Expiry_Date__c: ' + filterLastModifiedDate.Expiry_Date__c);
                    Date lmd = ca.LastModifiedDate.date();
                    if (filterLastModifiedDate.Expiry_Date__c == lmd) {
                        pass = TRUE;
                    } else {
                        pass = FALSE;
                        continue;
                    }
                }
                
                if (filterSetExpiryDate.Expiry_Date__c != NULL) {
                    system.debug('filterSetExpiryDate.Expiry_Date__c: ' + filterSetExpiryDate.Expiry_Date__c);
                    if (filterSetExpiryDate.Expiry_Date__c == ca.Expiry_Date__c) {
                        pass = TRUE;
                    } else {
                        pass = FALSE;
                        continue;
                    }
                }
                
                if (pass) {
                    caList.add(ca);
                }
            }
            
        } else {
            caList = tradeCoverageMap.get(selectedTrade).values();
        }
        
        //only get max list number
        if (btn == 'previous') { //resort for prev
            List<Coverage_Area__c> sortList = caList;
            caList = new List<Coverage_Area__c>();
            
            for(Integer i = sortList.size()-1; i>=0;i--) {
                caList.add(sortList.get(i));
            }
        } 
        
        Integer j = 0;
        for (Coverage_Area__c ca: caList) { //get the starting position
            system.debug('ca.Distance_Miles__c: ' + ca.Distance_Miles__c);
            if (btn == 'previous') {
                if (ca.Distance_Miles__c < distanceFirst) {
                    break;
                }
            } else {
                if (ca.Distance_Miles__c > distanceLast) {
                    break;
                }
            }
            j++;
        }
        
        tradeCoverageArea = new List<Coverage_Area__c>();
        for (Integer i = 0; i < list_size && j < caList.size(); i++) {
            tradeCoverageArea.add(caList[j]);
            j++;
        }
        
        if (!tradeCoverageArea.isEmpty()) {
            if (btn == 'previous') {
                distanceFirst = tradeCoverageArea[tradeCoverageArea.size()-1].Distance_Miles__c;
                distanceLast = tradeCoverageArea[0].Distance_Miles__c;
            } else {
                distanceFirst = tradeCoverageArea[0].Distance_Miles__c;
                distanceLast = tradeCoverageArea[tradeCoverageArea.size()-1].Distance_Miles__c;
            }
        }
        
        total_size = caList.size();

        lastSelectedTrade = selectedTrade;
    }
    
    private void getCAList() {
        system.debug('getCAList');
        
        existingZipsToTrades = new Map<String,Set<String>>();
        tradeCoverageMap = new Map<String,Map<String,Coverage_Area__c>>();
        for (String trade : avlTrades) {
            tradeCoverageMap.put(trade, new Map<String,Coverage_Area__c>());
        }
        
        system.debug('tradeCoverageMap: ' + tradeCoverageMap);
        
        for (Coverage_Area__c ca : [SELECT Id, Contractor__c, Trade__c, Trade_Acronym__c, Travel_Fee__c, Expiry_Date__c, Distance_Miles__c, UID__c, Zip_Code__r.Name, Zip_Code__r.City__c, Zip_Code__r.County__c, Zip_Code__r.State__c, Zip_Code__r.UID__c, CreatedDate, LastModifiedDate
                                    FROM Coverage_Area__c 
                                    WHERE Contractor__c = :con.Id ORDER BY Trade__c, Distance_Miles__c]) {
                                        if (existingZipsToTrades.get(ca.Zip_Code__r.Name) == NULL) {
                                            existingZipsToTrades.put(ca.Zip_Code__r.Name, new Set<String>{ca.Trade_Acronym__c});
                                        } else {
                                            existingZipsToTrades.get(ca.Zip_Code__r.Name).add(ca.Trade_Acronym__c);
                                        }
                                        
                                        Map<String,Coverage_Area__c> caMap = tradeCoverageMap.get(ca.Trade__c);
                                        caMap.put(ca.UID__c, ca);
                                        
                                        tradeCoverageMap.put(ca.Trade__c, caMap);
                                    }
        
        tradeCoverageArea = new List<Coverage_Area__c>();

        //getCoverageAreaListNoFilter();
    }
    
    private void getCASList() {
        system.debug('getCASList');
        
        Set<String> zipSet = existingZipsToTrades.keySet();

        //set total size
        if (total_size == NULL) {
            String queryString = 'SELECT count(Id) c FROM Coverage_Area_Staging__c WHERE Contractor__c = \''+con.Id+'\' AND Type__c = \''+step+'\'';
            system.debug('queryString1: ' + queryString);
            List<AggregateResult> ar = new List<AggregateResult>();
            ar = Database.query(queryString);
            for(AggregateResult a : ar){
                total_size = Integer.valueof(a.get('c')); //set the total size in the constructor
            }
        }
        
        //get full list
        casWrapList = new List<CovAreaStagingWrapper>();
        String queryString = 'SELECT Id, Contractor__c, Distance_Miles__c, Travel_Fee__c, Expiry_Date__c, UID__c, Zip_Code__c, Zip_Code__r.Name, Zip_Code__r.City__c, Zip_Code__r.County__c, Zip_Code__r.State__c FROM Coverage_Area_Staging__c WHERE Contractor__c = \''+con.Id+'\' AND Type__c = \''+step+'\' AND ';
        
        if (btn == 'previous') {
            queryString+= 'Distance_Miles__c < '+distanceFirst+' ORDER BY Distance_Miles__c DESC LIMIT '+list_size;
        } else if (distanceLast == 0) {
            queryString+= 'Distance_Miles__c >= '+distanceLast+' ORDER BY Distance_Miles__c ASC LIMIT '+list_size;
        } else {
            queryString+= 'Distance_Miles__c > '+distanceLast+' ORDER BY Distance_Miles__c ASC LIMIT '+list_size;
        }
        
        distanceFirst = NULL;
        system.debug('queryString2: ' + queryString);
        for (Coverage_Area_Staging__c cas : Database.query(queryString)) {
            casWrapList.add(new CovAreaStagingWrapper(cas, existingZipsToTrades.get(cas.Zip_Code__r.Name)));
        }
        system.debug('casWrapList: ' + casWrapList);
        casWrapList.sort();
        
        if (!casWrapList.isEmpty()) {
            distanceFirst = casWrapList[0].cas.Distance_Miles__c;
            distanceLast = casWrapList[casWrapList.size()-1].cas.Distance_Miles__c;
        }
    }
    
    public void emailList() {
        system.debug('emailList');
        successMsg = '';
        errorMsg = '';
        
        system.debug('emailType: ' + emailType);
        system.debug('con.Business_Email__c: ' + con.Business_Email__c);
        
        List<String> emailIds = new List<String>{con.Business_Email__c};
            
            PageReference pdf = page.CoverageAreaPDF;
        pdf.getParameters().put('id',con.Id);
        
        Id templateId = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'Coverage_Area' AND Folder.Name = 'Contractor Quick Email' LIMIT 1].Id;
        if (emailType == 'trade') {
            pdf.getParameters().put('qryType',selectedTrade);
        } else if (emailType == 'cas') {
            pdf.getParameters().put('cas','Y');
        } 
        
        Blob b;
        if(Test.isRunningTest()) { 
            b = blob.valueOf('Test');
        } else {
            b = pdf.getContentAsPDF();
        }
        
        Messaging.SingleEmailMessage eml = new Messaging.SingleEmailMessage();
        
        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        efa.setFileName('Coverage Area.pdf');
        efa.setBody(b);
        
        Id contactId = [SELECT Id FROM Contact WHERE AccountId = :con.Id LIMIT 1].Id;
        
        String addresses;
        eml.setToAddresses(emailIds);
        eml.setTargetObjectId(contactId);
        eml.setWhatId(con.Id);
        eml.setTemplateId(templateId);
        eml.setSaveAsActivity(false);
        eml.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
        Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {eml});
        if (r[0].isSuccess()) {
            successMsg = 'PDF Sent to Contractor';
            Task emlTask = new Task(
                OwnerId = UserInfo.getUserId(),
                Subject = 'Email: ' + eml.getSubject(),
                WhatId = eml.getWhatId(),
                ActivityDate = Date.today(),
                Description = eml.getPlainTextBody(),
                Status = 'Completed',
                TaskSubtype = 'Email'
            );
            INSERT emlTask;
            
            Attachment att = new Attachment();
            att.ParentId   = emlTask.Id;
            att.body       = b;
            att.Name       = 'Coverage Area.pdf';
            INSERT att;
        } else {
            errorMsg = String.join(r[0].getErrors(), ',');
        }
    }
    
    /*public class ZipCodeWrapper implements comparable{
        public Zip_Code__c zc {get;set;}
        public Boolean selected {get;set;}
        public Decimal distance {get;set;}
        public String trades {get;set;}
        public ZipCodeWrapper(Zip_Code__c zc, Set<String> trades) {
            this.zc = zc;
            this.distance = (Decimal)zc.get('distance');
            this.trades = String.join(new List<String>(trades), ',');
        } public Integer compareTo(Object compareTo) {
            ZipCodeWrapper compareToRec = (ZipCodeWrapper)compareTo;
            if (distance == compareToRec.distance) {
                return 0;
            }
            if (distance > compareToRec.distance) {
                return 1;
            }
            return -1;
        }
    }*/
    
    public class CovAreaStagingWrapper implements comparable{
        public Coverage_Area_Staging__c cas {get;set;}
        public Boolean selected {get;set;}
        public Decimal distance {get;set;}
        public String trades {get;set;}
        public CovAreaStagingWrapper(Coverage_Area_Staging__c cas, Set<String> trades) {
            this.cas = cas;
            this.distance = cas.Distance_Miles__c;
            this.trades = String.join(new List<String>(trades), ',');
        } public Integer compareTo(Object compareTo) {
            CovAreaStagingWrapper compareToRec = (CovAreaStagingWrapper)compareTo;
            if (distance == compareToRec.distance) { //closest
                return 0;
            }
            if (distance > compareToRec.distance) {
                return 1;
            }
            return -1;
        }
    }
    
    
    /*pagnation*/
    public PageReference Beginning() { //user clicked beginning
        counter = 0;
        distanceLast = 0;
        btn = 'beginning';
        
        if (step == 'main') {
            getCoverageAreaList();
        } else {
            updateCAS();
            getCASList();
        }
        
        return null;
    }
    
    public PageReference Previous() { //user clicked previous button
        counter -= list_size;
        btn = 'previous';
        
        if (step == 'main') {
            getCoverageAreaList();
        } else {
            updateCAS();
            getCASList();
        }
        
        return null;
    }
    
    public PageReference Next() { //user clicked next button
        counter += list_size;
        btn = 'next';
        
        if (step == 'main') {
            getCoverageAreaList();
        } else {
            updateCAS();
            getCASList();
        }
        
        return null;
    }
    
    /*public PageReference End() { //user clicked end
        counter = total_size - math.mod(total_size, list_size);
        btn = 'end';
        
        if (step == 'main') {
            getCoverageAreaList();
        } else {
            updateCAS();
            getCASList();
        }
        
        return null;
    }*/
    
    public Boolean getDisablePrevious() { 
        //this will disable the previous and beginning buttons
        if (counter>0) return false; else return true;
    }
    
    public Boolean getDisableNext() { //this will disable the next and end buttons
        if (counter + list_size < total_size) return false; else return true;
    }
    
    public Integer getTotal_size() {
        return total_size;
    }
    
    public Integer getPageNumber() {
        return counter/list_size + 1;
    }
    
    public Integer getTotalPages() {
        if (math.mod(total_size, list_size) > 0) {
            return total_size/list_size + 1;
        } else {
            return (total_size/list_size);
        }
    }
    
    
}