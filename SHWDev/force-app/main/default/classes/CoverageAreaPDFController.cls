public class CoverageAreaPDFController {
    private final Account con {get;set;}
    
    public Map<String,List<Coverage_Area__c>> tradeCoverageMap  {get;set;}
    public List<Coverage_Area_Staging__c> coverageStagingList  {get;set;}
    public String cas {get;set;}

    public CoverageAreaPDFController(ApexPages.StandardController stdController) {
        this.con = (Account)stdController.getRecord();
        cas = apexpages.currentpage().getparameters().get('cas');

        String qryType = apexpages.currentpage().getparameters().get('qryType');
        String qryString = 'SELECT Id, Contractor__c, Trade__c, Expiry_Date__c, Distance_Miles__c, Travel_Fee__c, UID__c, Zip_Code__r.Name, Zip_Code__r.AreaCode__c, Zip_Code__r.City__c, Zip_Code__r.County__c, Zip_Code__r.State__c FROM Coverage_Area__c WHERE Contractor__c = \''+con.Id+'\' AND Active__c = TRUE ';
        if (qryType != NULL) {
            qryString+= 'AND Trade__c = \''+qryType+'\' ';
        } 
        qryString+= 'ORDER BY Trade__c, Distance_Miles__c ASC';
        
        if (cas == NULL) {
            tradeCoverageMap = new Map<String,List<Coverage_Area__c>>();
            for (Coverage_Area__c ca : Database.query(qryString)) {
                if (tradeCoverageMap.get(ca.Trade__c)==NULL) {
                    tradeCoverageMap.put(ca.Trade__c, new List<Coverage_Area__c>{ca});
                } else {
                    tradeCoverageMap.get(ca.Trade__c).add(ca);
                }
            }
        } else {
            coverageStagingList = new List<Coverage_Area_Staging__c>();
            coverageStagingList = [SELECT Id, Contractor__c, Expiry_Date__c, Distance_Miles__c, Travel_Fee__c, Zip_Code__r.Name, Zip_Code__r.AreaCode__c, Zip_Code__r.City__c, Zip_Code__r.County__c, Zip_Code__r.State__c FROM Coverage_Area_Staging__c WHERE Contractor__c = :+con.Id AND Active__c = TRUE AND Type__c = 'addZips' ORDER BY Distance_Miles__c ASC];
        }
        
    }
}