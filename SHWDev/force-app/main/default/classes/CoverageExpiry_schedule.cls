global class CoverageExpiry_schedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        CoverageItemExpiry_batch batchRun = new CoverageItemExpiry_batch(); 
        ID batchId = Database.executeBatch(batchRun, 1000);
    }
}