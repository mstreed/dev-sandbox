global class CoverageItemExpiry_batch implements Database.Batchable<SObject> {
    global Database.QueryLocator start(Database.BatchableContext Bc){
        return Database.getQueryLocator([SELECT Id, Expiry_Date__c FROM Coverage_Item__c WHERE Expiry_Date__c <= :Date.today() AND Active__c = TRUE]);
    }
    
    Boolean keepGoing = FALSE;

    global void execute(Database.BatchableContext Bc, List<Coverage_Item__c> scope){
        Savepoint sp = Database.setSavepoint();
        
        try {            
            for (Coverage_Item__c rec : scope){
                rec.Active__c = FALSE;
            }
            
            UPDATE scope;
            
        } catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
            Database.rollback(sp);
        }
        DispatchTriggerHandler.enableTriggers();
    }

    global void finish(Database.BatchableContext Bc){
        ID batchId = Database.executeBatch(new CoverageAreaExpiry_batch(), 1000);
    }

}