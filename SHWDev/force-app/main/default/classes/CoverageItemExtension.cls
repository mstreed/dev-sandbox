public class CoverageItemExtension {
    public String errorMsg {get;set;}
    public String successMsg {get;set;}

    public String step {get;set;}
    public String selType {get;set;}
    public Coverage_Item__c massApply {get;set;} //record used to mass apply

    private final Account con {get;set;}
    public List<CovItemWrapper> conItemWrapList {get;set;}
    private Set<String> existingCovItems;
    public Map<String,List<SelectOption>> tradeItemOptions {get;set;}
    public Map<String,String[]> tradeItemSelected {get;set;}
    
    public Global_Setting__c gs {get;set;}

    public CoverageItemExtension(ApexPages.StandardController stdController) {
        if (!Test.isRunningTest()) {
            stdController.addFields(new List<String>{'ShippingLatitude'});
            stdController.addFields(new List<String>{'ShippingLongitude'});
            stdController.addFields(new List<String>{'ShippingStateCode'});
        }
        this.con = (Account)stdController.getRecord();
        gs = [Select Id, System_Administrator_User_ID__c FROM Global_Setting__c LIMIT 1];

        getCoverageItems();

        step = 'edit';
        selType = '';
        
        massApply = new Coverage_Item__c();
    }
    
    public void add() {
        system.debug('add');
        successMsg = '';
        errorMsg = '';
        
        Map<String,List<String>> dependentPicklistValuesMap = Util.getDependentPicklistValues(Coverage_Item__c.Item__c);
        tradeItemOptions = new Map<String,List<SelectOption>>();
        tradeItemSelected = new Map<String,String[]>();
        for (String trade : dependentPicklistValuesMap.keySet()) {
            List<SelectOption> items = new List<SelectOption>();
            for (String item : dependentPicklistValuesMap.get(trade)) {
                String itemKey = con.Id + trade + item;
                if (!existingCovItems.contains(itemKey)) {
                    items.add(new SelectOption(item,item));
                }
            }
            if (!items.isEmpty()) {
                tradeItemOptions.put(trade, items);
                tradeItemSelected.put(trade, new String[]{});
            }
        }
        
        step = 'add';
    }
    
    public void addNewCoverageItems() {
        system.debug('addNewCoverageItems');
        successMsg = '';
        errorMsg = '';
        
        try {
            
            List<Coverage_Item__c> ciList = new List<Coverage_Item__c>();
            //add new ci to list
            for (String trade : tradeItemSelected.keySet()) {
                for (String item : tradeItemSelected.get(trade)) {
                    Coverage_Item__c ci = new Coverage_Item__c(
                        Contractor__c = con.Id,
                        Trade__c = trade,
                        Item__c = item,
                        UID__c = con.Id+trade+item
                    );
                    ciList.add(ci);
                }
            }
            
            if (!ciList.isEmpty()) {
                UPSERT ciList UID__c;
                step = 'edit';
                successMsg = 'Coverage Items added and saved to Contractor.';
            } else {
                errorMsg = 'Please select at least one Item to add.';
                return;
            }
            
            getCoverageItems();
        } catch (Exception ex) {
            if(ex.getMessage().contains('CANNOT_INSERT_UPDATE_ACTIVATE_ENTITY')) {
                errorMsg ='An error ociurred while recording to Database your Transaction data. Please contact your Administrator';
            } else {
                if ( String.isNotBlank( ex.getMessage()) && ex.getMessage().contains( 'error:' ) ) {
                    errorMsg = ex.getMessage().split('error:')[1].split(':')[0] + '.';
                } else {
                    errorMsg = ex.getMessage();  
                }
            }
        }
    }
    
    public void getCoverageItems() {
        system.debug('getCoverageItems');
        successMsg = '';
        errorMsg = '';
        
        try {
            conItemWrapList = new List<CovItemWrapper>();
            existingCovItems = new Set<String>();
            
            //get existing coverage items
            for (Coverage_Item__c ci : [SELECT Id, Active__c, Contractor__c, Expiry_Date__c, Trade__c, Item__c, UID__c, New__c, 
                                        Install_SHW_Parts__c, Install_SHW_Equipment__c, Service_Call_Fee__c, Hourly_Labor_Rate__c,
                                        Item_Pre_Approval__c, CreatedDate
                                        FROM Coverage_Item__c WHERE Contractor__c = :con.Id ORDER BY Trade__c, Item__c]) {
                                            existingCovItems.add(ci.UID__c);
                                            conItemWrapList.add(new CovItemWrapper(ci));
                                        }
                    
            conItemWrapList.sort();
        } catch (Exception ex) {
            if(ex.getMessage().contains('CANNOT_INSERT_UPDATE_ACTIVATE_ENTITY')) {
                errorMsg ='An error ociurred while recording to Database your Transaction data. Please contact your Administrator';
            } else {
                if ( String.isNotBlank( ex.getMessage()) && ex.getMessage().contains( 'error:' ) ) {
                    errorMsg = ex.getMessage().split('error:')[1].split(':')[0] + '.';
                } else {
                    errorMsg = ex.getMessage();  
                }
            }
        }
    }
    
    
    public PageReference saveCoverageItems() {
        system.debug('saveCoverageItems');
        successMsg = '';
        errorMsg = '';
        
        try {
            List<Coverage_Item__c> ciList = new List<Coverage_Item__c>();
            for (CovItemWrapper cai : conItemWrapList) {
                if (cai.record.Expiry_Date__c > Date.today() || cai.record.Expiry_Date__c == NULL) {
                    cai.record.Active__c = TRUE;
                } else {
                    cai.record.Active__c = FALSE;
                }
                ciList.add(cai.record);
            }
            
            UPSERT ciList UID__c;
            successMsg = 'Coverage Items updated.';

            /*PageReference redirect = new PageReference('/'+con.Id);
            redirect.setRedirect(true);
            return redirect;*/
        } catch (Exception ex) {
            if(ex.getMessage().contains('CANNOT_INSERT_UPDATE_ACTIVATE_ENTITY')) {
                errorMsg ='An error ociurred while recording to Database your Transaction data. Please contact your Administrator';
            } else {
                if ( String.isNotBlank( ex.getMessage()) && ex.getMessage().contains( 'error:' ) ) {
                    errorMsg = ex.getMessage().split('error:')[1].split(':')[0] + '.';
                } else {
                    errorMsg = ex.getMessage();  
                }
            }
        }
        return NULL;   
    }
    
    public void back() {
        system.debug('back');
        successMsg = '';
        errorMsg = '';
        step = 'edit';
    }
    
    public void selectAll() {
        system.debug('selectAll');
        for (CovItemWrapper cai : conItemWrapList) {
            if (selType == 'installParts') {
                cai.record.Install_SHW_Parts__c = TRUE;
            } else if (selType == 'installEquipment') {
                cai.record.Install_SHW_Equipment__c = TRUE;
            } else if (selType == 'applyExpiryDate') {
                cai.record.Expiry_Date__c = massApply.Expiry_Date__c;
            } else if (selType == 'applySCF') {
                cai.record.Service_Call_Fee__c = massApply.Service_Call_Fee__c;
            } else if (selType == 'applyHourlyRate') {
                cai.record.Hourly_Labor_Rate__c = massApply.Hourly_Labor_Rate__c;
            } else if (selType == 'applyItemPreApproval') {
                cai.record.Item_Pre_Approval__c = massApply.Item_Pre_Approval__c;
            }
        }
    }
    
    public void selectNone() {
        system.debug('selectNone');
        for (CovItemWrapper cai : conItemWrapList) {
            if (selType == 'installParts') {
                cai.record.Install_SHW_Parts__c = FALSE;
            } else if (selType == 'installEquipment') {
                cai.record.Install_SHW_Equipment__c = FALSE;
            }
        }
    }
    
    public class CovItemWrapper implements comparable{
        public Coverage_Item__c record {get;set;}
        public String trade {get;set;}
        public String item {get;set;}
        
        public CovItemWrapper(Coverage_Item__c record) {
            this.record = record;
            this.trade = record.Trade__c;
            this.item = record.Item__c;
            
        } public Integer compareTo(Object compareTo) {
            CovItemWrapper compareToEmp = (CovItemWrapper)compareTo;
            if (trade == compareToEmp.trade) {
                if (item == compareToEmp.item) {
                    return -2;
                }
                if (item < compareToEmp.item) {
                    return -1;
                }
                return 1;
            }
            if (trade > compareToEmp.trade) {
                return 2;
            }
            return 0;
        }
    }
}