@isTest
private class CoverageTest {
    private static DataFactory df = new DataFactory();
    
    @isTest static void coverageItemTest() {
        df.createGlobalSetting();
        Coverage_Item__c ci = new Coverage_Item__c(
            Contractor__c = df.contractor.Id,
            Trade__c = 'Appliances',
            Item__c = 'Built-In Microwave',
            Expiry_Date__c = Date.today()
        );
        ci.UID__c = df.contractor.Id+ci.Trade__c+ci.Item__c;
        INSERT ci;

        Test.StartTest();
        PageReference pageRef = Page.CoverageItem;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',df.contractor.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(df.contractor);
        CoverageItemExtension extension = new CoverageItemExtension(controller);
        extension.add();
        extension.tradeItemSelected.put('Appliances',new String[]{'Clothes Dryer Electric'});
        extension.addNewCoverageItems();
        extension.add();
        extension.tradeItemSelected.put('Appliances',new String[]{'Clothes Dryer'});
        extension.addNewCoverageItems();
        extension.back();
        extension.saveCoverageItems();
        extension.conItemWrapList.add(new CoverageItemExtension.CovItemWrapper(ci));
        extension.saveCoverageItems();
        Test.StopTest();
    }
    
    @isTest static void coverageAreaTest() {
        df.createGlobalSetting();
        Coverage_Item__c ci = new Coverage_Item__c(
            Contractor__c = df.contractor.Id,
            Trade__c = 'Appliances',
            Item__c = 'Built-In Microwave',
            Expiry_Date__c = Date.today().addDays(2)
        );
        ci.UID__c = df.contractor.Id+ci.Trade__c+ci.Item__c;
        INSERT ci;
        
        Coverage_Item__c ci2 = new Coverage_Item__c(
            Contractor__c = df.contractor.Id,
            Trade__c = 'HVAC',
            Item__c = 'Boiler - LP Gas',
            Expiry_Date__c = Date.today().addDays(2)
        );
        ci.UID__c = df.contractor.Id+ci.Trade__c+ci.Item__c;
        INSERT ci2;
        
        Zip_Code__c zc = new Zip_Code__C(
            Name = '00100',
            City__c = df.contractor.ShippingCity, 
            State__c = df.contractor.ShippingStateCode,
            County__c = df.contractor.ShippingCity,
            AreaCode__c = '123',
            Geolocation__Latitude__s = df.contractor.ShippingLatitude,
            Geolocation__Longitude__s = df.contractor.ShippingLongitude,
            PreferredLastLineKey__c = '123',
            CityStateKey__c = '456',
            UID__c = '00100123456'
        );
        INSERT zc;
        
        Coverage_Area__c ca = new Coverage_Area__c(
            Contractor__c = df.contractor.Id, 
            Zip_Code__c = zc.Id,
            Travel_Fee__c = 1,
            Trade__c = 'Appliances',
            Active__c = TRUE,
            Expiry_Date__c = Date.today()
        );
        ca.UID__c = String.valueOf(ca.Zip_Code__c) + ca.Contractor__c + ca.Trade__c;
        INSERT ca;
        
        Coverage_Area_Staging__c cas = new Coverage_Area_Staging__c(
            Contractor__c = df.contractor.Id, 
            Zip_Code__c = zc.Id, 
            UID__c = df.contractor.Id + String.valueOf(zc.Id)+'addZips',
            Type__c = 'addZips'
        );
        INSERT cas;

        Test.StartTest();
        PageReference pageRef = Page.CoverageAreaManager;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',df.contractor.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(df.contractor);
        CoverageAreaManagerExtension extension = new CoverageAreaManagerExtension(controller);
        extension.getRadiusOptions();
        extension.getExpireOptions();
        extension.saveData();
        extension.step = 'addZips';
        extension.pageSwitcher();
        extension.addZipsToTrades();
        extension.selectedTrades.add('Appliances');
        extension.addZipsToTrades();
        extension.searchZips();
        extension.clearZips();
        extension.searchZip = zc.Name;
        extension.searchState = zc.State__c;
        extension.searchCounty = zc.County__c;
        extension.searchCity = zc.City__c;
        extension.searchZips();
        extension.casPDF();
        extension.emailType = 'cas';
        extension.emailList();
        extension.selectNone();
        extension.selectAll();
        extension.addZipsToTrades();
        extension.backToMain();
        
        extension.step = 'expireZips';
        extension.pageSwitcher();
        extension.searchZip = zc.Name;
        extension.searchZips();
        extension.expireZipsToTrades();
        extension.selectedTrades.add('Appliances');
        extension.casWrapList[0].selected = TRUE;
        extension.filterSetExpiryDate.Expiry_Date__c = Date.today();
        extension.expireZipsToTrades();
        
        extension.step = 'copyZips';
        extension.pageSwitcher();
        extension.copyZipsToTrades();
        extension.copyTradeFrom = 'Appliances';
        extension.copyTradeTo = 'Appliances';
        extension.copyZipsToTrades();
        extension.copyTradeTo = 'HVAC';
        extension.copyZipsToTrades();
        
        extension.getCoverageAreaListFilter();
        extension.filterZip = zc.Name;
        extension.getCoverageAreaListFilter();
        extension.getCoverageAreaListNoFilter();
        extension.filterTravelFee = '1';
        extension.getCoverageAreaListFilter();
        extension.getCoverageAreaListNoFilter();
        extension.filterCity = zc.City__c;
        extension.getCoverageAreaListFilter();
        extension.getCoverageAreaListNoFilter();
        extension.filterCounty = zc.County__c;
        extension.getCoverageAreaListFilter();
        extension.getCoverageAreaListNoFilter();
        extension.filterState = zc.State__c;
        extension.getCoverageAreaListFilter();
        extension.getCoverageAreaListNoFilter();
        extension.filterCreatedDate.Expiry_Date__c = Date.today();
        extension.getCoverageAreaListFilter();
        extension.getCoverageAreaListNoFilter();
        extension.filterLastModifiedDate.Expiry_Date__c = Date.today();
        extension.getCoverageAreaListFilter();
        extension.getCoverageAreaListNoFilter();
        extension.filterSetExpiryDate.Expiry_Date__c = Date.today();
        extension.getCoverageAreaListFilter();
        extension.getCoverageAreaListNoFilter();
        
        extension.Beginning();
        extension.Previous();
        extension.Next();
        extension.getDisablePrevious();
        extension.getDisableNext();
        extension.getTotal_size();
        extension.getPageNumber();
        extension.getTotalPages();
        
        PageReference pageRef2 = Page.CoverageAreaPDF;
        Test.setCurrentPage(pageRef2);
        ApexPages.currentPage().getParameters().put('id',df.contractor.id);
        ApexPages.StandardController controller2 = new ApexPages.StandardController(df.contractor);
        CoverageAreaPDFController extension2 = new CoverageAreaPDFController(controller2);
        
        PageReference pageRef3 = Page.CoverageAreaPDF;
        Test.setCurrentPage(pageRef3);
        ApexPages.currentPage().getParameters().put('id',df.contractor.id);
        ApexPages.currentPage().getParameters().put('cas','y');
        ApexPages.currentPage().getParameters().put('qryType','Appliances');
        ApexPages.StandardController controller3 = new ApexPages.StandardController(df.contractor);
        CoverageAreaPDFController extension3 = new CoverageAreaPDFController(controller3);
        
        Test.StopTest();
    }
    
    @isTest static void expiryBatchTest() {
        df.createGlobalSetting();
        Coverage_Item__c ci = new Coverage_Item__c(
            Contractor__c = df.contractor.Id,
            Trade__c = 'Appliances',
            Item__c = 'Built-In Microwave',
            Active__c = TRUE,
            Expiry_Date__c = Date.today()
        );
        ci.UID__c = df.contractor.Id+ci.Trade__c+ci.Item__c;
        INSERT ci;
        
        Zip_Code__c zc = new Zip_Code__C(
            Name = '00100',
            City__c = df.contractor.ShippingCity, 
            State__c = df.contractor.ShippingStateCode,
            County__c = df.contractor.ShippingCity,
            AreaCode__c = '123',
            Geolocation__Latitude__s = df.contractor.ShippingLatitude,
            Geolocation__Longitude__s = df.contractor.ShippingLongitude,
            PreferredLastLineKey__c = '123',
            CityStateKey__c = '456',
            UID__c = '00100123456'
        );
        INSERT zc;
        
        Coverage_Area__c ca = new Coverage_Area__c(
            Contractor__c = df.contractor.Id, 
            Zip_Code__c = zc.Id,
            Travel_Fee__c = 1,
            Trade__c = 'Appliances',
            Active__c = TRUE,
            Expiry_Date__c = Date.today()
        );
        ca.UID__c = String.valueOf(ca.Zip_Code__c) + ca.Contractor__c + ca.Trade__c;
        INSERT ca;
        
        Test.StartTest();
        CoverageExpiry_schedule sh1 = new CoverageExpiry_schedule();
        String sch = '0 0 23 * * ?'; system.schedule('Test', sch, sh1);
        CoverageItemExpiry_batch batch = new CoverageItemExpiry_batch();
        ID batchprocessid = Database.executeBatch(batch);
        Test.stopTest();
    }
    
    @isTest static void zipStagingBatchTest() {
        df.createGlobalSetting();
        
        Zip_Code_Staging__c zcs = new Zip_Code_Staging__c(
            ZipCode__c = '00100',
            City__c = df.contractor.ShippingCity, 
            State__c = df.contractor.ShippingStateCode,
            County__c = df.contractor.ShippingCity,
            AreaCode__c = '123',
            Geolocation__Latitude__s = df.contractor.ShippingLatitude,
            Geolocation__Longitude__s = df.contractor.ShippingLongitude,
            PreferredLastLineKey__c = '123',
            CityStateKey__c = '456',
            PrimaryRecord__c = 'P'
        );
        INSERT zcs;
        
        Test.StartTest();
        ZipCodeProcessing_schedule sh1 = new ZipCodeProcessing_schedule();
        String sch = '0 0 23 * * ?'; system.schedule('Test', sch, sh1);
        ZipCodeProcessing_batch batch = new ZipCodeProcessing_batch();
        ID batchprocessid = Database.executeBatch(batch);
        Test.stopTest();
    }
    
}