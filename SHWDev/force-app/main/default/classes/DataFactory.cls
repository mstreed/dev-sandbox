public without sharing class DataFactory {
	public Global_Setting__c gs {get; set;}
	public Website_Setting__c ws {get; set;}
	public Pricing__c pricing {get; set;}
	public Lead newLead {get; set;}
	public Lead newContractorLead {get; set;}
	public Lead newRealtorLead {get; set;}
	public Account acc {get; set;}
	public Account contractor {get; set;}
	public Account affiliate {get; set;}
	public Id conId {get; set;}
	public Contract newPolicy {get; set;}
	public Admin_Note__c adminNoteLead {get; set;}
	public Admin_Note__c adminNotePolicy {get; set;}
	public Payment_Method__c pMethodCardLead {get; set;}
	public Payment_Method__c pMethodCheckLead {get; set;}
    public Payment_Method__c pMethodCardPolicy {get; set;}
	public Payment_Method__c pMethodCheckPolicy {get; set;}
	public List<ChargentOrders__ChargentOrder__c> boListLead {get; set;}
	public List<ChargentOrders__ChargentOrder__c> boListPolicy {get; set;}
	public ChargentBase__Gateway__c newGateway {get; set;}
	public ChargentOrders__Transaction__c transactionLead {get; set;}
	public ChargentOrders__Transaction__c transactionPolicy {get; set;}
	public Case newCase {get; set;}
	public Case newInquiry {get; set;}
	public Work_Order__c workOrder {get; set;}
	public Policy_Renewal__c policyRenewal {get; set;}
	public Related_Policies__c rPolicy1 {get; set;}
	public Related_Policies__c rPolicy2 {get; set;}    
	public User userSales {get; set;}    
	public User userClaims {get; set;}    
    private static recordType rtAuthorize = [SELECT Id,Name,DeveloperName FROM RecordType WHERE SobjectType ='ChargentBase__Gateway__c' AND DeveloperName = 'Authorizenet' LIMIT 1];

    public void createGlobalSetting() {
        newGateway = new ChargentBase__Gateway__c (
            Name= 'Authorize.net Gateway (test)',
            ChargentBase__Merchant_ID__c= '4K56Lqa7Fy52',
            ChargentBase__Security_Key__c = '8Ht6C9ey3cV52W66',
            ChargentBase__Active__c = true,
            ChargentBase__Test_Mode__c = true,
            ChargentBase__Credit_Card_Data_Handling__c = 'Never Clear',
            ChargentBase__Available_Payment_Methods__c = 'Credit Card',
            ChargentBase__Available_Card_Types__c = 'Visa; Mastercard; American Express',
            recordTypeId = rtAuthorize.id
        );
        INSERT newGateway;
        
        Gateway_Assignment__c ga = new Gateway_Assignment__c(
            State__c = 'Default',
        	Primary_Gateway__c = newGateway.Id,
        	Secondary_Gateway__c = newGateway.Id,
        	Tertiary_Gateway__c = newGateway.Id
        );
        INSERT ga;
        
        gs = new Global_Setting__c();
        gs.Default_Gateway__c = newGateway.Id;
        gs.Deductible_Gateway__c = newGateway.Id;
        //gs.Pardot_API_Key_Last_Request__c = DateTime.now().addMinutes(-50);
        gs.Affiliate_Lead_Access_Start_Date__c = Date.today().addDays(-1);
        gs.Org_ID_PROD__c = UserInfo.getOrganizationId();
        INSERT gs;
        
        createContractor();
        gs.NSA_Contractor_ID__c = contractor.Id;
        update gs;
        
        Discount__c dc = new Discount__c(
        	Name = 'DISCOUNT123',
            Plan__c = 'Both',
            Starts__c = Date.today(),
            Type__c = 'Standard',
            Dollar_Amount__c = 1
        );
        INSERT dc;
        update dc;
        delete dc;
        undelete dc;
    }
    
    public void createContractorRating() {
        Contractor_Rating__c newContractorRating = new Contractor_Rating__c ();
        INSERT newContractorRating;
    }
    
    public void createWebsiteSettings(){
        ws = new Website_Setting__c();
        ws.Header_Image_URL__c = 'https://marketing.selecthomewarranty.com/assets/images/icons/icon-award-a76189ee.png';
        ws.Header_Text__c = '2019 EDITOR’S CHOICE BY HOMEWARRANTYREVIEWS.COM';
        ws.Plans_Header_Text_1__c = 'Shield Your Wallet';
        ws.Plans_Header_Text_2__c = 'From Expensive Home Repairs';
        ws.Plans_Subheader_Text_1__c = 'Pick a plan that works best for you, customize your add-ons.';
        ws.Quote_Promo_Text_1__c = '$100 OFF ANY PLAN';
        ws.Quote_Promo_Text_2__c = 'FREE ROOF COVERAGE';
        ws.Quote_Promo_Text_3__c = '2 EXTRA MONTHS FREE';
        ws.Active__c = TRUE;
        ws.RecordTypeId = [SELECT Id,Name,DeveloperName FROM RecordType WHERE SobjectType ='Website_Setting__c' AND DeveloperName = 'Global_Setting' LIMIT 1].Id;
        INSERT ws;
        
        pricing = new Pricing__c(
        	//Effective_Date__c = Date.today().addDays(1),
            Website_Setting__c = ws.Id
        );
        INSERT pricing; 
        
        ws.Active_Pricing__c = pricing.Id;
        update ws;
    }
    
    public void setupUsers(){
        userSales = [SELECT Id FROM User Where Profile.Name = 'SHW Sales' AND IsActive = TRUE LIMIT 1];
        userClaims = [SELECT Id FROM User Where Profile.Name = 'SHW Claims' AND IsActive = TRUE LIMIT 1];
    }
    
    public void createLead(){
        recordType rt = [SELECT Id,Name,DeveloperName FROM RecordType WHERE SobjectType ='Lead' AND DeveloperName = 'Queue_Lead' LIMIT 1];
            
        newlead = new Lead(
            FirstName ='test',
            LastName = 'testLast',
            Company = 'Test',
            Email = 'test@test.com',
            Email_with_Multiple_Address__c = 'test@test.com',
            Phone = '1234',
            MobilePhone = '3242',
            CountryCode = 'US',
            Contract_Term__c = '1 Year',
            Street = 'testStreet',
            City = 'testCity',
            StateCode = 'TX',
            PostalCode = '32423',
            status = 'Warranty Sold',
            recordTypeId = rt.id,
            Total_Price_Manual__c = 1,
            Customer_Id__c = '123',
            CreatedDate = DateTime.now().addDays(-3),
            Gateway__c = newGateway.Id,
            Processing_Fee__c = '25',
            Sales_Agent_Lookup__c = UserInfo.getUserId()
        );
        INSERT newLead;
    }
    
    public void createContractorLead(){
        newContractorLead = new Lead(
            FirstName ='test',
            LastName = 'testLast',
            Company = 'Test Con',
            Email = 'test@test.com',
            Email_with_Multiple_Address__c = 'test@test.com',
            Phone = '1234567890',
            MobilePhone = '3242',
            CountryCode = 'US',
            Street = 'testStreet',
            City = 'testCity',
            StateCode = 'DE',
            PostalCode = '32423',
            status = 'New',
            LeadSource = 'Website'
        );
        newContractorLead.RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Lead' AND DeveloperName = 'Contractor' LIMIT 1].Id;
        INSERT newContractorLead;
    }
    
    public void createRealtorLead(){
        newRealtorLead = new Lead(
            FirstName ='test',
            LastName = 'testLast',
            Company = 'Test Con',
            Email = 'test@test.com',
            Email_with_Multiple_Address__c = 'test@test.com',
            Phone = '1234567890',
            MobilePhone = '3242',
            CountryCode = 'US',
            Street = 'testStreet',
            City = 'testCity',
            StateCode = 'DE',
            PostalCode = '32423',
            status = 'New',
            LeadSource = 'Website'
        );
        newRealtorLead.RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Lead' AND DeveloperName = 'Realtor' LIMIT 1].Id;
        INSERT newRealtorLead;
    }
    
    public void createContractor(){
        contractor = new Account(
            Name='Test',
            ShippingStreet = '120 2nd Ave',
            ShippingCity = 'New York',
            ShippingStateCode = 'DE',
            ShippingCountryCode = 'US',
            ShippingPostalCode = '10022',
            Business_Email__c = 'test@test.com',
            ShippingLatitude = 40.728004,
            ShippingLongitude = -73.987649,
            Approved_for_Quick_Dispatch__c = true,
            Maximum_Distance_mi_for_Quick_Dispatch__c = 900,
            Services_Provided__c = 'Plumber'
        );
        contractor.RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Contractor_Account' LIMIT 1].Id;

        INSERT contractor;
        
        Contact conCon = new Contact(
        	FirstName = 'Test',
            LastName = 'Test',
            AccountId = contractor.Id,
            Email = 'test@test.com'
        );
        INSERT conCon;
    }
    
    public void createAffiliate(){
        affiliate = new Account(
            Name='Test',
            Affiliate_First_Name__c = 'John',
            Affiliate_Last_Name__c = 'Test',
            ShippingStreet = '120 2nd Ave',
            ShippingCity = 'New York',
            ShippingStateCode = 'NY',
            ShippingCountryCode = 'US',
            ShippingPostalCode = '10022',
            Business_Email__c = 'test@test.com',
            ShippingLatitude = 40.728004,
            ShippingLongitude = -73.987649,
            Affiliate_ID__c = '12345',
            Leads_Report__c = TRUE,
            Leads_Report_Type__c = 'Month-to-Date'
        );
        affiliate.RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Affiliate_Account' LIMIT 1].Id;

        INSERT affiliate;
    }
    
    public void createPolicy(){
        Service_Contract_Agreement__c sca = new Service_Contract_Agreement__c(
            Name = 'Other - Default'
        );
        
        INSERT sca;
        
        acc = new Account(
            FirstName ='John',
            LastName = 'Test',
            PersonEmail = 'test@test.com'
        );
        acc.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'PersonAccount'].Id;
        INSERT acc;
        
        conId = [Select personContactId FROM Account WHERE Id = :acc.Id LIMIT 1].personContactId;
    
        newPolicy = new Contract (
            Name ='TestPolicy Tab',
            accountId = acc.id,
            Primary_Policy__c = true,
            Email_with_Multiple_Address__c = 'test@test.com',
            Email__c = 'test@test.com',
            Contract_Number__c = '7654321',
            Home_Phone__c = '1234567890',
            Status = 'Not Yet Active',
            Annual_Price_Manual__c = 1,
            Sales_Agent_Lookup__c = UserInfo.getUserId(),
            Converted_Date__c = Date.today(),
            Purchase_Date__c = Date.today(),
            Manager_Total_Discounted_Price_Override__c = 100000,
            Payment_Terms__c = 'Monthly',
            Contract_Term__c = '1 Year',
            Service_Call_Fee_Dropdown__c = '75',
            Gateway__c = newGateway.Id,
            StartdateAndGracePeriod__c = Date.today(),
            Original_Policy_End_Date__c = Date.today().addYears(1),
            of_Free_Service_Calls__c = 3,
            OldStartDate__c = Date.today(),
            BillingStreet = '120 2nd Ave',
            BillingCity = 'New York',
            BillingStateCode = 'NY',
            BillingCountryCode = 'US',
            BillingPostalCode = '10022'
        );
        INSERT newPolicy;
    }
    
    public void createAdminNoteLead(){
        adminNoteLead = new Admin_Note__c  (
            Repair__c = 'test',
            Type__c = 'test',
            Lead__c = newLead.id,
            Agent_Name__c = 'testAgent'
            
        );
        INSERT adminNoteLead;
    }
        
    public void createAdminNotePolicy(){
        adminNotePolicy = new Admin_Note__c  (
            Repair__c = 'test',
            Type__c = 'test',
            Policy__c = newPolicy.id,
            Agent_Name__c = 'testAgent'
            
        );
        INSERT adminNotePolicy;
    }
    
    public void createPaymentMethodsLead(){
        pMethodCardLead = new Payment_Method__c(
            Lead_Name__c= newLead.id,
            Month__c = 05,
            Card_Number_Encrypted__c = '4111111111111111',
            Set_as_default__c = true,
            Year__c = 2021,
            Payment_Method__c = 'Credit Card'
        );
        INSERT pMethodCardLead;
        
        pMethodCheckLead = new Payment_Method__c(
            Lead_Name__c = newLead.id,
            Payment_Method__c = 'Check (Paper)'
        );
        INSERT pMethodCheckLead;
    }
    
    public void createPaymentMethodsPolicy(){
        pMethodCardPolicy = new Payment_Method__c(
            Policy_Name__c= newPolicy.id,
            Month__c = 05,
            Card_Number_Encrypted__c = '4111111111111111',
            Set_as_default__c = true,
            Year__c = 2021,
            Payment_Method__c = 'Credit Card'
        );
        INSERT pMethodCardPolicy;
        
        pMethodCheckPolicy = new Payment_Method__c(
            Policy_Name__c = newPolicy.id,
            Payment_Method__c = 'Check (Paper)'
        );
        INSERT pMethodCheckPolicy;
    }
    
    public void createOrdersLead(){
        boListLead = new List<ChargentOrders__ChargentOrder__c>{
            new ChargentOrders__ChargentOrder__c(
                Lead__c = newLead.id,
                Payment_Terms_New__c = '2 Payments',
                Payment_Method__c = 'One Time',
                Related_Payment_Method__c = pMethodCardLead.id,
                ChargentOrders__Charge_Amount__c = 20.00
            ),new ChargentOrders__ChargentOrder__c(
                Lead__c = newLead.id,
                Payment_Terms_New__c = '2 Payments',
                Payment_Method__c = 'Multiple/scheduled',
                Related_Payment_Method__c = pMethodCardLead.id,
                ChargentOrders__Charge_Amount__c = 10.00
            )};
                INSERT boListLead;
    }
    
    public void createOrdersPolicy(){
        boListPolicy = new List<ChargentOrders__ChargentOrder__c>{
            new ChargentOrders__ChargentOrder__c(
                Policy__c = newPolicy.id,
                Payment_Terms_New__c = '2 Payments',
                Payment_Method__c = 'One Time',
                Related_Payment_Method__c = pMethodCardPolicy.id,
                ChargentOrders__Charge_Amount__c = 20.00
            ),new ChargentOrders__ChargentOrder__c(
                Policy__c = newPolicy.id,
                Payment_Terms_New__c = '2 Payments',
                Payment_Method__c = 'Multiple/scheduled',
                Related_Payment_Method__c = pMethodCardPolicy.id,
                ChargentOrders__Charge_Amount__c = 10.00
            )};
                INSERT boListPolicy;
    }
    
    public void createTransactionLead(){
        transactionLead = new ChargentOrders__Transaction__c (
            ChargentOrders__Order__c = boListLead[0].id,
            ChargentOrders__Type__c = 'Charge',
            ChargentOrders__Gateway_Date__c = Date.today(),
            ChargentOrders__Response_Message__c = 'This transaction has been approved.',
            ChargentOrders__Gateway_ID__c = '60102691449',
            ChargentOrders__Gateway__c = newGateway.id,
            ChargentOrders__Response_Status__c = 'Approved',
            ChargentOrders__Amount__c = 20.00,
            ChargentOrders__Amount_available_for_Refund__c = 10.00,
            ChargentOrders__Authorization__c = '60RKMH',
            ChargentOrders__Reason_Code__c = 1,
            ChargentOrders__Reason_Text__c = 'This transaction has been approved.',
            ChargentOrders__Card_Last_4__c = '1111',
            ChargentOrders__Payment_Method__c = 'Credit Card',
            ChargentOrders__Credit_Card_Name__c = 'test',
            ChargentOrders__Credit_Card_Type__c = 'Visa',
            ChargentOrders__Billing_Last__c = 'test',
            ChargentOrders__Gateway_Response__c = 'test'
            
        );
        INSERT transactionLead;
    }
    
    public void createTransactionPolicy(){
        transactionPolicy = new ChargentOrders__Transaction__c (
            ChargentOrders__Order__c = boListPolicy[0].id,
            ChargentOrders__Type__c = 'Refund',
            ChargentOrders__Gateway_Date__c = Date.today(),
            ChargentOrders__Response_Message__c = 'This transaction has been approved.',
            ChargentOrders__Gateway_ID__c = '60102691449',
            ChargentOrders__Gateway__c = newGateway.id,
            ChargentOrders__Response_Status__c = 'Approved',
            ChargentOrders__Account__c = acc.id,
            ChargentOrders__Amount__c = 20.00,
            ChargentOrders__Amount_available_for_Refund__c = 10.00,
            ChargentOrders__Authorization__c = '60RKMH',
            ChargentOrders__Reason_Code__c = 1,
            ChargentOrders__Reason_Text__c = 'This transaction has been approved.',
            ChargentOrders__Card_Last_4__c = '1111',
            ChargentOrders__Payment_Method__c = 'Credit Card',
            ChargentOrders__Credit_Card_Name__c = 'test',
            ChargentOrders__Credit_Card_Type__c = 'Visa',
            ChargentOrders__Billing_Last__c = 'test',
            ChargentOrders__Gateway_Response__c = 'test',
            Charge_Reason__c = 'Renewal'
            
        );
        INSERT transactionPolicy;
    }
    
    public void createCase(){
        newCase = new Case(
            Policy__c = newPolicy.id,
            AccountId = acc.Id,
            ContactId = conId,
            Subject='subject',
            Status = 'Not Set',
            Description='description',
            Type='Claim Submission',
            Issue_Started__c = Date.today(),
            Contractor_Authorized_Amount__c=1100,
            Customer_Invoice_Received_Date__c = Date.today(),
            Customer_Authorized_Amount__c=1200,
            Customer_Reimbursement_Amount__c=1200,
            Invoice_Due_Date__c = Date.today(),
            Invoice_Received_Date__c = Date.today(),
            Contractor__c = contractor.id,
            Reason = 'Additional AC Unit(each)',
            Issue_Detail__c = 'Humidifier is leaking water',
            Cancellation_Date__c = Date.today(),
            Cancellation_Fee__c = 0,
            Plan_Amount__c = 0,
            Claims_Paid__c = 0,
            Customer_Paid__c = 0
        );
        INSERT newCase;
    }
    
    public void createInquiry(){
        newInquiry = new Case(
            Subject='subject',
            Status = 'Open',
            Origin = 'Email',
            SuppliedEmail = 'test@test.com',
            Initial_Queue__c = 'Claims@',
            recordTypeId = [SELECT Id,Name,DeveloperName FROM RecordType WHERE SobjectType ='Case' AND DeveloperName = 'Inquiry' LIMIT 1].Id
        );
        INSERT newInquiry;
    }
    
    public void createWorkOrder(){
        workOrder = new Work_Order__c(
            Claim__c = newCase.id,
            Status__c = 'New',
            WO_Authorized_Date__c=Date.today(),
            Description__c = 'WO Description',
            Rating__c = 5
        );
        INSERT workOrder;
    }
    
    public void createRenewal(){
        policyRenewal = new Policy_Renewal__c (
            Policy__c = newPolicy.id,
            Contract_Term__c = '1 Year',
            Free_Months__c = '1',
            of_Payments__c = 'Single Payment',
            Renewal_Start_Date__c = Date.today(),
            Renewal_Status__c = 'Active',
            Renewal_Amount__c = 10000,
            Service_Call_fee_DD__c = '75',
            Sales_Agent__c = UserInfo.getUserId(),
            CreatedById = UserInfo.getUserId()
        );
        INSERT policyRenewal;
    }
    
    public void createRelatedPolicies(){
        rPolicy1 = new Related_Policies__c (
            Current_Policy__c = newPolicy.id
        );
        INSERT rPolicy1;
        
        rPolicy2 = new Related_Policies__c (
            Related_Policy__c = newPolicy.id
        );
        INSERT rPolicy2;
    }
}