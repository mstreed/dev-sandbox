global class DispatchOrgRetry_batch implements Database.Batchable<sObject> {
	String query = '';
	global DispatchOrgRetry_batch() {
		query = 'SELECT Id FROM dispconn__Service_Provider__c ' +
                'WHERE dispconn__Last_Sync_Response_Code__c = \'422\' AND ' +  // this is for go live when running manually. If automating should be only 500 errors
                '(dispconn__Email__c != NULL OR dispconn__Phone__c != NULL) ' +
                'AND dispconn__Address_State__c != NULL';
       
		system.debug('Dispatch: query '+query);		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<dispconn__Service_Provider__c> scope) {
		List<dispconn__Service_Provider__c> lstRecordsToUpdate = new List<dispconn__Service_Provider__c>();
		system.debug('Dispatch: query size '+scope.size());        
		DispatchTriggerHandler.disableTriggers();
		for(dispconn__Service_Provider__c rec : scope) {
			rec.dispconn__Last_Sync_Response__c = 'Retrying: ' + Datetime.now();
			lstRecordsToUpdate.add(rec);
		}
		if(lstRecordsToUpdate.size() > 0) { 
			update lstRecordsToUpdate; 
		}	   	
		DispatchTriggerHandler.enableTriggers();
	
	}
	
	global void finish(Database.BatchableContext BC) {
	}
	
}