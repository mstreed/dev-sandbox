public class DispatchScheduleController {
public Id acctId;
    public Id caseId {get;set;}
    public Case caseRec {get;set;}
    public String dispatchAction {get;set;}
    public Account contractorRec {get;set;}
    public DispatchScheduleController(){
        caseRec = new Case();
        contractorRec = new Account();
        if(apexpages.currentpage().getparameters().get('id') != null && apexpages.currentpage().getparameters().get('id') != ''){
            caseId  = apexpages.currentpage().getparameters().get('id');
            for(Case cRecord : [SELECT Id,Policy_Name__c,Contractor__c,Customer_Phone_Number_New__c,Customer_Mobile_Phone__c,Property_Street__c,
                               Property_State__c,Property_City__c,Property_Zip__c,Case_and_Claim_Number_Text__c,Coverage_Items__c,
                                Service_Call_Fee_Final__c,Description FROM Case WHERE Id =: caseId LIMIT 1]){
                                  caseRec = cRecord;  
                                }
        }
        System.debug('account' + apexpages.currentpage().getparameters().get('accId'));
        if(apexpages.currentpage().getparameters().get('accId') != null && apexpages.currentpage().getparameters().get('accId') != ''){
        acctId = apexpages.currentpage().getparameters().get('accId');
            System.debug('acctId^^' + acctId);
            for(Account acct : [SELECT Id,Name,Phone,PersonEmail,isPersonAccount FROM Account WHERE Id =: caseRec.Contractor__c LIMIT 1]){
                contractorRec = acct;
            }
        }
        System.debug('dispatch' + apexpages.currentpage().getparameters().get('dispatchAction'));
        if(apexpages.currentpage().getparameters().get('dispatchAction') != null && apexpages.currentpage().getparameters().get('dispatchAction') != ''){
        dispatchAction = apexpages.currentpage().getparameters().get('dispatchAction');
            System.debug('dispatchAction^^' + dispatchAction);
        }
        }
}