public class EZTextingJSON{
    public responseWrapper Response;
    
    public static EZTextingJSON parse(String jsonStr){
        return (EZTextingJSON) System.JSON.deserialize(jsonStr, EZTextingJSON.class);
        //return (EZTextingJSON) System.JSON.deserializeStrict(jsonStr, EZTextingJSON.class);
    }
    
    public Class responseWrapper {
        public String Status;
        public String Code;
        public List<entryWrapper> Entries;
        public entryWrapper Entry;
        public List<String> Errors;
        public responseWrapper() {}
    }
    
    public Class entryWrapper {
        public String Id {get;set;}
        public String PhoneNumber {get;set;}
        public String[] Groups {get;set;}
        //public String CreatedAt {get;set;}
        //public String UpdatedAt {get;set;}
        public String Subject {get;set;}
        public String Message {get;set;}
        public String StampToSend {get;set;}
        public entryWrapper() {}
    }
}