global class EZTextingPaymentMethod_schedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        EZTextingPaymentMethod_batch batchRun = new EZTextingPaymentMethod_batch(); 
        ID batchId = Database.executeBatch(batchRun,1);
    }
}