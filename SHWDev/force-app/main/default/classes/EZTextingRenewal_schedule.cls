global class EZTextingRenewal_schedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        EZTextingRenewal_batch batchRun = new EZTextingRenewal_batch(); 
        ID batchId = Database.executeBatch(batchRun,1);
    }
}