@isTest
private class EZTextingTest {
    private static DataFactory df = new DataFactory();

    @isTest
    static void leadEzTextTriggerHandlerTests1() {
        Test.StartTest();
        // Set mock callout class for querying with found
        Test.setMock(HttpCalloutMock.ezTextMockQueryFound.class, new MockHttpResponseGenerator.ezTextMockQueryFound());

        df.createGlobalSetting();
        df.createLead();
        RecursiveTriggerHandler.isFirstTime = TRUE;
        RecursiveTriggerHandler.isFirstTime2 = TRUE;
        
        df.newLead.Phone = '999999999';
        df.newLead.Status = 'New';
        df.newLead.EZ_Texting_Queue_Groups__c = 'Buy It Now, SF Test';
        update df.newLead;
        
        EzTextingLead_schedule sh1 = new EzTextingLead_schedule();
        String sch = '0 0 23 * * ?'; system.schedule('Test', sch, sh1);
        EzTextingLead_batch batch = new EzTextingLead_batch();
        ID batchprocessid = Database.executeBatch(batch);
        
        EZTextingJSON.entryWrapper ezw = new EZTextingJSON.entryWrapper();
        system.debug('ezw.Subject: ' + ezw.Subject);
        system.debug('ezw.Message: ' + ezw.Message);
        system.debug('ezw.StampToSend: ' + ezw.StampToSend);
        Test.StopTest();

    }
    
    @isTest
    static void leadEzTextTriggerHandlerTests2() {
        Test.StartTest();
        // Set mock callout class for querying without found
        Test.setMock(HttpCalloutMock.ezTextMockQueryNone.class, new MockHttpResponseGenerator.ezTextMockQueryNone());

        df.createGlobalSetting();
        df.createLead();
        RecursiveTriggerHandler.isFirstTime = TRUE;
        RecursiveTriggerHandler.isFirstTime2 = TRUE;
        
        df.newLead.Phone = '999999999';
        df.newLead.Status = 'New';
        df.newLead.EZ_Texting_Queue_Groups__c = 'Buy It Now, SF Test';
        update df.newLead;
        
        EzTextingLead_schedule sh1 = new EzTextingLead_schedule();
        String sch = '0 0 23 * * ?'; system.schedule('Test', sch, sh1);
        EzTextingLead_batch batch = new EzTextingLead_batch();
        ID batchprocessid = Database.executeBatch(batch);
        Test.StopTest();
    }

    @isTest
    static void leadEzTextTriggerHandlerTests3() {
        Test.StartTest();
        // Set mock callout class for querying with id
        Test.setMock(HttpCalloutMock.ezTextMockId.class, new MockHttpResponseGenerator.ezTextMockId());

        df.createGlobalSetting();
        df.createLead();
        RecursiveTriggerHandler.isFirstTime = TRUE;
        RecursiveTriggerHandler.isFirstTime2 = TRUE;
        
        df.newLead.Phone = '999999999';
        df.newLead.Status = 'Warranty Sold';
        df.newLead.EZ_Texting_Id__c = '5eacb157e4770908b62b69e5';
        df.newLead.EZ_Texting_Groups__c = 'Buy It Now';
        df.newLead.EZ_Texting_Queue_Groups__c = 'Buy It Now, SF Test';
        update df.newLead;
        
        EzTextingLead_schedule sh1 = new EzTextingLead_schedule();
        String sch = '0 0 23 * * ?'; system.schedule('Test', sch, sh1);
        EzTextingLead_batch batch = new EzTextingLead_batch();
        ID batchprocessid = Database.executeBatch(batch);
        Test.StopTest();
    }
    
    @isTest
    static void leadEzTextTriggerHandlerTests4() {
        Test.StartTest();

        df.createGlobalSetting();
        df.createLead();
        RecursiveTriggerHandler.isFirstTime = TRUE;
        RecursiveTriggerHandler.isFirstTime2 = TRUE;
        
        df.newLead.Phone = '999999999';
        df.newLead.Status = 'Warranty Sold';
        df.newLead.EZ_Texting_Queue_Groups__c = 'Buy It Now, SF Test';
        update df.newLead;
        Test.StopTest();
    }
    
    @isTest
    static void leadEzTextTriggerHandlerTests5() {
        Test.StartTest();
        // Set mock callout class for querying with id
        Test.setMock(HttpCalloutMock.ezTextMockId.class, new MockHttpResponseGenerator.ezTextMockId());

        df.createGlobalSetting();
        df.createLead();
        RecursiveTriggerHandler.isFirstTime = TRUE;
        RecursiveTriggerHandler.isFirstTime2 = TRUE;
        
        df.newLead.Phone = '999999999';
        df.newLead.Status = 'New';
        df.newLead.EZ_Texting_Id__c = '5eacb157e4770908b62b69e5';
        df.newLead.EZ_Texting_Queue_Groups__c = 'Buy It Now, SF Test';
        update df.newLead;
        
        EzTextingLead_schedule sh1 = new EzTextingLead_schedule();
        String sch = '0 0 23 * * ?'; system.schedule('Test', sch, sh1);
        EzTextingLead_batch batch = new EzTextingLead_batch();
        ID batchprocessid = Database.executeBatch(batch);
        Test.StopTest();
    }
    
    @isTest
    static void testRenewalBatch() {
        // Set mock callout class for message
        Test.setMock(HttpCalloutMock.ezTextMockMessage.class, new MockHttpResponseGenerator.ezTextMockMessage());
        
        df.createGlobalSetting();
        df.createPolicy();
        
        Renewal_Response__c newRR = new Renewal_Response__c (
            Policy__c = df.newPolicy.Id,
            Policy_End_Date__c = df.newPolicy.Original_Policy_End_Date__c,
            Customer_Email__c = df.newPolicy.Email__c,
            Total_List_Price__c = df.newPolicy.Total_List_Price_Calculated__c
        );
        insert newRR;
        
        Test.StartTest();
        EzTextingRenewal_schedule sh1 = new EzTextingRenewal_schedule();
        String sch = '0 0 23 * * ?'; system.schedule('Test', sch, sh1);
        EzTextingRenewal_batch batch = new EzTextingRenewal_batch();
        ID batchprocessid = Database.executeBatch(batch);
        Test.StopTest();
    }
    
    @isTest
    static void testPaymentBatch() {
        // Set mock callout class for message
        Test.setMock(HttpCalloutMock.ezTextMockMessage.class, new MockHttpResponseGenerator.ezTextMockMessage());
        
        df.createGlobalSetting();
        df.createPolicy();
        df.createPaymentMethodsPolicy();
        df.pMethodCardPolicy.Error_Declined__c = DateTime.now();
        update df.pMethodCardPolicy;
        
        Test.StartTest();
        EzTextingPaymentMethod_schedule sh1 = new EzTextingPaymentMethod_schedule();
        String sch = '0 0 23 * * ?'; system.schedule('Test', sch, sh1);
        EzTextingPaymentMethod_batch batch = new EzTextingPaymentMethod_batch();
        ID batchprocessid = Database.executeBatch(batch);
        Test.StopTest();
    }
    
    /*@isTest
    static void leadEzTextBatchTests1() {
        Test.StartTest();

        df.createGlobalSetting();
        df.createLead();
        RecursiveTriggerHandler.isFirstTime = TRUE;
        
        df.newLead.Phone = '999999999';
        df.newLead.Status = 'New';
        df.newLead.EZ_Texting_Queue_Groups__c = 'Buy It Now, SF Test';
        update df.newLead;
        
        Test.setMock(HttpCalloutMock.ezTextMockQueryNone.class, new MockHttpResponseGenerator.ezTextMockQueryNone());
        EzTextingLead_schedule sh1 = new EzTextingLead_schedule();
        String sch = '0 0 23 * * ?'; system.schedule('Test', sch, sh1);
        EzTextingLead_batch batch = new EzTextingLead_batch();
        ID batchprocessid = Database.executeBatch(batch);
        Test.StopTest();
    }
    
    @isTest
    static void leadEzTextBatchTests2() {
        Test.StartTest();

        df.createGlobalSetting();
        df.createLead();
        RecursiveTriggerHandler.isFirstTime = TRUE;
        
        df.newLead.Phone = '999999999';
        df.newLead.Status = 'New';
        df.newLead.EZ_Texting_Queue_Groups__c = 'Buy It Now, SF Test';
        update df.newLead;
        
        Test.setMock(HttpCalloutMock.ezTextMockQueryNone.class, new MockHttpResponseGenerator.ezTextMockQueryNone());
        EzTextingLead_schedule sh1 = new EzTextingLead_schedule();
        String sch = '0 0 23 * * ?'; system.schedule('Test', sch, sh1);
        EzTextingLead_batch batch = new EzTextingLead_batch();
        ID batchprocessid = Database.executeBatch(batch);
        Test.StopTest();
    }*/
}