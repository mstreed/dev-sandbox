public without sharing class EarningsReportController {
    public String errorMsg {get;set;} 
    public String successMsg {get;set;}
    
    public String startMonthSelected {get;set;}
    public List<SelectOption> monthsOption {get;set;}
    
    public List<String> polStatusSelected {get;set;}
    public List<SelectOption> polStatusOptions {get;set;}
    
    public List<String> renStatusSelected {get;set;}
    public List<SelectOption> renStatusOptions {get;set;}
    
    public List<String> stateSelected {get;set;}
    public List<SelectOption> stateOptions {get;set;}
    
    //public List<String> contractTermSelected {get;set;}
    private List<String> contractTerms = new List<String>{'Monthly','1 Year','2 Year','3 Year','4 Year','5 Year','10 Year','Total'};
    /*public List<SelectOption> contractTermOptions {
        get {
            if (contractTermOptions == NULL) {
                contractTermOptions = new List<SelectOption>();
                for (String ct :contractTerms) {
                    if (ct != 'Total') {
                        contractTermOptions.add(new SelectOption(ct,ct));
                    }
                }
            }
            return contractTermOptions;
        }
        private set;
    }*/
    
    public Boolean policies {get;set;}
    public Boolean polWithRenewals {get;set;}
    public Boolean renewals {get;set;}
    
    public List<WrapTerm> earningsList {get;set;}
    public List<String> reportData {get;set;} //yyyy-mmm
    
    public EarningsReportController() {
        system.debug('EarningsReportController');

       	Contract firstPolicy = [SELECT Id, StartdateAndGracePeriod__c FROM Contract ORDER BY StartdateAndGracePeriod__c ASC LIMIT 1];
        system.debug('firstPolicy: ' + firstPolicy);
        
        Date startDate = Date.newInstance(firstPolicy.StartdateAndGracePeriod__c.year(), firstPolicy.StartdateAndGracePeriod__c.month(), 1); //Date.valueOf(DateTime.now.format('yyyy-MM-dd',  UserInfo.getTimeZone().toString()))
        system.debug('startDate: ' + startDate);
        startDate = Date.newInstance(startDate.year(), startDate.month(), 1).addMonths(-10);
        system.debug('startDate2: ' + startDate);
        
        startMonthSelected = String.valueOf(Date.newInstance(Date.Today().year(), Date.Today().month(), 1));
        system.debug('startMonthSelected: ' + startMonthSelected);

        Date currentDate = Date.valueOf(DateTime.now().format('yyyy-MM-dd', UserInfo.getTimeZone().toString()));
        currentDate = Date.newInstance(currentDate.year(), currentDate.month(), 1);

        Integer monthDiff = startDate.monthsBetween(currentDate);
        
        monthsOption = new List<SelectOption>();

        //Set the month picklist and also set the start of the reporting
        for (Integer i = 0, j = 0; i < (monthDiff+120); i++) { //monthsDiff + 120 ahead = 10 years
            monthsOption.add(new SelectOption(String.valueOf(startDate), String.valueOf(startDate).left(7)));
            startDate = startDate.addMonths(1);
        } 

        //set status stuff
        polStatusOptions = new List<SelectOption>();
        for (Schema.PicklistEntry ple : Contract.Status.getDescribe().getPicklistValues()) {
            if (!ple.getLabel().contains('Do Not Use')) {
                polStatusOptions.add(new SelectOption(ple.getValue(), ple.getLabel()));
            }
        }
        polStatusSelected = new List<String>();
        polStatusSelected.add('Active');
        renStatusOptions = new List<SelectOption>();
        for (Schema.PicklistEntry ple : Policy_Renewal__c.Renewal_Status__c.getDescribe().getPicklistValues()) {
            renStatusOptions.add(new SelectOption(ple.getValue(), ple.getLabel()));
            /*if (ple.getLabel() != 'Canceled') {
                renStatusSelected.add(ple.getLabel());
            }*/
        }
        renStatusSelected = new List<String>();
        renStatusSelected.add('Active');
        
        //set state stuff
        stateSelected = new List<String>();
        stateOptions = new List<SelectOption>();
        Schema.DescribeFieldResult dfrType2 = Website_Setting__c.State__c.getDescribe();
        //statusOptions.add(new SelectOption('', '--None--'));
        for (Schema.PicklistEntry ple :  dfrType2.getPicklistValues()) {
            if (!ple.getLabel().contains('Default')) {
                stateOptions.add(new SelectOption(ple.getValue(), ple.getLabel()));
                stateSelected.add(ple.getLabel());
            }
        }
        
        policies = TRUE;
        polWithRenewals = FALSE;
        renewals = FALSE;
        
        //contractTermSelected = contractTerms.clone();
        /*contractTermSelected = new List<String>();
        contractTermSelected.add('Monthly');
        contractTermSelected.add('1 Year');*/
        getEarnings();
    }
    
    public void getEarnings() {
        system.debug('getEarnings');
        errorMsg = '';
        try{
            if (!policies && !renewals) {
                errorMsg = 'Report must be run to include either Policies or Renewals or Both.';
                return;
            }
            
            if (stateSelected.size() == 0) {
                errorMsg = 'At least one State must be selected for filtering.';
                return;
            }
            
            //set report data
            reportData = new List<String>();
            reportData.add('# of Policies');
            reportData.add('# of Renewals');
            Boolean reportDataStart = false;
            
            Integer j = 0;
            for (SelectOption mo : monthsOption) {
                if (startMonthSelected == mo.getValue()) {
                    reportDataStart = TRUE;
                }
                
                if (j < 6 && reportDataStart) { //only pull data next 6 month
                    reportData.add(mo.getLabel());
                    j++;
                }
            } 
            reportData.add('Total');

            system.debug('reportData: ' + reportData);
            
            
            //Start Earning List
            earningsList = new List<WrapTerm>();
            for (String ct : contractTerms) {
                earningsList.add(new WrapTerm(ct));
            }
            
            for (WrapTerm el : earningsList) {
                el.data = new Map<String,Decimal>();
                for (String rm : reportData) {
                    el.data.put(rm, 0);
                }
            }
            
            Date startDate = Date.valueOf(startMonthSelected);
            //Date endDate = startDate.addMonths(13);
            system.debug('startDate: ' + startDate);
            
            String endPoint = URL.getOrgDomainUrl().toExternalForm()+'/services/data/v48.0/query/?q=';
            String conTermQry = '';

            if (policies) {
                if (polStatusSelected.size() == 0) {
                    errorMsg = 'At least one Policy Status must be selected for filtering.';
                    return;
                }
                
                String queryStr = 'SELECT Contract_Term__c, Count(Id) c, SUM(Monthly_Subtotal__c) a FROM Contract';
                system.debug('queryStr: ' + queryStr);
                String statusQry = '';
                if (polStatusSelected.size() > 0) {
                    if (polStatusSelected.size() == 1) {
                        statusQry += ' WHERE Status = \''+polStatusSelected[0]+'\'';
                    } else {
                        statusQry += ' WHERE (Status = \'';
                        for (String status :polStatusSelected) {
                            statusQry += status+'\' OR Status =\'';
                        }
                        statusQry = statusQry.left(statusQry.length()-13) + ')';
                    }
                    system.debug('statusQry: ' + statusQry);
                }
                String stateQry = '';
                if (stateSelected.size() != stateOptions.size()) {
                    if (stateSelected.size() == 1) {
                        stateQry += ' AND BillingState = \''+stateSelected[0]+'\'';
                    } else {
                        stateQry += ' AND (BillingState = \'';
                        for (String state :stateSelected) {
                            stateQry += state+'\' OR BillingState =\'';
                        }
                        stateQry = stateQry.left(stateQry.length()-19) + ')';
                    }
                    system.debug('stateQry: ' + stateQry);
                }
                String withRenQry = '';
                if (!polWithRenewals) {
                    withRenQry = ' AND Total_Policy_Renewals__c >= 0';
                } else {
                    withRenQry = ' AND Total_Policy_Renewals__c = 0';
                }
                system.debug('withRenQry: ' + withRenQry);
                
                for (String rd : reportData) {
                    if (rd != '# of Policies' && rd != '# of Renewals' && rd != 'Total') {
                        String dtQry = '';
                        dtQry = ' AND Earnings_Start_Date__c <= '+rd+'-01';
                        system.debug('dtQry: ' + dtQry);
                        
                        String finalQry = queryStr+statusQry+stateQry+withRenQry+dtQry+' AND (Earnings_End_Date__c >= '+rd+'-01 OR Contract_Term__c = \'Monthly\') Group BY Contract_Term__c';
                        system.debug('finalQry: ' + finalQry);
                        
                        AggregateResult[] groupedResults = Database.query(finalQry);
                        for (AggregateResult ar : groupedResults)  {
                            System.debug('Term' + ar.get('Contract_Term__c'));
                            System.debug('Count' + ar.get('c'));
                            System.debug('Amount' + ar.get('a'));
                            
                            updateData(String.valueOf(ar.get('Contract_Term__c')), Integer.valueOf(ar.get('c')), Decimal.valueOf(String.valueOf(ar.get('a'))), rd, 'Policy');
                        }
                    }
                }
                /*
                if (con.First_Renewal_Start_Date__c != NULL) {
                if (con.First_Renewal_Start_Date__c.addMonths(-1) >= Date.valueOf(rd+'-01')) {
                updateEarnings(con.Contract_Term__c, rd, con.Monthly_Subtotal__c);
                countMe = TRUE;
                }
                } else {
                updateEarnings(con.Contract_Term__c, rd, con.Monthly_Subtotal__c);
                countMe = TRUE;
                }*/
            }
            
            if (renewals) {
                /*if (renStatusSelected.size() == 0) {
                    errorMsg = 'At least one Renewal Status must be selected for filtering.';
                    return;
                }*/

                String queryStr = 'SELECT Contract_Term__c, Count(Id) c, SUM(Monthly_Subtotal__c) a FROM Policy_Renewal__c';
                system.debug('queryStr2: ' + queryStr);
                String statusQry = '';
                if (polStatusSelected.size() > 0) {
                    if (polStatusSelected.size() == 1) {
                        statusQry += ' WHERE Renewal_Status__c = \''+renStatusSelected[0]+'\'';
                    } else {
                        statusQry += ' WHERE (Renewal_Status__c = \'';
                        for (String status :renStatusSelected) {
                            statusQry += status+'\' OR Status =\'';
                        }
                        statusQry = statusQry.left(statusQry.length()-13) + ')';
                    }
                    system.debug('statusQry: ' + statusQry);
                }
                String stateQry = '';
                if (stateSelected.size() != stateOptions.size()) {
                    if (stateSelected.size() == 1) {
                        stateQry += ' AND Policy__r.BillingState = \''+stateSelected[0]+'\'';
                    } else {
                        stateQry += ' AND (Policy__r.BillingState = \'';
                        for (String state :stateSelected) {
                            stateQry += state+'\' OR Policy__r.BillingState =\'';
                        }
                        stateQry = stateQry.left(stateQry.length()-29) + ')';
                    }
                    system.debug('stateQry2: ' + stateQry);
                }
                
                for (String rd : reportData) {
                    if (rd != '# of Policies' && rd != '# of Renewals' && rd != 'Total') {
                        String dtQry = '';
                        dtQry = ' AND Earnings_Start_Date__c <= '+rd+'-01';
                        system.debug('dtQry2: ' + dtQry);
                        
                        String finalQry = queryStr+statusQry+stateQry+dtQry+' AND (Earnings_End_Date__c >= '+rd+'-01 OR Contract_Term__c = \'Monthly\') Group BY Contract_Term__c';
                        system.debug('finalQry2: ' + finalQry);
                        
                        AggregateResult[] groupedResults = Database.query(finalQry);
                        for (AggregateResult ar : groupedResults)  {
                            System.debug('Term2' + ar.get('Contract_Term__c'));
                            System.debug('Count2' + ar.get('c'));
                            System.debug('Amount2' + ar.get('a'));
                            
                            updateData(String.valueOf(ar.get('Contract_Term__c')), Integer.valueOf(ar.get('c')), Decimal.valueOf(String.valueOf(ar.get('a'))), rd, 'Renewal');
                        }
                    }
                }

                /*String queryStr = 'SELECT Id, Policy__r.Status, Policy__r.BillingState, Renewal_Status__c, Contract_Term__c, Renewal_Start_Date__c, Renewal_End_Date__c, Monthly_Subtotal__c FROM Policy_Renewal__c';
                system.debug('queryStr2: ' + queryStr);
                String statusQry = '';
                statusQry += ' WHERE Renewal_Status__c = \''+renStatusSelected+'\'';
                String stateQry = '';
                if (stateSelected.size() != stateOptions.size()) {
                    if (stateSelected.size() == 1) {
                        stateQry += ' AND Policy__r.BillingState = \''+stateSelected[0]+'\'';
                    } else {
                        stateQry += ' AND (Policy__r.BillingState = \'';
                        for (String state :stateSelected) {
                            stateQry += state+'\' OR Policy__r.BillingState =\'';
                        }
                        stateQry = stateQry.left(stateQry.length()-29) + ')';
                    }
                    system.debug('stateQry2: ' + stateQry);
                }
                
                
                for (String conTerm :contractTermSelected) {
                    String renQueryString = queryStr+statusQry+stateQry+' AND Contract_Term__c = \''+conTerm+'\'';
                    system.debug('renQueryString: ' + renQueryString);
                    
                    for (Policy_Renewal__c pr : Database.query(renQueryString + ' LIMIT 50000')) {
                        Boolean countMe = FALSE;
                        for (String rd : reportData) {
                            if (rd != '# of Policies' && rd != '# of Renewals'&& rd != 'Total' &&
                                (Date.newInstance(pr.Renewal_Start_Date__c.year(), pr.Renewal_Start_Date__c.month(), 1) <= Date.valueOf(rd+'-01') && 
                                 (pr.Renewal_End_Date__c.addMonths(-1) >= Date.valueOf(rd+'-01') || pr.Contract_Term__c == 'Monthly'))) {
                                     //updateEarnings(pr.Contract_Term__c, rd, pr.Monthly_Subtotal__c);
                                     countMe = TRUE;
                                 }
                        }
                        if (countMe) {
                            //updateCount(pr.Contract_Term__c, 'Renewal');
                        }
                    }
                }*/
            }
            
            //Calculate and set the row total
            Integer i = 0;
            for (WrapTerm wt :earningsList) {
                //system.debug('wt.Term: ' + wt.Term);
                Decimal rowTotal = 0;
                for (String rd :wt.data.keySet()) {
                    if (rd == '# of Policies' || rd == '# of Renewals' || rd == 'Total') {
                        continue;
                    } else {
                        Decimal amount = wt.data.get(rd);
                        rowTotal+= amount;
                    }
                }
                //system.debug('rowTotal: ' + rowTotal);
                earningsList[i].data.put('Total', rowTotal); //set the row total
                i++;
            }
            
            system.debug('earningsList: ' + earningsList);
        } catch ( Exception ex ) {
            if ( String.isNotBlank( ex.getMessage()) && ex.getMessage().contains( 'error:' ) ) {
                errorMsg = ex.getMessage().split('error:')[1].split(':')[0] + '.';
            } else {
                errorMsg = ex.getMessage();  
            }
        }
    }
    
    private Integer findTerm(String term) {
        //system.debug('updateEarnings');
        Integer i = 7;
        if (term == contractTerms[0]) { //Monthly
            i = 0;
        } else if (term == contractTerms[1]) { //1 Year
            i = 1;
        } else if (term == contractTerms[2]) { //2 Year
            i = 2;
        } else if (term == contractTerms[3]) { //3 Year
            i = 3;
        } else if (term == contractTerms[4]) { //4 Year
            i = 4;
        } else if (term == contractTerms[5]) { //5 Year
            i = 5;
        } else if (term == contractTerms[6]) { //10 Year
            i = 6;
        }
        
        return i;
    }
    
    /*private void updateEarnings(String term, String rd, Decimal amount) {
        //system.debug('updateEarnings');
        Integer i = findTerm(term);
        if (i < 7) {
            Decimal price = earningsList[i].data.get(rd);
            earningsList[i].data.put(rd, price+amount); //set the month and amount
            
            Decimal colTotal = earningsList[7].data.get(rd);
            earningsList[7].data.put(rd, colTotal+amount); //set the column total
        }
        
    }*/
    
    private void updateData(String term, Integer count, Decimal amount, String rd, String obj) {
        //system.debug('updateCount');
        Integer i = findTerm(term);

        if (i < 7) {
            earningsList[i].data.put(rd, amount); //set the month and amount
            Decimal colTotal = earningsList[7].data.get(rd);
            earningsList[7].data.put(rd, colTotal+amount); //set the column total
            
            if (obj == 'Policy') {
                earningsList[i].data.put('# of Policies', count); //set the # of policies and count
                Decimal total = earningsList[7].data.get('# of Policies');
                earningsList[7].data.put('# of Policies', total+count); //set the total
            } else { //Renewals
                earningsList[i].data.put('# of Renewals', count); //set the # of policies and count
                Decimal total = earningsList[7].data.get('# of Renewals');
                earningsList[7].data.put('# of Renewals', total+count); //set the total
            }
        }
    }
    
    public class WrapTerm{
        public String term {get; set;}
        public Map<String,Decimal> data {get; set;}

        public WrapTerm(String term) {            
            this.term = term;
            this.data = new Map<String,Decimal>();
        }
    }
}