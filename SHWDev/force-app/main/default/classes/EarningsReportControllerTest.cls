@isTest
private class EarningsReportControllerTest {
    private static DataFactory df = new DataFactory();
    
    @isTest
    static void testController() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createRenewal();
        df.newPolicy.Status = 'Active';
        update df.newPolicy;
        
        Test.StartTest();
        // Set mock callout class for querying
        //Test.setMock(HttpCalloutMock.contractEarningsQuery.class, new MockHttpResponseGenerator.contractEarningsQuery());
                
        EarningsReportController customCon = new EarningsReportController();
        //system.debug(customCon.contractTermOptions);
        //customCon.policies = FALSE;
        
        //Test.setMock(HttpCalloutMock.renewalEarningsQuery.class, new MockHttpResponseGenerator.renewalEarningsQuery());
        customCon.renewals = TRUE;
        customCon.stateSelected.add('Delaware');
        customCon.getEarnings();
        Test.StopTest();
    }
}