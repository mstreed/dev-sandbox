public class EmailMessageController{
    public EmailMessage thisEmail {set; get;}

    public EmailMessageController( ApexPages.StandardController stdController) {
        this.thisEmail = (EmailMessage)stdController.getRecord();
    }
    public PageReference markRead() {
        thisEmail.Read__c = TRUE;
        update thisEmail;
        
        PageReference returnURL;
        returnURL = new PageReference('/'+thisEmail.Id);
        returnURL.getParameters().put('nooverride', '1');
        returnURL.setRedirect(true);
        return returnURL;    
    }
}