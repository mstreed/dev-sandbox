@isTest
private class EmailMessageControllerTest {
    private static DataFactory df = new DataFactory();

    @isTest static void caseTriggerHandlerTests() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        
        EmailMessage emlMsg = new EmailMessage(parentId = df.newCase.Id);
        insert emlMsg;
        
        Test.setCurrentPage( Page.EmailMessage );
        Test.startTest();
        ApexPages.currentPage().getParameters().put('id',emlMsg.id );
        ApexPages.StandardController controller = new ApexPages.StandardController(emlMsg);
        EmailMessageController extension = new EmailMessageController(controller);
        extension.markRead();
        
        Test.stopTest();
    }
}