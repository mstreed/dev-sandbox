@isTest
public class EmailPolicyCongaRedirectTest {
    static testMethod void testEmailPolicyCongaRedirect() {
        DataFactory df = new DataFactory();
        df.createGlobalSetting();
        df.createPolicy();
        
        Task emlMsg = new Task(
            WhatId = df.newPolicy.id,
            Status = 'Completed',
            TaskSubtype = 'Email'
        );
        Database.insert(emlMsg);
        
        Attachment att = new Attachment(
        	Body = Blob.valueOf('123'),
			Name = 'Select Home Warranty Policy ' + Date.today(),            
        	ParentId = df.newPolicy.id
        );
        Database.insert(att);
        
        Id attParent = [SELECT Id, ParentId FROM Attachment].ParentId;
		System.assertEquals(attParent, emlMsg.Id);
	}
}