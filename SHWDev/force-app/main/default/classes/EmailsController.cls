public without sharing class EmailsController {
    
    private Integer counter=0;  //keeps track of the offset
    private Integer list_size=500; //sets the page size or number of rows
    public Integer total_size; //used to show user the total size of the list
    public DateTime firstRecord;
    public DateTime lastRecord;
    private String button;

    public List<EmailWrap> emails {get;set;}
    public User currentUser {get;set;}
    public Boolean claims {get;set;}
    public Boolean contract {get;set;}
    public Boolean info {get;set;}
    public Boolean invoices {get;set;}
    public Boolean faxes {get;set;}
    private String whereClause;
    
    public EmailsController() {
        currentUser = [SELECT Id, Name, Profile.Name, Email2Case__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];

        if (currentUser.Email2Case__c == NULL && currentUser.Profile.Name.contains('Administrator')) {
            claims = true;
            contract = true;
            info = true;
            invoices = true;
            faxes = true;
        } else {
            claims = false;
            contract = false;
            info = false;
            invoices = false;
            faxes = false;
            
            if (currentUser.Email2Case__c != NULL) {
                if (currentUser.Email2Case__c.contains('Claims@')) {
                    claims = true;
                }
                if (currentUser.Email2Case__c.contains('Contract@')) {
                    contract = true;
                }
                if (currentUser.Email2Case__c.contains('Info@')) {
                    info = true;
                }
                if (currentUser.Email2Case__c.contains('Invoices@')) {
                    invoices = true;
                }
                if (currentUser.Email2Case__c.contains('Faxes@')) {
                    faxes = true;
                }
            } 
        }
        
        String queryString = 'SELECT count(Id) c FROM EmailMessage';
        whereClause = '';
        if (claims) {
            whereClause+= 'ToAddress LIKE \'%Claims@%\' OR FromAddress LIKE \'%Claims@%\' OR ';
        }
        /*if (contract) {
            whereClause+= 'ToAddress LIKE \'%Contract@%\' OR FromAddress LIKE \'%Contract@%\' OR ';
        }
        if (info) {
            whereClause+= 'ToAddress LIKE \'%Info@%\' OR FromAddress LIKE \'%Info@%\' OR ';
        }
        if (invoices) {
            whereClause+= 'ToAddress LIKE \'%Invoices@%\' OR FromAddress LIKE \'%Invoices@%\' OR ';
        }
        if (faxes) {
            whereClause+= 'ToAddress LIKE \'%Faxes@%\' OR FromAddress LIKE \'%Faxes@%\' OR ';
        }*/
        whereClause = whereClause.left(whereClause.length()-3) + ') AND Incoming = TRUE AND Parent.RecordType.Name = \'Claim\'';
        
        if (whereClause.length()>8) {
            queryString = queryString + ' WHERE (' + whereClause;
        }
        
        system.debug('queryString: ' + queryString);
        List<AggregateResult> ar = new List<AggregateResult>();
        ar = Database.query(queryString);
        for(AggregateResult a : ar){
            total_size = Integer.valueof(a.get('c')); //set the total size in the constructor
        }
        button = 'beginning';
        lastRecord = NULL;
        emails();
    }
    
    
    public void emails() {
        system.debug('getEmails');
        try {
            String queryString = 'SELECT Id, Parent.CaseNumber, Parent.OwnerId, Parent.Status, ToAddress, FromAddress, ActivityId, Subject, CreatedDate, Status, ValidatedFromAddress, ParentId, Parent.ParentId, Incoming, Read__c, HasAttachment, LastModifiedById, LastModifiedDate FROM EmailMessage';
            
            if (whereClause.length()>8) {
                queryString = queryString + ' WHERE (' + whereClause;
            }
            
            if (lastRecord != NULL) {
                if (button == 'beginning' || button == 'next') {
                    queryString+= ' AND CreatedDate < '+lastRecord.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
                } else { //previous or end
                    queryString+= ' AND CreatedDate > '+firstRecord.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
                }
            }
            
            if (button == 'beginning' || button == 'next') {
                queryString+= ' ORDER BY CreatedDate DESC ';
                firstRecord = NULL;
            } else { //previous or end
                queryString+= ' ORDER BY CreatedDate ASC ';
                lastRecord = NULL;
            }
            
            if (button == 'end' && math.mod(total_size, list_size) > 0) {
                queryString = queryString + 'LIMIT ' + math.mod(total_size, list_size);
            } else {
                queryString = queryString + 'LIMIT ' + list_size;
            }
            
            //queryString = queryString + 'ORDER BY CreatedDate DESC limit ' + list_size + ' offset ' + counter;

            system.debug('queryString: ' + queryString);
            emails = new List<EmailWrap>();
            for (EmailMessage eml : Database.query(queryString)) {
                if (eml.Subject == NULL) {
                    eml.Subject = '[NO SUBJECT]';
                }
                
                emails.add(new EmailWrap(eml));      
                
                if (button == 'beginning' || button == 'next') {
                    if (firstRecord == NULL) {
                        firstRecord = eml.CreatedDate;
                    }
                    lastRecord = eml.CreatedDate;
                } else { //previous or end 
                    firstRecord = eml.CreatedDate;
                    if (lastRecord == NULL) {
                        lastRecord = eml.CreatedDate;
                    }
                }
            }
            
            emails.sort();
        } catch (QueryException e) {
            ApexPages.addMessages(e);   
        }
    }
    
    public PageReference Beginning() { //user clicked beginning
        button = 'beginning';
        lastRecord = NULL;
        counter = 0;
        emails();
        return null;
    }
    
    public PageReference Previous() { //user clicked previous button
        button = 'previous';
        counter -= list_size;
        emails();
        return null;
    }
    
    public PageReference Next() { //user clicked next button
        button = 'next';
        counter += list_size;
        emails();
        return null;
    }
    
    public PageReference End() { //user clicked end
        button = 'end';
        lastRecord = NULL;
        counter = total_size - math.mod(total_size, list_size);
        emails();
        return null;
    }
    
    public Boolean getDisablePrevious() { 
        //this will disable the previous and beginning buttons
        if (counter>0) return false; else return true;
    }
    
    public Boolean getDisableNext() { //this will disable the next and end buttons
        if (counter + list_size < total_size) return false; else return true;
    }
    
    public Integer getTotal_size() {
        return total_size;
    }
    
    public Integer getPageNumber() {
        return counter/list_size + 1;
    }
    
    public Integer getTotalPages() {
        if (math.mod(total_size, list_size) > 0) {
            return total_size/list_size + 1;
        } else {
            return (total_size/list_size);
        }
    }
    
    public class EmailWrap implements Comparable {
        public DateTime createdDate {get;set;}
        public EmailMessage eml {get;set;}

        public EmailWrap(EmailMessage eml) {
            this.createdDate = eml.CreatedDate;
            this.eml = eml;
        } 
        public Integer compareTo(Object objToCompare) {
            EmailWrap ew = (EmailWrap)objToCompare;
            if (createdDate == ew.createdDate){
                return 0;
            } else if (createdDate < ew.createdDate){
                return 1;
            } else {
                return -1;        
            }
        }
    }
}