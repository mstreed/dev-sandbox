@isTest 
public class EmailsControllerTest {
    private static DataFactory df = new DataFactory();

    @isTest 
    static void testAsAdmin() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        
        List<EmailMessage> emlMsgs = new List<EmailMessage>();
        EmailMessage emlMsg1 = new EmailMessage();
        emlMsg1.FromAddress = 'invoices@selecthomewarranty.com';
        emlMsg1.ParentId = df.newCase.Id;
        emlMsg1.Subject = 'Subject';
        emlMsgs.add(emlMsg1);
        EmailMessage emlMsg2 = emlMsg1.clone(false,false,false,false);
        emlMsg2.FromAddress = 'claims@selecthomewarranty.com';
        emlMsg2.Subject = 'Subject';
        emlMsgs.add(emlMsg2);
        EmailMessage emlMsg3 = emlMsg1.clone(false,false,false,false);
        emlMsg3.FromAddress = 'info@selecthomewarranty.com';
        emlMsg3.Subject = 'Subject';
        emlMsgs.add(emlMsg3);
        EmailMessage emlMsg4 = emlMsg1.clone(false,false,false,false);
        emlMsg4.FromAddress = 'contract@selecthomewarranty.com';
        emlMsg4.Subject = 'Subject';
        emlMsgs.add(emlMsg4);
        EmailMessage emlMsg5 = emlMsg1.clone(false,false,false,false);
        emlMsg5.FromAddress = 'contract@selecthomewarranty.com';
        emlMsg5.Subject = NULL;
        emlMsgs.add(emlMsg5);
        EmailMessage emlMsg6 = emlMsg1.clone(false,false,false,false);
        emlMsg6.FromAddress = 'test@selecthomewarranty.com';
        emlMsg6.Subject = NULL;
        emlMsg6.Incoming = TRUE;
        emlMsg6.MessageDate = DateTime.now();
        emlMsgs.add(emlMsg6);
		insert emlMsgs;
        
        Test.StartTest();
        PageReference pageRef = Page.Emails;
        Test.setCurrentPage(pageRef);
        //ApexPages.StandardController controller = new ApexPages.StandardController();
        EmailsController con = new EmailsController(); 
        con.next();
        con.previous();
        con.beginning();
        con.end();
        con.getDisablePrevious();
        con.getDisableNext();
        con.getTotal_size();
        con.getPageNumber();
        con.getTotalPages();
        Test.stopTest();
    }
    
    @isTest 
    static void testAsUser() {
        Test.StartTest();

        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        
        List<EmailMessage> emlMsgs = new List<EmailMessage>();
        EmailMessage emlMsg1 = new EmailMessage();
        emlMsg1.FromAddress = 'invoices@selecthomewarranty.com';
        emlMsg1.ParentId = df.newCase.Id;
        emlMsg1.Subject = 'Subject';
        emlMsgs.add(emlMsg1);
        EmailMessage emlMsg2 = emlMsg1.clone(false,false,false,false);
        emlMsg2.FromAddress = 'claims@selecthomewarranty.com';
        emlMsg2.Subject = 'Subject';
        emlMsgs.add(emlMsg2);
        EmailMessage emlMsg3 = emlMsg1.clone(false,false,false,false);
        emlMsg3.FromAddress = 'info@selecthomewarranty.com';
        emlMsg3.Subject = 'Subject';
        emlMsgs.add(emlMsg3);
        EmailMessage emlMsg4 = emlMsg1.clone(false,false,false,false);
        emlMsg4.FromAddress = 'contract@selecthomewarranty.com';
        emlMsg4.Subject = 'Subject';
        emlMsgs.add(emlMsg4);
		insert emlMsgs;
        
        User testUser = [SELECT Id FROM User WHERE Email2Case__c != NULL AND (NOT Profile.Name LIKE '%Administrator%') AND IsActive = TRUE LIMIT 1];
        System.runAs(testUser) {
            PageReference pageRef = Page.Emails;
            Test.setCurrentPage(pageRef);
            //ApexPages.StandardController controller = new ApexPages.StandardController();
            EmailsController con = new EmailsController(); 
            con.next();
            con.previous();
            con.beginning();
            con.getDisablePrevious();
            con.getDisableNext();
            con.getTotal_size();
            con.getPageNumber();
            con.getTotalPages();
            con.end();
        }
        Test.stopTest();

    }
}