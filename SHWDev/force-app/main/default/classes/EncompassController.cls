public class EncompassController {
    public String model {get;set;}
    public User currentUser {get;set;}
    public String encompassUsername {get;set;}
    public String encompassPassword {get;set;}
    
    
    public EncompassController() { 
        currentUser = [SELECT Id, Order_on_Encompass__c, Profile.Name FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        if (currentUser.Profile.Name.contains('Administrator') || currentUser.Order_on_Encompass__c) {
            encompassUsername = '172537';
            encompassPassword = '172537';
        } else {
            encompassUsername = '203824';
            encompassPassword = '1234';
        }

        model = ApexPages.currentPage().getParameters().get('model');
    }
}