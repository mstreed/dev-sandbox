@isTest
private class EncompassControllerTest {
    private static DataFactory df = new DataFactory();
    
    @isTest 
    static void controllerTestAsAdmin() {
        Test.StartTest();
        PageReference pageRef = Page.Encompass;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('model','12345');
        EncompassController controller = new EncompassController();
        Test.stopTest();
    }
    
    @isTest 
    static void controllerTestAsNonAdmin() {
        Test.StartTest();
        User u1 = [SELECT Id from User WHERE IsActive = TRUE AND Profile.Name = 'SHW Claims' AND Order_on_Encompass__c = FALSE LIMIT 1];        
        System.runas(u1) {
            PageReference pageRef = Page.Encompass;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('model','12345');
            EncompassController controller = new EncompassController();
        }
        Test.stopTest();
    }
}