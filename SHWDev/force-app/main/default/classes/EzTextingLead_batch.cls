global class EzTextingLead_batch implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {
    global static Global_Setting__c gs = [Select AES256_Key__c, EZ_Texting_Username__c, EZ_Texing_Password_Encrypted__c
                                          FROM Global_Setting__c LIMIT 1];
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([SELECT Id, EZ_Texting_Queue_Groups__c FROM Lead WHERE EZ_Texting_Queue_Groups__c != NULL]);
    }

    global void execute(Database.BatchableContext BC, List<Lead> scope) {
        List<Lead> ldsToUpdate = new List<Lead>();
        for (Lead ld : scope) {
            system.debug('ld: ' + ld);
            
            ld = EzTextCampaignAdd(ld.Id, ld.EZ_Texting_Queue_Groups__c);
            ld.EZ_Texting_Queue_Groups__c = NULL;
            system.debug('ld2: ' + ld);
            ldsToUpdate.add(ld);
        }
        
        update ldsToUpdate;
    }   

    global void finish(Database.BatchableContext BC) {
    }
    
    private static Lead EzTextCampaignAdd(Id recordId, String campaigns){
        system.debug('EzTextCampaign');
        system.debug('recordId: ' + recordId);
        system.debug('campaigns: ' + campaigns);
        
        Lead ld;
        //Contract pol;
        String cleanFirstName;
        String cleanLastName;
        String cleanEmail;
        String cleanPhone;
        String exTextId;
        
        if (String.valueOf(recordId).left(3) == '00Q') { //Lead
            ld = [SELECT Id, FirstName, LastName, Email, Phone, EZ_Texting_ID__c, EZ_Texting_Groups__c, EZ_Texting_Queue_Groups__c FROM Lead WHERE Id = :recordId];
            cleanFirstName = ld.FirstName.substring(0, Math.min(ld.FirstName.length(), 10));
            cleanLastName = ld.LastName.substring(0, Math.min(ld.LastName.length(), 10));
            cleanEmail = ld.Email.substring(0, Math.min(ld.Email.length(), 40));
            cleanPhone = ld.Phone.replaceAll('\\D','');
            cleanPhone = cleanPhone.substring(0, Math.min(cleanPhone.length(), 10));
            exTextId = ld.EZ_Texting_ID__c;
        }
        
        system.debug('cleanPhone: ' + cleanPhone);
        
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        
        /*TO REENCRYPT PW
        Blob key = EncodingUtil.base64Decode(gs.AES256_Key__c);
        Blob data = Blob.valueOf('PasswordString');
        Blob encrypted = Crypto.encryptWithManagedIV('AES256', key, data);
        system.debug(EncodingUtil.base64Encode(encrypted));*/ 
        
        String password = Crypto.decryptWithManagedIV('AES256', EncodingUtil.base64Decode(gs.AES256_Key__c), EncodingUtil.base64Decode(gs.EZ_Texing_Password_Encrypted__c)).toString();
        
        //Find Lead/Contract via Id
        if (exTextId != NULL) {
            req = new HttpRequest();
            req.setEndpoint( 'https://app.eztexting.com/contacts/'+exTextId+'?format=json&User='+gs.EZ_Texting_Username__c+'&Password='+password);
            req.setMethod( 'GET' );
            System.debug('req-addToEzText-viaId: ' + req );
            System.debug('reqBody-addToEzText-viaId: ' + req.getBody());
        } else { //Find Lead/Contract via Phone
            if (cleanPhone != NULL) {
                req = new HttpRequest();
                req.setEndpoint( 'https://app.eztexting.com/contacts?format=json&User='+gs.EZ_Texting_Username__c+'&Password='+password+'&query='+cleanPhone);
                req.setMethod( 'GET' );
                System.debug('req-addToEzText-viaPhone: ' + req );
                System.debug('reqBody-addToEzText-viaPhone: ' + req.getBody());
            }
        }
        
        EZTextingJSON.entryWrapper ezTextContact = new EZTextingJSON.entryWrapper();
        
        if (req != NULL) {
            req.setTimeout(50000);
            res = new Http().send( req );
            System.debug('res-addToEzText-findRequest: ' + res );
            System.debug('res.getStatus()-addToEzText-findRequest: ' + res.getStatus());
            System.debug('res.getBody()-addToEzText-findRequest: ' + res.getBody());
            
            EZTextingJSON ezText = EZTextingJSON.parse(res.getBody());
            system.debug('ezText: '+ ezText);
            
            if (ezText.Response.Entry != NULL) { //id search
                ezTextContact = ezText.Response.Entry;
            } else if (ezText.Response.Entries != NULL && !ezText.Response.Entries.isEmpty()) { //query
                for (EZTextingJSON.entryWrapper entry : ezText.Response.Entries) {
                    system.debug('entry: '+ entry);
                    if (entry.PhoneNumber == cleanPhone) {
                        ezTextContact = entry;
                        Break;
                    }
                }
            } else { //no match! create contact if adding to campaign, otherwise, skip
                req = new HttpRequest();
                req.setEndpoint( 'https://app.eztexting.com/contacts?format=json&User='+gs.EZ_Texting_Username__c+'&Password='+password);
                req.setMethod('POST');    
                req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
                String payload = 'PhoneNumber='+cleanPhone;
                System.debug(payload);
                req.setBody(payload);
                req.setTimeout(50000);
                res = new Http().send( req );
                System.debug('res-addToEzText-createRequest: ' + res );
                System.debug('res.getStatus()-addToEzText-createRequest: ' + res.getStatus());
                System.debug('res.getBody()-addToEzText-createRequest: ' + res.getBody());
                
                ezText = EZTextingJSON.parse(res.getBody());
                system.debug('ezText: '+ ezText);
                
                //assign contact
                ezTextContact = ezText.Response.Entry;
            }
        }
        
        if (ezTextContact != NULL) {
            //set groups
            String groupsPayload = '';
            String groups = '';
            
            Integer i = 0;
            system.debug('add to groups');
            List<String> groupList = ld.EZ_Texting_Queue_Groups__c.split(', ');
            //loop through groups queue and add
            for (String grp : groupList) {
                if (groups == '') {
                    groups = grp;
                } else {
                    groups = groups + ', ' + grp;
                }
                groupsPayload = groupsPayload + '&Groups[' + i + ']=' + grp;
                i++;
            }
            system.debug('groups: ' + groups);
            system.debug('groupsPayload: ' + groupsPayload);
            
            //add existing eztext groups
            if (ezTextContact.Groups != NULL) {
                for (String grp : ezTextContact.Groups) {
                    if (grp == 'No Campaigns' || groupList.contains(grp)) {
                        Break;
                    }
                    groupsPayload = groupsPayload + '&Groups[' + i + ']=' + grp;
                    groups = groups + ', ' + grp;
                    i++;
                }
            }
            
            //if there are no groups, set to default 'No Campaigns'
            if (groupsPayload == '') {
                groupsPayload = '&Groups=No Campaigns';
                groups = 'No Campaigns';
            } 
            
            system.debug('groups: ' + groups);
            
            //update contact groups and details
            req = new HttpRequest();
            req.setEndpoint( 'https://app.eztexting.com/contacts/'+ezTextContact.Id+'?format=json&User='+gs.EZ_Texting_Username__c+'&Password='+password);
            req.setMethod('POST');    
            req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            String payload = 'PhoneNumber='+cleanPhone+'&FirstName='+cleanFirstName+'&LastName='+cleanLastName+'&Email='+cleanEmail+'&Custom1='+String.valueOf(recordId).substring(0, 10)+'&Custom2='+String.valueOf(recordId).substring(10, 18)+groupsPayload;
            
            System.debug(payload);
            req.setBody(payload);
            req.setTimeout(50000);
            res = new Http().send( req );
            System.debug('res-addToEzText-updateRequest: ' + res );
            System.debug('res.getStatus()-addToEzText-updateRequest: ' + res.getStatus());
            System.debug('res.getBody()-addToEzText-updateRequest: ' + res.getBody());
            
            if (String.valueOf(recordId).left(3) == '00Q') { //Lead
                ld.EZ_Texting_ID__c = ezTextContact.Id;
                ld.EZ_Texting_Groups__c = groups;
            }
        }
        return ld;
    }
}