global class EzTextingLead_schedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        EzTextingLead_batch batchRun = new EzTextingLead_batch(); 
        ID batchId = Database.executeBatch(batchRun,1);
        
    }
}