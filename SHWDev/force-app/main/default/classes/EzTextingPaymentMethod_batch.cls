global class EzTextingPaymentMethod_batch implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {
    global static Global_Setting__c gs = [Select AES256_Key__c, EZ_Texting_Username__c, EZ_Texing_Password_Encrypted__c
                                          FROM Global_Setting__c LIMIT 1];
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([SELECT Id, Policy_Name__r.Name__c, Policy_Name__r.First_Name__c, Policy_Name__r.Contract_Number__c, Policy_Name__r.Home_Phone__c, Customer_Access_Code__c, Payment_Method_URL__c, Send_Text_Error_Declined_Date_Time__c, Send_Text_Error_Declined_Status__c
                                         FROM Payment_Method__c 
                                         WHERE Send_Text_Error_Declined__c = TRUE]);
    }

    global void execute(Database.BatchableContext BC, List<Payment_Method__c> scope) {
        for (Payment_Method__c pm : scope) {
            system.debug('pm: ' + pm);
            pm.Send_Text_Error_Declined_Status__c = EzTextCampaignSendText(pm);
            pm.Send_Text_Error_Declined_Date_Time__c = DateTime.now();
        }
        update scope;
    }   

    global void finish(Database.BatchableContext BC) {
    }
    
    private static String EzTextCampaignSendText(Payment_Method__c pm){
        system.debug('EzTextCampaignSendText-PaymentMethod');
        
        String cleanPhone;
        cleanPhone = pm.Policy_Name__r.Home_Phone__c.replaceAll('\\D','');
        cleanPhone = cleanPhone.substring(0, Math.min(cleanPhone.length(), 10));

        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        String password = Crypto.decryptWithManagedIV('AES256', EncodingUtil.base64Decode(gs.AES256_Key__c), EncodingUtil.base64Decode(gs.EZ_Texing_Password_Encrypted__c)).toString();

        req = new HttpRequest();
        req.setEndpoint( 'https://app.eztexting.com/sending/messages?format=json&User='+gs.EZ_Texting_Username__c+'&Password='+password);
        req.setMethod('POST');    
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        String payload1 = 'PhoneNumbers[0]='+cleanPhone+'&Subject=Select Home Warranty&Message=Dear '+pm.Policy_Name__r.First_Name__c+',\nThere was a problem processing your payment for Policy #'+pm.Policy_Name__r.Contract_Number__c+'.';
        System.debug(payload1);
        req.setBody(payload1);
        req.setTimeout(50000);
        res = new Http().send( req );
        System.debug('res-EzTextCampaignSendText1: ' + res );
        System.debug('res.getStatus()-EzTextCampaignSendText1: ' + res.getStatus());
        System.debug('res.getBody()-EzTextCampaignSendText1: ' + res.getBody());
        
        String payload2 = 'PhoneNumbers[0]='+cleanPhone+'&Message=Proceed to the following link to update your credit card.\n'+pm.Payment_Method_URL__c+'%26code='+pm.Customer_Access_Code__c;
        System.debug(payload2);
        req.setBody(payload2);
        req.setTimeout(50000);
        res = new Http().send( req );
        System.debug('res-EzTextCampaignSendText2: ' + res );
        System.debug('res.getStatus()-EzTextCampaignSendText2: ' + res.getStatus());
        System.debug('res.getBody()-EzTextCampaignSendText2: ' + res.getBody());
        
        EZTextingJSON ezText = EZTextingJSON.parse(res.getBody());
        system.debug('ezText: '+ ezText);
        
        String responseStatus = ezText.Response.Status;
        if (ezText.Response.Errors != NULL) {
            responseStatus = responseStatus + ' ' + ezText.Response.Errors[0];
        }
        
        return responseStatus;
    }
}