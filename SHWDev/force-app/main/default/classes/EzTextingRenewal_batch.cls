global class EzTextingRenewal_batch implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {
    global static Global_Setting__c gs = [Select AES256_Key__c, EZ_Texting_Username__c, EZ_Texing_Password_Encrypted__c
                                          FROM Global_Setting__c LIMIT 1];
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([SELECT Id, Policy__r.Name__c, Policy__r.First_Name__c, Policy__r.Contract_Number__c, Policy__r.Home_Phone__c, Renewal_Response_URL__c, Send_Text_Date_Time__c, Send_Text_Status__c
                                         FROM Renewal_Response__c 
                                         WHERE Send_Text__c = TRUE]);
    }

    global void execute(Database.BatchableContext BC, List<Renewal_Response__c> scope) {
        for (Renewal_Response__c rr : scope) {
            system.debug('rr: ' + rr);
            rr.Send_Text_Status__c = EzTextCampaignSendText(rr);
            rr.Send_Text_Date_Time__c = DateTime.now();
        }
        update scope;
    }   

    global void finish(Database.BatchableContext BC) {
    }
    
    private static String EzTextCampaignSendText(Renewal_Response__c rr){
        system.debug('EzTextCampaignSendText-RenewalResponse');
        
        String cleanPhone;
        cleanPhone = rr.Policy__r.Home_Phone__c.replaceAll('\\D','');
        cleanPhone = cleanPhone.substring(0, Math.min(cleanPhone.length(), 10));

        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        String password = Crypto.decryptWithManagedIV('AES256', EncodingUtil.base64Decode(gs.AES256_Key__c), EncodingUtil.base64Decode(gs.EZ_Texing_Password_Encrypted__c)).toString();

        req = new HttpRequest();
        req.setEndpoint( 'https://app.eztexting.com/sending/messages?format=json&User='+gs.EZ_Texting_Username__c+'&Password='+password);
        req.setMethod('POST');    
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        String payload1 = 'PhoneNumbers[0]='+cleanPhone+'&Subject=Select Home Warranty&Message=Dear '+rr.Policy__r.First_Name__c+',\nYour Policy#'+rr.Policy__r.Contract_Number__c+' is expiring soon.';
        System.debug(payload1);
        req.setBody(payload1);
        req.setTimeout(50000);
        res = new Http().send( req );
        System.debug('res-EzTextCampaignSendText1: ' + res );
        System.debug('res.getStatus()-EzTextCampaignSendText1: ' + res.getStatus());
        System.debug('res.getBody()-EzTextCampaignSendText1: ' + res.getBody());
        
        String payload2 = 'PhoneNumbers[0]='+cleanPhone+'&Message=Proceed to the following link to let us know if you would like to renew your policy.\n'+rr.Renewal_Response_URL__c;
        System.debug(payload2);
        req.setBody(payload2);
        req.setTimeout(50000);
        res = new Http().send( req );
        System.debug('res-EzTextCampaignSendText2: ' + res );
        System.debug('res.getStatus()-EzTextCampaignSendText2: ' + res.getStatus());
        System.debug('res.getBody()-EzTextCampaignSendText2: ' + res.getBody());
        
        EZTextingJSON ezText = EZTextingJSON.parse(res.getBody());
        system.debug('ezText: '+ ezText);
        
        String responseStatus = ezText.Response.Status;
        if (ezText.Response.Errors != NULL) {
            responseStatus = responseStatus + ' ' + ezText.Response.Errors[0];
        }
        
        return responseStatus;
    }
}