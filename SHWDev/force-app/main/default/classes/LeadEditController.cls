public class LeadEditController{
    public Lead thisLead {set; get;}
    public String errorMsg{set;get;} 
    public String rType {get; set;}
    public String retURL {get; set;}
    public String ent {get; set;}
    public RecordType recordType{get;set;}
    public List<Lead> existingLeads {set; get;}

    public LeadEditController( ApexPages.StandardController stdController) {
        this.thisLead = (Lead)stdController.getRecord();
        
        retURL = ApexPages.currentPage().getParameters().get('retURL');
        rType = ApexPages.currentPage().getParameters().get('RecordType');
        ent = ApexPages.currentPage().getParameters().get('ent');
        recordType = [
            SELECT
            Id,
            Name,
            DeveloperName
            FROM RecordType
            WHERE SobjectType ='Lead' 
            AND DeveloperName = 'Queue_Lead' 
            LIMIT 1
        ];
        thisLead.CountryCode = 'US';
        thisLead.Total_Price_Manual__c = 499.99;
    }
    public PageReference redirect() {
        PageReference returnURL;
        // Purpose: To redirect the Custom Lead Edit Page when the Record type is Queue Lead on new button
        if(rType == recordType.Id || rType  == null) {
            return NULL;    
        }  
        System.debug('Yes I am here');
        // Redirect differnt recordtype standard page on new button
        Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe();
        Schema.SObjectType sobjType = gd.get('Lead');
        Schema.DescribeSObjectResult result = sobjType.getDescribe(); 
        returnURL = new PageReference('/'+result.getKeyPrefix()+'/e');
        returnURL.getParameters().put('retURL', retURL);
        returnURL.getParameters().put('RecordType', rType);
        returnURL.getParameters().put('ent', ent);
        returnURL.getParameters().put('nooverride', '1');
        returnURL.setRedirect(true);
        return returnURL;    
    }
    public PageReference saveRecord(){
        errorMsg ='';
        try {
            // insert lead fields
            if( errorMsg == '') {
                
                if (thisLead.Exclude_De_Dupe__c == false) {
                    checkForDupe();
                }
                
                if (existingLeads.size()>0 && thisLead.Exclude_De_Dupe__c == false) {
                    return null;
                } else {
                    //To assign the Queue Lead record type id before insert
                    thisLead.recordTypeId =  recordType.Id; 
                    thisLead.Email_with_Multiple_Address__c = thisLead.Email;
                    
                    thisLead.Grace_Period_Days_Manual_Override__c = 30;
                    
                    insert thisLead; 
                }
            }
        } catch ( Exception err ) {
            if ( String.isNotBlank( err.getMessage()) && err.getMessage().contains( 'error:' ) )
                errorMsg = err.getMessage().split('error:')[1].split(':')[0] + '.';
            else
                errorMsg = err.getMessage();  
        }
        return NULL;
    }
    
    public void checkForDupe() {
        //check if there is a parent lead
        existingLeads = new List<Lead>();
        existingLeads = [SELECT Id, Email_with_Multiple_Address__c
                         FROM Lead 
                         WHERE RecordType.DeveloperName = 'Queue_Lead' AND IsConverted = FALSE AND Parent_Lead__c = null AND Parent_Policy__c = null AND Email_with_Multiple_Address__c = :thisLead.Email ORDER BY CreatedDate];
    }
}