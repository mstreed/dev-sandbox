/*

    @Description    LeadEditController Class 
    @createdate     20 May 2018

*/
@isTest
private class LeadEditControllerTest {
    private static DataFactory df = new DataFactory();
    private static recordType rt = [SELECT Id,Name,DeveloperName FROM RecordType WHERE SobjectType ='Lead' AND DeveloperName = 'Queue_Lead' LIMIT 1];
    
    @isTest 
    static void testRedirectQueueLead() {
        df.createGlobalSetting();
        df.createLead();

        Test.StartTest();
        PageReference pageRef = Page.LeadEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('RecordType',rt.id); 
        ApexPages.currentPage().getParameters().put('retURL','/00Q/o');
        ApexPages.currentPage().getParameters().put('ent','Lead');
        ApexPages.currentPage().getParameters().put('id','test');
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newlead);
        LeadEditController extension = new LeadEditController(controller);
        extension.redirect();
        Test.StopTest();
    }
    
    @isTest 
    static void testRedirectOther() {
        df.createGlobalSetting();
        df.createLead();

        Test.StartTest();
        Id rType = [SELECT Id FROM RecordType WHERE DeveloperName = 'Contractor'].Id;
        PageReference pageRef = Page.LeadEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('RecordType',rType); 
        ApexPages.currentPage().getParameters().put('retURL','/00Q/o');
        ApexPages.currentPage().getParameters().put('ent','Lead');
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newlead);
        LeadEditController extension = new LeadEditController(controller);
        extension.redirect();
        Test.StopTest();
    }
    @isTest 
    static void testDuplicate() {
        df.createGlobalSetting();
        df.createLead();

        Test.StartTest();
        PageReference pageRef = Page.LeadEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newlead);
        LeadEditController extension = new LeadEditController(controller);
        extension.thisLead = df.newLead;
        extension.saveRecord();
        extension.thisLead.Exclude_De_Dupe__c = true;
        extension.saveRecord();
        
        Lead dupeLead = df.newlead.clone(false, true, false, false);
        dupeLead.Exclude_De_Dupe__c = false;
        insert dupeLead;
        
        Test.StopTest();
    }
}