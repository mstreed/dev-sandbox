global class LeadOwnership_batch implements Database.Batchable<sObject>, Database.Stateful {

    private Id affId;
    private Id ownerId;
    private Boolean includeDupes;
    global static Global_Setting__c gs = [Select Id, System_Administrator_User_ID__c, Affiliate_Lead_Access_Start_Date__c FROM Global_Setting__c LIMIT 1];

    public LeadOwnership_batch(Id affiliteId, Id partnerId, Boolean dupes) {
        affId = affiliteId;
        includeDupes = dupes;
        ownerId = partnerId;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id, Status, OwnerId, Affiliate_Account__c, CreatedDate FROM Lead WHERE Affiliate_Account__c = \'' + affId +'\'';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Lead> scope) {
        system.debug('==========check string====='+affId);    
        
        for (Lead ld : scope) {
            if (ownerId == NULL) {
                ld.OwnerId = gs.System_Administrator_User_ID__c;
            } else {
                if ((ld.Status == 'New' || ld.Status == 'Proforma' || ld.Status == 'To Be Billed' || ld.Status == 'GAQ Order' || ld.Status == 'Warranty Sold' || (ld.Status == 'Duplicate Lead' && includeDupes)) &&
                    ld.CreatedDate >= gs.Affiliate_Lead_Access_Start_Date__c) {
                        ld.OwnerId = ownerId;
                    } else {
                        ld.OwnerId = gs.System_Administrator_User_ID__c;
                    }
            }
        }
        
        update scope;
    }   

    global void finish(Database.BatchableContext BC) {
    }
}