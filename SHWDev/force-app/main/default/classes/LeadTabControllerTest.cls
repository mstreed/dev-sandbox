/*

@Description    LeadTabController Class 
@createdate     20 May 2018

*/
@isTest
private class LeadTabControllerTest {
    private static DataFactory df = new DataFactory();

    @isTest
    static void testOffsets() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createLead();
        df.createAdminNoteLead();
        
        Admin_Note__c adNote2 = df.adminNoteLead.clone(false, true, false, false);
        insert adNote2;
        
        View__c logView2 = new View__c(
            Lead__c = df.newLead.id,
            Viewed_By__c = UserInfo.getUserId(),
            Viewed_On__c = DateTime.now()
        );
        insert logView2;
        
        ApexPages.currentPage().getParameters().put('id',df.newlead.Id);
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newlead);
        LeadTabController extension = new LeadTabController(controller);
        extension.list_size = 1;
        extension.NotesEnd();
        extension.NotesBeginning();
        extension.NotesNext();
        extension.NotesPrevious();
        extension.getNotesPageNumber();
        extension.getNotesTotalPages();
        
        extension.ViewsEnd();
        extension.ViewsBeginning();
        extension.ViewsNext();
        extension.ViewsPrevious();
        extension.getViewsPageNumber();
        extension.getViewsTotalPages();
        
        Test.StopTest();
    }
    
    @isTest
    static void testNoLeadId() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createLead();
        
        ApexPages.currentPage().getParameters().put('id',NULL);
        System.assertEquals( NULL, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newlead );
        LeadTabController extension = new LeadTabController(controller);
        System.assertEquals( extension.errorMsg,'No Lead Id provided.' );
        Test.StopTest();
    }
    
    @isTest
    static void testCannotFindLead() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createLead();
        
        ApexPages.currentPage().getParameters().put('id','00Q1I00000MFgRB');
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newlead);
        LeadTabController extension = new LeadTabController(controller);
        System.assertEquals( extension.errorMsg,'Cannot Find Lead.' );
        Test.StopTest();
    }
    
    @isTest
    static void testStateNotCovered() {
        Test.StartTest();
        df.createGlobalSetting();
        df.setupUsers();
        df.createLead();
        
        df.newlead.Status = 'State Not Covered';
        update df.newLead;
        
        system.runAs(df.userSales) {
            ApexPages.currentPage().getParameters().put('id',df.newlead.id );
            System.assertEquals( df.newlead.id, Apexpages.currentpage().getparameters().get( 'id' ) );
            ApexPages.StandardController controller = new ApexPages.StandardController(df.newlead);
            LeadTabController extension = new LeadTabController(controller);
        } 
        
        Test.StopTest();
    }
    
    @isTest
    static void testDeleteRecord() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createLead();
        df.createPaymentMethodsLead();
        df.createOrdersLead();
        df.createTransactionLead();
        
        ApexPages.currentPage().getParameters().put('id',df.newlead.id );
        System.assertEquals( df.newlead.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newlead);
        LeadTabController extension = new LeadTabController(controller);
        
        extension.ObjectName = 'ChargentOrders__ChargentOrder__c';
        extension.deleteRelatedRecordId = df.boListLead[1].id;
        extension.deleteRecord();
        
        extension.ObjectName = 'ChargentOrders__Transaction__c';
        extension.deleteRelatedRecordId = df.transactionLead.id;
        extension.deleteRecord();
        
        extension.ObjectName = 'test';
        extension.deleteRelatedRecordId = 'testId';
        //delete record 
        extension.deleteRecord();
        
        //delete record exception
        System.assert( extension.errorMsg.contains('sObject type \'test\' is not supported.'));
        Test.StopTest();
    }
    @isTest
    static void testConvertLead() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createLead();
        df.createPaymentMethodsLead();
        df.createOrdersLead();
        df.createTransactionLead();
        df.createAdminNoteLead();
        RecursiveTriggerHandler.isFirstTime = true;

        ApexPages.currentPage().getParameters().put('id',df.newlead.id );
        System.assertEquals( df.newlead.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newlead );
        LeadTabController extension = new LeadTabController(controller);
        extension.thisLead.Months_Free__c = '1 Month';
        extension.thisLead.Contract_Term__c = '2 Year';
        extension.updateRecord();
        extension.convertSalesLead();
        Test.StopTest();
    }
    
    @isTest
    static void testConvertBilledInLead() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createLead();
        df.createPolicy();
        
        Payment_Method__c pMethodBilledIn = new Payment_Method__c(
            Lead_Name__c = df.newlead.id,
            Payment_Method__c = 'Billed In',
            Set_as_default__c = true,
            Billed_In__c = df.newPolicy.Id
        );
        insert pMethodBilledIn;
        
        ApexPages.currentPage().getParameters().put('id',df.newlead.id );
        System.assertEquals( df.newlead.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newlead );
        LeadTabController extension = new LeadTabController(controller);
        extension.thisLead.Months_Free__c = '1 Month';
        extension.thisLead.Contract_Term__c = '2 Year';
        extension.convertSalesLead();
        Test.StopTest();
    }
    
    @isTest
    static void testDupeLead() {
        Test.StartTest();

        df.createGlobalSetting();
        df.createLead();
        df.createPolicy();
        
        Lead dupeLead1 = df.newlead.clone(false, true, false, false);
        insert dupeLead1;
        Lead dupeLead2 = df.newlead.clone(false, true, false, false);
        dupeLead2.Parent_Lead__c = df.newLead.Id;
        insert dupeLead2;
        
        ApexPages.currentPage().getParameters().put('id',df.newlead.id );
        System.assertEquals( df.newlead.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newlead );
        LeadTabController extension = new LeadTabController(controller);
        
        extension.removeDupeId = dupeLead1.Id;
        extension.removeDupe();
        
        ApexPages.currentPage().getParameters().put('id',dupeLead2.id );
        System.assertEquals( dupeLead2.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        controller = new ApexPages.StandardController( dupeLead2 );
        extension = new LeadTabController(controller);
        extension.redirectLead();
        
        Test.StopTest();
    }
    
    @isTest
    static void testDupeLead2() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createLead();
        df.createPolicy();
        
        Lead dupeLead3 = df.newlead.clone(false, true, false, false);
        dupeLead3.Parent_Policy__c = df.newPolicy.Id;
		dupeLead3.Exclude_De_Dupe__c = true;
        insert dupeLead3;
        Lead dupeLead4 = df.newlead.clone(false, true, false, false);
        dupeLead4.Exclude_De_Dupe__c = true;
        insert dupeLead4;
        
        ApexPages.currentPage().getParameters().put('id',df.newlead.id );
        System.assertEquals( df.newlead.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newlead );
        LeadTabController extension = new LeadTabController(controller);

        ApexPages.currentPage().getParameters().put('id',dupeLead3.id );
        System.assertEquals( dupeLead3.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        controller = new ApexPages.StandardController( dupeLead3 );
        extension = new LeadTabController(controller);
        extension.redirectLead();
        
        ApexPages.currentPage().getParameters().put('id',dupeLead4.id );
        System.assertEquals( dupeLead4.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        controller = new ApexPages.StandardController( dupeLead4 );
        extension = new LeadTabController(controller);
        extension.mergeToPolicy();
        
        Test.StopTest();
    }
    
    @isTest
    static void testDupeLead3GAQ() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createLead();
                
        Lead dupeLead1 = df.newlead.clone(false, true, false, false);
        insert dupeLead1;
        Lead dupeLead2 = df.newlead.clone(false, true, false, false);
        dupeLead2.Parent_Lead__c = df.newLead.Id;
        insert dupeLead2;
        
        ChargentOrders__ChargentOrder__c gaqOrder = new ChargentOrders__ChargentOrder__c(
            Lead__c = dupeLead2.id,
            Payment_Terms_New__c = '2 Payments',
            Payment_Method__c = 'One Time',
            ChargentOrders__Card_Number__c = '4111111111111111',
            ChargentOrders__Card_Expiration_Month__c = '05',
            ChargentOrders__Card_Expiration_Year__c = '3000',
            ChargentOrders__Charge_Amount__c = 20.00,
            Create_from_Web__c = TRUE
        );
        insert gaqOrder;
        
        Test.StopTest();
    }
    
    @isTest
    static void testContractorLead() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createContractorLead();
        
        ApexPages.currentPage().getParameters().put('id',df.newContractorLead.id);
        System.assertEquals(df.newContractorLead.id, Apexpages.currentpage().getparameters().get('id'));
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newContractorLead);
        LeadTabController extension = new LeadTabController(controller);
        extension.updateRecord();
        extension.thisLead.Company = NULL;
        extension.updateRecord();
        extension.thisLead.Email = NULL;
        extension.updateRecord();
        
        PageReference checkPg = extension.convertNonSales();
        Test.StopTest();
    }
    
    @isTest
    static void testDeclarationVariables() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createLead();
        df.createPaymentMethodsLead();
        df.createOrdersLead();
        
        ApexPages.currentPage().getParameters().put('id',df.newlead.id );
        System.assertEquals( df.newlead.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newlead );
        LeadTabController extension = new LeadTabController(controller);
        system.debug('chargeReasonList: ' + extension.chargeReasonList);

        Test.StopTest();
    }
    
    @isTest
    static void testUpdateSalesLeadRecord1() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createLead();
        df.createPaymentMethodsLead();
        df.createOrdersLead();
        
        ApexPages.currentPage().getParameters().put('id',df.newlead.id );
        System.assertEquals( df.newlead.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newlead );
        LeadTabController extension = new LeadTabController(controller);

        extension.removeBounce();
        extension.thisLead.Email_with_Multiple_Address__c = 'test@test.com,test1@test.com';
        extension.thisLead.Contract_Term__c = '4 Year';
        extension.thisLead.Months_Free__c = '1 Month';
        extension.thisLead.Grace_Period_Days_Manual_Override__c = 5;
        extension.thisLead.Discount_Percentage__c = '5';
        extension.UpdateRecord();
        extension.thisLead.Contract_Term__c = NULL;
        extension.thisLead.Email_with_Multiple_Address__c = 'test@test.com';
        extension.UpdateRecord();
        extension.thisLead.Contract_Term__c = '1 Year';
        extension.thisLead.Months_Free__c = NULL;
        extension.thisLead.Grace_Period_Days_Manual_Override__c = NULL;
        extension.UpdateRecord();
        extension.thisLead.Contract_Term__c = NULL;
        extension.UpdateRecord();
        extension.thisLead.X1st_Service_Call_Free__c = TRUE;
        extension.thisLead.No_Cancellation_Fee__c = TRUE;
        extension.UpdateRecord();
        System.assertEquals( extension.errorMsg.contains('Argument'), false);
        
        extension.createSpecialRequest();
        extension.getSpecialRequestDepts();
        extension.specialRequestDepartment = 'Sales';
        extension.specialRequestComment = '123';
        extension.getDiscountAndMonthsFree();

        extension.thisLead.Email_with_Multiple_Address__c = NULL;
        extension.UpdateRecord();
        extension.thisLead.Sales_Agent_Lookup__c = NULL;
        extension.UpdateRecord();
        extension.thisLead.Phone = NULL;
        extension.UpdateRecord();
        extension.thisLead.LastName = NULL;
        extension.UpdateRecord();
        extension.thisLead.FirstName = NULL;
        extension.UpdateRecord();
        extension.thisLead.Status = NULL;
        extension.UpdateRecord();
        
        Test.StopTest();
    }
    
    @isTest
    static void testUpdateSalesLeadRecord2() {
        Test.StartTest();

        df.createGlobalSetting();
        df.createLead();
        
        ApexPages.currentPage().getParameters().put('id',df.newlead.id );
        System.assertEquals( df.newlead.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newlead );
        LeadTabController extension = new LeadTabController(controller);
        
        RecursiveTriggerHandler.isFirstTime = true;
        
        extension.thisLead.DoNotCall = True;
        extension.thisLead.Status = 'Proforma';
        extension.thisLead.Proforma_Closing_Date__c = Date.today();
        extension.thisLead.Realtor_s_Email__c = 'test@test.com';
        extension.thisLead.StateCode = 'CA';
        extension.thisLead.Pool__c = TRUE;
        extension.thisLead.Central_Vacuum__c = TRUE;
        extension.thisLead.Second_Refrigerator__c = TRUE;
        extension.thisLead.Sump_Pump__c = TRUE;
        extension.thisLead.Well_Pump__c = TRUE;
        extension.thisLead.Roof_Leak__c = TRUE;
        extension.thisLead.Stand_Alone_Freezer__c = TRUE;
        extension.thisLead.Lawn_Sprinkler_System__c = TRUE;
        extension.thisLead.Septic_System__c = TRUE;
        extension.thisLead.Ice_Maker_In_Refrigerator__c = TRUE;
        extension.thisLead.Lighting_Fixtures_Plumbing_Fixtures__c = TRUE;
        extension.thisLead.Spa__c = TRUE;
        extension.thisLead.Freon__c = TRUE;
        extension.thisLead.Geo_Thermal__c = TRUE;
        extension.thisLead.Salt_Water_Pool__c = TRUE;
        extension.thisLead.Water_Softener__c = TRUE;
        extension.thisLead.Tankless_Water_Heater__c = TRUE;
        extension.thisLead.Additional_AC_Unit_each__c = '1';
        extension.thisLead.Additional_Heat_Furnace_each__c = '2';
        extension.thisLead.Additional_Water_Heater_each__c = '3';
        extension.updateRecord();
        
        RecursiveTriggerHandler.isFirstTime = true;
        df.newLead.StateCode = 'DE';
        update df.newLead;
        
        RecursiveTriggerHandler.isFirstTime = true;
        df.newLead.StateCode = 'CA';
        update df.newLead;
        
        RecursiveTriggerHandler.isFirstTime = true;
        df.gs.Gateway_Counter_CA__c = 5;
        update df.newGateway;
        Test.StopTest();
    }
    
    @isTest
    static void testUpdateNonSalesLeadRecord() {
        Test.StartTest();

        df.createGlobalSetting();
        df.createRealtorLead();
        
        ApexPages.currentPage().getParameters().put('id',df.newRealtorLead.id );
        System.assertEquals( df.newRealtorLead.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newRealtorLead );
        LeadTabController extension = new LeadTabController(controller);        
        
        extension.thisLead.Email = 'test1@test.com';
        extension.UpdateRecord();
        Test.StopTest();
    }
    
    @isTest
    static void testaddAdminNoteRecord() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createLead();
        df.createAdminNoteLead();
        
        ApexPages.currentPage().getParameters().put('id',df.newlead.id );
        System.assertEquals( df.newlead.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newlead );
        LeadTabController extension = new LeadTabController(controller);
        extension.addAdminNoteRecord();
    }
    
    @isTest
    static void testTransactionRecords() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createLead();
        df.createPaymentMethodsLead();
        df.createOrdersLead();
        df.createTransactionLead();
        df.createAdminNoteLead();
        
        ApexPages.currentPage().getParameters().put('id',df.newlead.id );
        System.assertEquals( df.newlead.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newlead );
        LeadTabController extension = new LeadTabController(controller);
        extension.billingOrderList = df.boListLead;
        extension.transactionRecords();     
        
        Test.StopTest();
    }
    @isTest
    static void testUpsertPaymentMethod() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createLead();
        df.createPaymentMethodsLead();
        df.createOrdersLead();
        df.createTransactionLead();
        
        ApexPages.currentPage().getParameters().put('id',df.newlead.id );
        System.assertEquals( df.newlead.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newlead );
        LeadTabController extension = new LeadTabController(controller);
        extension.paymentMethodRecordUpdateId = df.pMethodCardLead.id;
        extension.getPaymentMethod();
        extension.thisPaymentMethod.Payment_Method__c = 'Credit Card';
        extension.thisPaymentMethod.Month__c = 10;
        extension.thisPaymentMethod.Year__c = 2050;
        extension.thisPaymentMethod.Card_Number_Encrypted__c = '4111111111111111';
        extension.upsertPaymentMethod();
        extension.paymentMethodRecordUpdateId = df.pMethodCardLead.id;
        extension.getPaymentMethod();
        extension.thisPaymentMethod.Payment_Method__c = 'Credit Card';
        extension.thisPaymentMethod.Month__c = 10;
        extension.thisPaymentMethod.Year__c = 2050;
        extension.thisPaymentMethod.Card_Number_Encrypted__c = '5411111111111111';
        extension.upsertPaymentMethod();
        extension.paymentMethodRecordUpdateId = df.pMethodCardLead.id;
        extension.getPaymentMethod();
        extension.thisPaymentMethod.Payment_Method__c = 'Credit Card';
        extension.thisPaymentMethod.Month__c = 1;
        extension.thisPaymentMethod.Year__c = 2050;
        extension.thisPaymentMethod.Card_Number_Encrypted__c = '341111111111111';
        extension.upsertPaymentMethod();
        extension.paymentMethodRecordUpdateId = df.pMethodCardLead.id;
        extension.getPaymentMethod();
        extension.paymentMethodRecordUpdateId = df.pMethodCardLead.id;
        extension.thisPaymentMethod.Payment_Method__c = 'Credit Card';
        extension.thisPaymentMethod.Month__c = 1;
        extension.thisPaymentMethod.Year__c = 2050;
        extension.thisPaymentMethod.Card_Number_Encrypted__c = '4111111111111111';
        extension.upsertPaymentMethod();
        extension.upsertPaymentMethod();
        extension.thisPaymentMethod.Payment_Method__c = 'Check (Paper)';
        extension.upsertPaymentMethod();
        extension.thisPaymentMethod.Payment_Method__c = 'Credit Card';
        extension.upsertPaymentMethod();
        extension.thisPaymentMethod.Payment_Method__c = 'Credit Card';
        extension.thisPaymentMethod.Card_Number_Encrypted__c = '4111111111111111';
        extension.upsertPaymentMethod();
        extension.thisPaymentMethod.Payment_Method__c = 'Credit Card';
        extension.thisPaymentMethod.Card_Number_Encrypted__c = '4111111111111111';
        extension.thisPaymentMethod.Month__c = 10;
        extension.upsertPaymentMethod();
        extension.thisPaymentMethod.Payment_Method__c = 'Credit Card';
        extension.thisPaymentMethod.Card_Number_Encrypted__c = '4111111111111111';
        extension.thisPaymentMethod.Month__c = 10;
        extension.thisPaymentMethod.Year__c = 2050;
        extension.upsertPaymentMethod();
        extension.thisPaymentMethod.Payment_Method__c = 'Credit Card';
        extension.thisPaymentMethod.Card_Number_Encrypted__c = '4111111111111111';
        extension.thisPaymentMethod.Month__c = 15;
        extension.thisPaymentMethod.Year__c = 2050;
        extension.upsertPaymentMethod();
        extension.thisPaymentMethod.Payment_Method__c = 'Credit Card';
        extension.thisPaymentMethod.Card_Number_Encrypted__c = '4111111111111111';
        extension.thisPaymentMethod.Month__c = 10;
        extension.thisPaymentMethod.Year__c = 2015;
        extension.upsertPaymentMethod();
        extension.thisPaymentMethod.Payment_Method__c = 'Credit Card';
        extension.thisPaymentMethod.Card_Number_Encrypted__c = '4111111111111111';
        extension.thisPaymentMethod.Month__c = 10;
        extension.thisPaymentMethod.Year__c = 2050;
        extension.thisPaymentMethod.Set_as_default__c = true;
        extension.upsertPaymentMethod();
        extension.thisPaymentMethod.Payment_Method__c = 'Billed In';
        extension.billedIn = '';
        extension.upsertPaymentMethod();
        extension.thisPaymentMethod.Payment_Method__c = 'Billed In';
        extension.billedIn = '12345677';
        extension.upsertPaymentMethod();
        Test.StopTest();
    }
    @isTest
    static void testCharge() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createLead();
        df.createPaymentMethodsLead();
        df.createOrdersLead();
        df.createTransactionLead();
        
        ApexPages.currentPage().getParameters().put('id',df.newlead.id );
        System.assertEquals( df.newlead.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newlead );
        LeadTabController extension = new LeadTabController(controller);
        extension.submitCharge();
        extension.paymentMethodRecordUpdateId = df.pMethodCardLead.id;
        extension.chargePaymentAmount = 20.00;
        extension.chargeReason = 'New Policy';
        extension.submitCharge();
        extension.charge();
        extension.paymentMethodRecordUpdateId = df.pMethodCardLead.id;
        extension.chargePaymentAmount = 20.00;
        extension.chargeReason = 'New Policy';
        extension.submitCharge();
        extension.charge();
        Test.StopTest();
    }
    @isTest
    static void testVoidTrans() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createLead();
        df.createPaymentMethodsLead();
        df.createOrdersLead();
        df.createTransactionLead();
        
        ApexPages.currentPage().getParameters().put('id',df.newlead.id );
        System.assertEquals( df.newlead.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newlead );
        LeadTabController extension = new LeadTabController(controller);
        
        extension.thisTransactionId = df.transactionLead.id;
        extension.transactionName = '12345';
        extension.voidTrans();   
        
        //cover exception block 
        extension.thisTransactionId = df.newGateway.id;
        extension.voidTrans();    
        Test.StopTest();
    }
    
    @isTest
    static void testRefundTrans() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createLead();
        df.createPaymentMethodsLead();
        df.createOrdersLead();
        df.createTransactionLead();
        
        ApexPages.currentPage().getParameters().put('id',df.newlead.id );
        System.assertEquals( df.newlead.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newlead );
        LeadTabController extension = new LeadTabController(controller);
        
        extension.thisTransactionId = df.transactionLead.id;
        extension.refundTrans();   
        
        //cover exception block 
        extension.thisTransactionId = df.newGateway.id;
        extension.refundTrans();    
        Test.StopTest();
    }
    
    @isTest
    static void testCreditCardRequest() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createLead();
        
        ApexPages.currentPage().getParameters().put('id',df.newlead.id );
        System.assertEquals( df.newlead.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newlead );
        LeadTabController extension = new LeadTabController(controller);
        
        extension.creditCardRequest();   
        
        Test.StopTest();
    }
}