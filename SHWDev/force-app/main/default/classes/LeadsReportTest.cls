@isTest
private class LeadsReportTest {
    private static DataFactory df = new DataFactory();

    @isTest
    static void testBatchAndSchedule() {
        df.createGlobalSetting();
        df.createAffiliate();
        df.createLead();
        
        df.newLead.Affiliate_Account__c = df.affiliate.Id;
        update df.newLead;
        
        Test.StartTest();
        LeadsReport_schedule sh1 = new LeadsReport_schedule();
        String sch = '0 0 23 * * ?'; system.schedule('Test', sch, sh1);
        LeadsReport_batch batch = new LeadsReport_batch();
        ID batchprocessid = Database.executeBatch(batch);
    }
}