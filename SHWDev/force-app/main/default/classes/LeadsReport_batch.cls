global class LeadsReport_batch implements Database.Batchable<SObject> {
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id,Name,Business_Email__c,View_Duplicates__c,Leads_Report__c,Leads_Report_Type__c FROM Account WHERE RecordType.DeveloperName = \'Affiliate_Account\' AND Leads_Report__c = TRUE';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Account> scope) {   
        for (Account acct : scope) {
            List<Messaging.EmailFileAttachment> csvAttcs = new List<Messaging.EmailFileAttachment>();
            
            String reportDateFormat = '';
            if (acct.Leads_Report_Type__c == 'Yesterday') {
                reportDateFormat = Datetime.newInstance(Date.today().addDays(-1), Time.newInstance(12, 0, 0, 0)).format('yyyy-MM-dd',  UserInfo.getTimeZone().toString());
            } else {
                if (Date.today().day()==1) {
                    reportDateFormat = Datetime.newInstance(Date.today().addDays(-1).toStartOfMonth(), Time.newInstance(12, 0, 0, 0)).format('yyyy-MM-dd',  UserInfo.getTimeZone().toString())+'-'+Datetime.newInstance(Date.today().addDays(-1), Time.newInstance(12, 0, 0, 0)).format('yyyy-MM-dd',  UserInfo.getTimeZone().toString());
                } else {
                    reportDateFormat = Datetime.newInstance(Date.today().toStartOfMonth(), Time.newInstance(12, 0, 0, 0)).format('yyyy-MM-dd',  UserInfo.getTimeZone().toString())+'-'+Datetime.newInstance(Date.today(), Time.newInstance(12, 0, 0, 0)).format('yyyy-MM-dd',  UserInfo.getTimeZone().toString());
                }
            }
            
            Messaging.EmailFileAttachment csv = new Messaging.EmailFileAttachment();
            String csvname = 'SHW_Leads_Report_'+acct.Name+'_'+reportDateFormat+'.csv';
            String header = 'STATUS,CONVERTED,EMAIL,SUB ID,SUB ID2,CREATED DATE\n';
            String finalstr = header;
            
            List<Lead> leadList = getLeads(acct.Id);
            for(Lead ld : leadList){
                 if (ld.Status == 'New' || ld.Status == 'Proforma' || ld.Status == 'To Be Billed' || ld.Status == 'GAQ Order' || ld.Status == 'Warranty Sold' || (ld.Status == 'Duplicate Lead' && acct.View_Duplicates__c)) {
                     String subId1 = '';
                     if (ld.Sub_Id__c != NULL) {
                         subId1 = ld.Sub_Id__c;
                     }
                     String subId2 = '';
                     if (ld.Sub_Id2__c != NULL) {
                         subId2 = ld.Sub_Id2__c;
                     }
                     
                     String recordString = '"'+ld.Status+'","'+ld.IsConverted+'","'+ld.Email+'","'+subId1+'","'+subId2+'","'+ld.CreatedDate.format('yyyy-MM-dd hh:mm',  UserInfo.getTimeZone().toString())+'"\n';
                     finalstr = finalstr + recordString;
                 }
            }
            
            Blob csvBlob = Blob.valueOf(finalstr);

            csv.setFileName(csvname);
            csv.setBody(csvBlob);

            csvAttcs.add(csv);
            
            String[] toAddresses = new String[] {acct.Business_Email__c};
            OrgWideEmailAddress owa = [SELECT Id, DisplayName, Address FROM OrgWideEmailAddress WHERE DisplayName = 'SHW No Reply' LIMIT 1];
            Messaging.SingleEmailMessage eml = new Messaging.SingleEmailMessage();
            eml.setSubject('SHW Leads Report for '+acct.Name+' - '+reportDateFormat);
            if (Date.today().day()==1 && acct.Leads_Report_Type__c == 'Month-to-Date') {
                eml.setPlainTextBody('SHW Leads Report for '+acct.Name+' - '+reportDateFormat +'\n\nFor the first of the month, the previous month\'s data is generated.');
            } else {
                eml.setPlainTextBody('SHW Leads Report for '+acct.Name+' - '+reportDateFormat);
            }
            eml.setOrgWideEmailAddressId(owa.id);
            eml.setToAddresses(toAddresses);
            eml.setFileAttachments(csvAttcs);
            eml.setWhatId(acct.Id);
            eml.setSaveAsActivity(false); //need to set to false to store attachment
            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {eml});
            
            Task emlTask = new Task(
                OwnerId = UserInfo.getUserId(),
                Subject = 'Email: ' + eml.getSubject(),
                WhatId = eml.getWhatId(),
                ActivityDate = Date.today(),
                Description = eml.getPlainTextBody(),
                Status = 'Completed',
                TaskSubtype = 'Email'
            );
            insert emlTask;
            
            Attachment attachment = new Attachment();
            attachment.Body = csvBlob;
            attachment.Name = csvname;
            attachment.ParentId = emlTask.Id; 
            insert attachment;
        }
    }   

    global void finish(Database.BatchableContext BC) {
    }
    
    private static List<Lead> getLeads(Id affiliateAccountId){
        List<Lead> leadList = new List<Lead>();
        for (Lead ld : [SELECT Id, Status, IsConverted, Email, Sub_Id__c, Sub_Id2__c, CreatedDate FROM Lead WHERE Affiliate_Leads_Report__c = TRUE AND Affiliate_Account__c = :affiliateAccountId]) {
            leadList.add(ld);
        }
        return leadList;
    }
}