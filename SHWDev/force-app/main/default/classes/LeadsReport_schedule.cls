global class LeadsReport_schedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        LeadsReport_batch batchRun = new LeadsReport_batch(); 
        ID batchId = Database.executeBatch(batchRun,1);
    }
}