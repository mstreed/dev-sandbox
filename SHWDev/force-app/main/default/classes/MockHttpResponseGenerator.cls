@isTest
global class MockHttpResponseGenerator {
    global class ezTextMockQueryFound implements HttpCalloutMock {
        global HTTPResponse respond(HTTPRequest req) {
            // Optionally, only send a mock response for a specific endpoint
            // and method.
            //System.assertEquals('http://example.com/example/test', req.getEndpoint());
            //System.assertEquals('GET', req.getMethod());
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            //res.setBody('{"Response": {"Status": "Success","Code": 200,"Entries": []}}');
            res.setBody('{"Response": {"Status": "Success","Code": 200,"Entries": [{"ID": "5eacb157e4770908b62b69e5","PhoneNumber": "999999999","FirstName": "Homer","LastName": "Simpson","Email": "homer@gmail.com","Note": "","Source": "API","Groups": ["SF Test"],"CreatedAt": "05-01-2020","UpdatedAt": "05-06-2020","Custom1": "00QR000000","Custom2": "IYj69MAD"}]}}');
            res.setStatusCode(200);
            return res;
        }
    }
    
    global class ezTextMockQueryNone implements HttpCalloutMock {
        global HTTPResponse respond(HTTPRequest req) {
            // Optionally, only send a mock response for a specific endpoint
            // and method.
            //System.assertEquals('http://example.com/example/test', req.getEndpoint());
            //System.assertEquals('GET', req.getMethod());
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"Response": {"Status": "Success","Code": 200,"Entries": []}}');
            res.setStatusCode(200);
            return res;
        }
    }
    
    global class ezTextMockId implements HttpCalloutMock {
        global HTTPResponse respond(HTTPRequest req) {
            // Optionally, only send a mock response for a specific endpoint
            // and method.
            //System.assertEquals('http://example.com/example/test', req.getEndpoint());
            //System.assertEquals('GET', req.getMethod());
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"Response": {"Status": "Success","Code": 200,"Entry": {"ID": "5eacb157e4770908b62b69e5","PhoneNumber": "999999999","FirstName": "Homer","LastName": "Simpson","Email": "homer@gmail.com","Note": "","Source": "API","Groups": ["No Campaigns"],"CreatedAt": "05-01-2020","UpdatedAt": "05-06-2020","Custom1": "00QR000000","Custom2": "IYj69MAD"}}}');
            
            res.setStatusCode(200);
            return res;
        }
    }
    
    global class ezTextMockMessage implements HttpCalloutMock {
        global HTTPResponse respond(HTTPRequest req) {
            // Optionally, only send a mock response for a specific endpoint
            // and method.
            //System.assertEquals('http://example.com/example/test', req.getEndpoint());
            //System.assertEquals('GET', req.getMethod());
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"Response": {"Status": "Success","Code": 201,"Entry": {"ID": 207098911,"Subject": "Your Policy #12345 is expiring soon!","Message": "http://preprod-selecthomewarranty.cs2.force.com/pm?token=a0uR0000004AuhyIAC&code=123","MessageTypeID": 1,"RecipientsCount": 1,"Credits": 1,"StampToSend": "06-02-2020 12:40 PM","SendToNewRecipients": false,"PhoneNumbers": ["4168790648"],"Groups": []}}}');
            
            res.setStatusCode(200);
            return res;
        }
    }
    
    /*global class contractEarningsQuery implements HttpCalloutMock {
        global HTTPResponse respond(HTTPRequest req) {             
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"totalSize" : 1,"done" : true,"records" : [ {"attributes" : {"type" : "Contract","url" : "/services/data/v48.0/sobjects/Contract/8008A000000RO7xQAG"},"Id" : "8008A000000RO7xQAG","Status" : "Active","BillingState" : "Alaska","Contract_Term__c" : "1 Year","StartdateAndGracePeriod__c" : "2019-09-09","Original_Policy_End_Date__c" : "2100-09-09","Monthly_Subtotal__c" : 41.67,"First_Renewal_Start_Date__c" : null}]}');

            res.setStatusCode(200);
            return res;
        }
    }
    
    global class renewalEarningsQuery implements HttpCalloutMock {
        global HTTPResponse respond(HTTPRequest req) {      
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"totalSize" : 1,"done" : true,"records" : [ {"attributes" : {"type" : "Policy_Renewal__c","url" : "/services/data/v48.0/sobjects/Policy_Renewal__c/a0s8A0000038T3FQAU"},"Id" : "a0s8A0000038T3FQAU","Policy__r" : {"attributes" : {"type" : "Contract","url" : "/services/data/v48.0/sobjects/Contract/8008A000000RLexQAG"},"Status" : "Active","BillingState" : "Alaska"},"Renewal_Status__c" : "Active","Contract_Term__c" : "1 Year","Renewal_Start_Date__c" : "2020-11-27","Renewal_End_Date__c" : "2021-11-27","Monthly_Subtotal__c" : 41.67}]}');

            res.setStatusCode(200);
            return res;
        }
    }*/
    
}