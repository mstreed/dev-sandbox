@isTest
private class NoteTriggerTest {
    static testMethod void noteTest() {  
        Test.startTest();
        try{
            ContentNote cn1 = new ContentNote();
            cn1.OwnerId = UserInfo.getUserId();
            cn1.SharingPrivacy = 'N';
            cn1.Title = 'Test';
            INSERT cn1;
            DELETE cn1;
            
            ContentNote cn2 = new ContentNote();
            cn2.OwnerId = UserInfo.getUserId();
            cn2.SharingPrivacy = 'N';
            cn2.Title = 'Test';
            cn2.CreatedDate = DateTime.now().addDays(-3);
            INSERT cn2;
            UPDATE cn2;
        } catch(Exception e) {
            String message = e.getMessage();
        }
        Test.StopTest();

    }
}