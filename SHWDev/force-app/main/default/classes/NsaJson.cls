public class NsaJson{
    public String clientCaseNumber;
    public String nsaDispatchNumber;
    public eventWrapper event;
    public statusWrapper status;
    public List<claimsWrapper> claims;
    
    public static NsaJson parse(String jsonStr){
        return (NsaJson) System.JSON.deserialize(jsonStr, NsaJson.class);
    }
    
    public Class eventWrapper {
        public String eventID {get;set;}
        public String eventTimestamp {get;set;}
        public String eventTypeID {get;set;}
        public String eventTypeName {get;set;}
        public String eventDescription {get;set;}
        public eventWrapper() {}
    }
    
    public Class statusWrapper {
        public String clientCaseNumber {get;set;}
        public String authorizationNumber {get;set;}
        public Double preAuthAmount {get;set;}
        public Double deductible {get;set;}
        public String plan {get;set;}
        public String exclusions {get;set;}
        public Date createDate {get;set;}
        public Date closeDate {get;set;}
        public Date currentScheduleDate {get;set;}
        public Date originalScheduleDate {get;set;}
        public String timeSlot {get;set;}
        public String headerStatus {get;set;}
        public String subStatusCode {get;set;}
        public String subStatusDescription {get;set;}
        public Date maxPartOrderDate {get;set;}
        public Date maxPartSupplierETADate {get;set;}
        public Date maxPartDueDate {get;set;}
        public String servicerName {get;set;}
        public String servicerPhoneNumber {get;set;}
        public String servicerEmail {get;set;}
        public String program {get;set;}
        public String productType {get;set;}
        public String brand {get;set;}
        public String model {get;set;}
        public String complaint {get;set;}
        public String servicePerformed {get;set;}
        public String cancelReason {get;set;}
        public Date mstNSABookStatic {get;set;}
        public List<partOrdersWrapper> partOrders {get;set;}
        public List<shipmentsWrapper> shipments {get;set;}
        public List<estimatesWrapper> estimates {get;set;}
        public statusWrapper() {}
    }
    
    public Class partOrdersWrapper {
        public String orderNumber {get;set;}
        public Date orderDate {get;set;}
        public String poNumber {get;set;}
        public List<shipmentsWrapper> shipments {get;set;}
        public partOrdersWrapper() {}
    }
    
    public Class shipmentsWrapper {
        public String shipmentType {get;set;}
        public String trackingNumber {get;set;}
        public String shippingCompany {get;set;}
        public Date shipmentDate {get;set;}
        public shipmentsWrapper() {}
    }
    
    public Class estimatesWrapper {
        public String estimateID {get;set;}
        public String submissionStatusCode {get;set;}
        public Decimal totalAmount {get;set;}
        public Decimal deductible {get;set;}
        public Decimal totalAmountLessDeductible {get;set;}
        public String createTimestamp {get;set;}
        public String declineReason {get;set;}
        public List<estimateLinesWrapper> lines {get;set;}
        public List<estimatePartsWrapper> parts {get;set;}
        public List<estimateNotesWrapper> notes {get;set;}
        public estimatesWrapper() {}
    }
    
    public Class estimateLinesWrapper {
        public String coverageTypeCode {get;set;}
        public Decimal amount {get;set;}
        public estimateLinesWrapper() {}
    }
    
    public Class estimatePartsWrapper {
        public String brand {get;set;}
        public String partNumber {get;set;}
        public String description {get;set;}
        public Integer quantity {get;set;}
        public Decimal costEach {get;set;}
        public Decimal extendedCost {get;set;}
        public String alreadyUsed {get;set;}
        public String alreadyOrdered {get;set;}
        public estimatePartsWrapper() {}
    }
    
    public Class estimateNotesWrapper {
        public String noteID {get;set;}
        public String note {get;set;}
        public String createdBy {get;set;}
        public String createUserEntity {get;set;}
        public String createTimestamp {get;set;}
        public estimateNotesWrapper() {}
    }
    
    public Class claimsWrapper {
        public Integer billingID {get;set;}
        public String billingType {get;set;}
        public String processedStatus {get;set;}
        public String processedNotes {get;set;}
        public String previousProcessedNotes {get;set;}
        public String createTimestamp {get;set;}
        public String batchedTimestamp {get;set;}
        public Integer batchNumber {get;set;}
        public Decimal totalAmount {get;set;}
        public Decimal deductible {get;set;}
        public Decimal totalAmountLessDeductible {get;set;}
        public List<estimateLinesWrapper> lines {get;set;}
        public claimsWrapper() {}
    }
}