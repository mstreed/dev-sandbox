@isTest
private class NsaTest {
    private static DataFactory df = new DataFactory();

    @isTest
    static void testHttpPost() {
        // prepare test-data
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        df.createWorkOrder();

        //As Per Best Practice it is important to instantiate the Rest Context
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/nsa'; //Request URL
        req.httpMethod = 'POST';
        req.addHeader('Content-Type','text/html; charset=UTF-8');
        //system.debug(df.newCase.CaseNumber);
        String caseNumber = [SELECT Id, CaseNumber FROM Case WHERE Id = :df.newCase.Id LIMIT 1].CaseNumber;
        system.debug(caseNumber);
        req.requestBody = Blob.valueOf('{"clientCaseNumber":"'+caseNumber+'","nsaDispatchNumber":"SHW2020062219073","externalReference1":null,"event":{"eventID":"19770584","eventTimestamp":"2020-06-26T16:09:47Z","eventTypeID":"2","eventTypeName":"SO Create","eventDescription":"A dispatch was created. Dispatch number: SHW2020062219073"},"status":{"clientCaseNumber":"01371617","authorizationNumber":"","customerNumber":null,"claimNumber":"","preAuthAmount":"250.0","deductible":"75.0","plan":"DISHWASHER","exclusions":"Baskets, Customer Maintenance Items, Door Seals, Doors, Filter-Screens, Liners and Cavities, Racks, Rollers, Trays, Tub","createDate":"2020-06-26","closeDate":null,"currentScheduleDate":null,"originalScheduleDate":null,"timeSlot":"","headerStatus":"waitingTriage","subStatusCode":"CR","subStatusDescription":"Call received","maxPartOrderDate":null,"maxPartSupplierETADate":null,"maxPartDueDate":null,"servicerName":null,"servicerPhoneNumber":null,"servicerEmail":null,"program":"serviceCallWithoutParts","productType":"Refrigerator","brand":"BROOKSTONE","model":"fdgdfgd","complaint":"Refrigerator - Compressor is not working","servicePerformed":null,"cancelReason":null,"partOrders":[],"shipments":[],"estimates":[{"estimateID": 31481,"submissionStatusCode": "Test","processedStatusCode": "EstCancelled","totalAmount": 105,"createTimestamp": "2019-03-12T11:11:11Z","submissionTimestamp": null,"processedTimestamp": "2019-03-12T11:11:49Z","lines": [{"coverageTypeCode": "Labor","amount": 100},{"coverageTypeCode": "Parts","amount": 25.12}],"parts": [{"brand": "Sharp","partNumber": "Part1","description": "Part1Desc","quantity": 2,"costEach": 5,"extendedCost": 10,"alreadyUsed": true,"alreadyOrdered": true},{"brand": "Sharp","partNumber": "Part2","description": "Part2Desc","quantity": 1,"costEach": 15.12,"extendedCost": 15.12,"alreadyUsed": true,"alreadyOrdered": true}],"notes": [{"noteID": 1234,"note": "this is a note","createdBy": "userRJ","createUserEntity": "sf","createTimestamp": "2019-12-30 12:41:00"}],"additionalQuestions": [{"questionCode": "unitAge","answer": "<1 Year"},{"questionCode": "causeOfFailure","answer": "Normal wear and tear failure"}]}],"mstNSABookStatic":null},"claims":[{"billingID":11111,"billingType":"normal","processedStatus":"clientReview","processedNotes":"note","previousProcessedNotes":"previousnote","createTimestamp":"2018-01-0100:00:00","batchedTimestamp":"2018-01-0100:00:00","batchNumber":1234,"totalAmount":100.01,"deductible":10.0,"totalAmountLessDeductible":90.01,"lines":[{"coverageTypeCode":"Labor","amount":90.0},{"coverageTypeCode":"Parts","amount":10.01}]},{"billingID":22222,"billingType":"manual","processedStatus":"clientReview","processedNotes":"note","previousProcessedNotes":"previousnote","createTimestamp":"2018-01-0100:00:00","batchedTimestamp":"2018-01-0100:00:00","batchNumber":1234,"totalAmount":100.01,"deductible":10.0,"totalAmountLessDeductible":90.01,"lines":[{"coverageTypeCode":"Labor","amount":90.0},{"coverageTypeCode":"Parts","amount":10.01}]}]}');
        
        RestContext.request = req;
        RestContext.response= res;
        
        Test.startTest();
        String actual = NsaWebService.doPost();
        NsaJson nsa = NsaJson.parse(req.requestBody.toString());
		system.debug('nsa.event.eventID: ' + nsa.event.eventID);
		system.debug('nsa.event.eventTimestamp: ' + nsa.event.eventTimestamp);
		system.debug('nsa.event.eventTypeID: ' + nsa.event.eventTypeID);
		system.debug('nsa.event.eventTypeName: ' + nsa.event.eventTypeName);
		system.debug('nsa.event.eventDescription: ' + nsa.event.eventDescription);
        
		system.debug('nsa.status.clientCaseNumber: ' + nsa.status.clientCaseNumber);
		system.debug('nsa.status.authorizationNumber: ' + nsa.status.authorizationNumber);
		system.debug('nsa.status.preAuthAmount: ' + nsa.status.preAuthAmount);
		system.debug('nsa.status.deductible: ' + nsa.status.deductible);
		system.debug('nsa.status.plan: ' + nsa.status.plan);
		system.debug('nsa.status.exclusions: ' + nsa.status.exclusions);
		system.debug('nsa.status.createDate: ' + nsa.status.createDate);
		system.debug('nsa.status.closeDate: ' + nsa.status.closeDate);
		system.debug('nsa.status.currentScheduleDate: ' + nsa.status.currentScheduleDate);
		system.debug('nsa.status.originalScheduleDate: ' + nsa.status.originalScheduleDate);
		system.debug('nsa.status.timeSlot: ' + nsa.status.timeSlot);
		system.debug('nsa.status.headerStatus: ' + nsa.status.headerStatus);
		system.debug('nsa.status.subStatusCode: ' + nsa.status.subStatusCode);
		system.debug('nsa.status.subStatusDescription: ' + nsa.status.subStatusDescription);
		system.debug('nsa.status.maxPartOrderDate: ' + nsa.status.maxPartOrderDate);
		system.debug('nsa.status.maxPartSupplierETADate: ' + nsa.status.maxPartSupplierETADate);
		system.debug('nsa.status.maxPartDueDate: ' + nsa.status.maxPartDueDate);
		system.debug('nsa.status.servicerName: ' + nsa.status.servicerName);
		system.debug('nsa.status.servicerPhoneNumber: ' + nsa.status.servicerPhoneNumber);
		system.debug('nsa.status.servicerEmail: ' + nsa.status.servicerEmail);
		system.debug('nsa.status.program: ' + nsa.status.program);
		system.debug('nsa.status.productType: ' + nsa.status.productType);
		system.debug('nsa.status.brand: ' + nsa.status.brand);
		system.debug('nsa.status.model: ' + nsa.status.model);
		system.debug('nsa.status.complaint: ' + nsa.status.complaint);
		system.debug('nsa.status.servicePerformed: ' + nsa.status.servicePerformed);
		system.debug('nsa.status.cancelReason: ' + nsa.status.cancelReason);
		system.debug('nsa.status.mstNSABookStatic: ' + nsa.status.mstNSABookStatic);

		system.debug('nsa.partOrders: ' + nsa.status.partOrders);
        NsaJSON.PartOrdersWrapper pow = new NsaJSON.PartOrdersWrapper();
		system.debug('pow.orderNumber: ' + pow.orderNumber);
		system.debug('pow.orderDate: ' + pow.orderDate);
		system.debug('pow.poNumber: ' + pow.poNumber);
		system.debug('pow.orderNumber: ' + pow.orderNumber);

		system.debug('nsa.status.shipments: ' + nsa.status.shipments);
        NsaJSON.shipmentsWrapper sw = new NsaJSON.shipmentsWrapper();
		system.debug('sw.shipmentType: ' + sw.shipmentType);
		system.debug('sw.trackingNumber: ' + sw.trackingNumber);
		system.debug('sw.shippingCompany: ' + sw.shippingCompany);
		system.debug('sw.shipmentDate: ' + sw.shipmentDate);
        
        system.debug('nsa.claims: ' + nsa.claims);
        NsaJSON.claimsWrapper cw = new NsaJSON.claimsWrapper();
		system.debug('cw.billingID: ' + cw.billingID);
		system.debug('cw.billingType: ' + cw.billingType);
		system.debug('cw.processedStatus: ' + cw.processedStatus);
		system.debug('cw.processedNotes: ' + cw.processedNotes);
		system.debug('cw.previousProcessedNotes: ' + cw.previousProcessedNotes);
		system.debug('cw.createTimestamp: ' + cw.createTimestamp);
		system.debug('cw.batchedTimestamp: ' + cw.batchedTimestamp);
		system.debug('cw.batchNumber: ' + cw.batchNumber);
		system.debug('cw.totalAmount: ' + cw.totalAmount);
		system.debug('cw.deductible: ' + cw.deductible);
		NsaWebService.doPost();
       
        Test.stopTest();
        
        //System.assertEquals('expected value', actual, 'Value is incorrect');
    }
    
    @isTest
    static void testFail() {
        // prepare test-data
        df.createGlobalSetting();
        df.createPolicy();

        //As Per Best Practice it is important to instantiate the Rest Context
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/nsa'; //Request URL
        req.httpMethod = 'POST';
        req.addHeader('Content-Type','text/html; charset=UTF-8');
        req.requestBody = Blob.valueOf('{"clientCaseNumber":"123456","nsaDispatchNumber":"SHW2020062219073","externalReference1":null,"event":{"eventID":"19770584","eventTimestamp":"2020-06-26T16:09:47Z","eventTypeID":"2","eventTypeName":"SO Create","eventDescription":"A dispatch was created. Dispatch number: SHW2020062219073"},"status":{"clientCaseNumber":"01371617","authorizationNumber":"","customerNumber":null,"claimNumber":"","preAuthAmount":"250.0","deductible":"75.0","plan":"DISHWASHER","exclusions":"Baskets, Customer Maintenance Items, Door Seals, Doors, Filter-Screens, Liners and Cavities, Racks, Rollers, Trays, Tub","createDate":"2020-06-26","closeDate":null,"currentScheduleDate":null,"originalScheduleDate":null,"timeSlot":"","headerStatus":"waitingTriage","subStatusCode":"CR","subStatusDescription":"Call received","maxPartOrderDate":null,"maxPartSupplierETADate":null,"maxPartDueDate":null,"servicerName":null,"servicerPhoneNumber":null,"servicerEmail":null,"program":"serviceCallWithoutParts","productType":"Refrigerator","brand":"BROOKSTONE","model":"fdgdfgd","complaint":"Refrigerator - Compressor is not working","servicePerformed":null,"cancelReason":null,"partOrders":[],"shipments":[],"estimates":[{"estimateID": 31481,"submissionStatusCode": "Approved","processedStatusCode": "EstCancelled","totalAmount": 105,"createTimestamp": "2019-03-12T11:11:11Z","submissionTimestamp": null,"processedTimestamp": "2019-03-12T11:11:49Z","lines": [{"coverageTypeCode": "Labor","amount": 100},{"coverageTypeCode": "Parts","amount": 25.12}],"parts": [{"brand": "Sharp","partNumber": "Part1","description": "Part1Desc","quantity": 2,"costEach": 5,"extendedCost": 10,"alreadyUsed": true,"alreadyOrdered": true},{"brand": "Sharp","partNumber": "Part2","description": "Part2Desc","quantity": 1,"costEach": 15.12,"extendedCost": 15.12,"alreadyUsed": true,"alreadyOrdered": true}],"notes": [{"noteID": 1234,"note": "this is a note","createdBy": "userRJ","createUserEntity": "sf","createTimestamp": "2019-12-30 12:41:00"}],"additionalQuestions": [{"questionCode": "unitAge","answer": "<1 Year"},{"questionCode": "causeOfFailure","answer": "Normal wear and tear failure"}]}],"mstNSABookStatic":null}}');
        
        RestContext.request = req;
        RestContext.response= res;
        
        Test.startTest();
        String actual = NsaWebService.doPost();
        NsaJson nsa = NsaJson.parse(req.requestBody.toString());
        Test.stopTest();
        
        //System.assertEquals('expected value', actual, 'Value is incorrect');
    }
}