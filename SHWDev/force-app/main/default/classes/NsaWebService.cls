@RestResource(urlMapping='/nsa/*')
global class NsaWebService {
    //https://nsa-selecthomewarranty.cs21.force.com/webhooks/services/apexrest/nsa
    //https://preprod-selecthomewarranty.cs92.force.com/webhooks/services/apexrest/nsa
    @HttpPost 
    //global static string doPost(String clientCaseNumber, String nsaDispatchNumber, eventWrapper event, statusWrapper status) {
    global static string doPost() {
        RestRequest req = RestContext.request;  
        String jsonStr = req.requestBody.toString();
        //system.debug('jsonStr: '+ jsonStr);         
        NsaJson nsa = NsaJson.parse(jsonStr);
        //system.debug('nsa: '+ nsa);
        
        Boolean ok = dispatchToNSA(nsa);
        
        if (ok) {
            return 'Post Request Successful for Case#' + nsa.clientCaseNumber;
        } else {
            return 'Post Request NOT Successful for Case#' + nsa.clientCaseNumber;
        }
    }
    
    /*
    @HttpGet
    global static String doGet() {
    return 'Hiii';
    }*/
    
    public static Boolean dispatchToNSA(NsaJson nsa) {
        try {
            
            String caseNumber = nsa.clientCaseNumber;
            String woSequenceNumber = nsa.clientCaseNumber;
            if (caseNumber.contains('-')) {
                caseNumber = caseNumber.left(caseNumber.indexOf('-'));
            } else {
                woSequenceNumber = caseNumber + '-0';
            }
            
            system.debug('caseNumber: ' + caseNumber);
            system.debug('woSequenceNumber: ' + woSequenceNumber);
            
            Case[] claim = [SELECT Id, Contractor__c, CaseNumber, ContactId, ContactEmail, Reason, Status, Policy__r.BillingStateCode, Policy__r.of_Free_Service_Calls__c, AccountId, (SELECT NSA_Estimate_ID__c, Authorized_Amount__c, NSA_Status__c FROM Authorization_Forms__r WHERE NSA_Estimate_ID__c != NULL) FROM Case WHERE CaseNumber = :caseNumber LIMIT 1];
            Work_Order__c[] workOrder = [Select Id, NSA_Amount_TotalLessDeductable__c, Claim_Work_Order__c, NSA_ID__c, Claim__c, Contractor__c, Contractor_Email__c, Status__c 
                                         FROM Work_Order__c WHERE Claim_Work_Order__c = :woSequenceNumber LIMIT 1];
            Global_Setting__c gs = [Select Id, System_Administrator_User_ID__c, NSA_Contractor_ID__c FROM Global_Setting__c LIMIT 1];
            system.debug('claim: ' + claim);
            system.debug('workOrder: ' + workOrder);
            List<Field_Note__c> notes = new List<Field_Note__c>(); //store event descriptions from NSA

            if (workOrder.size() > 0) {

                //only do this for new NSA dispatches
                if (workOrder[0].NSA_ID__c == NULL) {
                    Account[] con = [SELECT Id, Business_Email__c, Dispatch_Me__c, Dispatch_Method__c, Not_To_Exceed__c FROM Account WHERE Id = :gs.NSA_Contractor_ID__c LIMIT 1];
                    Account nsaCon = new Account();
                    if (con.size()>0) {
                        nsaCon = con[0];
                    } else {
                        nsaCon = new Account(
                            Name='National Service Alliance',
                            ShippingStreet = '6762 South 1300 East',
                            ShippingCity = 'Salt Lake City',
                            ShippingStateCode = 'UT',
                            ShippingCountryCode = 'US',
                            ShippingPostalCode = '84121',
                            Business_Email__c = 'support@nationalservicealliance.com'
                        );
                        nsaCon.RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Contractor_Account' LIMIT 1].Id;
                        
                        INSERT nsaCon;
                        gs.NSA_Contractor_ID__c = nsaCon.Id;
                    }
                    
                    claim[0].Status = 'Sent to Dispatch Network';
                    //claim[0].Status = 'Contractor Accepted';
                    claim[0].Contractor__c = nsaCon.Id;
                    if (claim[0].Policy__r.of_Free_Service_Calls__c > 0) {
                        claim[0].Policy__r.of_Free_Service_Calls__c--;
                        claim[0].Service_Call_Fee_Override__c = 0;
                        UPDATE claim[0].Policy__r;
                    }
                    
                    Dispatcher_History__c dh = new Dispatcher_History__c(
                        case__c = claim[0].Id,
                        Type__c = 'Dispatched',
                        User__c = UserInfo.getUserId(),
                        Dispatch_Status__c = claim[0].Status
                    );
                    INSERT dh;
                    
                    if (workOrder.size() > 0) {
                        workOrder[0].NSA_ID__c = nsa.nsaDispatchNumber;
                        workOrder[0].Contractor__c = nsaCon.Id;
                        workOrder[0].Contractor_Email__c = nsaCon.Business_Email__c;
                        workOrder[0].Not_To_Exceed__c = nsaCon.Not_To_Exceed__c;
                        workOrder[0].Status__c = nsa.status.headerStatus;
                        workOrder[0].Dispatch_Status_Reason__c = nsa.status.subStatusDescription;
                        workOrder[0].Date_Scheduled__c = nsa.status.originalScheduleDate;
                        workOrder[0].Date_Rescheduled__c = nsa.status.currentScheduleDate;
                        workOrder[0].Date_Completed__c = nsa.status.closeDate;
                        /*workOrder[0].Dispatched_Date__c = Date.today();
                        workOrder[0].Last_Dispatched_By__c = UserInfo.getUserId();
                        if (workOrder[0].First_Dispatch_By__c == NULL) {
                            workOrder[0].First_Dispatch_By__c = UserInfo.getUserId();
                        } else if (workOrder[0].Second_Dispatch_By__c == NULL) {
                            workOrder[0].Second_Dispatch_By__c = UserInfo.getUserId();
                        }*/
                    }
                } else if (workOrder[0].Contractor__c == gs.NSA_Contractor_ID__c && workOrder[0].Status__c != 'Canceled') { //update the work order status only when update request and contractor still nsa and NOT Canceled
                    workOrder[0].Status__c = nsa.status.headerStatus;
                    workOrder[0].Dispatch_Status_Reason__c = nsa.status.subStatusDescription;
                }
                
                //set contractor auth amounts
                claim[0].Contractor_Authorized_Amount__c = 0;

                //always create/update auth forms
                if (nsa.status.estimates != NULL) {
                    Set<String> nsaEstIds = new Set<String>();
                    for (Authorization_Form__c af : claim[0].Authorization_Forms__r) {
                        nsaEstIds.add(af.NSA_Estimate_ID__c);
                    }
                    
                    List<Authorization_Form__c> afList = new List<Authorization_Form__c>();
                    for (NsaJson.estimatesWrapper est : nsa.status.estimates) {
                        Authorization_Form__c af = new Authorization_Form__c(
                            Claim__c = claim[0].Id,
                            NSA_Estimate_ID__c = est.estimateID,
                            NSA_Status__c = est.submissionStatusCode,
                            Authorized_Amount__c = est.totalAmountLessDeductible,
                            Decline_Reason__c = est.declineReason
                        );
                        
                        //not in existing IDs
                        /*if (!nsaEstIds.contains(est.estimateID)) {
                            af.CreatedDate = (DateTime)json.deserialize('"'+est.createTimestamp+'"', datetime.class);
                        }*/
                        
                        for (NsaJson.estimateLinesWrapper line : est.lines) {
                            if (line.coverageTypeCode == 'Labor') {
                                af.Total_Cost_of_Labor__c = line.amount;
                                af.Labor_Included_In_Pricing__c = TRUE;
                            } else if (line.coverageTypeCode == 'Parts') {
                                af.Part_1_Name__c = 'Total NSA Parts';
                                af.Part_1_Price__c = line.amount;
                                af.Part_1_Included_In_Pricing__c = TRUE;
                            }
                        }
                        
                        String partsBreakdown = '';
                        for (NsaJson.estimatePartsWrapper part : est.parts) {
                            partsBreakdown+= 'Brand: '+part.brand+', Part Number: '+part.partNumber+', Desc: '+part.description+', QTY: '+part.quantity+', Cost Each: '+part.costEach+', Extended Cost: '+part.extendedCost+', Already Used: '+part.alreadyUsed+', Already Ordered: '+part.alreadyOrdered+'\n\n';
                        }
                        af.NSA_Parts_Breakdown__c = partsBreakdown;
                        
                        String diagnosis = 'Total: ' + est.totalAmount + ' , Deductable: '+ est.deductible + '\n\n';
                        for (NsaJson.estimateNotesWrapper note : est.notes) {
                            diagnosis+= 'Note ID: '+note.NoteId+' (Created By: '+note.createdBy+', Created Date: '+note.createTimestamp+')\nNote: '+note.note+'\n\n';
                        }
                        af.Diagnosis__c = diagnosis;
                        
                        if (af.NSA_Status__c == 'Approved') {
                            claim[0].Contractor_Authorized_Amount__c += af.Authorized_Amount__c;
                        }
                        
                        afList.add(af);
                    }
                    
                    if (!afList.isEmpty()) {
                        upsert afList NSA_Estimate_ID__c;
                    }
                }
                
                //total less deductable update
                if (nsa.claims != NULL) {
                    workOrder[0].NSA_Amount_TotalLessDeductable__c = 0;
                    
                    for (NsaJson.claimsWrapper cw : nsa.claims) {
                        workOrder[0].NSA_Amount_TotalLessDeductable__c+= cw.totalAmountLessDeductible;
                    }
                }
                
                UPDATE claim[0];
                UPDATE workOrder[0];
                
                
                //add notes to upsert 
                if (nsa.event.eventID == '24' || nsa.event.eventID == '25') {   // event type 24 and 25                  
                //if (nsa.event.eventTypeID != NULL) {
                    //DateTime createDate = DateTime.valueOf(nsa.event.eventTimestamp);
                    
                    notes.add(new Field_Note__c(
                        Work_Order__c = workOrder[0].Id,
                        External_ID__c = nsa.event.eventID,
                        Body__c = nsa.event.eventDescription.left(131072),
                        Name = nsa.event.eventTypeName.left(80),
                        Type_ID__c = nsa.event.eventTypeID
                    ));
                }
				
                if (!notes.isEmpty()) {
                    UPSERT notes External_ID__c;
                }
                                
                return TRUE;
            } else {
                return FALSE;
            }
            
        } catch( Exception ex) {
            system.debug('->>>'+ ex.getMessage());
            return FALSE;
        }
    }
}