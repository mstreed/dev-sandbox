/*

    @Description    ObjectLookupController Class 
    @createdate     23 Jan 2018

*/
@isTest
private class ObjectLookupControllerTest {
    private static DataFactory df = new DataFactory();
    
    @isTest
    static void testMethod1() {
        df.createGlobalSetting();
        df.createPolicy();
        
        System.assertNotEquals(df.acc, NULL) ;
        Test.StartTest();
        ObjectLookupController controller = new ObjectLookupController();
        controller.searchText = df.acc.FirstName;
        controller.objectName  = 'Account';
        controller.runQuery();
        System.assertNotEquals( controller, NULL );
        controller.setObjectName( 'Account' );
        System.assertEquals( controller.searchText, df.acc.FirstName );
        System.assertEquals( controller.objectName, 'Account' );
        
        Test.StopTest();    
    }
    @isTest
    static void testMethod2() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        
        Test.StartTest();
        ObjectLookupController controller = new ObjectLookupController();
        controller.searchText = df.newCase.Subject;
        controller.objectName  = 'Case';
        System.assertNotEquals( controller, NULL );
        controller.setObjectName( 'Case' );
        controller.runQuery();
        System.assertEquals( controller.searchText, df.newCase.Subject );
        System.assertEquals( controller.objectName, 'Case' );
        Test.StopTest();    
    }
}