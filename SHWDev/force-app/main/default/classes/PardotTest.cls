@isTest
private class PardotTest {
    private static DataFactory df = new DataFactory();
    
    @isTest 
    static void testContractorLead() {
        df.createGlobalSetting();
        df.createContractorLead();

        Test.StartTest();
        RecursiveTriggerHandler.isFirstTime = TRUE;
        df.newContractorLead.Pardot_Visitor_ID_from_Site__c = '123456';
        df.newContractorLead.pi__url__c = 'http://pi.pardot.com/prospect/read?id=404940267';
        update df.newContractorLead;
        Test.StopTest();
    }
    
    @isTest 
    static void testRealtorLead() {
        df.createGlobalSetting();
        df.createRealtorLead();

        Test.StartTest();
        RecursiveTriggerHandler.isFirstTime = TRUE;
        df.newRealtorLead.Pardot_Visitor_ID_from_Site__c = '123456';
        df.newRealtorLead.pi__url__c = 'http://pi.pardot.com/prospect/read?id=404940267';
        update df.newRealtorLead;
        Test.StopTest();
    }
}