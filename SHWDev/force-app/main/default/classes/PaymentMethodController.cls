global without sharing class PaymentMethodController {
    public Payment_Method__c thisPaymentMethod {get;set;}  
    public Payment_Method__c newPaymentMethod {get;set;}  
    private ApexPages.StandardController controller;
    private String recordId;
    public String cardNumber {get;set;}
    public Integer cardMonth {get;set;}
    public Integer cardYear {get;set;}
    public String accessCode {get;set;}
    public Boolean display {get;set;}
    public Boolean access {get;set;}
    public Boolean thankYou {get;set;}
    public String ipAddress {get; set;}
	public String errorMsg {get; set;}

    public PaymentMethodController() {
        recordId = ApexPages.currentPage().getParameters().get('token');
        accessCode = ApexPages.currentPage().getParameters().get('code');
        
        thisPaymentMethod = [SELECT Id, Lead_Name__c, Policy_Name__c, Customer_Name__c, Customer_Address__c, Credit_Card_Request_Subject__c,
                             Card_Number_Encrypted__c, Month__c, Year__c, Card_Expiring__c, Set_as_default__c, Customer_Access_Code__c,
                             Error_Declined__c, Error_Declined_Message__c, Payment_Method__c, Policy_Name__r.of_Active_Subscriptions__c,
                             Policy_Name__r.Contract_Number__c, Quote_Policy__c, Parent_Type__c, CreatedDate, Policy_Name__r.Amount_Per_Payment__c,
                             Lead_Name__r.Amount_Per_Payment__c
                             FROM Payment_Method__c 
                             WHERE Id = :recordId];
                
        newPaymentMethod = new Payment_Method__c(
            Policy_Name__c = thisPaymentMethod.Policy_Name__c,
            set_as_default__c = true,
            Payment_Method__c = 'Credit Card'
        );
        
        display = false;
        if (thisPaymentMethod.Payment_Method__c != 'Credit Card Request' && thisPaymentMethod.Set_as_default__c == true) { //only show page for default payment methods
            if (thisPaymentMethod.Error_Declined__c != NULL || Date.today().daysBetween(thisPaymentMethod.Card_Expiring__c) <= 30) { //show if declined page or expiry still less than 30 days
                display = true;
            } 
        } else if (thisPaymentMethod.Payment_Method__c == 'Credit Card Request' && (Date.today().daysBetween(Date.valueOf(thisPaymentMethod.CreatedDate)) == 0 || Date.today().daysBetween(Date.valueOf(thisPaymentMethod.CreatedDate)) == -1)) {
            display = true;
        }
                
        if (accessCode == thisPaymentMethod.Customer_Access_Code__c) {
            access = true;
        } else {
            access = false;
        }
        thankYou = false;

        ipAddress = ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');
    }
    
    public void validateAccessCode() {
        errorMsg = '';
        if (accessCode == thisPaymentMethod.Customer_Access_Code__c) {
            access = true;
        } else {
            errorMsg = 'Error: Access Code Incorrect';
        }
    }
    
    public void updatePaymentMethod() {
        system.debug('updatePaymentMethod');
        errorMsg = '';

        try{
            if (thisPaymentMethod.Payment_Method__c == 'Credit Card Request') {
                thisPaymentMethod.Card_Number_Encrypted__c = cardNumber;
                thisPaymentMethod.Month__c = cardMonth;
                thisPaymentMethod.Year__c = cardYear;
                if (thisPaymentMethod.Card_Number_Encrypted__c == NULL) {
                    errorMsg = 'Error: Missing Card Number';
                } else if (thisPaymentMethod.Month__c == NULL) {
                    errorMsg = 'Error: Missing Month';
                } else if (thisPaymentMethod.Year__c == NULL) {
                    errorMsg = 'Error: Missing Year';
                } else if (thisPaymentMethod.Month__c == 0 || thisPaymentMethod.Month__c > 12) {
                    errorMsg = 'Error: Invalid Month';
                } else if (thisPaymentMethod.Year__c < Date.today().year()) {
                    errorMsg = 'Error: Invalid Year';
                } else if (thisPaymentMethod.Month__c <= Date.today().month() && thisPaymentMethod.Year__c <= Date.today().year()) {
                    errorMsg = 'Error: Card Expired'; 
                } else if (Date.newInstance(Integer.valueOf(thisPaymentMethod.Year__c), Integer.valueOf(thisPaymentMethod.Month__c), 1) == Date.newInstance(Integer.valueOf(Date.today().addMonths(1).year()), Integer.valueOf(Date.today().addMonths(1).month()), 1)) {
                    errorMsg = 'Error: Expiration Date Cannot Be Next Month';
                }
            } else {
                newPaymentMethod.Card_Number_Encrypted__c = cardNumber;
                newPaymentMethod.Month__c = cardMonth;
                newPaymentMethod.Year__c = cardYear;
                if (newPaymentMethod.Card_Number_Encrypted__c == NULL) {
                    errorMsg = 'Error: Missing Card Number';
                } else if (newPaymentMethod.Month__c == NULL) {
                    errorMsg = 'Error: Missing Month';
                } else if (newPaymentMethod.Year__c == NULL) {
                    errorMsg = 'Error: Missing Year';
                } else if (newPaymentMethod.Month__c == 0 || newPaymentMethod.Month__c > 12) {
                    errorMsg = 'Error: Invalid Month';
                } else if (newPaymentMethod.Year__c < Date.today().year()) {
                    errorMsg = 'Error: Invalid Year';
                } else if (newPaymentMethod.Month__c <= Date.today().month() && newPaymentMethod.Year__c <= Date.today().year()) {
                    errorMsg = 'Error: Card Expired'; 
                } else if (Date.newInstance(Integer.valueOf(newPaymentMethod.Year__c), Integer.valueOf(newPaymentMethod.Month__c), 1) == Date.newInstance(Integer.valueOf(Date.today().addMonths(1).year()), Integer.valueOf(Date.today().addMonths(1).month()), 1)) {
                    errorMsg = 'Error: Expiration Date Cannot Be Next Month';
                }
            }
            
            if (errorMsg != '') {
                return;
            } else {
                thisPaymentMethod.Customer_Response_IP__c = ipAddress;
                Global_Setting__c gs = [Select Id, Re_Bill_Task_User_Id__c, Special_Request_Task_User_ID__c FROM Global_Setting__c LIMIT 1];

                if (thisPaymentMethod.Payment_Method__c != 'Credit Card Request') {
                    if (thisPaymentMethod.Error_Declined__c != NULL) {
                        Util.newTask(thisPaymentMethod.Policy_Name__c, 'Billings', thisPaymentMethod.Credit_Card_Request_Subject__c + ' - Payment Declined - Customer Updated Credit Card', 'Customer updated credit card. Card initially declined because: ' + thisPaymentMethod.Error_Declined_Message__c, gs.Re_Bill_Task_User_Id__c, 'Task', false);
                    }
                    
                    if (thisPaymentMethod.Policy_Name__r.of_Active_Subscriptions__c > 0) {
                        Util.newTask(thisPaymentMethod.Policy_Name__c, 'Billings', thisPaymentMethod.Credit_Card_Request_Subject__c + ' - No Active Sub - Customer Updated Credit Card', 'Customer updated credit card on a Policy with no active sub.', gs.Re_Bill_Task_User_Id__c, 'Task', false);
                    }
                    
                    insert newPaymentMethod;
                    thisPaymentMethod.Set_as_default__c = false;
                    thisPaymentMethod.New_Customer_Entered_Credit_Card__c = newPaymentMethod.Id;
                    
                    update thisPaymentMethod;
                } else {
                    thisPaymentMethod.Payment_Method__c = 'Credit Card';
                    thisPaymentMethod.Set_as_default__c = true;
                    update thisPaymentMethod;
                    
                    if (thisPaymentMethod.Lead_Name__c != NULL) {
                        Util.newTask(thisPaymentMethod.Lead_Name__c, 'Sales', thisPaymentMethod.Credit_Card_Request_Subject__c + ' - Credit Card Request - Customer Updated Credit Card', 'Customer updated credit card from a credit card request.', gs.Special_Request_Task_User_ID__c, 'Task', false);
                    } else {
                        Util.newTask(thisPaymentMethod.Policy_Name__c, 'Sales', thisPaymentMethod.Credit_Card_Request_Subject__c + ' - Credit Card Request - Customer Updated Credit Card', 'Customer updated credit card from a credit card request.', gs.Special_Request_Task_User_ID__c, 'Task', false);
                    }

                }

                thankYou = true;
            }
        } catch(Exception ex) {
            if ( String.isNotBlank( ex.getMessage()) && ex.getMessage().contains( 'error:' ) ) {
                system.debug('----------------'+ex.getMessage());
                errorMsg = ex.getMessage().split('error:')[1].split(':')[0] + '.';
            } else {
                errorMsg = ex.getMessage();  
            }
        }
    }
}