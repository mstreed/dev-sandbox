@isTest
private class PaymentMethodTest {
    private static DataFactory df = new DataFactory();

    @isTest
    static void testExpiringBatch() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createPaymentMethodsPolicy();
        
        df.newPolicy.of_Active_Subscriptions__c = 1;
        update df.newPolicy;
                    
        Date expired = Date.today().addMonths(-1);
        df.pMethodCardPolicy.Month__c = expired.month();
        df.pMethodCardPolicy.Year__c = expired.year();
        update df.pMethodCardPolicy;
        
        //Payment_Method__c[] pmList = [SELECT Id, Card_Expiring__c, Set_as_default__c, Payment_Method__c, Policy_Name__c, Error_Declined__c, Policy_Name__r.Status, Last_Reminder_Sent__c, Policy_Name__r.of_Active_Subscriptions__c FROM Payment_Method__c];

        Test.StartTest();
        paymentMethodExpiring_schedule sh1 = new paymentMethodExpiring_schedule();
        String sch = '0 0 23 * * ?'; system.schedule('Test', sch, sh1);
        paymentMethodExpiring_batch batch = new paymentMethodExpiring_batch();
        ID batchprocessid = Database.executeBatch(batch);
        Test.StopTest();
    }
    
    @isTest
    static void testExpiringCard() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createPaymentMethodsPolicy();
        
        df.newPolicy.of_Active_Subscriptions__c = 1;
        update df.newPolicy;
        
        Date expired = Date.today().addMonths(-1);
        df.pMethodCardPolicy.Month__c = expired.month();
        df.pMethodCardPolicy.Year__c = expired.year();
        update df.pMethodCardPolicy;

        Test.StartTest();
        paymentMethodExpiring_batch batch = new paymentMethodExpiring_batch();
        ID batchprocessid = Database.executeBatch(batch);
        
        PageReference pageRef = Page.PaymentMethod;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('token',df.pMethodCardPolicy.Id);
        PaymentMethodController extension = new PaymentMethodController();
        
        //ENTER ACCESS CODE
        extension.thisPaymentMethod.Customer_Access_Code__c = '123';
        extension.validateAccessCode(); //exception
        extension.thisPaymentMethod.Customer_Access_Code__c = [Select Customer_Access_Code__c FROM Payment_Method__c WHERE Id = :df.pMethodCardPolicy.Id LIMIT 1].Customer_Access_Code__c;
        extension.validateAccessCode();

        //UPDATE PAYMENT METHOD
        Date valid = Date.today().addMonths(5);
        extension.updatePaymentMethod(); //exception missing card
        extension.cardNumber = df.pMethodCardPolicy.Card_Number_Encrypted__c;
        extension.updatePaymentMethod(); //exception missing month
        extension.cardMonth = 15;
        extension.updatePaymentMethod(); //exception missing year
        extension.cardYear = 201;
        extension.updatePaymentMethod(); //exception invalid month
        extension.cardMonth = Date.today().addMonths(1).month();
        extension.updatePaymentMethod(); //exception invalid year
        extension.cardYear = 2015;
        extension.updatePaymentMethod(); //exception expired
        extension.cardYear = valid.year();
        extension.updatePaymentMethod(); //exception cannot be next month
        extension.cardMonth = valid.month();

        extension.updatePaymentMethod(); 

        Test.StopTest();
    }
    
    
    @isTest
    static void testErrorCard() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createPaymentMethodsPolicy();
        df.createOrdersPolicy();
        
        df.newPolicy.of_Active_Subscriptions__c = 1;
        update df.newPolicy;
        
        Date expired = Date.today().addMonths(-1);
        df.pMethodCardPolicy.Month__c = expired.month();
        df.pMethodCardPolicy.Year__c = expired.year();
        update df.pMethodCardPolicy;

        Test.StartTest();
        
        //create invalid trand
        ChargentOrders__Transaction__c trans = new ChargentOrders__Transaction__c (
            ChargentOrders__Order__c = df.boListPolicy[0].id,
            ChargentOrders__Type__c = 'Charge',
            ChargentOrders__Gateway_Date__c = Date.today(),
            ChargentOrders__Response_Message__c = 'Invalid Credit Card.',
            ChargentOrders__Gateway_ID__c = '60102691449',
            ChargentOrders__Gateway__c = df.newGateway.id,
            ChargentOrders__Response_Status__c = 'Declined',
            ChargentOrders__Account__c = df.acc.id,
            ChargentOrders__Amount__c = 20.00,
            ChargentOrders__Amount_available_for_Refund__c = 10.00,
            ChargentOrders__Authorization__c = '60RKMH',
            ChargentOrders__Reason_Code__c = 6,
            ChargentOrders__Reason_Text__c = 'Invalid Credit Card.',
            ChargentOrders__Card_Last_4__c = '1111',
            ChargentOrders__Payment_Method__c = 'Credit Card',
            ChargentOrders__Credit_Card_Name__c = 'test',
            ChargentOrders__Credit_Card_Type__c = 'Visa',
            ChargentOrders__Billing_Last__c = 'test',
            ChargentOrders__Gateway_Response__c = 'test',
            ChargentOrders__Recurring__c = true
            
        );
        insert trans;
                    
        PageReference pageRef = Page.PaymentMethod;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('token',df.pMethodCardPolicy.Id);
        PaymentMethodController extension = new PaymentMethodController();
        
        //ENTER ACCESS CODE
        extension.thisPaymentMethod.Customer_Access_Code__c = [Select Customer_Access_Code__c FROM Payment_Method__c WHERE Id = :df.pMethodCardPolicy.Id LIMIT 1].Customer_Access_Code__c;
        extension.validateAccessCode();

        //UPDATE PAYMENT METHOD
        Date valid = Date.today().addMonths(5);
        extension.cardNumber = df.pMethodCardPolicy.Card_Number_Encrypted__c;
        extension.cardYear = valid.year();
        extension.cardMonth = valid.month();
        extension.updatePaymentMethod(); 

        Test.StopTest();
    }
    @isTest
    static void testErrorBatch() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createPaymentMethodsPolicy();
        
        df.newPolicy.of_Active_Subscriptions__c = 1;
        df.newPolicy.Status = 'Payment Declined';
        update df.newPolicy;
                    
        df.pMethodCardPolicy.Error_Declined__c = DateTime.now().addDays(-15);
        update df.pMethodCardPolicy;
        
        Test.StartTest();
        paymentMethodDeclined_schedule sh1 = new paymentMethodDeclined_schedule();
        String sch = '0 0 23 * * ?'; system.schedule('Test', sch, sh1);
        paymentMethodDeclined_batch batch = new paymentMethodDeclined_batch();
        ID batchprocessid = Database.executeBatch(batch);
        Test.StopTest();
    }
    
    @isTest
    static void testRequestCard() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createPaymentMethodsPolicy();
        
        df.newPolicy.of_Active_Subscriptions__c = 1;
        update df.newPolicy;
        
        Blob blobKey = crypto.generateAesKey(128);
        String key = EncodingUtil.convertToHex(blobKey);
                
        Date expired = Date.today().addMonths(-1);
        df.pMethodCardPolicy.Month__c = expired.month();
        df.pMethodCardPolicy.Year__c = expired.year();
        df.pMethodCardPolicy.Payment_Method__c = 'Credit Card Request';
        df.pMethodCardPolicy.Customer_Access_Code__c = key.substring(0,10);
        df.pMethodCardPolicy.Payment_Method_URL__c = Util.siteURL() + '/pm?token=' + df.pMethodCardPolicy.Id; 
        df.pMethodCardPolicy.Customer_Email__c = 'test@test.com';
        df.pMethodCardPolicy.Customer_Access_Code__c = '123';
        
        update df.pMethodCardPolicy;

        Test.StartTest();
        PageReference pageRef = Page.PaymentMethod;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('token',df.pMethodCardPolicy.Id);
        PaymentMethodController extension = new PaymentMethodController();
        
        //ENTER ACCESS CODE
        extension.thisPaymentMethod.Customer_Access_Code__c = '123';
        extension.validateAccessCode();

        //UPDATE PAYMENT METHOD
        Date valid = Date.today().addMonths(5);
        extension.updatePaymentMethod(); //exception missing card
        extension.cardNumber = df.pMethodCardPolicy.Card_Number_Encrypted__c;
        extension.updatePaymentMethod(); //exception missing month
        extension.cardMonth = 15;
        extension.updatePaymentMethod(); //exception missing year
        extension.cardYear = 201;
        extension.updatePaymentMethod(); //exception invalid month
        extension.cardMonth = Date.today().addMonths(1).month();
        extension.updatePaymentMethod(); //exception invalid year
        extension.cardYear = 2015;
        extension.updatePaymentMethod(); //exception expired
        extension.cardYear = valid.year();
        extension.updatePaymentMethod(); //exception cannot be next month
        extension.cardMonth = valid.month();

        extension.updatePaymentMethod(); 

        Test.StopTest();
    }
}