@isTest
private class PoliciesReportTest {
    private static DataFactory df = new DataFactory();

    @isTest
    static void testBatchAndSchedule() {
        df.createGlobalSetting();
        df.createAffiliate();
        df.createPolicy();
        
        Test.StartTest();
        PoliciesReport_schedule sh1 = new PoliciesReport_schedule();
        String sch = '0 0 23 * * ?'; system.schedule('Test', sch, sh1);
        PoliciesReport_batch batch = new PoliciesReport_batch();
        ID batchprocessid = Database.executeBatch(batch);
    }
}