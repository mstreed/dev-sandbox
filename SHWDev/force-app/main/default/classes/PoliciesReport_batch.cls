global class PoliciesReport_batch implements Database.Batchable<SObject>, Database.Stateful{
    String csvname = '';
    String header = 'Purchase Date,Status,First NAme,Last Name,Property Address line 1,Property City,Property State,Property Zip Code,Amount Per Payment\n';
    String finalstr = header;
    Date qryDate = Date.today().addMonths(-1).addDays(-3);
    String reportDateFormat = '';
    String emails = [SELECT Policies_Report_Emails__c FROM Global_Setting__c LIMIT 1].Policies_Report_Emails__c;

    public PoliciesReport_batch() {
        csvname = 'SHW_Policies_Report_'+Date.today()+'.csv';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        //String query = 'SELECT Id,Purchase_Date__c,Status,First_Name__c,Last_Name__c,BillingStreet,BillingCity,BillingState,BillingPostalCode,Amount_Per_Payment__c FROM Contract WHERE Created';
        //return Database.getQueryLocator(query);
        return Database.getQueryLocator([SELECT Id,Purchase_Date__c,Status,First_Name__c,Last_Name__c,BillingStreet,BillingCity,BillingState,BillingPostalCode,Amount_Per_Payment__c FROM Contract WHERE Purchase_Date__c > :qryDate ORDER BY Purchase_Date__c ASC]);
    }

    global void execute(Database.BatchableContext BC, List<Contract> scope) {   
        for (Contract rec : scope) {
            finalstr = finalstr + '"'+Datetime.newInstance(rec.Purchase_Date__c.year(), rec.Purchase_Date__c.month(),rec.Purchase_Date__c.day()).format('yyyy-MM-dd')+'","'+rec.Status+'","'+rec.First_Name__c+'","'+rec.Last_Name__c+'","'+rec.BillingStreet+'","'+rec.BillingCity+'","'+rec.BillingState+'","'+rec.BillingPostalCode+'","'+rec.Amount_Per_Payment__c+'"\n';
        }
    }
    
    global void finish(Database.BatchableContext BC) {
    List<Messaging.EmailFileAttachment> csvAttcs = new List<Messaging.EmailFileAttachment>();

        Messaging.EmailFileAttachment csv = new Messaging.EmailFileAttachment();

        Blob csvBlob = Blob.valueOf(finalstr);
        
        csv.setFileName(csvname);
        csv.setBody(csvBlob);
        csvAttcs.add(csv);
            OrgWideEmailAddress owa = [SELECT Id, DisplayName, Address FROM OrgWideEmailAddress WHERE DisplayName = 'SHW No Reply' LIMIT 1];
        Messaging.SingleEmailMessage eml = new Messaging.SingleEmailMessage();
        eml.setSubject('SHW Policies Report - '+Date.today());
        eml.setPlainTextBody('SHW Policies Report - Data From '+qryDate);
        eml.setOrgWideEmailAddressId(owa.id);
        eml.setFileAttachments(csvAttcs);
        //eml.setWhatId(acct.Id);
        eml.setSaveAsActivity(false); //need to set to false to store attachment
        if (emails != NULL) {
            String[] toAddresses = emails.split(',');
            eml.setToAddresses(toAddresses);
            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {eml});
        }
        
        /*Task emlTask = new Task(
OwnerId = UserInfo.getUserId(),
Subject = 'Email: ' + eml.getSubject(),
WhatId = eml.getWhatId(),
ActivityDate = Date.today(),
Description = eml.getPlainTextBody(),
Status = 'Completed',
TaskSubtype = 'Email'
);
insert emlTask;

Attachment attachment = new Attachment();
attachment.Body = csvBlob;
attachment.Name = csvname;
attachment.ParentId = emlTask.Id; 
insert attachment;*/
    }
}