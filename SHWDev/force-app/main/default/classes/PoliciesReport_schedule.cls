global class PoliciesReport_schedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        PoliciesReport_batch batchRun = new PoliciesReport_batch(); 
        ID batchId = Database.executeBatch(batchRun, 250);
    }
}