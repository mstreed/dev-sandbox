global without sharing class PolicyRenewalController {
    public Renewal_Response__c thisRenewalResponse {get;set;}  
    public Payment_Method__c defaultPaymentMethod {get;set;}  
    public Payment_Method__c newPaymentMethod {get;set;}  
    public String comment {get;set;}
    public String cardNumber {get;set;}
    public Integer cardMonth {get;set;}
    public Integer cardYear {get;set;}
    public Boolean cardRequired {get;set;}
    public String discountCode {get;set;}
    private ApexPages.StandardController controller;
    private String recordId;
    public String selectedResponse {get;set;}
    public List<SelectOption> yesNo {
        get {
            List<SelectOption> options = new List<SelectOption>(); 
            options.add(new SelectOption('Yes','Yes')); 
            options.add(new SelectOption('No','No')); 
            return options;
        }
        private set;
    }
    public Boolean display {get;set;}
    public String ipAddress {get; set;}
    public String errorMsg {get; set;}
    public Boolean thankYou {get;set;}

    public PolicyRenewalController() {
        recordId = ApexPages.currentPage().getParameters().get('token');
        display = false;
        
        thisRenewalResponse = [SELECT Id, Policy__c, Policy__r.Contract_Term__c, Policy__r.Original_Policy_End_Date__c, Source_Policy_Renewal__c, Policy__r.Name, Policy__r.Contract_Number__c, Policy__r.Property_Address__c, Amount_Per_Payment__c, 
                               Contract_Term__c, Customer_Renewal_Response__c, Free_Months__c,Payment_Terms__c,Policy_End_Date__c,Renewal_Response_IP__c,Service_Call_Fee__c,Total_Price__c, Total_List_Price__c, Comment__c, CreatedDate, Discount_Lookup__c
                        FROM Renewal_Response__c 
                        WHERE Id = :recordId];
        
        newPaymentMethod = new Payment_Method__c(
            Policy_Name__c = thisRenewalResponse.Policy__c,
            set_as_default__c = true,
            Payment_Method__c = 'Credit Card'
        );
        
        for (Payment_Method__c pm :[SELECT Card_Number_Encrypted__c, Payment_Method__c, Set_as_default__c,Billed_In__r.Contract_Number__c, Month__c, Year__c
                                    FROM Payment_Method__c
                                    WHERE Policy_Name__c = :thisRenewalResponse.Policy__c AND Set_as_default__c = true AND Payment_Method__c = 'Credit Card' LIMIT 1]) {
            defaultPaymentMethod = pm;
        }
        
        cardRequired = false;
        if (defaultPaymentMethod == NULL) {
            cardRequired = true;
        } else if (defaultPaymentMethod.Card_Number_Encrypted__c == NULL || 
                   defaultPaymentMethod.Month__c == NULL || 
                   defaultPaymentMethod.Year__c == NULL || 
                   defaultPaymentMethod.Month__c == 0 || 
                   defaultPaymentMethod.Month__c > 12 || 
                   defaultPaymentMethod.Year__c < Date.today().year() ||
                   (defaultPaymentMethod.Month__c <= Date.today().month() && defaultPaymentMethod.Year__c <= Date.today().year()) ||
                   (Date.newInstance(Integer.valueOf(defaultPaymentMethod.Year__c), Integer.valueOf(defaultPaymentMethod.Month__c), 1) == Date.newInstance(Integer.valueOf(Date.today().addMonths(1).year()), Integer.valueOf(Date.today().addMonths(1).month()), 1))) {
                       cardRequired = true;
                   }
        
        if (thisRenewalResponse.CreatedDate >= Date.today().addDays(-60)) {
            display = true;
        }
        
        selectedResponse = 'Yes';
        comment = '';
        thankYou = false;
        
        if (thisRenewalResponse.Customer_Renewal_Response__c != NULL) {
            thankYou = true;
        } else {
            thankYou = false;
        }

        ipAddress = ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');
    }
    
    public void response() {
        system.debug('response');
        system.debug('selectedResponse: ' + selectedResponse);
        system.debug('comment: ' + comment);
        system.debug('cardNumber: ' + cardNumber);
        system.debug('newPaymentMethod.Month__c: ' + newPaymentMethod.Month__c);
        system.debug('newPaymentMethod.Year__c: ' + newPaymentMethod.Year__c);
        system.debug('discountCode: ' + discountCode);
        errorMsg = '';

        Global_Setting__c gs = [Select Id, System_Administrator_User_ID__c FROM Global_Setting__c LIMIT 1];

        thisRenewalResponse.Customer_Renewal_Response__c = selectedResponse;
        thisRenewalResponse.Renewal_Response_IP__c = ipAddress;
        thisRenewalResponse.Comment__c = comment;
        system.debug('thisRenewalResponse.Comment__c: ' + thisRenewalResponse.Comment__c);

        try{
            if (selectedResponse == 'Yes') {
                system.debug('yes');

                /*Policy_Renewal__c pr = new Policy_Renewal__c();
                pr.Contract_Term__c = thisRenewalResponse.Contract_Term__c;
                pr.Free_Months__c = thisRenewalResponse.Free_Months__c.replaceAll(' Month','').replaceAll(' Months','');
                pr.of_Payments__c = thisRenewalResponse.Payment_Terms__c;
                pr.Policy__c = thisRenewalResponse.Policy__c;
                pr.Previous_Policy_End_Date__c = thisRenewalResponse.Policy__r.Original_Policy_End_Date__c;
                pr.Renewal_Amount__c = thisRenewalResponse.Total_Price__c;
                Date dtVal = thisRenewalResponse.Policy__r.Original_Policy_End_Date__c;
                pr.Renewal_Start_Date__c = dtVal.addDays(1);
                pr.Renewal_Status__c = 'Active';
                pr.Service_Call_fee_DD__c = String.valueOf(Integer.valueOf(thisRenewalResponse.Service_Call_Fee__c));
                pr.Wait_Period__c = 0;
                
                insert pr;
                
                thisRenewalResponse.New_Policy_Renewal__c = pr.Id;
                
                //Start calculating new policy end date
                List<Policy_Renewal__c>  tempRenewalList= [ SELECT
                Id,
                Name,
                Renewal_Status__c,
                Free_Months__c,
                Contract_Term__c,
                Wait_Period__c,
                Renewal_Start_Date__c,
                Previous_Policy_End_Date__c,
                of_Payments__c,
                Renewal_Amount__c,
                CreatedDate
                FROM Policy_Renewal__c
                WHERE Policy__c = :thisRenewalResponse.Policy__c 
                AND Renewal_Status__c = 'Active'
                ORDER BY CreatedDate DESC
                LIMIT 50000
                ];
                Date dateCal;
                if (thisRenewalResponse.Policy__r.Contract_Term__c == 'Monthly') {
                dateCal = Date.today();
                } else {
                dateCal = thisRenewalResponse.Policy__r.Original_Policy_End_Date__c;
                }
                
                for(Policy_Renewal__c pRenew : tempRenewalList){
                if (pRenew.CreatedDate > thisRenewalResponse.Policy__r.Original_Policy_End_Date__c) {
                DateTime cDate = pRenew.CreatedDate;
                dateCal = Date.newinstance(cDate.year(), cDate.month(), cDate.day());
                } else if (pRenew.CreatedDate < thisRenewalResponse.Policy__r.Original_Policy_End_Date__c) {
                dateCal = thisRenewalResponse.Policy__r.Original_Policy_End_Date__c;
                }
                
                if (pRenew.Renewal_Start_Date__c == null) {
                if (pRenew.Wait_Period__c > 0) {
                pRenew.Renewal_Start_Date__c = dateCal.addDays(Integer.valueOf(pRenew.Wait_Period__c));
                } else {
                pRenew.Renewal_Start_Date__c = dateCal.addDays(1);
                }
                }
                if (pRenew.Previous_Policy_End_Date__c == null) {
                pRenew.Previous_Policy_End_Date__c = thisRenewalResponse.Policy__r.Original_Policy_End_Date__c;
                }
                
                dateCal = dateCal.addDays(Integer.valueOf(pRenew.Wait_Period__c));
                
                if(pRenew.Free_Months__c != NULL ){
                dateCal = dateCal.addMonths(Integer.valueOf(pRenew.Free_Months__c));    
                }
                if(pRenew.Contract_Term__c != NULL){
                dateCal = dateCal.addYears(Integer.valueOf(pRenew.Contract_Term__c.split(' ')[0]));    
                }
                }
                
                update tempRenewalList;
                
                Contract con = new Contract();
                con.Id = thisRenewalResponse.Policy__c;
                con.Service_Call_Fee_Dropdown__c = pr.Service_Call_fee_DD__c;
                con.Original_Policy_End_Date__c = dateCal;            
                
                update con;*/
                List<Discount__c> foundCode = new List<Discount__c>();
                if (discountCode != NULL && discountCode != '') {
                    foundCode = [SELECT Id, Name, Type__c, Active__c, Applies_To__c, Dollar_Amount__c, Percentage_Amount__c FROM Discount__c WHERE Name = :discountCode AND Type__c = 'Renewal' AND Active__c = TRUE AND Applies_To__c LIKE '%Annual%' LIMIT 1];
                    if (!foundCode.isEmpty()) {
                        system.debug('foundCode: ' + foundCode);
                        thisRenewalResponse.Discount_Lookup__c = foundCode[0].Id;
                    } else {
                        errorMsg = 'Error: Discount Code not found';
                        return;
                    }
                }
                
                system.debug('one');
                system.debug('cardRequired: ' + cardRequired);
                system.debug('cardNumber: ' + cardNumber);

                if (cardRequired == true || cardNumber != '') {
                    system.debug('two');

                    newPaymentMethod.Card_Number_Encrypted__c = cardNumber;
                    newPaymentMethod.Month__c = cardMonth;
                    newPaymentMethod.Year__c = cardYear;
                    if (newPaymentMethod.Card_Number_Encrypted__c == NULL || newPaymentMethod.Card_Number_Encrypted__c == '') {
                        errorMsg = 'Error: Missing Card Number';
                    } else if (newPaymentMethod.Month__c == NULL) {
                        errorMsg = 'Error: Missing Month';
                    } else if (newPaymentMethod.Year__c == NULL) {
                        errorMsg = 'Error: Missing Year';
                    } else if (newPaymentMethod.Month__c == 0 || newPaymentMethod.Month__c > 12) {
                        errorMsg = 'Error: Invalid Month';
                    } else if (newPaymentMethod.Year__c < Date.today().year()) {
                        errorMsg = 'Error: Invalid Year';
                    } else if (newPaymentMethod.Month__c <= Date.today().month() && newPaymentMethod.Year__c <= Date.today().year()) {
                        errorMsg = 'Error: Card Expired'; 
                    } else if (Date.newInstance(Integer.valueOf(newPaymentMethod.Year__c), Integer.valueOf(newPaymentMethod.Month__c), 1) == Date.newInstance(Integer.valueOf(Date.today().addMonths(1).year()), Integer.valueOf(Date.today().addMonths(1).month()), 1)) {
                        errorMsg = 'Error: Expiration Date Cannot Be Next Month';
                    }
                    
                    if (errorMsg != '') {
                        return;
                    } else {
                        newPaymentMethod.Customer_Response_IP__c = ipAddress;
                        insert newPaymentMethod;
                    }
                }
                
                String yesSubject = thisRenewalResponse.Policy__r.Contract_Number__c + ' - Customer Renewal Response - YES';
                String yesMsg = 'Customer agreed to auto-renewal. Please reach out to the customer. \n\n Customer Comment: ' + thisRenewalResponse.Comment__c;
                if (thisRenewalResponse.Discount_Lookup__c != NULL) {
                    yesSubject = yesSubject + ' with Discount';
                    
                    yesMsg = yesMsg + '\n\n Discount Code: ' + discountCode;
                    
                    if (thisRenewalResponse.Discount_Lookup__c != NULL) {
                        yesMsg = yesMsg + ' = $' + foundCode[0].Dollar_Amount__c + ' off';
                    } else {
                        yesMsg = yesMsg + ' = ' + foundCode[0].Percentage_Amount__c + '% off';
                    }
                    
                }
                
                Util.newTask(thisRenewalResponse.Policy__c, 'Sales', yesSubject, yesMsg, gs.System_Administrator_User_ID__c, 'Task', false);
                
                Util.newTask(thisRenewalResponse.Policy__c, 'Sales', 'Email: Policy #' + thisRenewalResponse.Policy__r.Contract_Number__c + ' - Thanks!', 'Customer sent thank you email for responding "Yes" to renewal', gs.System_Administrator_User_ID__c, 'Email', true);
            } else {
                system.debug('no');
                    system.debug(thisRenewalResponse.Comment__c);

                if (thisRenewalResponse.Comment__c == NULL || thisRenewalResponse.Comment__c == '') {
                    errorMsg = 'Comment Required';
                    system.debug(errorMsg);

                    return;
                }
                
                Util.newTask(thisRenewalResponse.Policy__c, 'Sales', thisRenewalResponse.Policy__r.Contract_Number__c + ' - Customer Renewal Response - NO', 'Customer did not agreed to auto-renewal. Please reach out to the customer.\n\n Customer Comment: ' + thisRenewalResponse.Comment__c, gs.System_Administrator_User_ID__c, 'Task', false);
                Util.newTask(thisRenewalResponse.Policy__c, 'Sales', 'Email: Policy #' + thisRenewalResponse.Policy__r.Contract_Number__c, 'Customer sent discount email for responding "No" to renewal', gs.System_Administrator_User_ID__c, 'Email', true);
            }
            
            system.debug('errorMsg: ' + errorMsg);
            
            if (errorMsg == '') {
                update thisRenewalResponse;
                thankYou = true;
            }

        } catch(Exception ex) {
            if ( String.isNotBlank( ex.getMessage()) && ex.getMessage().contains( 'error:' ) ) {
                system.debug('----------------'+ex.getMessage());
                errorMsg = ex.getMessage().split('error:')[1].split(':')[0] + '.';
            } else {
                errorMsg = ex.getMessage();  
            }
        }
    }
}