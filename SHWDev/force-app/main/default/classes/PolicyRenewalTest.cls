@isTest
private class PolicyRenewalTest {
    private static DataFactory df = new DataFactory();

    @isTest
    static void testRenewalBatch() {
        df.createGlobalSetting();
        df.createPolicy();
        
        df.newPolicy.Original_Policy_End_Date__c = Date.today().addDays(60);
        update df.newPolicy;
        
        Test.StartTest();
        policyRenewal_schedule sh1 = new policyRenewal_schedule();
        String sch = '0 0 23 * * ?'; system.schedule('Test', sch, sh1);
        policyRenewal_batch batch = new policyRenewal_batch();
        ID batchprocessid = Database.executeBatch(batch);
        Test.stopTest();
    }
    
    @isTest
    static void testRenewalBatchWithRenewal() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createRenewal();
        df.newPolicy.Original_Policy_End_Date__c = Date.today().addDays(60);
        update df.newPolicy;
        
        Test.StartTest();
        
        policyRenewal_schedule sh1 = new policyRenewal_schedule();
        String sch = '0 0 23 * * ?'; system.schedule('Test', sch, sh1);
        policyRenewal_batch batch = new policyRenewal_batch();
        ID batchprocessid = Database.executeBatch(batch);
        Test.StopTest();
    }
    
    @isTest
    static void testReminderBatch() {
        df.createGlobalSetting();
        df.createPolicy();
        df.newPolicy.Original_Policy_End_Date__c = Date.today().addDays(60);
        update df.newPolicy;
        
        
        Test.StartTest();
        Renewal_Response__c renewalRsp = new Renewal_Response__c (
            Policy__c = df.newPolicy.Id,
            Customer_Email__c = 'test@test.com',
            CreatedDate = Date.today().addDays(-4)
        );
        insert renewalRsp;
        
        policyRenewalReminder_schedule sh1 = new policyRenewalReminder_schedule();
        String sch = '0 0 23 * * ?'; system.schedule('Test', sch, sh1);
        policyRenewalReminder_batch batch = new policyRenewalReminder_batch();
        ID batchprocessid = Database.executeBatch(batch);
    }
   
    @isTest
    static void testRenewalResponse() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createPaymentMethodsPolicy();
        df.newPolicy.Original_Policy_End_Date__c = Date.today().addDays(60);
        update df.newPolicy;
        
        Test.StartTest();
        policyRenewal_batch batch = new policyRenewal_batch();
        ID batchprocessid = Database.executeBatch(batch);
        
        Renewal_Response__c renewalRsp = new Renewal_Response__c (
            Policy__c = df.newPolicy.Id,
            Customer_Email__c = 'test@test.com',
            CreatedDate = Date.today().addDays(-4)
        );
        insert renewalRsp;
        
        PageReference pageRef = Page.PolicyRenewal;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('token',renewalRsp.Id);
        PolicyRenewalController extension = new PolicyRenewalController();
        system.debug(extension.yesNo);
        
        //UPDATE RESPONSE
        extension.selectedResponse = 'Yes';
        extension.cardNumber = '411111111111';
        extension.cardMonth = 10;
        extension.cardYear = Date.today().addYears(3).year();
        extension.discountCode = '123';
		extension.response();
        extension.discountCode = NULL;
		extension.response();
        extension.selectedResponse = 'No';
		extension.response();
        extension.thisRenewalResponse.Comment__c = '123';
		extension.response();
        Test.StopTest();
    }
}