/*

@Description    PolicyTabController Class 
@createdate     20 May 2018

*/

@isTest
private class PolicyTabControllerTest {
    private static DataFactory df = new DataFactory();

    @isTest
    static void testOffsets() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        Case case2 = df.newCase.clone(false, true, false, false);
        INSERT case2;

       


        
        df.createInquiry();
        Case inq2 = df.newInquiry.clone(false, true, false, false);
        INSERT inq2;
        
        df.createAdminNotePolicy();
        Admin_Note__c adNote2 = df.adminNotePolicy.clone(false, true, false, false);
        adNote2.Type__c = 'Transaction';
        INSERT adNote2;
        
        View__c logView2 = new View__c(
            Policy__c = df.newPolicy.id,
            Viewed_By__c = UserInfo.getUserId(),
            Viewed_On__c = DateTime.now()
        );
        INSERT logView2;
        
        Test.StartTest();
        ApexPages.currentPage().getParameters().put('id',df.newPolicy.Id);
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        extension.list_size = 1;
        
        extension.CaseEnd();
        extension.CaseBeginning();
        extension.CaseNext();
        extension.CasePrevious();
        extension.getCasePageNumber();
        extension.getCaseTotalPages();
        
        extension.InquiryBeginning();
        extension.InquiryEnd();
        extension.InquiryNext();
        extension.InquiryPrevious();
        extension.getInquiryPageNumber();
        extension.getInquiryTotalPages();
        
        extension.NotesBeginning();
        extension.NotesEnd();
        extension.NotesNext();
        extension.NotesPrevious();
        extension.getNotesPageNumber();
        extension.getNotesTotalPages();
        
        extension.ViewsBeginning();
        extension.ViewsEnd();
        extension.ViewsNext();
        extension.ViewsPrevious();
        extension.getViewsPageNumber();
        extension.getViewsTotalPages();
        
        Test.StopTest();
    }
    
    @isTest
    static void testNoPolicyId() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createPolicy();
        
        ApexPages.currentPage().getParameters().put('id',NULL);
        System.assertEquals( NULL, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        system.debug(extension.statusList);
        system.debug(extension.chargeReasonList);
        System.assertEquals( extension.errorMsg,'No Policy ID provided.' );
        Test.StopTest();
    }
    
    @isTest
    static void testUpdateRecord1() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createPolicy();
        df.createAdminNotePolicy();
        df.createPaymentMethodsPolicy();
        df.createOrdersPolicy();
        
        ApexPages.currentPage().getParameters().put( 'id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        extension.removeBounce();
        extension.thisContract.Contract_Term__c = '1 Year';
        extension.thisContract.Months_Free__c = '2 Months';
        extension.thisContract.Grace_Period_Days_Manual_Override__c = 10;
        extension.thisContract.Payment_Terms__c = '2 Payments';
        extension.thisContract.Email_with_Multiple_Address__c = 'test@test.com,test2@test.com';
        extension.UpdateRecord();
        extension.thisContract.Payment_Terms__c = 'Single Payment';
        extension.thisContract.Months_Free__c = '2 Months';
        extension.thisContract.Grace_Period_Days_Manual_Override__c = NULL;
        extension.thisContract.Email_with_Multiple_Address__c = 'test@test.com';
        extension.thisContract.X1st_Service_Call_Free__c = TRUE;
        extension.thisContract.No_Cancellation_Fee__c = TRUE;
        extension.UpdateRecord();
        
        extension.thisContract.Payment_Terms__c = NULL;
        extension.UpdateRecord();
        extension.thisContract.Contract_Term__c = NULL;
        extension.UpdateRecord();
        extension.thisContract.Purchase_Date__c = NULL;
        extension.UpdateRecord();
        extension.thisContract.Home_Phone__c = NULL;
        extension.UpdateRecord();
        extension.thisContract.Email_with_Multiple_Address__c = NULL;
        extension.UpdateRecord();
        extension.lname = NULL;
        extension.UpdateRecord();
        extension.fname = NULL;
        extension.UpdateRecord();
        extension.thisContract.Status = NULL;
        extension.UpdateRecord();
        extension.thisContract.Sales_Agent_Lookup__c = NULL;
        extension.UpdateRecord();
        Test.StopTest();
    }
    
    @isTest
    static void testUpdateRecord2() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createPolicy();
        df.createAdminNotePolicy();
        df.createPaymentMethodsPolicy();
        df.createOrdersPolicy();
        
        ApexPages.currentPage().getParameters().put( 'id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        extension.thisContract.Contract_Term__c = '3 Year';
        extension.thisContract.Payment_Terms__c = 'Single Payment';
        extension.thisContract.Months_Free__c = '3 Months';
        extension.UpdateRecord();
        Test.StopTest();
    }
    
    @isTest
    static void testUpdateRecord2b() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createPolicy();
        df.createAdminNotePolicy();
        df.createPaymentMethodsPolicy();
        df.createOrdersPolicy();
        
        ApexPages.currentPage().getParameters().put( 'id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        extension.thisContract.Contract_Term__c = '2 Year';
        extension.thisContract.Grace_Period_Days_Manual_Override__c = 10;
        extension.thisContract.Months_Free__c = '3 Months';
        extension.UpdateRecord();
        Test.StopTest();
    }
    
    @isTest
    static void testUpdateRecord2c() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createPolicy();
        df.createAdminNotePolicy();
        df.createPaymentMethodsPolicy();
        df.createOrdersPolicy();
        
        ApexPages.currentPage().getParameters().put( 'id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        extension.thisContract.Original_Policy_End_Date__c = Date.today();
        extension.thisPolicyRenewal.Contract_Term__c = '1 Year';
        extension.thisPolicyRenewal.of_Payments__c = '2 Payments';
        extension.thisPolicyRenewal.Renewal_Amount__c = 1;
        extension.updateTempEndDate();
        
        extension.thisPolicyRenewal.Wait_Period__c = 1;
        extension.updateTempEndDate();
        extension.getDiscountAndMonthsFreeRenewal();
        Test.StopTest();
    }
    
    @isTest
    static void testUpdateRecord3() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createPolicy();
        df.createAdminNotePolicy();
        df.createPaymentMethodsPolicy();
        df.createOrdersPolicy();
        
        ApexPages.currentPage().getParameters().put( 'id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        extension.thisContract.Original_Policy_End_Date__c = Date.today();
        extension.thisPolicyRenewal.Contract_Term__c = '1 Year';
        extension.thisPolicyRenewal.of_Payments__c = '2 Payments';
        extension.thisPolicyRenewal.Renewal_Amount__c = 1;
        extension.updateTempEndDate();
        extension.updateTempRenewalPrice();
        extension.thisPolicyRenewal.Free_Months__c = '1';
        extension.thisPolicyRenewal.Wait_Period__c = 1;
        extension.updateTempEndDate();
        extension.getDiscountAndMonthsFreeRenewal();
        extension.UpdateRecord();

        Test.StopTest();
    }

    @isTest
    static void testCancellationContract() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createPolicy();
        
        ApexPages.currentPage().getParameters().put( 'id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        
        extension.cancellationId = df.newPolicy.Id;
        extension.cancellation.Cancellation_Refund_Years__c = '1';
        extension.cancellation.Cancellation_Refund_Amount__c = 1;
        extension.cancellation();
        
        extension.cancellation.Cancellation_Reason__c = 'Dispute';
        extension.cancellation();

        Test.StopTest();
    }
    
    @isTest
    static void testCancellationRenewal() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createPolicy();
        df.createRenewal();
        
        ApexPages.currentPage().getParameters().put( 'id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        
        extension.cancellationId = df.policyRenewal.Id;
        extension.cancellation.Cancellation_Refund_Years__c = '1';
        extension.cancellation.Cancellation_Refund_Amount__c = 1;
        extension.cancellation();
        
        extension.cancellation.Cancellation_Reason__c = 'Dispute';
        extension.cancellation();

        extension.updateEndDateWithRenewals();
        Test.StopTest();
    }
    
    @isTest
    static void testDowngradeContract() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createPolicy();
        
        ApexPages.currentPage().getParameters().put( 'id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        
        extension.downgradeId = df.newPolicy.Id;
        extension.downgrade.Downgrade_Refund_Years__c = '1';
        extension.downgrade.Downgrade_Refund_Amount__c = 1;
        extension.downgrade();
        
        extension.downgrade.Downgrade_Reason__c = 'Dispute';
        extension.downgrade();

        Test.StopTest();
    }
    
    @isTest
    static void testDowngradeRenewal() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createPolicy();
        df.createRenewal();
        
        ApexPages.currentPage().getParameters().put( 'id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        
        extension.downgradeId = df.policyRenewal.Id;
        extension.downgrade.Downgrade_Refund_Years__c = '1';
        extension.downgrade.Downgrade_Refund_Amount__c = 1;
        extension.downgrade();
        
        extension.downgrade.Downgrade_Reason__c = 'Dispute';
        extension.downgrade();
        
        Test.StopTest();
    }
    
    @isTest
    static void testDupleLeadRecord() { 
        Test.StartTest();
        df.createGlobalSetting();
        df.createLead();
        df.createPolicy();
        
        df.newLead.Parent_Policy__c = df.newPolicy.id;
        update df.newLead;
        
        ApexPages.currentPage().getParameters().put( 'id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        extension.removeDupeId = df.newLead.Id;
        extension.removeDupe();
        Test.StopTest();
    }
    
    @isTest
    static void testUpdateRecordContractTrigger1() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createPolicy();
        
        RecursiveTriggerHandler.isFirstTime2 = TRUE;
        
        df.newPolicy.BillingState = 'Oklahoma';
        df.newPolicy.BillingStateCode = 'OK';
        df.newPolicy.Contract_Term__c = 'Monthly';
        df.newPolicy.Priority_Override__c = '1';
        update df.newPolicy;
        df.newPolicy.Contract_Term__c = '1 Year';
        df.newPolicy.Priority_Override__c = '3';
        update df.newPolicy;
        
        df.newPolicy.BillingState = 'Texas';
        df.newPolicy.BillingStateCode = 'TX';
        df.newPolicy.Contract_Term__c = 'Monthly';
        df.newPolicy.Priority_Override__c = '1';
        update df.newPolicy;
        
        df.newPolicy.BillingState = 'Utah';
        df.newPolicy.BillingStateCode = 'UT';
        df.newPolicy.Contract_Term__c = '1 Year';
        df.newPolicy.Priority_Override__c = '1';
        update df.newPolicy;
        Test.StopTest();
    }
    
    @isTest
    static void testUpdateRecordContractTrigger2() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createPolicy();
        
        RecursiveTriggerHandler.isFirstTime2 = TRUE;

        //test case for Virginia state
        df.newPolicy.Payment_Terms__c = 'Single Payment';
        df.newPolicy.Do_Not_Call__c = TRUE;
        df.newPolicy.Status = 'Payment Declined';
        df.newPolicy.BillingState = 'California';
        df.newPolicy.BillingStateCode = 'CA';
        df.newPolicy.Priority_Override__c = '1';
        df.newPolicy.Pool__c = TRUE;
        df.newPolicy.Central_Vacuum__c = TRUE;
        df.newPolicy.Second_Refrigerator__c = TRUE;
        df.newPolicy.Sump_Pump__c = TRUE;
        df.newPolicy.Well_Pump__c = TRUE;
        df.newPolicy.Roof_Leak__c = TRUE;
        df.newPolicy.Stand_Alone_Freezer__c = TRUE;
        df.newPolicy.Lawn_Sprinkler_System__c = TRUE;
        df.newPolicy.Septic_System__c = TRUE;
        df.newPolicy.Ice_Maker_In_Refrigerator__c = TRUE;
        df.newPolicy.Lighting_Fixtures_Plumbing_Fixtures__c = TRUE;
        df.newPolicy.Spa__c = TRUE;
        df.newPolicy.Freon__c = TRUE;
        df.newPolicy.Geo_Thermal__c = TRUE;
        df.newPolicy.Salt_Water_Pool__c = TRUE;
        df.newPolicy.Water_Softener__c = TRUE;
        df.newPolicy.Tankless_Water_Heater__c = TRUE;
        df.newPolicy.Additional_AC_Unit_each__c = '1';
        df.newPolicy.Additional_Heat_Furnace_each__c = '2';
        df.newPolicy.Additional_Water_Heater_each__c = '3';
        df.newPolicy.of_Ovens__c = '1';
        df.newPolicy.of_Refrigerators__c = '3';
        df.newPolicy.of_Stand_Alone_Freezers__c = '3';
        df.newPolicy.Package__c = 'Silver';
        update df.newPolicy;
        
        df.newPolicy.BillingStateCode = 'VA';
        df.newPolicy.Contract_Term__c = 'Monthly';
        df.newPolicy.Priority_Override__c = '2';
        update df.newPolicy;
        Test.StopTest();
    }
    
    @isTest
    static void testaddAdminNoteRecord() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createPolicy();
        df.createAdminNotePolicy();  
        
        ApexPages.currentPage().getParameters().put('id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        extension.addAdminNoteRecord();
        //Cover exception part
        extension.currentRecordId = 'testId';
        extension.addAdminNoteRecord();
        Test.StopTest();
    }
    
    @isTest
    static void testTransactionRecords() {
        Test.StartTest();

        df.createGlobalSetting();
        df.createPolicy();
        df.createPaymentMethodsPolicy();
        df.createOrdersPolicy();
        df.createTransactionPolicy();
        
        ApexPages.currentPage().getParameters().put('id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        extension.billingOrderList = df.boListPolicy;
        extension.transactionRecords(); 
        Test.StopTest();
    }
    @isTest
    static void testTransactionRecords1() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createPolicy();
        df.createPaymentMethodsPolicy();
        df.createOrdersPolicy();
        df.createTransactionPolicy();
        
        ApexPages.currentPage().getParameters().put('id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        extension.billingOrderList = df.boListPolicy;
        extension.transactionRecords();     
        Test.StopTest();
    }
    
    @isTest
    static void testUpsertPaymentMethod1() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createPolicy();
        df.createPaymentMethodsPolicy();
        df.createOrdersPolicy();
        
        ApexPages.currentPage().getParameters().put('id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        extension.upsertPaymentMethod();
        extension.paymentMethodRecordUpdateId = df.pMethodCardPolicy.id;
        extension.getPaymentMethod();
        extension.thisPaymentMethod.Payment_Method__c = 'Credit Card';
        extension.thisPaymentMethod.Month__c = 10;
        extension.thisPaymentMethod.Year__c = 2050;
        extension.thisPaymentMethod.Card_Number_Encrypted__c = '4111111111111111';
        extension.upsertPaymentMethod();
        extension.paymentMethodRecordUpdateId = df.pMethodCardPolicy.id;
        extension.getPaymentMethod();
        extension.thisPaymentMethod.Payment_Method__c = 'Credit Card';
        extension.thisPaymentMethod.Month__c = 10;
        extension.thisPaymentMethod.Year__c = 2050;
        extension.thisPaymentMethod.Card_Number_Encrypted__c = '5411111111111111';
        extension.upsertPaymentMethod();
        extension.paymentMethodRecordUpdateId = df.pMethodCardPolicy.id;
        extension.getPaymentMethod();
        extension.thisPaymentMethod.Payment_Method__c = 'Credit Card';
        extension.thisPaymentMethod.Month__c = 1;
        extension.thisPaymentMethod.Year__c = 2050;
        extension.thisPaymentMethod.Card_Number_Encrypted__c = '341111111111111';
        extension.upsertPaymentMethod();
        extension.paymentMethodRecordUpdateId = df.pMethodCardPolicy.id;
        extension.getPaymentMethod();
        
        Test.StopTest();
    }
    
    @isTest
    static void testUpsertPaymentMethod2() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createPolicy();
        df.createPaymentMethodsPolicy();
        df.createOrdersPolicy();
        df.createTransactionPolicy();
        
        ApexPages.currentPage().getParameters().put('id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        extension.thisPaymentMethod.Payment_Method__c = 'Check (Paper)';
        extension.upsertPaymentMethod();
        extension.thisPaymentMethod.Payment_Method__c = 'Credit Card';
        extension.upsertPaymentMethod();
        extension.thisPaymentMethod.Payment_Method__c = 'Credit Card';
        extension.thisPaymentMethod.Card_Number_Encrypted__c = '4111111111111111';
        extension.upsertPaymentMethod();
        extension.thisPaymentMethod.Payment_Method__c = 'Credit Card';
        extension.thisPaymentMethod.Card_Number_Encrypted__c = '4111111111111111';
        extension.thisPaymentMethod.Month__c = 10;
        extension.upsertPaymentMethod();
        extension.thisPaymentMethod.Payment_Method__c = 'Credit Card';
        extension.thisPaymentMethod.Card_Number_Encrypted__c = '4111111111111111';
        extension.thisPaymentMethod.Month__c = 10;
        extension.thisPaymentMethod.Year__c = 2050;
        extension.upsertPaymentMethod();
        extension.thisPaymentMethod.Payment_Method__c = 'Credit Card';
        extension.thisPaymentMethod.Card_Number_Encrypted__c = '4111111111111111';
        extension.thisPaymentMethod.Month__c = 15;
        extension.thisPaymentMethod.Year__c = 2050;
        extension.upsertPaymentMethod();
        extension.thisPaymentMethod.Payment_Method__c = 'Credit Card';
        extension.thisPaymentMethod.Card_Number_Encrypted__c = '4111111111111111';
        extension.thisPaymentMethod.Month__c = 10;
        extension.thisPaymentMethod.Year__c = 2015;
        extension.upsertPaymentMethod();
        Test.StopTest();
    }
    
    @isTest
    static void testUpsertPaymentMethod3() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createPolicy();
        df.createPaymentMethodsPolicy();
        df.createOrdersPolicy();
        df.createTransactionPolicy();
        
        ApexPages.currentPage().getParameters().put('id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        extension.thisPaymentMethod.Payment_Method__c = 'Credit Card';
        extension.thisPaymentMethod.Card_Number_Encrypted__c = '4111111111111111';
        extension.thisPaymentMethod.Month__c = 10;
        extension.thisPaymentMethod.Year__c = 2050;
        extension.thisPaymentMethod.Set_as_default__c = true;
        extension.upsertPaymentMethod();
        extension.thisPaymentMethod.Payment_Method__c = 'Billed In';
        extension.billedIn = '';
        extension.upsertPaymentMethod();
        extension.thisPaymentMethod.Payment_Method__c = 'Billed In';
        extension.billedIn = '7654321';
        extension.upsertPaymentMethod();
        extension.thisPaymentMethod.Payment_Method__c = 'Billed In';
        extension.billedIn = '1234567';
        extension.upsertPaymentMethod();
        Test.StopTest();
    }
    
    @isTest
    static void testaddPolicyRenewalMethod() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createPolicy();
        df.createPaymentMethodsPolicy();
        df.createOrdersPolicy();
        df.createTransactionPolicy();
        
        ApexPages.currentPage().getParameters().put('id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        extension.thisPolicyRenewal.Policy__c = df.newPolicy.id;
        extension.thisPolicyRenewal.Contract_Term__c = '1 Year';
        extension.thisPolicyRenewal.Service_Call_fee_DD__c = '100';
        extension.thisPolicyRenewal.Renewal_Status__c = 'Active';
        extension.thisPolicyRenewal.of_Payments__c = '2 Payments';
        extension.thisPolicyRenewal.Renewal_Amount__c = 1;
        extension.thisPolicyRenewal.Wait_Period__c = 1;
        extension.thisPolicyRenewal.Free_Months__c = '1';
        extension.selectedRenewalUpgradeTransId = df.transactionPolicy.Id;
        extension.addPolicyRenewalMethod();    
        extension.thisPolicyRenewal = NULL ;
        extension.addPolicyRenewalMethod();
        Test.StopTest();
    }
    
    @isTest
    static void testupdatePolicyRenewalMethod() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createPolicy();
        df.createPaymentMethodsPolicy();
        df.createOrdersPolicy();
        df.createTransactionPolicy();
        
        ApexPages.currentPage().getParameters().put('id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        extension.updatePolicyRenewalMethod();
        Test.StopTest();
    }
    
    @isTest
    static void testChargeCard() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createPolicy();
        df.createPaymentMethodsPolicy();
        
        ApexPages.currentPage().getParameters().put('id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        extension.paymentMethodRecordUpdateId = df.pMethodCardPolicy.id;
        extension.chargePaymentAmount = 20.00;
        extension.chargeReason = 'Additional Policy';
        extension.submitCharge();
        extension.charge();
        Test.StopTest();
    }
    
    @isTest
    static void testChargeCheck() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createPolicy();
        df.createPaymentMethodsPolicy();
        
        ApexPages.currentPage().getParameters().put('id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        
        extension.paymentMethodRecordUpdateId = df.pMethodCheckPolicy.id;
        extension.chargePaymentAmount = 20.00;
        extension.chargeReason = 'Additional Policy';
        extension.submitCharge();
        extension.charge();
        Test.StopTest();
    }
    
    @isTest
    static void testVoidTrans() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createPolicy();
        df.createPaymentMethodsPolicy();
        df.createOrdersPolicy();
        df.createTransactionPolicy();
        
        ApexPages.currentPage().getParameters().put('id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        
        extension.thisTransactionId = df.transactionPolicy.id;
        extension.voidTrans();   
        
        //cover exception block 
        extension.thisTransactionId = df.newGateway.id;
        extension.voidTrans();    
        Test.StopTest();
    }
    @isTest
    static void testRefundTrans() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createPolicy();
        df.createPaymentMethodsPolicy();
        df.createOrdersPolicy();
        df.createTransactionPolicy();
        
        ApexPages.currentPage().getParameters().put('id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        extension.refundTrans(); 
        
        extension.thisTransactionId = df.transactionPolicy.id;
        extension.refundTrans();   
        
        //cover exception block 
        extension.thisTransactionId = df.newGateway.id;
        extension.refundTrans();    
        Test.StopTest();
    }
    @isTest
    static void testRebillTrans() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createPolicy();
        df.createPaymentMethodsPolicy();
        df.createOrdersPolicy();
        df.createTransactionPolicy();
        
        ApexPages.currentPage().getParameters().put('id', df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        
        extension.thisTransactionId = df.transactionPolicy.id;
        extension.rebillTrans();   
        
        //cover exception block 
        extension.thisTransactionId = df.newGateway.id;
        extension.rebillTrans();    
        Test.StopTest();
    }
    @isTest
    static void testSaveCaseInlineRecords() {     
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        
        Case case2a = df.newCase.clone(false, true, false, false);
        INSERT case2a;

        Case case3a = df.newCase.clone(false, true, false, false);
        INSERT case3a;
        
        case3a.Status = 'Redispatch';
        update case3a;
        
        
        

        ApexPages.currentPage().getParameters().put('id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        extension.saveCaseInlineRecords();
        extension.caseList[0].status = NULL;
        extension.saveCaseInlineRecords();
        System.assert( extension.errorMsg.contains('Please fill the Status Field')); 
        Test.StopTest();

    }
    
    @isTest
    static void testDispatchAndUnassign() {     
        DispatchTests.createProductionSettings('SPOFFER');
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        df.createWorkOrder();
        
        df.newCase.Status = 'Ready to Dispatch';
        update df.newCase;
        
        Test.StartTest();
        ApexPages.currentPage().getParameters().put('id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        extension.selectedCaseId = df.newCase.Id;
        extension.selectedCaseSuggestedContractor = df.contractor.Id;
        extension.selectedWOId = df.workOrder.Id;
        extension.selectedCaseServiceProvided = 'Hvac';
        extension.quickDispatch();
        extension.unassignCase();
        
        Test.StopTest();
    }
    
    @isTest
    static void testAddRelatedPolicy() {
        Test.StartTest();

        df.createGlobalSetting();
        df.createPolicy();
        df.createPaymentMethodsPolicy();
        df.createOrdersPolicy();
        df.createRelatedPolicies();
        
        ApexPages.currentPage().getParameters().put('id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        Contract pol2 = df.newPolicy.clone(false, true, false, false);
        INSERT pol2;
        
        extension.findRelatedPolicy();
        extension.addRelatedPolicy();

        extension.relatedPolicySearchStr = 'test@test.com';
        extension.findRelatedPolicy();
        extension.relatedPolicyAdds[0].add = TRUE;
        extension.addRelatedPolicy();
        
        df.newPolicy.Status = 'Payment Declined';
        update df.newPolicy;
        Test.StopTest();

    }
    /*@isTest
    static void testAddRelatedPolicy1() {
        Test.StartTest();

        df.createGlobalSetting();
        df.createPolicy();
        df.createPaymentMethodsPolicy();
        df.createOrdersPolicy();
        df.createRelatedPolicies();
        
        ApexPages.currentPage().getParameters().put('id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        extension.thisRelatedPolicy = df.rPolicy2;
        extension.currentRecordId = df.newPolicy.id;
        extension.addRelatedPolicy();
        
        df.newPolicy.Status = 'Payment Declined';
        update df.newPolicy;
        Test.StopTest();
    }*/
    
    @isTest
    static void testAddNewSubscription() {
        Test.StartTest();

        df.createGlobalSetting();
        df.createPolicy();
        
        ApexPages.currentPage().getParameters().put('id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        extension.currentRecordId = df.newPolicy.id;
        extension.addNewSubscription();
        Test.StopTest();
    }
    
    @isTest
    static void testCreateSpecialRequest() {
        Test.StartTest();

        df.createGlobalSetting();
        df.createPolicy();
        
        ApexPages.currentPage().getParameters().put('id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        extension.getSpecialRequestDepts();
        extension.specialRequestComment = 'test';
        extension.createSpecialRequest();
        extension.specialRequestDepartment = 'Sales';
        extension.createSpecialRequest();
        Test.StopTest();
    }
    
    @isTest
    static void testDeleteRecord1() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createPolicy();
        df.createPaymentMethodsPolicy();
        df.createRenewal();
        df.createCase();
        df.createAdminNotePolicy();

        ApexPages.currentPage().getParameters().put('id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.currentPage().getParameters().put('tab','claimAndCase');
        System.assertEquals( 'claimAndCase', Apexpages.currentpage().getparameters().get( 'tab' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController( controller );
        
        //Case record delete
        extension.ObjectName = 'Case';
        extension.deleteRelatedRecordId = df.newCase.id;
        extension.deleteRecord();
        
        //adminNote record delete
        extension.ObjectName = 'Admin_Note__c';
        extension.deleteRelatedRecordId = df.adminNotePolicy.id;
        extension.deleteRecord();
        
        //policy renewal record delete
        extension.ObjectName = 'Policy_Renewal__c';
        extension.deleteRelatedRecordId = df.policyRenewal.id;
        extension.deleteRecord();
        Test.StopTest();
    }
    
    @isTest
    static void testDeleteRecord2() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createPolicy();
        df.createPaymentMethodsPolicy();
        df.createRelatedPolicies();

        ApexPages.currentPage().getParameters().put('id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.currentPage().getParameters().put('tab','claimAndCase');
        System.assertEquals( 'claimAndCase', Apexpages.currentpage().getparameters().get( 'tab' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController( controller );
        
        //Related Policy record delete
        extension.ObjectName = 'Related_Policies__c';
        extension.deleteRelatedRecordId = df.rPolicy1.id;
        extension.deleteRecord();
        
        extension.ObjectName = 'test';
        extension.deleteRelatedRecordId = 'testId';
        //delete record 
        extension.deleteRecord();
        //delete record exception
        System.assert( extension.errorMsg.contains('sObject type \'test\' is not supported.')); 
        Test.StopTest();
    }
    
    @isTest
    static void testCreditCardRequest() {
        Test.StartTest();
        
        df.createGlobalSetting();
        df.createPolicy();
        
        ApexPages.currentPage().getParameters().put('id',df.newPolicy.id );
        System.assertEquals( df.newPolicy.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.newPolicy );
        PolicyTabController extension = new PolicyTabController(controller);
        
        extension.creditCardRequest();   
        
        Test.StopTest();
    }
}