global without sharing class QuickDispatchRequestController {
    public Quick_Dispatch_Request__c thisQDR {get;set;}  
    private ApexPages.StandardController controller;
    private String recordId;
    public String selectedResponse {get;set;}
    public List<SelectOption> yesNo {
        get {
            List<SelectOption> options = new List<SelectOption>(); 
            options.add(new SelectOption('Yes','Yes')); 
            options.add(new SelectOption('No','No')); 
            return options;
        }
        public set;
    }
    public Boolean display {get;set;}
    public String ipAddress {get; set;}
    public String errorMsg {get; set;}
    public String comment {get; set;}
    public Boolean thankYou {get;set;}

    public QuickDispatchRequestController() {
        system.debug('QuickDispatchRequestController');

        recordId = ApexPages.currentPage().getParameters().get('token');
        display = false;
        
        thisQDR = [SELECT Id, Account__r.Name, Response__c, Comment__c, Response_Date_Time__c, Response_IP__c
                   FROM Quick_Dispatch_Request__c 
                   WHERE Id = :recordId];
        
        if (thisQDR.Response_Date_Time__c == NULL) {
            display = true;
        }
        
        selectedResponse = 'Yes';
        thankYou = false;
        
        if (thisQDR.Response__c != NULL) {
            thankYou = true;
        } else {
            thankYou = false;
        }

        ipAddress = ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');
    }
    
    public void response() {
        system.debug('response');
        system.debug('selectedResponse: ' + selectedResponse);
        errorMsg = '';

        try{
            thisQDR.Response__c = selectedResponse;
            thisQDR.Response_IP__c = ipAddress;
            thisQDR.Response_Date_Time__c = DateTime.now();
            thisQDR.Comment__c = comment;

            update thisQDR;
            thankYou = true;
        } catch(Exception ex) {
            if ( String.isNotBlank( ex.getMessage()) && ex.getMessage().contains( 'error:' ) ) {
                system.debug('----------------'+ex.getMessage());
                errorMsg = ex.getMessage().split('error:')[1].split(':')[0] + '.';
            } else {
                errorMsg = ex.getMessage();  
            }
        }
    }
}