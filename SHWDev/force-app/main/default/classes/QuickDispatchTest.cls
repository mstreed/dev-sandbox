@isTest
private class QuickDispatchTest {
    private static DataFactory df = new DataFactory();

    @isTest
    static void testQuickDispatchRequest() {
        Test.StartTest();
        df.createGlobalSetting();
        
        df.contractor.Business_Email__c = 'test@test.com';
        df.contractor.GL_Policy__c = TRUE;
        df.contractor.GL_Expiration__c = Date.today().addDays(15);
        df.contractor.Crim__c = 'Y';
        df.contractor.Average_Rating__c = 5;
        df.contractor.of_Ratings__c = 10;
        df.contractor.Average_Rating_Higher_Than_3_5_Since__c = Date.today().addDays(-15);
        df.contractor.Quick_Dispatch_Goodbye_Triggered__c = TRUE;
        df.contractor.Quick_Dispatch_Goodbye_Trigger_Date_Time__c = DateTime.now().addDays(10);
        //df.contractor.Do_Not_Qualify_for_Quick_Dispatch__c;
        //df.contractor.Qualified_for_Quick_Dispatch__c;
        //df.contractor.Qualified_Unqualified_for_Quick_Dispatch__c;
        update df.contractor;
                
        Quick_Dispatch_Request__c qdr = new Quick_Dispatch_Request__c (
            Account__c = df.contractor.Id,
            Contractor_Email__c = df.contractor.Business_Email__c
        );
        insert qdr;
        
        PageReference pageRef = Page.QuickDispatchRequest;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('token',qdr.Id);
        QuickDispatchRequestController extension = new QuickDispatchRequestController();

        //UPDATE QDR
        List<SelectOption> yesNo = extension.yesNo;
        extension.selectedResponse = 'Yes';
		extension.response();
        extension.selectedResponse = 'No';
        extension.thisQDR.Comment__c = 'Nopeee';
		extension.response();
        
        df.contractor.Average_Rating_Higher_Than_3_5_Since__c = NULL;
        update df.contractor;
        
        df.contractor.Average_Rating__c = 1;
        df.contractor.Send_GL_Expiration_Email__c = TRUE;
        df.contractor.Quick_Dispatch_Reevaluate_Trigger__c = TRUE;
        update df.contractor;
        Test.StopTest();
    }
}