public class RecursiveTriggerHandler{
    public static Boolean isFirstTimeAccount = TRUE;
    public static Boolean isFirstTimeWorkOrder = TRUE;
    public static Boolean isFirstTime = TRUE;
    public static Boolean isFirstTime2 = TRUE;
    public static Boolean isFirstTimePardotEmail = TRUE;
    public static boolean runOnce(){
        if(isFirstTime){
            isFirstTime = FALSE;
            return TRUE;
        }else{
            return isFirstTime;
        }
    }
    public static boolean runOnce2(){
        if(isFirstTime2){
            isFirstTime2 = FALSE;
            return TRUE;
        }else{
            return isFirstTime2;
        }
    }
    public static boolean runOnceAccount(){
        if(isFirstTimeAccount){
            isFirstTimeAccount = FALSE;
            return TRUE;
        }else{
            return isFirstTimeAccount;
        }
    }
    public static boolean runOnceWorkOrder(){
        if(isFirstTimeWorkOrder){
            isFirstTimeWorkOrder = FALSE;
            return TRUE;
        }else{
            return isFirstTimeWorkOrder;
        }
    }
}