/*

    @Description    RecursiveTriggerHandler Class 
    @createdate     21 May 2018

*/
@isTest
private class RecursiveTriggerHandlerTest {
	
	@isTest 
	private static void test1() {
		Test.startTest();

		RecursiveTriggerHandler.runOnce();
		System.assertEquals(FALSE,RecursiveTriggerHandler.isFirstTime);
		
        RecursiveTriggerHandler.runOnce2();
		System.assertEquals(FALSE,RecursiveTriggerHandler.isFirstTime2);
        
        RecursiveTriggerHandler.runOnceAccount();
		System.assertEquals(FALSE,RecursiveTriggerHandler.isFirstTimeAccount);
        
        RecursiveTriggerHandler.runOnceWorkOrder();
		System.assertEquals(FALSE,RecursiveTriggerHandler.isFirstTimeWorkOrder);
		Test.stopTest();
	}
	
}