global class ReimbursementAgreementController {
    public RA_Consent__c thisRAConsent {get;set;}  
    public Boolean display {get;set;}
    public String ipAddress {get; set;}
    private ApexPages.StandardController controller;
    private Id recordId;

    public ReimbursementAgreementController() {
        recordId = ApexPages.currentPage().getParameters().get('token');
        
        thisRAConsent = [SELECT
                         Id,
                         Name, 
                         CreatedDate, 
                         CreatedById,
                         CreatedBy.Name,
                         LastModifiedDate, 
                         LastModifiedById, 
                         Claim__c, 
                         Claim__r.Status, 
                         Claim__r.ContactId, 
                         Claim__r.ContactEmail, 
                         Customer_IP_Address__c, 
                         Sent__c, 
                         Customer_Agreed__c, 
                         Viewed__c, 
                         Agreed__c, 
                         Amount__c, 
                         Claim_Text__c, 
                         Property_Address__c, 
                         Policy_Text__c, 
                         Policy_Name__c, 
                         View_Counter__c, 
                         RA_URL__c,
                         RA_Link__c
                         FROM RA_Consent__c 
                         WHERE Id = :recordId];
        
        for (RA_Consent__c ra : [SELECT 
                                 Id,
                                 Customer_Agreed__c
                                 FROM RA_Consent__c
                                 WHERE Claim__c = :thisRaConsent.Claim__c
                                 ORDER BY CreatedDate
                                 LIMIT 50000]) {
                                     if (ra.Customer_Agreed__c) {
                                         display = FALSE;
                                         break;
                                     }
                                     if (ra.Id == thisRaConsent.Id) {
                                         display = TRUE;
                                     } else {
                                         display = FALSE;
                                     }
                                 }
        
        ipAddress = ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');
        
    }
    
    public void setViewed() {
        if (thisRAConsent.Viewed__c == null) {
            thisRAConsent.Viewed__c = DateTime.now();
        }
        if (thisRAConsent.Customer_Agreed__c == false) {
            thisRAConsent.Customer_IP_Address__c = ipAddress;
            thisRAConsent.View_Counter__c++;
        }
        update thisRAConsent;
    }
    
    public void saveAndTY() {
        thisRAConsent.Customer_IP_Address__c = ipAddress;
        thisRAConsent.Customer_Agreed__c = true;
		thisRAConsent.Agreed__c = DateTime.now();
        thisRAConsent.Claim__r.Status = 'RA Received';
        
        Case thisCase = new Case(
            Id=thisRAConsent.Claim__c,
            Status = 'RA Received'
        );

        update thisRAConsent;
        update thisCase;
        
        OrgWideEmailAddress owa = [SELECT Id, DisplayName, Address FROM OrgWideEmailAddress WHERE DisplayName = 'SHW Claims' LIMIT 1];
        EmailTemplate templateId = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'Reimbursement_Agreement_Confirm'];
        String[] toAddresses = new String[] {thisRAConsent.Claim__r.ContactEmail};
            system.debug('thisRAConsent.Claim__r.ContactEmail: ' + thisRAConsent.Claim__r.ContactEmail);
        
        Messaging.SingleEmailMessage eml = new Messaging.SingleEmailMessage();
        eml.setTargetObjectId(thisRAConsent.Claim__r.ContactId); 
        eml.setTemplateID(templateId.Id); 
        eml.setSaveAsActivity(true);
        eml.setOrgWideEmailAddressId(owa.id);
        eml.setToAddresses(toAddresses);
        eml.setWhatId(thisRAConsent.Id);
        system.debug('eml: ' + eml);
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { eml });
        system.debug('result: ' + r);
        
    }
    
}