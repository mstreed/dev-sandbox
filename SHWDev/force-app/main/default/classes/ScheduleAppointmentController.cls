public class ScheduleAppointmentController {

    @AuraEnabled
    public static AvailabilityWrapper getAvailability(String recordId){       
        SearsAvailabilityQWrapper saqw = new SearsAvailabilityQWrapper();
        SearsAvailabilitySWrapper sasw = new SearsAvailabilitySWrapper();
        AvailabilityWrapper sw = new AvailabilityWrapper();
        HttpResponse response = new HttpResponse();
        if(response.)
        response = SearsRequest.searsCheckAvailability(SearsAvailabilityQWrapper.stringify(saqw));
        sasw = SearsAvailabilitySWrapper.parse(response.getBody());
        sw.availWrapper= sasw;
        return sw;
        
    }

    @AuraEnabled
    public static Case getCaseRecord(String recordId){
        
        if(String.isBlank(recordId)){
            return null;
        }
        Map<String, Schema.sObjectField> sObjectFieldMap = Schema.getGlobalDescribe().get('Case').getDescribe().fields.getMap();
        Map<String, String> fieldMap = new Map<String, String>();
        Case caseRec;    
        for(Schema.SObjectField sfield: sObjectFieldMap.Values()){
            Schema.DescribeFieldResult fieldDesc = sfield.getDescribe();
            if(Schema.SObjectType.Case.fields.getMap().get(fieldDesc.getName()).getDescribe().isAccessible()){
                fieldMap.put(fieldDesc.getName(), fieldDesc.getType().name());
            }
        }
        String sObjectFields = '';
        for(String field: fieldMap.keySet()){
            sObjectFields += field+',';
        }
        sObjectFields = sObjectFields.removeEnd(',');
        String soqlQuery = 'SELECT '+ sObjectFields +' FROM Case WHERE Id =: recordId LIMIT 1';
        for(Case csRec : Database.query(soqlQuery)){
            csRec.Property_Address__c.stripHtmlTags();
            caseRec = csRec;
        }
        System.debug('caseRec^^' + caseRec);
        return caseRec;
    }
    @AuraEnabled
    public static AvailabilityWrapper getAllAppointments(){
        SearsAvailabilitySWrapper availWrapper = new SearsAvailabilitySWrapper();
        AvailabilityWrapper aWrapper = new AvailabilityWrapper();
        Map<String,List<SearsAvailabilitySWrapper.Offers>> offerDataMap = new Map<String,List<SearsAvailabilitySWrapper.Offers>>();
        Map<String,List<SearsAvailabilitySWrapper.Offers>> offerDataStoreMap = new Map<String,List<SearsAvailabilitySWrapper.Offers>>();
        String jsonBody = '{ "CorrelationId":"SB-aug4-test3", "customer":{ "customerCity":"SCHAUMBURG", "customerState":"IL", "customerAddress":"1273 REGENCY CT", "customerZipCode":"60193", "customerZipCodeExt":null }, "zipCode": "60193", "merchandiseCode":"REFRIG", "serviceTypeNeeded":"R", "coverageCode": "CC" } ';
        HttpResponse res = new HttpResponse();
        
        try{
            res = SearsRequest.searsCheckAvailability(jsonBody);
            System.debug('DEBUG|res: ' + res);
            System.debug('DEBUG|res.getBody(): ' + res.getBody());
            availWrapper = (SearsAvailabilitySWrapper)JSON.deserialize(res.getBody(), SearsAvailabilitySWrapper.class);
            if(res.getStatusCode() == 200){
                aWrapper.statusCode = 'Success';
            }else{
                aWrapper.statusCode = 'Error';
                aWrapper.statusMessage = String.valueOf(availWrapper.messages);
            }
            availWrapper.todayDateValue = System.now().format('EEEE');
            aWrapper.availWrapper = availWrapper;
            if(availWrapper.offers != null && availWrapper.offers.size() > 0){
                for(SearsAvailabilitySWrapper.Offers offerRec : availWrapper.offers){
                    String dayData = offerRec.availableDay + ',' + offerRec.availableDate;
                    if(!offerDataMap.containsKey(dayData)){
                        offerDataMap.put(dayData,new List<SearsAvailabilitySWrapper.Offers>());
                    }
                    offerDataMap.get(dayData).add(offerRec);
                }
                
                for(SearsAvailabilitySWrapper.Offers offerRec : availWrapper.offers){
                    String dayData = offerRec.availableDay + ',' + offerRec.availableDate;
                    OffersData oData = new OffersData();
                    oData.offerName = 'Offer' + Integer.valueOf(Math.random() * 100);
                    oData.availableDay = offerRec.availableDay;
                    DateTime offerDate = Date.valueOf(offerRec.availableDate);
                    oData.availableDate = offerDate.format('MMMM') + ' ' +Date.valueOf(offerRec.availableDate).day();
                    if(offerDataMap.containsKey(dayData) && !offerDataStoreMap.containsKey(dayData)){
                        offerDataStoreMap.put(dayData,offerDataMap.get(dayData));
                        oData.availOffers = offerDataMap.get(dayData);
                        aWrapper.offerData.add(oData);
                    }
                    
                }
            }
            System.debug('aWrapper^^' + JSON.serialize(aWrapper));
            System.debug('availWrapper^^' + JSON.serialize(availWrapper));
        }catch(Exception ex){
            System.debug('exception occured on check availability^^' + ex.getMessage());
            aWrapper.statusCode = 'Error';
            aWrapper.statusMessage = String.valueOf(ex);
        }
        return aWrapper;
    }
    
    public class AvailabilityWrapper{
        @AuraEnabled public String statusCode;
        @AuraEnabled public String statusMessage;
        @AuraEnabled public SearsAvailabilitySWrapper availWrapper {get;set;}
        @AuraEnabled public List<OffersData> offerData {get;set;}
        public AvailabilityWrapper(){
            offerData = new List<OffersData>();
        }
    }

    @AuraEnabled
    public static CreateOrderWrapper scheduleAnAppointment(String availWrapData, String selectedOfferData, String caseData){
        Case caseRec = (Case)JSON.deserialize(caseData, Case.class);
        AvailabilityWrapper aWrapperData = (AvailabilityWrapper)JSON.deserialize(availWrapData, AvailabilityWrapper.class);
        SearsAvailabilitySWrapper.Offers offerRec = (SearsAvailabilitySWrapper.Offers)JSON.deserialize(selectedOfferData, SearsAvailabilitySWrapper.Offers.class);
        
        System.debug('caseRec^^' + caseRec);
        System.debug('offerRec^^' + offerRec);
        SearsCreateOrderQWrapper createOrderWrap = new SearsCreateOrderQWrapper();
        createOrderWrap.correlationId = aWrapperData.availWrapper.CorrelationId;
        createOrderWrap.svcUnitNumber = aWrapperData.availWrapper.serviceUnitNumber;
        createOrderWrap.creatorUnit = '0007999';
        createOrderWrap.creatorId = '9999494';
        createOrderWrap.offerId = aWrapperData.availWrapper.offerID;
        createOrderWrap.providerId = offerRec.provider;
        createOrderWrap.originationCode = 'SHH';
        createOrderWrap.servicingOrgCd = 'A';
        createOrderWrap.firstAvailableDate = offerRec.availableDate;
        createOrderWrap.orderType = 'SIT';
        createOrderWrap.customer = new SearsCreateOrderQWrapper.Customer();
        createOrderWrap.customer.key = '';
        createOrderWrap.customer.specialInstructions = 'Autho Req : N , Notes : null';
        createOrderWrap.customer.name = new SearsCreateOrderQWrapper.Name();
        createOrderWrap.customer.name.firstName = caseRec.First_Name__c;
        createOrderWrap.customer.name.lastName = caseRec.Last_Name__c;
        createOrderWrap.customer.address = new SearsCreateOrderQWrapper.Address();
        createOrderWrap.customer.address.addressLine1 = caseRec.Address_1_RA__c;
        createOrderWrap.customer.address.addressLine2 = null;
        createOrderWrap.customer.address.aptNumber = null;
        createOrderWrap.customer.address.city = caseRec.Property_City__c;
        createOrderWrap.customer.address.state = caseRec.Property_State__c;
        createOrderWrap.customer.address.zipCode = caseRec.Property_Zip__c;
        createOrderWrap.customer.address.zipcodeSuffix = null;
        createOrderWrap.customer.contactInfo = new SearsCreateOrderQWrapper.ContactInfo();
        createOrderWrap.customer.contactInfo.contactType = 'H';
        createOrderWrap.customer.contactInfo.contactAddress = '4024401781';
        createOrderWrap.customer.contactInfo.contactAddressType = 'Phone';
        createOrderWrap.customer.custType = 'H';
        createOrderWrap.merchandise = new SearsCreateOrderQWrapper.Merchandise();
        createOrderWrap.merchandise.modelNumber = caseRec.Model__c;
        createOrderWrap.merchandise.brandName= caseRec.Brand__c;
        createOrderWrap.merchandise.purchaseDate = String.valueOf(caseRec.Policy_Purchase_Date__c);
        createOrderWrap.merchandise.installedDate = '2021-07-29';
        createOrderWrap.merchandise.serialNumber = caseRec.Serial_Number__c;
        createOrderWrap.merchandise.merchCode = 'DRYERE';
        createOrderWrap.serviceInfo = new SearsCreateOrderQWrapper.ServiceInfo();
        createOrderWrap.serviceInfo.requestedDate = offerRec.availableDate;
        createOrderWrap.serviceInfo.requestedStartTime = offerRec.timeWindowStart;
        createOrderWrap.serviceInfo.requestedEndTime = offerRec.timeWindowEnd;
        createOrderWrap.serviceInfo.svcRequestedText = offerRec.techDescription;
        createOrderWrap.serviceInfo.serviceSpecialInstructions = 'Autho Req : N , Notes : null';
        createOrderWrap.serviceInfo.svcProvidedCode = caseRec.Service_Provided__c;
        createOrderWrap.serviceInfo.remittance = new SearsCreateOrderQWrapper.Remittance();
        createOrderWrap.serviceInfo.remittance.coverageCode = 'CC';
        createOrderWrap.serviceInfo.remittance.paymentMethod = 'CA';
        createOrderWrap.serviceInfo.remittance.chargeAccount = null;
        createOrderWrap.serviceInfo.remittance.expirationDate = null;
        createOrderWrap.serviceInfo.remittance.token = null;
        createOrderWrap.serviceInfo.remittance.chargeTokenFlag = null;
        createOrderWrap.serviceInfo.thirdPartyInfo = new SearsCreateOrderQWrapper.ThirdPartyInfo();
        createOrderWrap.serviceInfo.thirdPartyInfo.thirdPartyId = 'SHH000';
        createOrderWrap.serviceInfo.thirdPartyInfo.thirdPartyAuthNumber = null;
        createOrderWrap.serviceInfo.thirdPartyInfo.contractNumber = caseRec.Contractor_Number__c;
        createOrderWrap.serviceInfo.thirdPartyInfo.contractExpirationDate = String.valueOf(caseRec.Invoice_Due_Date__c);
        createOrderWrap.serviceInfo.thirdPartyInfo.deleteFlag = 'Y';
        createOrderWrap.serviceInfo.emergencyFlag = 'N';
        createOrderWrap.serviceInfo.serviceDuration = '43';
        createOrderWrap.serviceInfo.serviceLocation = new SearsCreateOrderQWrapper.ServiceLocation();
        createOrderWrap.serviceInfo.serviceLocation.type = 'SiteRepairServiceInfo';
        createOrderWrap.serviceInfo.serviceLocation.workAreaCode = '';
        createOrderWrap.serviceInfo.serviceLocation.capacityArea = '';
        createOrderWrap.svcFamCd = 'A';
        
        HttpResponse res = new HttpResponse();
        SearsCreateOrderSWrapper createOrderSWrapper = new SearsCreateOrderSWrapper();
        CreateOrderWrapper oWrapper = new CreateOrderWrapper();
        String jsonBody = JSON.serialize(createOrderWrap);
        try{
            res = SearsRequest.searsCreateOrder(jsonBody);
            System.debug('DEBUG|res:^^' + res);
            System.debug('DEBUG|schedule^^' + res.getBody());
            createOrderSWrapper = (SearsCreateOrderSWrapper)JSON.deserialize(res.getBody(), SearsCreateOrderSWrapper.class);
            if(res.getStatusCode() == 200){
                oWrapper.statusCode = 'Success';
            }else{
                oWrapper.statusCode = 'Error';
                oWrapper.statusMessage = String.valueOf(createOrderSWrapper.messages);
            }
        }catch(Exception ex){
            System.debug('exception occured on schedule^^' + ex.getMessage());
            oWrapper.statusCode = 'Error';
            oWrapper.statusMessage = String.valueOf(ex);
        }
        System.debug('oWrapper^^' + JSON.serialize(oWrapper));
        return oWrapper;
    }
    
    public class CreateOrderWrapper{
        @AuraEnabled public String statusCode;
        @AuraEnabled public String statusMessage;
        @AuraEnabled public SearsCreateOrderSWrapper orderSWrapper;
        public CreateOrderWrapper(){
            this.orderSWrapper = new SearsCreateOrderSWrapper();
        }
    }
    public class OffersData{
        @AuraEnabled public String offerName {get;set;}
        @AuraEnabled public String availableDay {get;set;}
        @AuraEnabled public String availableDate {get;set;}
        @AuraEnabled public List<SearsAvailabilitySWrapper.Offers> availOffers {get;set;}
        public OffersData(){
            availOffers = new List<SearsAvailabilitySWrapper.Offers>();
        }
    }
    
}