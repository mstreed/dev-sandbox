/**
 * @description       : Wrapper class to create request for availability callout
 * @author            : DYaste
 * @last modified on  : 09-06-2021
 * @last modified by  : DYaste
 * @Unit Test         : SearsWrapperTests.testSearsAvailabilityQWrapper
 * Modifications Log
 * Ver   Date         Author   Modification
 * 1.0   09-05-2021   DYaste   Initial Version
**/
public without sharing class SearsAvailabilityQWrapper {
    public SearsAvailabilityQWrapper(){}
    //this follows the rest documentation for reference
    public String serviceName {get;set;};
    public String serviceVersion {get;set;};
    public String clientId {get;set;};
    public String userId {get;set;};
    public String currentDateTime {get;set;};
    public String authorization {get;set;};
    public String clientCode {get;set;};    
    public String correlationId {get;set;};
    //public String serviceUnitNum;
    //public String serviceOrderNum;
    public String providerId {get;set;};
	public String zipCode {get;set;};
    public String serviceOrderNum {get;set;};
    public String bandName {get;set;};
	public String zipCodeExt {get;set;};
	public Customer customer {get;set;};	
    public String merchandiseCode {get;set;};
    public String brandName {get;set;};
    public String industryCode {get;set;};
    public String merchandiseSpecialtyCode {get;set;};
    public String coverageCode {get;set;};
    public String requestStartDate {get;set;};//YYYY-MM-DD
	public String requestEndDate {get;set;}; //YYYY-MM-DD
    public String numberOfTimeWindows {get;set;};
    public String serviceUnitNumber {get;set;};
    public String servicingOrganization {get;set;};
    public String serviceOrderTypeCode {get;set;};
    public String serviceTypeNeeded {get;set;};
	public String visitCount {get;set;};
	public String priorityCode {get;set;};
	public String jobCode {get;set;};
    public String jobCodeChargeCode {get;set;};
    public String serviceTimeRequired {get;set;};	
	public String processId {get;set;};
	public String helperRequired {get;set;};
    public String helperType {get;set;};
    public String emergencyFlag {get;set;};
    public String forceFlag {get;set;};
    public String rescheduleFlag {get;set;};
    public String recallFlag {get;set;};
    public String storeStock {get;set;};
    public String installDate {get;set;};
    public String purchaseDate {get;set;};
    public String paCoverage {get;set;};
    public String ssaCoverage {get;set;};
    public String requestPartCount {get;set;};
    public String requestPartType {get;set;};
    public String requestPartDate {get;set;};
    public Object partPickupFlag {get;set;};
	public Object partPickupUnitNum {get;set;};
    // public Date partEadDate; 
    public String requiredTech {get;set;};
    public String requestTechnicianID {get;set;};	
	public String requiredTechType {get;set;};
	public String capacityArea {get;set;};
	public String workAreaCode {get;set;};
	public String merchPriceTierCode {get;set;};
	public String chargeCodeAm {get;set;};
	public String remark1 {get;set;};
	public String remark2 {get;set;};
	public String timeZoneSign {get;set;};
	public String timeZoneValue {get;set;};
    public String thirdPartyId {get;set;};
    public String creatorUnitNo {get;set;};	  
	public String business {get;set;};
	public List<TimeSlot> TimeSlots {get;set;};
    public String itemSuffix {get;set;};
    public String repairAddress {get;set;};    
		
    public class TimeSlot{
        String fromTime {get;set;};
        String toTime {get;set;};
    }

    public class Customer {
        public Customer() {}
        public String customerNumber {get;set;};
        public String customerFirstName {get;set;};
        public String customerMiddleName {get;set;};
        public String customerLastName {get;set;};
        public String customerPhone {get;set;};
        public String customerAltPhone {get;set;};
        public String customerAddress {get;set;};
        public String customerCity {get;set;};
        public String customerState {get;set;};
        public String customerZipCode {get;set;};
        public String customerZipCodeExt {get;set;};
        public String customerLanguage {get;set;};        
    }

    public static String stringify(SearsAvailabilityQWrapper mySearsAvailabilityQWrapper){
        return  System.JSON.serialize(mySearsAvailabilityQWrapper, true);
    }

    public static String stringifyPretty(SearsAvailabilityQWrapper mySearsAvailabilityQWrapper){
        return  System.JSON.serializePretty(mySearsAvailabilityQWrapper, true);
    }
}