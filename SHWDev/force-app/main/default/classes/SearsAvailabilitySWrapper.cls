/**
 * @description       : Sears Availability ReSponse Wrapper class to be used to set requests
 * @author            : DYaste
 * @last modified on  : 09-06-2021
 * @last modified by  : DYaste
 * @Unit Test         :SearsWrapperTests.testSearsAvailabilitySWrapper
 * Modifications Log
 * Ver   Date         Author   Modification
 * 1.0   09-05-2021   DYaste   Initial Version
**/
public without sharing class SearsAvailabilitySWrapper {
	public SearsAvailabilitySWrapper(){}

	//this follows the rest documentation for reference
    @AuraEnabled public String responseCode;
	@AuraEnabled public String responseMessage;
    @AuraEnabled public String correlationId;
    @AuraEnabled public List<String>messages;
    @AuraEnabled public String dataFoundFlag;
	@AuraEnabled public String serviceUnitNumber;
	@AuraEnabled public String capacityArea;
	@AuraEnabled public Integer capacityNeeded;
	@AuraEnabled public String wrkAreaCd;
	@AuraEnabled public String mdsPriceTierCd;
	@AuraEnabled public Decimal chargeCodeAmount;
	@AuraEnabled public String unitType;
	@AuraEnabled public Integer numberOfOccurences;
	@AuraEnabled public String minorFlag;
	@AuraEnabled public String allDayFlag;
    @AuraEnabled public String alternateFromTime;
    @AuraEnabled public String alternateToTime;
    @AuraEnabled public String notFromTime;
    @AuraEnabled public String notToTime;
	@AuraEnabled public String emergency;
    @AuraEnabled public String timeZoneSign;
	@AuraEnabled public String timeZoneValue;
    @AuraEnabled public String coverageFlag;
    @AuraEnabled public String remark1;
    @AuraEnabled public String remark2;
	@AuraEnabled public List<BusinessHours> businessHours;
	@AuraEnabled public String offerID;
	@AuraEnabled public List<String> holidayDate;
	@AuraEnabled public List<Offers> offers;	
    @AuraEnabled public String todayDateValue;
	
	public class BusinessHours {
		@AuraEnabled public String fromTime;
		@AuraEnabled public String toTime;
	}

	public class Offers {
		@AuraEnabled public String provider;
		@AuraEnabled public String techId;
		@AuraEnabled public String techDescription;
		@AuraEnabled public String availableDate;
		@AuraEnabled public String timeWindowCd;
		@AuraEnabled public String timeWindowStart;
		@AuraEnabled public String timeWindowEnd;
		@AuraEnabled public String availableDay;
		@AuraEnabled public String priorityFlag;
        @AuraEnabled public String firmId;
        @AuraEnabled public boolean isOfferSelected;
        
}

	public static SearsAvailabilitySWrapper parse(String json){
		return (SearsAvailabilitySWrapper) System.JSON.deserialize(json, SearsAvailabilitySWrapper.class);
	}
}