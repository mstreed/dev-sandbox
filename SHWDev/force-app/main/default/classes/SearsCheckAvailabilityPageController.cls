// Controller for VF page that demos the checkAvailability callout
public class SearsCheckAvailabilityPageController {
    
    public String jsonResBody{get; set;}
    public SearsAvailabilitySWrapper wrapperObj{get; set;}
        
    public String getJsonReqBody(){
        String jsonReqBody = '{ "CorrelationId":"SB-August14-test4", "customer":{ "customerCity":"SCHAUMBURG", "customerState":"IL", "customerAddress":"1273 REGENCY CT", "customerZipCode":"60193", "customerZipCodeExt":null }, "zipCode": "60193", "merchandiseCode":"REFRIG", "serviceTypeNeeded":"R", "coverageCode": "CC" } ';
        return jsonReqBody;
    }
    
    // Send Http request and set response variables to be displayed
    public void sendRequest(){
        String jsonBody = getJsonReqBody();
        HttpResponse res = new HttpResponse();
        SearsAvailabilitySWrapper wrapperObj = new SearsAvailabilitySWrapper();
        
        res = SearsRequest.searsCheckAvailability(jsonBody);
        this.jsonResBody = res.getBody();
        
        wrapperObj = SearsAvailabilitySWrapper.parse(res.getBody());
        this.wrapperObj = wrapperObj;
        
    }
    
    

}