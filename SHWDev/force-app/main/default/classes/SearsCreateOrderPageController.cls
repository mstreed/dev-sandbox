// Controller for VF page that demos the createOrder callout
public class SearsCreateOrderPageController {
    
    public String jsonResBody{get; set;}
    public SearsCreateOrderSWrapper wrapperObj{get; set;}
    public String correlationId{get; set;}
    public String order{get; set;}
    public String unit{get; set;}
        
    // Change JSON body here for test/demo purposes
    public String getJsonReqBody(){
        String jsonReqBody = '{ "correlationId": "SB-August14-test12", "svcUnitNumber": "0007999", "creatorUnit": "0007999", "creatorId": "9999494", "offerId": "023615", "providerId": "W2", "originationCode": "SHH", "servicingOrgCode": "A", "firstAvailableDate": "2021-09-15", "orderType": "SIT", "customer": { "key": "", "specialInstructions": "Autho Req : N , Notes : null", "name": { "firstName": "MARGE", "lastName": "SIMPSON" }, "address": { "addressLine1": "742 EVERGREEN TERRACE", "addressLine2": null, "aptNumber": null, "city": "HOFFMAN ESTATES", "state": "IL", "zipCode": "60179", "zipcodeSuffix": null }, "contactInfo": { "contactType": "H", "contactAddress": "4024401781", "contactAddressType": "Phone" }, "custType": "H" }, "merchandise": { "modelNumber": "11068133410", "brandName": "Kenmore", "purchaseDate": "2021-07-28", "installedDate": "2021-07-29", "serialNumber": "", "merchCode": "DRYERE", "selectedMerchandise": { "id": "DRYERE", "label": "DRYERE - OVEN, BUILT-IN", "merchCode": "DRYERE", "merchDesc": "OVEN, BUILT-IN", "active": true, "specialtyCode": "AECOOKING1", "industryDesc": "MISC - A&E" }, "selectedBrand": { "id": "6076", "name": "Kenmore" } }, "serviceInfo": { "requestedDate": "2021-09-18", "requestedStartTime": "13:00", "requestedEndTime": "17:00", "svcRequestedText": "STILL NOT WORKING", "serviceSpecialInstructions": "Autho Req : N , Notes : null", "svcProvidedCode": "R", "remittance": { "coverageCode": "CC", "paymentMethod": "CA", "chargeAccount": null, "expirationDate": null, "token": null, "chargeTokenFlag": null }, "thirdPartyInfo": { "thirdPartyId": "SHH000", "thirdPartyAuthNumber": null, "contractNumber": "443524342/946111", "contractExpirationDate": "2099-01-01", "deleteFlag": "Y" }, "emergencyFlag": "N", "serviceDuration": "43", "serviceLocation": { "type": "SiteRepairServiceInfo", "workAreaCode": "", "capacityArea": "" } }, "svcFamCd": "A" }';
        return jsonReqBody;
    }
    
    // Send Http request and set response variables to be displayed
    public void sendRequest(){
        String jsonBody = getJsonReqBody();
        HttpResponse res = new HttpResponse();
        SearsCreateOrderSWrapper wrapperObj = new SearsCreateOrderSWrapper();
        
        res = SearsRequest.searsCreateOrder(jsonBody);
        this.jsonResBody = res.getBody();
        
        wrapperObj = SearsCreateOrderSWrapper.parse(res.getBody());
        this.wrapperObj = wrapperObj;
        this.correlationId = wrapperObj.CorrelationId;
        this.order = wrapperObj.order;
        this.unit = wrapperObj.unit;
    }
    
    
}