/**
 * @description       : 
 * @author            : DYaste
 * @last modified on  : 09-06-2021
 * @last modified by  : DYaste
 * @Unit Test         : $file 
 * Modifications Log
 * Ver   Date         Author   Modification
 * 1.0   09-05-2021   DYaste   Initial Version
**/
public without sharing class SearsCreateOrderQWrapper {
    public SearsCreateOrderQWrapper() {}

    public String clientId;
    public String Authorization;
    public String userId;
    public String correlationId;
    public String originatorCode;
    public String svcUnitNumber;
    public String creatorUnit;
    public String creatorId;
    public String offerId;
    public String providerId;
    public String sessionId;
    public String originationCode;
    public String svcFamCd;
    public String servicingOrgCd;
    public String firstAvailableDate;
    public String pmAddOnFlag;
    public String orderType;
    public Technician technician;
    public List<ContactInfo> contactInfos;
    public Customer customer;
    public Merchandise merchandise;
    public ServiceInfo serviceInfo;        
    
    public class Technician{
        public Technician() {}
        public String techID;
        public String techDescription;
        public String userId;
        public String returnTechnicianType;
        public String name;
        public String type;
    }

    public class ContactInfo {
        public ContactInfo() {}
    
        public String contactType;
        public String contactAddress;
        public String contactAddressType;
        public String extension;
    }

    public class Customer{
        public String key;
        public String specialInstructions;
        public String promoteMa;
        public String promoteOther;
        public String locationSuffix;
        public String custType;
        public String preferredLanguage;
        public String sywMemberId;
        public String storeNumber;
        public Name name;
        public Address address;
        public ContactInfo contactInfo;                        
    }

    public class Name {
        public Name() {}
        public String firstName;
        public String lastName;
        public String prefix;
        public String suffix;
        public String secondName;
        public String middleName;
    }

    public class Address {
        public Address() {}
    
        public String addressLine1;
        public String addressLine2;
        public String aptNumber;
        public String city;
        public String state;
        public String zipCode;
        public String zipcodeSuffix;
        public String crossStreet;
        public String addressFlag;
    }

    public class Merchandise{
        public String itemSuffix;
        public String merchCode;
        public String div;
        public String itemNum;
        public String modelNumber;
        public String brandName;
        public String serialNumber;
        public String purchaseDate;
        public String installedDate;
        public String searsPurchaseFlag;
        public String promoteMa;
        public String notPromoteMaReasonCode;
        public String promoteOther;
        public String notPromoteOtherReasonCode;
        public String purchaseStoreNo;
        // public Promote promote;               
    }
             
    public class Promote{
        public String promoteMa;
        public String notPromoteMaReasonCode;
        public String promoteOther;
        public String doNotPromoteOtherReasonCode;           
    } 

    public class ServiceInfo {
        public ServiceInfo() {}

        public String requestedDate;
        public String requestedStartTime;
        public String requestedEndTime;
        public String svcRequestedText;
        public String serviceSpecialInstructions;
        public String recallFlag;
        public String svcProvidedCode;
        public String emergencyFlag;
        public String marketCode;
        public String serviceDuration;
        public Remittance remittance ;
        public ThirdPartyInfo thirdPartyInfo ;
        public SiteRepairServiceInfo siteRepairServiceInfo ;
        public StoreRepairServiceInfo storeRepairServcieInfo ;
        public ShopRepairServiceInfo shopRepairServiceInfo ;
        public ServiceLocation serviceLocation;
        // public InstallationServiceInfo installationServiceInfo ;    
    }
    public class ServiceLocation{
        public String type;
        public String workAreaCode;
        public String capacityArea;
    }
    public class Remittance{
        public String coverageCode;
        public String additionalCoverageCode;
        public String paymentMethod;
        public String chargeAccount;
        public String expirationDate;
        public String purchaseOrder;
        public String token;
        public String couponNumber;
        public String taxGeoCode;
        public String countyCode;
        public String chargeTransKey;
        public String chargeTokenFlag;
        public String chargeHSDWFlag;                 
    }
    
    public class ThirdPartyInfo{            
        public String thirdPartyId;
        public String thirdPartyAuthNumber;
        public String contractNumber;
        public String contractExpirationDate;
        public String deleteFlag;
        public String thirdPartyOrderNumber ;
        public String thirdPartyShippingNumber;
    }
    
    public class SiteRepairServiceInfo{
        public String workAreaCode;
        public String capacityArea;
        public Address address;
        public ContactInfo contactInfo;               
    }
    
    public class StoreRepairServiceInfo{
        public String workAreaCode;
        public String capacityArea;
        public String storeNumber;
        public Name name;
        public Address address;
        public ContactInfo contactInfo;                               
    }
    
    public class ShopRepairServiceInfo{
        public String workAreaCode;
        public String capacityArea;
        public String svcTypeCd;
        public String returnToAddressCd;
        public String retToPartnerStoreNo;
        public String loanerNo;
        public String loanerSerialNo;
        public String loanerModelNo;
        public String repairUnitNo;
        public String repairTag;
        public String binLocationCode;
        public String estimateFlag;
        public String maxAmount;
        public String printSOFlag;
        public String printerId;
        public String rateCode;
        public String rateAmount;
        public String methodOfShipment;
        public String strOriCd;
        public String mdsInUnitFl;
        public String diagnosticFee;
        public String thirdPartySalesCheckNo;
    }
    
    public class InstallationServiceInfo{
        public String installerPickUpFlag;
        public String merchandisePickUp;
        public String deliveryScheduleDate;
        public String deliveryTimeCode;
        public List<String> jobCodes;
    }   

    public static String stringify(SearsCreateOrderQWrapper mySearsCreateOrderQWrapper){
        return  System.JSON.serialize(mySearsCreateOrderQWrapper, true);
    }

    public static String stringifyPretty(SearsCreateOrderQWrapper mySearsCreateOrderQWrapper){
    return  System.JSON.serializePretty(mySearsCreateOrderQWrapper, true);
    }
}