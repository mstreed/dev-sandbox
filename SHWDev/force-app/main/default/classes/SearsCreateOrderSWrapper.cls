/**
 * @description       : Sears Create Order ReSponse Wrapper
 * @author            : DYaste
 * @last modified on  : 09-06-2021
 * @last modified by  : DYaste
 * @Unit Test         : SearsWrapperTest 
 * Modifications Log
 * Ver   Date         Author   Modification
 * 1.0   09-05-2021   DYaste   Initial Version
**/
public without sharing class SearsCreateOrderSWrapper {
    public SearsCreateOrderSWrapper() {}

    //public String ContentType;
    //public DateTime date;
    @AuraEnabled public String CorrelationId;
    @AuraEnabled public String ResponseCode;
	@AuraEnabled public String ResponseMessage;    
    @AuraEnabled public String unit;
    @AuraEnabled public String order;
    @AuraEnabled public Integer custKey;
    @AuraEnabled public String itemSuffix;
    @AuraEnabled public String thirdPartyOrderNumber;
    @AuraEnabled public String thirdPartyShippingNumber;
    @AuraEnabled public List<String> messages;

    public static SearsCreateOrderSWrapper parse(String json){
		return (SearsCreateOrderSWrapper) System.JSON.deserialize(json, SearsCreateOrderSWrapper.class);
	}
}