/**
 * @description       : 
 * @author            : DYaste
 * @last modified on  : 09-15-2021
 * @last modified by  : DYaste
 * @Unit Test         : $file 
 * Modifications Log
 * Ver   Date         Author   Modification
 * 1.0   09-15-2021   DYaste   Initial Version
**/
// Controller for VF page that demos the cancelOrder callout
public class SearsDeleteOrderPageController {
    
    public String jsonResBody{get; set;}
    public SearsDeleteSWrapper wrapperObj{get; set;}
     
    public String getJsonReqBody(){
        String jsonReqBody = '{ "correlationId": "SB-August14-test4", "cancelerUser": "SHH", "cancelerEmployeeId": "9999494", "cancelerUnit": "0007999", "unit": "0007999", "order": "95000405", "instructions": "null", "reasonCode": "UC10" }';
        return jsonReqBody;
    }
    
    // Send Http request and set response variables to be displayed
    public void sendRequest(){
        String jsonBody = getJsonReqBody();
        HttpResponse res = new HttpResponse();
        SearsDeleteSWrapper wrapperObj = new SearsDeleteSWrapper();
        
        res = SearsRequest.searsCancelOrder(jsonBody);
        this.jsonResBody = res.getBody();
        
        wrapperObj = SearsDeleteSWrapper.parse(res.getBody());
        this.wrapperObj = wrapperObj;
        
    }
}