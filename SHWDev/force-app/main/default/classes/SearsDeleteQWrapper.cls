/**
 * @description       : Sears cancel reQuest wrapper
 * @author            : DYaste
 * @last modified on  : 09-06-2021
 * @last modified by  : DYaste
 * @Unit Test         : SearsWrapperTests.testSearsDeleteQWrapper 
 * Modifications Log
 * Ver   Date         Author   Modification
 * 1.0   09-05-2021   DYaste   Initial Version
**/
public without sharing class SearsDeleteQWrapper {
    public SearsDeleteQWrapper() {}

    public String clientId;
    public String Authorization;
    public String userId;
    public String correlationId;
    public String originatorCode;
    public String unit;
    public String order;
    public String cancelerUnit;
    public String cancelerUser;
    public String cancelerEmployeeId;
    public String instructions;
    public String reasonCode;

  public static String stringify(SearsDeleteQWrapper mySearsDeleteQWrapper){
    return  System.JSON.serialize(mySearsDeleteQWrapper, true);
  }

  public static String stringifyPretty(SearsDeleteQWrapper mySearsDeleteQWrapper){
    return  System.JSON.serializePretty(mySearsDeleteQWrapper, true);
  }
}