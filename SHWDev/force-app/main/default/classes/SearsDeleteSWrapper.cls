/**
 * @description       : Sear cancel reSponse wrapper
 * @author            : DYaste
 * @last modified on  : 09-06-2021
 * @last modified by  : DYaste
 * @Unit Test         : SearsWrapperTests.testSearsDeleteSWrapper
 * Modifications Log
 * Ver   Date         Author   Modification
 * 1.0   09-05-2021   DYaste   Initial Version
**/
public without sharing class SearsDeleteSWrapper {
    public SearsDeleteSWrapper() {}

    //public String ContentType;
    //public DateTime date;
    public String correlationId;
    public String responseCode;
	public String responseMessage;        
    public List<String> messages;

    public static SearsDeleteSWrapper parse(String json){
		return (SearsDeleteSWrapper) System.JSON.deserialize(json, SearsDeleteSWrapper.class);
	}
}