/**
 * @description       : 
 * @author            : DYaste
 * @last modified on  : 09-15-2021
 * @last modified by  : DYaste
 * @Unit Test         : $file 
 * Modifications Log
 * Ver   Date         Author   Modification
 * 1.0   09-15-2021   DYaste   Initial Version
**/
// Controller for VF page that demos the getOrder callout
public class SearsGetOrderPageController {
    
    // No req body for this callout, just unit number and order number
    public String unit{get; set;}
    public String order{get; set;}
    public String jsonResBody{get; set;}
    public SearsLookupSWrapper wrapperObj{get; set;}
    
    public String getUnitNumber(){
        // NOTE: Change unit number here for testing/demo purposes
        String unitNum = '0007999';
        return unitNum;
    }
    
    public String getOrderNumber(){
        // NOTE: Change order number here for testing/demo purposes
        String orderNum = '95000408';
        return orderNum;
    }
    
    // Send Http request and set response variables to be displayed
    public void sendRequest(){
        HttpResponse res = new HttpResponse();
        SearsLookupSWrapper wrapperObj = new SearsLookupSWrapper();
        
        res = SearsRequest.searsGetOrder(getUnitNumber(), getOrderNumber());
        this.jsonResBody = res.getBody();
        
        wrapperObj = SearsLookupSWrapper.parse(res.getBody());
        this.wrapperObj = wrapperObj;
        
    }
}