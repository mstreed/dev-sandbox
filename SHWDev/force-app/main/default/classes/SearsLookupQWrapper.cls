/**
 * @description       : Sears lookup reQuest wrapper
 * @author            : DYaste
 * @last modified on  : 09-06-2021
 * @last modified by  : DYaste
 * @Unit Test         : SearsWrapperTests.testSearsLookupQWrapper
 * Modifications Log
 * Ver   Date         Author   Modification
 * 1.0   09-05-2021   DYaste   Initial Version
**/
public with sharing class SearsLookupQWrapper {
    public SearsLookupQWrapper() {}

    public String clientId;
    public String Authorization;
    public String userId;
    public DateTime currentDateTime;
    public String unitNumber;
    public String orderNumber;

    public static String stringify(SearsLookupQWrapper mySearsLookupQWrapper){
		  return  System.JSON.serialize(mySearsLookupQWrapper, true);
    }

    public static String stringifyPretty(SearsLookupQWrapper mySearsLookupQWrapper){
      return  System.JSON.serializePretty(mySearsLookupQWrapper, true);
	  }
}