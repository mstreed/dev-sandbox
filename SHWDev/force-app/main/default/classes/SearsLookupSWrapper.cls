/**
 * @description       : Sears Lookup ReSponse wrapper class
 * @author            : DYaste
 * @last modified on  : 09-06-2021
 * @last modified by  : DYaste
 * @Unit Test         : SearsWrapperTest.testSearsLookupSWrapper
 * Modifications Log
 * Ver   Date         Author   Modification
 * 1.0   09-05-2021   DYaste   Initial Version
**/
public with sharing class SearsLookupSWrapper {
    public SearsLookupSWrapper() {}

    public String CorrelationId;
    public String ResponseCode;
    public String ResponseMessage;
    public List<String> messages;
    
    public Address Address;
    public List<ContactInfo> preferredContacts;
    public ServiceOrder serviceOrder;
    public Lottery lottery;
    //public Customer customer;
    public Merchandise merchandise;
    public ServiceInfo serviceInfo;
    public Estimate estimate;
    // public List<OrderMessage> messages;
    public List<Attempt> previousAttempts;
    public List<JobCode> jobCodes;
    public List<Part> parts;
    public List<UDF> userDefinedFields;
    public PaymentTotalInformation paymentTotalInformation;
	

    public class ServiceOrder{
      public Customer customer;
      public String serviceUnit;
      public String orderNumber;
      public DateTime callCreated;
      public DateTime callClosed;
      public String createEmployee;
      public String createUnit;
      public String createOrderNumber;
      public String forceFI;
      public String ivUnit;
      public String lastUpdateBy;
      public DateTime lastUpdateDate;
      public String localNationalPromoCode;
      public String offerId;
      public String paymentMethod;
      public String providerId;
      public String providerName;
      public String providerPhoneNumber;
      public String statusCode;
      public String unitBusCode;      
    }
    
    public class Lottery{

    }
    
    public class Customer{
      public Integer key;
      public String specialInstructions;
      public String promoteMa;
      public String promoteMaNr;
      public String promoteOther;
      public String promoteOtherNr;
      public String custType;
      public String locationSuffix;
      public String preferredLanguage;
      public String sywMemberId;
      public String storeNumber;
      public String taxExemptNumber;
      public String gridCode;
      public String taxExemptFlag;
      public String refEmpUnitNo;
      public String aniPhone;
      public String dnisPhone;
      public Boolean newCustomer;
      public Name name;
      // new added
      public Address address;
      public PrimaryPhone primaryPhone;
      public SecondaryPhone secondaryPhone;
      public Email email;
      public List<ContactInfo> contactInfo;
    }

    public class Name{
      public String firstName;
      public String lastName;
      public String prefix;
      public String suffix;
      public String secondName;
      public String middleName;
      public String fullName;
    }

    public class Address{
      public String addressLine1;
      public String addressLine2;
      public String aptNumber;
      public String city;
      public String state;
      public String zipCode;
      public String zipCodSuffix;
      public String crossStreet;
      public String poBox;
      public String addressFlag;
    }

    public class PrimaryPhone{
      public String contactType;
      public String contactAddress;
      public String contactAddressType;
      public String extension;
    }

    public class SecondaryPhone{
      public String contactType;
      public String contactAddress;
      public String contactAddressType;
      public String extension;
    }

    public class Email{
      public String contactType;
      public String contactAddress;
      public String contactAddressType;
      public String extension;
    }

    public class ContactInfo {        
      public String contactType;
      public String contactAddress;
      public String contactAddressType;
      public String extension;
    }

    public class Merchandise{
      public String itemSuffix;
      public String merchCode;
      public String div;
      public String itemNum;
      public String modelNumber;
      public String brandName;
      public String serialNumber;
      public String purchaseDate;
      public String merchShipDate;
      public String installedDate;
      public String searsPurchaseFlag;
      public String purchaseStoreNo;
      public String promoteMa;
      public String notPromoteMaReasonCode;
      public Promote promote;
      public String description;
      public String lastPMCheck;
      public String productUsage;
      public Decimal foodLossDollars;
      public String pmStatus;
      public String merchInfoTrusted;
      public String pmPurchaseDate;
      public String tierCode;
      public String deliveryDate;
      public Specialty speciality; // new added
      public String paCoverageFlag;
      public String ssaCoverageFlag;
      public String productAgreementNo;
      public String productAgreementExpDt;      
      public String laborWarrantyExp;     
      public String laborWarrantyLength;
      public String partWarrantyExp;
      public String partWarrantyLength;
      public String exceptionPartWarrantyExp;
      public String exceptionPartWarrantyLength;
      public String planExp;
      public String bypassModeSerialReasonCode;
      public String planName;
      public Subcomponent subComponent;
      public MerchandiseHistory merchandiseHistory;
      public ServiceInfo serviceInfo;
    }

    public class Promote{
      public String promoteOther;
      public String notPromoteOtherReasonCode;
    }

    public class Specialty{
      public String specialtyCode;
      public String specialtyDescription;
      public Industry industry;
    }

    public class Industry{
      public String industryCode;
      public String industryDescription;
    }

    public class Subcomponent{
      public Brand brand;
      public String model;
      public String serial;
      public String subComp;
    }

    public class Brand{
      public String brandName;
      public String itemFI;
      public String subComFI;
    }

    public class MerchandiseHistory{
      public String serviceUnit;
      public String orderNumber;
      public Technician technician;
      public DateTime callCode;
      public String productReading;
      public List<Discount> discounts;
      public List<Labors> merchandiseHistoryLabor;
      public List<Part> parts;
    }

    public class Technician{
      public String id;
      public String description;
      public String type;
      public String userId;
      public String name;
      public String returnTechnicianType;
    }

    public class Discount{
      public String associatedDiscount;
      public Coupon coupon;
      public String discountType;
      public String promotionDiscount;
      public String reasonCode;
      public String waiveCharge;
      public Decimal laborDiscTotal;
      public Decimal laborTotal;
    }

    public class Coupon{
      public String couponNumber;
      public String couponAmount;      
    }

    public class Labors{
      public List<JobCode> jobCodes;
    }

    public class JobCode{
      public String jobCode;
      public String chargeCode;
      public String description;
      public String relatedFI;
      public String sequence;
      public String chargeAmount;
      public Coverage coverage;      
    }

    public class Coverage{
      public String coverageCode;
      public String coverageDescription;
      public Integer sortSequence;
    }

    public class Part{
      public String div;
      public String pls;
      //public String number;
      public String description;
      public String location;
      public Coverage coverage;
      public DateTime dateUsed;
      public String quantity;
      public Obligor obligor;
      public String originalPoNo;
      public String partPriceIndicator;
      public Decimal price;
      public String status;
      public SubComponent subComponent;
      public String truckInquiryCode;
      public String truckInventoryFlag;
      public Integer sequence;
      public String techComments;
    }
    
    public class Obligor{

    }

    public class CallCode{

    }

    public class ServiceInfo{
      public DateTime originalScheduleDte;
      public DateTime promiseDate;
      public DateTime requestedStartTime;
      public DateTime requestedEndTime;
      public String svcRequestedText;
      public String serviceSpecialInstructions;
      public String recallFlag;
      public String svcProvidedCode;
      public Remittance remittance;
      public ThirdPartyInfo thirdPartyInfo;
      public String emergencyFlag;
      public String marketCode;
      public String serviceDuration;
      public String orderType;  
      //new added
      public SiteRepairServiceInfo siteRepairServiceInfo;
      public StoreRepairServiceInfo storeRepairServiceInfo;
      public ShopRepairServiceInfo shopRepairServiceInfo;
      public InstallationServiceInfo installationServiceInfo;  
    }

    public class Remittance{
      public String coverageCode;
      public String additionalCoverageCode;
      public String paymentMethod;
      public String chargeAccount;
      public String expirationDate;
      public String purchaseOrder;
      public String token;
      public String couponNumber;
      public String taxGeoCode;
      public String countyCode;
      public String chargeTransKey;
      public String chargeTokenFlag;
      public String chargeHSDWFlag;                 
    }

    public class ThirdPartyInfo{            
      public String thirdPartyId;
      public String thirdPartyAuthNumber;
      public String contractNumber;
      public String contractExpirationDate;
      public String deleteContractFlag;
      public String deleteContractDateFlag;
      public String deleteFlag;
      public String thirdPartyOrderNumber;
      public String thirdPartyShippingNumber;
    } 
    
    public class SiteRepairServiceInfo{
      public String workAreaCode;
      public String capacityArea;
      public Address address;
      public ContactInfo contactInfo;               
    }
    
    public class StoreRepairServiceInfo{
        public String workAreaCode;
        public String capacityArea;
        public String storeNumber;
        public Name name;
        public ContactInfo contactInfo;                               
    }

    public class ShopRepairServiceInfo{
        public String svcTypeCd;
        public String returnToAddressCd;
        public String retToPartnerStoreNo;
        public String loanerNo;
        public String loanerSerialNo;
        public String loanerModelNo;
        public String repairUnitNo;
        public String repairTag;
        public String binLocationCode;
        public String estimateFlag;
        public String maxAmount;
        public String printSOFlag;
        public String printerId;
        public String rateCode;
        public String rateAmount;
        public String methodOfShipment;
        public String mdsInUnitFl;
        public String diagnosticFee;
        public String thirdPartySalesCheckNo;
    }

    public class InstallationServiceInfo{
        public String installerPickUpFlag;
        public String merchandisePickUp;
        public String deliveryScheduleDate;
        public String deliveryTimeCode;
        public List<String> jobCodes;
    }    

    public class Estimate{
      public Discount discount;
      public Decimal homeServChrgAmt;
      public Decimal laborTaxPer;
      public Decimal partsTaxPerc;
      public Decimal prepaidTotal;
      public String pricingScheduleNum;
      public Decimal regionalPriceAmt;
      public Decimal servProdSellPrice;
      public Decimal spCalcTotal;
      public Decimal spTaxPerc;
      public Decimal sutStLaborTaxPer;
      public Decimal sutStPartTaxPerc;
      public Decimal sutStProtAgrTaxPerc;
      public Decimal tripCharge;      
    }

    public class OrderMessage{
      public String message;
      public String sequence;
      public Technician techician;
      public Date modDate;
      public DateTime modTime;
      public String modId;
      public String sentFrom;
      public String sentTo;
    }

    public class Attempt{
      public Date callDate;
      public Decimal transitTime;
      public DateTime arriveTime;
      public DateTime endTime;
      public CallCode callCode;
      public String reasonCode;
      public Decimal amountCollected;
      public String techComments;
      public Technician technician;
    }

    public class UDF{
      public String category;
      public String fieldId;
      public String fieldName;
      public String fieldValue;
      public String lineItemId;
      public String lineItemXref;
      public String serviceUnit;
      public String orderNumber;
      public String profileFlag;
      public Integer ruleId;
      public String userId;      
    }

    public class PaymentTotalInformation{
      public String primaryPaymentMethod;
      public String primaryAmountCollected;
      public String primaryChargeAccount;
      public String primaryExpirationDate;
      public String primaryToken;
      public String primarySettlementKey;
      public String primaryAuthNumber;
      public String primaryMaskedCreditCardNo;
      public String secondaryPaymentMethod;
      public String secondaryAmountCollected;
      public String secondaryChargeAccount;
      public String secondaryExpirationDate;
      public String secondaryToken;
      public String secondarySettlementKey;
      public String secondaryAuthNumber;
      public String secondaryMaskedCreditCardNo;
      public PayrollTransfer subAccount;
    }

    public class PayrollTransfer{
      public String subAccount;
      public String accountType;
      public String transferToStore;
    }

    public static SearsLookupSWrapper parse(String json){
		  return (SearsLookupSWrapper) System.JSON.deserialize(json, SearsLookupSWrapper.class);
	  }
}