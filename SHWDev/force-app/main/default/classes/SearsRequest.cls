public class SearsRequest extends BaseRequest {
    public String correlationId {get;set;}
    public String clientId {get;set;}
    public String userId {get;set;}
    // Override addHeaders method for Sears callouts. These are headers that 
    // all requests should have. Some will have additional extra headers.
    public override HttpRequest addHeaders(HttpRequest req){
        try{
            System.debug('SearsRequest|addHeaders|Start');
            String token = getSearsBearerToken();
            System.debug('SearsRequest|addHeaders|token: ' + token);
            req.setHeader('Authorization', 'Bearer ' + token);
            req.setHeader('Accept-Encoding', 'gzip, deflate, br');
            req.setHeader('Connection', 'keep-alive');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('clientid', 'SHH');
            req.setHeader('userid', 'select-hw-qa');
            req.setHeader('Accept', 'application/json');
            System.debug('SearsRequest|addHeaders|End');
        } catch(System.Exception e) {
            System.debug('SearsRequest|addHeaders|Exception:  ' + e.getLineNumber() + ' | ' + e.getCause() + ' | ' + e.getMessage());
        }
        return req;
    }
    
    
    // Call the Sears authentication API and return bearer token String.
    public static String getSearsBearerToken(){
        String bearerToken = '';
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        HttpResponse res = new HttpResponse();
        Map<String, Object> jsonMap = new Map<String, Object>();
        try{
            System.debug('SearsRequest|getSearsBearerToken|Start');                                            
            SearsRequest searsReqObj = new SearsRequest();
            req = searsReqObj.buildHttpRequest('Sears_Auth');                      
            res = http.send(req);
            String resBody = res.getBody();
            jsonMap = (Map<String, Object>)JSON.deserializeUntyped(resBody);
            bearerToken = (String)jsonMap.get('token');
            System.debug('SearsRequest|getSearsBearerToken|bearerToken: ' + bearerToken);
            System.debug('SearsRequest|getSearsBearerToken|End');
        } catch(System.Exception e) {
            System.debug('SearsRequest|getSearsBearerToken|Exception:  ' + e.getLineNumber() + ' | ' + e.getCause() + ' | ' + e.getMessage());
        }
        return bearerToken;
    }
    
    public static HttpResponse searsCreateOrder(String jsonBody){
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        try {
            System.debug('SearsRequest|searsCreateOrder|End');
            SearsRequest searsReqObj = new SearsRequest();
            req = searsReqObj.buildHttpRequest('Sears_Create_Order');
            req = searsReqObj.addHeaders(req);
            req = searsReqObj.addBody(req, jsonBody);
            res = searsReqObj.sendHttpRequest(req);
            System.debug('SearsRequest|searsCreateOrder|End');
        } catch(System.Exception e) {
            System.debug('SearsRequest|searsCreateOrder|Exception:  ' + e.getLineNumber() + ' | ' + e.getCause() + ' | ' + e.getMessage());
        }
        return res;
    }
    
    public static HttpResponse searsCheckAvailability(String jsonBody){
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        try {
            System.debug('SearsRequest|searsCheckAvailability|End');
            SearsRequest searsReqObj = new SearsRequest();
            req = searsReqObj.buildHttpRequest('Sears_Check_Availability');
            req = searsReqObj.addHeaders(req);           
            req = searsReqObj.addBody(req, jsonBody);
            res = searsReqObj.sendHttpRequest(req);
            System.debug('SearsRequest|searsCheckAvailability|End');
        } catch(System.Exception e) {
            System.debug('SearsRequest|searsCheckAvailability|Exception:  ' + e.getLineNumber() + ' | ' + e.getCause() + ' | ' + e.getMessage());
        }
        return res;
    }
    
    public static HttpResponse searsGetOrder(String unitNumber, String orderNumber){
        HttpRequest req = new HttpRequest();
        String endpoint = '';
        HttpResponse res = new HttpResponse();
        try {
            System.debug('SearsRequest|searsGetOrder|End');
            SearsRequest searsReqObj = new SearsRequest();
            req = searsReqObj.buildHttpRequest('Sears_Get_Order');
            // buildHttpRequest method provides base endpoint and other details, but this callout has specified uri params.
            Web_Service_Setting__mdt mdtRec = [SELECT Endpoint__c
                                               FROM Web_Service_Setting__mdt 
                                               WHERE Unique_Request_Name__c = 'Sears_Get_Order' AND Active__c = True
                                               LIMIT 1];
            endpoint = mdtRec.Endpoint__c + unitNumber + '-' + orderNumber;
            req.setEndpoint(endpoint);
            req = searsReqObj.addHeaders(req);
            res = searsReqObj.sendHttpRequest(req);
            System.debug('SearsRequest|searsGetOrder|End');
        } catch(System.Exception e) {
            System.debug('SearsRequest|searsGetOrder|Exception:  ' + e.getLineNumber() + ' | ' + e.getCause() + ' | ' + e.getMessage());
        }
        return res;
    }
    
    public static HttpResponse searsDeleteOrder(String jsonBody){
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        try {
            System.debug('SearsRequest|searsDeleteOrder|End');
            SearsRequest searsReqObj = new SearsRequest();
            req = searsReqObj.buildHttpRequest('Sears_Delete_Order');
            req = searsReqObj.addHeaders(req);
            req = searsReqObj.addBody(req, jsonBody);
            res = searsReqObj.sendHttpRequest(req);
            System.debug('SearsRequest|searsDeleteOrder|End');
        } catch(System.Exception e) {
            System.debug('SearsRequest|searsDeleteOrder|Exception:  ' + e.getLineNumber() + ' | ' + e.getCause() + ' | ' + e.getMessage());
        }
        return res;
    }
    
    // This one is rough 
    public static HttpResponse searsRescheduleOrder(String unitNumber, String orderNumber, String jsonBody){
        HttpResponse getOrderRes = new HttpResponse();
        SearsLookupSWrapper getOrderResWrapper = new SearsLookupSWrapper();
        String lastUpdateDateStr = '';
        SearsRescheduleQWrapper rescheduleOrderReqWrapper = new SearsRescheduleQWrapper();
        HttpRequest req = new HttpRequest();
        HttpResponse rescheduleOrderRes = new HttpResponse();
        try {
            System.debug('SearsRequest|searsRescheduleOrder|Start');
            // First, call getOrder to get lastUpdateDate to put in reschedule request
            SearsRequest searsReqObj1 = new SearsRequest();
            getOrderRes = SearsRequest.searsGetOrder(unitNumber, orderNumber);
            System.debug('SearsRequest|searsRescheduleOrder|res.getBody(): ' + getOrderRes.getBody());
            
            // Cannot use parsing/wrapper class to get lastUpdateDate, because it changes the datetime format, so deserializeUntyped is used.
            Map<String, Object> m1 = (Map<String, Object>)JSON.deserializeUntyped(getOrderRes.getBody());
            String serviceOrderJson = JSON.serialize(m1.get('serviceOrder'));
            Map<String, Object> m2 = (Map<String, Object>)JSON.deserializeUntyped(serviceOrderJson);
            lastUpdateDateStr = String.valueOf(m2.get('lastUpdateDate'));
            System.debug('SearsRequest|searsRescheduleOrder|lastUpdateDateStr: ' + lastUpdateDateStr);
            
            // Have to parse the jsonBody being input into searsRescheduleOrder in order to change the lastUpdatedDate to match getOrder response
            rescheduleOrderReqWrapper = SearsRescheduleQWrapper.parse(jsonBody);
            rescheduleOrderReqWrapper.lastUpdatedDate = lastUpdateDateStr;
            
            // Then serialize Apex object back into JSON body to input into searsRescheduleOrder
            jsonBody = JSON.serialize(rescheduleOrderReqWrapper);
            System.debug('SearsRequest|searsRescheduleOrder|jsonBody: ' + jsonBody);
            
            // Finally, make reschedule order callout with the edited jsonBody
            SearsRequest searsReqObj2 = new SearsRequest();
            req = searsReqObj2.buildHttpRequest('Sears_Reschedule_Order');
            req = searsReqObj2.addHeaders(req);
            req = searsReqObj2.addBody(req, jsonBody);
            rescheduleOrderRes = searsReqObj2.sendHttpRequest(req);
            System.debug('SearsRequest|searsRescheduleOrder|End');
        } catch(System.Exception e) {
            System.debug('SearsRequest|searsRescheduleOrder|Exception:  ' + e.getLineNumber() + ' | ' + e.getCause() + ' | ' + e.getMessage());
        }
        return rescheduleOrderRes;
    }
    
    
}