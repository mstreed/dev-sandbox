@isTest
global class SearsRequestMockGenerator implements HttpCalloutMock {
    
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        if (req.getEndpoint() == 'https://api-st.shs-core.com/sis/proxy/HSSOMAuthService/services/auth/token') {
            System.debug('SearsRequestMockGenerator|respond|Endpoint: https://api-st.shs-core.com/sis/proxy/HSSOMAuthService/services/auth/token');
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{ "CorrelationId": "1701028908", "ResponseCode": "00", "ResponseMessage": "SUCCESS", "token": "testToken1234567890", "tokenLife": 5400 }');
            res.setStatusCode(200);
        } else if (req.getEndpoint() == 'https://api-st.shs-core.com/sis/proxy/HSSOMCreateOrderService/services/domi/orders') {
            System.debug('SearsRequestMockGenerator|respond|Endpoint: https://api-st.shs-core.com/sis/proxy/HSSOMCreateOrderService/services/domi/orders');
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{ "correlationId": "testCorrelationId1234", "responseCode": "00", "responseMessage": "SUCCESS", "unit": "0007999", "order": "95000399", "custKey": "1260624724", "itemSuffix": "0003" }');
            res.setStatusCode(200);
        } else if (req.getEndpoint() == 'https://api-st.shs-core.com/sis/proxy/HSSOMEventBrokerService/eventbroker/availability') {
            System.debug('SearsRequestMockGenerator|respond|Endpoint: https://api-st.shs-core.com/sis/proxy/HSSOMEventBrokerService/eventbroker/availability');
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{ "dataFoundFlag": "Y", "serviceUnitNumber": "testServiceUnitNumber123", "capacityArea": "8555_SW", "capacityNeeded": 43, "wrkAreaCd": "SRS", "mdsPriceTierCd": "E", "chargeCodeAmount": 149.00, "unitType": "SRS", "numberOfOccurences": 40, "minorFlag": "N", "allDayFlag": "N", "emergency": "N", "timeZoneValue": "00", "businessHours": [ { "fromTime": "0000", "toTime": "0000" }, { "fromTime": "0000", "toTime": "0000" } ], "offerID": "000068482576", "holidayDate": [], "offers": [ { "provider": "W2", "techId": "CCAST40", "techDescription": "CLICK Sears", "availableDate": "2021-09-20", "timeWindowCd": "AM", "timeWindowStart": "08:00", "timeWindowEnd": "12:00", "availableDay": "MON", "priorityFlag": "4" }, { "provider": "W2", "techId": "CCAST40", "techDescription": "CLICK Sears", "availableDate": "2021-09-20", "timeWindowCd": "JN", "timeWindowStart": "10:00", "timeWindowEnd": "14:00", "availableDay": "MON", "priorityFlag": "4" } ], "messages": null, "CorrelationId": "SB-Sept15-test1", "ResponseCode": "00", "ResponseMessage": "SUCCESS" }');
            res.setStatusCode(200);
        } else if (String.valueOf(req.getEndpoint()).contains('https://api-st.shs-core.com/sis/proxy/HSSOMOrderLookupService/services/domi/orders/')) {
            System.debug('SearsRequestMockGenerator|respond|Endpoint string contains: https://api-st.shs-core.com/sis/proxy/HSSOMOrderLookupService/services/domi/orders/');
            res.setHeader('Content-Type', 'application/json');
            
            res.setBody('{ "CorrelationId": "testCorrelationId1234", "ResponseCode": "00", "ResponseMessage": "SUCCESS", "serviceOrder": { "orderNumber": "95000399", "serviceUnit": "0007999", "statusCode": "WS", "providerId": "W2", "callCreated": "2021-09-14", "callCreatedTime": "13:49:00", "createUnit": "0007999", "createEmployee": "9999494", "lastUpdateDate": "2021-09-15T10:54:18.273276", "lastUpdateBy": "9999494" } }');
            res.setStatusCode(200);
        } else if (req.getEndpoint() == 'https://api-st.shs-core.com/sis/proxy/HSSOMCancelOrderService/services/domi/cancel') {
            System.debug('SearsRequestMockGenerator|respond|Endpoint: https://api-st.shs-core.com/sis/proxy/HSSOMCancelOrderService/services/domi/cancel');
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{ "CorrelationId": "testCorrelationId1234", "ResponseCode": "00", "ResponseMessage": "SUCCESS" }');
            res.setStatusCode(200);
        } else if (req.getEndpoint() == 'https://api-st.shs-core.com/sis/proxy/HSSOMRescheduleOrderService/services/domi/orders') {
            System.debug('SearsRequestMockGenerator|respond|Endpoint: https://api-st.shs-core.com/sis/proxy/HSSOMRescheduleOrderService/services/domi/orders');
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{ "CorrelationId": "testCorrelationId1234", "ResponseCode": "00", "ResponseMessage": "SUCCESS" }');
            res.setStatusCode(200);
        }
        
        
        
        return res;
    }
}