@isTest
public class SearsRequestTest {
    
    @isTest static void addHeadersTest(){
        try{
            System.debug('SearsRequestTest|addHeadersTest|Start');
            HttpRequest req = new HttpRequest();
            SearsRequest searsReqObj = new SearsRequest();
            req = searsReqObj.addHeaders(req);
            System.assert(req.getHeader('Content-Type') == 'application/json');
            System.debug('SearsRequestTest|addHeadersTest|End');
        } catch(System.Exception e) {
            System.debug('SearsRequestTest|addHeadersTest|Exception:  ' + e.getLineNumber() + ' | ' + e.getCause() + ' | ' + e.getMessage());
        }
    }
    
    @isTest static void getSearsBearerTokenTest(){
        try{
            System.debug('SearsRequestTest|getSearsBearerTokenTest|Start');
            Test.setMock(HttpCalloutMock.class, new SearsRequestMockGenerator());
            String token = '';
            token = SearsRequest.getSearsBearerToken();
            System.debug('SearsRequestTest|getSearsBearerTokenTest|End');
        } catch(System.Exception e) {
            System.debug('SearsRequestTest|getSearsBearerTokenTest|Exception:  ' + e.getLineNumber() + ' | ' + e.getCause() + ' | ' + e.getMessage());
        }
    }
    
    @isTest static void searsCreateOrderTest(){
        try{
            System.debug('SearsRequestTest|searsCreateOrderTest|Start');
            String returnedCorrelationId = '';
            Test.setMock(HttpCalloutMock.class, new SearsRequestMockGenerator());
            HttpResponse res = new HttpResponse();
            res = SearsRequest.searsCreateOrder('{"testJsonKey": "testJsonValue"}');
            Map<String, Object> m1 = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
            System.debug('SearsRequestTest|searsCreateOrderTest|m1: ' + m1);
            returnedCorrelationId = String.valueOf(m1.get('correlationId'));
            System.assert(returnedCorrelationId == 'testCorrelationId1234');
            System.debug('SearsRequestTest|searsCreateOrderTest|End');
        } catch(System.Exception e) {
            System.debug('SearsRequestTest|searsCreateOrderTest|Exception:  ' + e.getLineNumber() + ' | ' + e.getCause() + ' | ' + e.getMessage());
        }
    }
    
    @isTest static void searsCheckAvailabilityTest(){
        try{
            System.debug('SearsRequestTest|searsCheckAvailabilityTest|Start');
            String returnedServiceUnitNumber = '';
            Test.setMock(HttpCalloutMock.class, new SearsRequestMockGenerator());
            HttpResponse res = new HttpResponse();
            res = SearsRequest.searsCheckAvailability('{"testJsonKey": "testJsonValue"');
            Map<String, Object> m1 = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
            System.debug('SearsRequestTest|searsCreateOrderTest|m1: ' + m1);
            returnedServiceUnitNumber = String.valueOf(m1.get('serviceUnitNumber'));
            System.assert(returnedServiceUnitNumber == 'testServiceUnitNumber123');
            System.debug('SearsRequestTest|searsCheckAvailabilityTest|End');
        } catch(System.Exception e) {
            System.debug('SearsRequestTest|searsCheckAvailabilityTest|Exception:  ' + e.getLineNumber() + ' | ' + e.getCause() + ' | ' + e.getMessage());
        }
    }
    
    @isTest static void searsGetOrderTest(){
        try{
            System.debug('SearsRequestTest|searsGetOrderTest|Start');
            String returnedCorrelationId = '';
            Test.setMock(HttpCalloutMock.class, new SearsRequestMockGenerator());
            HttpResponse res = new HttpResponse();
            res = SearsRequest.searsGetOrder('1111111', '2222222');
            Map<String, Object> m1 = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
            System.debug('SearsRequestTest|searsGetOrderTest|m1: ' + m1);
            returnedCorrelationId = String.valueOf(m1.get('CorrelationId'));
            System.assert(returnedCorrelationId == 'testCorrelationId1234');
            System.debug('SearsRequestTest|searsGetOrderTest|End');
        } catch(System.Exception e) {
            System.debug('SearsRequestTest|searsGetOrderTest|Exception:  ' + e.getLineNumber() + ' | ' + e.getCause() + ' | ' + e.getMessage());
        }
    }
    
    @isTest static void searsCancelOrderTest(){
        try{
            System.debug('SearsRequestTest|searsCancelOrderTest|Start');
            String returnedCorrelationId = '';
            Test.setMock(HttpCalloutMock.class, new SearsRequestMockGenerator());
            HttpResponse res = new HttpResponse();
            res = SearsRequest.searsCancelOrder('{"testJsonKey": "testJsonValue"}');
            Map<String, Object> m1 = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
            System.debug('SearsRequestTest|searsCancelOrderTest|m1: ' + m1);
            returnedCorrelationId = String.valueOf(m1.get('CorrelationId'));
            System.assert(returnedCorrelationId == 'testCorrelationId1234');
            System.debug('SearsRequestTest|searsCancelOrderTest|End');
        } catch(System.Exception e) {
            System.debug('SearsRequestTest|searsCancelOrderTest|Exception:  ' + e.getLineNumber() + ' | ' + e.getCause() + ' | ' + e.getMessage());
        }
    }
    
    @isTest static void searsRescheduleOrderTest(){
        try{
            System.debug('SearsRequestTest|searsRescheduleOrderTest|Start');
            
            String returnedCorrelationId = '';
            Test.setMock(HttpCalloutMock.class, new SearsRequestMockGenerator());
            HttpResponse res = new HttpResponse();
            res = SearsRequest.searsRescheduleOrder('0000000', '11111111', '{"testJsonKey": "testJsonValue"}');
            Map<String, Object> m1 = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
            System.debug('SearsRequestTest|searsRescheduleOrderTest|m1: ' + m1);
            returnedCorrelationId = String.valueOf(m1.get('CorrelationId'));
            System.assert(returnedCorrelationId == 'testCorrelationId1234');
            
            System.debug('SearsRequestTest|searsRescheduleOrderTest|End');
        } catch(System.Exception e) {
            System.debug('SearsRequestTest|searsRescheduleOrderTest|Exception:  ' + e.getLineNumber() + ' | ' + e.getCause() + ' | ' + e.getMessage());
        }
    }
}