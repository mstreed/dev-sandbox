/**
 * @description       : 
 * @author            : DYaste
 * @last modified on  : 09-15-2021
 * @last modified by  : DYaste
 * @Unit Test         : $file 
 * Modifications Log
 * Ver   Date         Author   Modification
 * 1.0   09-15-2021   DYaste   Initial Version
**/
// Controller for VF page that demos the rescheduleOrder callout
public class SearsRescheduleOrderPageController {
    
    public String jsonResBody{get; set;}
    public SearsRescheduleSWrapper wrapperObj{get; set;}
     
    public String getJsonReqBody(){
        String jsonReqBody = '{ "correlationId": "SB-August14-test5", "lastUpdatedDate": "2021-09-14T13:49:53.060145", "offerId": "00000023615", "requestorUnit": "0007999", "requestorId": "9999494", "serviceInfo": { "requestedDate": "2021-09-30", "requestedStartTime": "09:00", "requestedEndTime": "10:00" }, "svcUnitNumber": "0007999", "providerId": "W2", "orderType": "SIT", "order": "95000399", "rescheduleReasonCode": "TR01" }';
        return jsonReqBody;
    }
    
    // Send Http request and set response variables to be displayed
    public void sendRequest(){
        String jsonBody = getJsonReqBody();
        HttpResponse res = new HttpResponse();
        SearsRescheduleSWrapper wrapperObj = new SearsRescheduleSWrapper();
        
        res = SearsRequest.searsRescheduleOrder('0007999', '95000399', jsonBody);
        this.jsonResBody = res.getBody();
        
        wrapperObj = SearsRescheduleSWrapper.parse(res.getBody());
        this.wrapperObj = wrapperObj;
        
    }
}