public class SearsRescheduleOrderQWrapper {
    public String correlationID ;
    public String originatorCode ;
    public String svcUnitNumber ;
    public String requestorUnit ;
    public String requestorID ;
    public String offerId ;
    public String providerId ;
    public String Order ;
    public DateTime lastUpdatedDate ;
    public String rescheduleReasonCode ;
    public Technician technician ;
    public ServiceInfo serviceInfo ;
    
    public class Technician{
        public String techID ;
        public String techDescription ;
        public String userId ;
        public String returnTechnicianType ;
        public String name ;
        public String orderType ;
    }
    public class ServiceInfo{
        public String requestedDate ;
        public String requestedStartTime ;
        public String requestedEndTime ;
        public String svcRequestedText ;
        public String serviceSpecialInstructions ;
        public String recallFlag ;
        public String svcProvidedCode ;
        public RemittanceData remittance ;
        public ThirdPartyInfoData thirdPartyInfo ;
        public String emergencyFlag ;
        public String marketCode ;
        public String serviceDuration ;
        public SiteRepairServiceInfo siteRepairServiceInfo ;
        public StoreRepairServiceInfo storeRepairServiceInfo ;
    }
    public class RemittanceData{
        public String coverageCode ;
        public String additionalCoverageCode ;
        public String paymentMethod ;
        public String chargeAccount ;
        public String expirationDate ;
        public String purchaseOrder ;
        public String token ;
        public String couponNumber ;
        public String taxGeoCode ;
        public String countyCode ;
        public String chargeTransKey ;
        public String chargeTokenFlag ;
    }
    public class ThirdPartyInfoData{
        public String thirdPartyId ;
        public String thirdPartyAuthNumber ;
        public String contractNumber ;
        public String contractExpirationDate ;
        public String deleteFlag ;
    }
    public class SiteRepairServiceInfo{
        public String workAreaCode ;
        public String capacityArea ;
        public Address address ;
        public ContactInfo contactInfo ;
    }
    public class Address{
        public String addressLine1 ;
        public String addressLine2 ;
        public String aptNumber ;
        public String city ;
        public String state ;
        public String zipCode ;
        public String zipcodeSuffix ;
        public String crossStreet ;
    }
    public class ContactInfo{
        public String contactType ;
        public String contactAddress ;
        public String contactAddressType ;
        public String extension ;
    }
    public class StoreRepairServiceInfo{
        public String workAreaCode ;
        public String capacityArea ;
        public String storeNumber ;
        public NameInfo name ;
        public ContactInfo contactInfo ;
    }
    public class NameInfo{
        public String firstName ;
        public String lastName ;
        public String prefix ;
        public String suffix ;
        public String secondName ;
        public String middleName ;
    }
    
    public static String stringify(SearsRescheduleOrderQWrapper mySearsRescheduleOrderQWrapper){
        return  System.JSON.serialize(mySearsRescheduleOrderQWrapper, true);
    }
}