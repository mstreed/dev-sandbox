public class SearsRescheduleOrderSWrapper {
public String CorrelationId;
    public String ResponseCode;
    public String ResponseMessage;
    public DateTime lastUpdatedDate;
    
    public static SearsRescheduleOrderSWrapper parse(String json){
		  return (SearsRescheduleOrderSWrapper) System.JSON.deserialize(json, SearsRescheduleOrderSWrapper.class);
	  }
}