/**
 * @description       : Sears Reschedule ReQuest Wrapper class
 * @author            : DYaste
 * @last modified on  : 09-06-2021
 * @last modified by  : DYaste
 * @Unit Test         : SearsWrapperTest.testSearsRescheduleQWrapper 
 * Modifications Log
 * Ver   Date         Author   Modification
 * 1.0   09-05-2021   DYaste   Initial Version
**/
public with sharing class SearsRescheduleQWrapper {
    public SearsRescheduleQWrapper() {}
    public String clientId;
    public String Authorization;
    public String userId;
    public String correlationID ;
    public String originatorCode;
    public String svcUnitNumber;
    public String requestorUnit;
    public String requestorID;
    public String offerId;
    public String providerId;
    public String order;
    public String lastUpdatedDate;
    public String rescheduleReasonCode;
    public Technician technician;
    public String orderType;
    public ServiceInfo serviceInfo ;
    
    public class Technician{
        public Technician() {}
        public String techID;
        public String techDescription;
        public String userId;
        public String returnTechnicianType;
        public String name;        
    }

    public class ServiceInfo {
        public ServiceInfo() {}

        public String requestedDate;
        public String requestedStartTime;
        public String requestedEndTime;
        public String svcRequestedText;
        public String serviceSpecialInstructions;
        public String recallFlag;
        public String svcProvidedCode;
        public String emergencyFlag;
        public String marketCode;
        public String serviceDuration;
        public Remittance remittance ;
        public ThirdPartyInfo thirdPartyInfo ;
        public SiteRepairServiceInfo siteRepairServiceInfo ;
        public StoreRepairServiceInfo storeRepairServcieInfo ;
        public ShopRepairServiceInfo shopRepairServiceInfo ;
        public InstallationServiceInfo installationServiceInfo ;    
    }
    
    public class Remittance{
        public String coverageCode;
        public String additionalCoverageCode;
        public String paymentMethod;
        public String chargeAccount;
        public String expirationDate;
        public String purchaseOrder;
        public String token;
        public String couponNumber;
        public String taxGeoCode;
        public String countyCode;
        public String chargeTransKey;
        public String chargeTokenFlag;
        public String chargeHSDWFlag;                 
    }
    
    public class ThirdPartyInfo{            
        public String thirdPartyId;
        public String thirdPartyAuthNumber;
        public String contractNumber;
        public String contractExpirationDate;
        public String deleteFlag;
    }
    
    public class SiteRepairServiceInfo{
        public String workAreaCode;
        public String capacityArea;
        public Address address;
        public ContactInfo contactInfo;               
    }
    
    public class StoreRepairServiceInfo{
        public String workAreaCode;
        public String capacityArea;
        public String storeNumber;
        public Name name;
        public ContactInfo contactInfo;                               
    }
    
    public class ContactInfo {
        public ContactInfo() {}
    
        public String contactType;
        public String contactAddress;
        public String contactAddressType;
        public String extension;
    }
    
    public class ShopRepairServiceInfo{
        public String svcTypeCd;
        public String returnToAddressCd;
        public String retToPartnerStoreNo;
        public String loanerNo;
        public String loanerSerialNo;
        public String loanerModelNo;
        public String repairUnitNo;
        public String repairTag;
        public String binLocationCode;
        public String estimateFlag;
        public String maxAmount;
        public String printSOFlag;
        public String printerId;
        public String rateCode;
        public String rateAmount;
        public String methodOfShipment;
        public String mdsInUnitFl;
        public String diagnosticFee;
        public String thirdPartySalesCheckNo;
    }
    
    public class InstallationServiceInfo{
        public String installerPickUpFlag;
        public String merchandisePickUp;
        public String deliveryScheduleDate;
        public String deliveryTimeCode;
        public List<String> jobCodes;
    }
    
    public static SearsRescheduleQWrapper parse(String json){
		  return (SearsRescheduleQWrapper) System.JSON.deserialize(json, SearsRescheduleQWrapper.class);
    }

}