/**
 * @description       : Sears Reschedule ReSponse Wrapper class
 * @author            : DYaste
 * @last modified on  : 09-06-2021
 * @last modified by  : DYaste
 * @Unit Test         : SearsWrapperTests.testSearsRescheduleSWrapper
 * Modifications Log
 * Ver   Date         Author   Modification
 * 1.0   09-05-2021   DYaste   Initial Version
**/
public without sharing class SearsRescheduleSWrapper {
    public SearsRescheduleSWrapper() {}

    public String Authorization;
    public String clientid;
    public String userid;
    public String CorrelationId;
    public String ResponseCode;
    public String ResponseMessage;
    public List<String> messages;
    public String lastUpdatedDate;

    public static SearsRescheduleSWrapper parse(String json){
		return (SearsRescheduleSWrapper) System.JSON.deserialize(json, SearsRescheduleSWrapper.class);
	}
}