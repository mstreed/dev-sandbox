/**
 * @description       : Test class for all the Sears Q/S Wrapper classes
 * @author            : DYaste
 * @last modified on  : 09-05-2021
 * @last modified by  : DYaste
 * @Unit Test         : None
 * Modifications Log
 * Ver   Date         Author   Modification
 * 1.0   09-05-2021   DYaste   Initial Version
**/
@IsTest (SeeAllData=false)
public with sharing class SearsWrapperTests {
  
    /**
    * @description Test Sears ReSponse Wrapper class
    * @author DYaste | 09-05-2021 
    * @return testMethod 
    **/
    static testMethod void testSearsAvailabilitySWrapper() {
		String json = '{'+
		'    \"correlationId\": \"arun-test-avail-ivr-3\",'+
		'    \"merchandiseCode\": \"OVENBI\",'+
		'    \"serviceTypeNeeded\": \"R\",'+
		'    \"providerId\": \"W2\",'+
		'    \"customer\": {'+
		'        \"customerFirstName\": \"THOMAS\",'+
		'        \"customerMiddleName\": null,'+
		'        \"customerLastName\": \"BURNS\",'+
		'        \"customerAddress\": \"953 N DUPONT BLVD\",'+
		'        \"customerCity\": \"MILFORD\",'+
		'        \"customerState\": \"DE\",'+
		'        \"customerZipCode\": \"19963\",'+
		'        \"customerZipCodeExt\": \"\",'+
		'        \"customerLanguage\": \"\"'+
		'    },'+
		'    \"brandName\": \"LENOVO\",'+
		'    \"coverageCode\": \"CC\",'+
		'    \"repairAddress\": \"955 N DUPONT BLVD\",'+
		'    \"zipCode\": \"19963\",'+
		'    \"zipCodeExt\": \"\",'+
		'    \"processId\": \"SHW075\",'+
		'    \"serviceOrderTypeCode\": \"O\",'+
		'    \"serviceTimeRequired\": \"\",'+
		'    \"requestStartDate\": \"2020-07-29\",'+
		'    \"requestEndDate\": \"\",'+
		'    \"emergencyFlag\": \"\",'+
		'    \"forceFlag\": \"\",'+
		'    \"rescheduleFlag\": \"\",'+
		'    \"recallFlag\": \"\",'+
		'    \"requiredTech\": \"\",'+
		'    \"requiredTechType\": \"\",'+
		'    \"servicingOrganization\": \"\",'+
		'    \"storeStock\": \"\",'+
		'    \"numberOfTimeWindows\": \"35\",'+
		'    \"visitCount\": \"\",'+
		'    \"requestPartCount\": \"\",'+
		'    \"requestTechnicianID\": \"\",'+
		'    \"requestPartType\": \"\",'+
		'    \"requestPartDate\": \"\",'+
		'    \"capacityArea\": \"\",'+
		'    \"business\": \"\",'+
		'    \"priorityCode\": \"1\",'+
		'    \"workAreaCode\": \"\",'+
		'    \"merchPriceTierCode\": \"\",'+
		'    \"remark1\": \"\",'+
		'    \"remark2\": \"\",'+
		'    \"timeZoneSign\": \"\",'+
		'    \"timeZoneValue\": \"\",'+
		'    \"installDate\": \"\",'+
		'    \"purchaseDate\": \"\",'+
		'    \"paCoverage\": \"\",'+
		'    \"creatorUnitNo\": \"\",'+
		'    \"ssaCoverage\": \"\",'+
		'    \"helperRequired\": \"\",'+
		'    \"partPickupFlag\": null,'+
		'    \"partPickupUnitNum\": null   '+
		'}';
		JSON2Apex r = JSON2Apex.parse(json);
		System.assert(r != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		JSON2Apex objJSON2Apex = new JSON2Apex(System.JSON.createParser(json));
		System.assert(objJSON2Apex != null);
		System.assert(objJSON2Apex.correlationId == null);
		System.assert(objJSON2Apex.merchandiseCode == null);
		System.assert(objJSON2Apex.serviceTypeNeeded == null);
		System.assert(objJSON2Apex.providerId == null);
		System.assert(objJSON2Apex.customer == null);
		System.assert(objJSON2Apex.brandName == null);
		System.assert(objJSON2Apex.coverageCode == null);
		System.assert(objJSON2Apex.repairAddress == null);
		System.assert(objJSON2Apex.zipCode == null);
		System.assert(objJSON2Apex.zipCodeExt == null);
		System.assert(objJSON2Apex.processId == null);
		System.assert(objJSON2Apex.serviceOrderTypeCode == null);
		System.assert(objJSON2Apex.serviceTimeRequired == null);
		System.assert(objJSON2Apex.requestStartDate == null);
		System.assert(objJSON2Apex.requestEndDate == null);
		System.assert(objJSON2Apex.emergencyFlag == null);
		System.assert(objJSON2Apex.forceFlag == null);
		System.assert(objJSON2Apex.rescheduleFlag == null);
		System.assert(objJSON2Apex.recallFlag == null);
		System.assert(objJSON2Apex.requiredTech == null);
		System.assert(objJSON2Apex.requiredTechType == null);
		System.assert(objJSON2Apex.servicingOrganization == null);
		System.assert(objJSON2Apex.storeStock == null);
		System.assert(objJSON2Apex.numberOfTimeWindows == null);
		System.assert(objJSON2Apex.visitCount == null);
		System.assert(objJSON2Apex.requestPartCount == null);
		System.assert(objJSON2Apex.requestTechnicianID == null);
		System.assert(objJSON2Apex.requestPartType == null);
		System.assert(objJSON2Apex.requestPartDate == null);
		System.assert(objJSON2Apex.capacityArea == null);
		System.assert(objJSON2Apex.business == null);
		System.assert(objJSON2Apex.priorityCode == null);
		System.assert(objJSON2Apex.workAreaCode == null);
		System.assert(objJSON2Apex.merchPriceTierCode == null);
		System.assert(objJSON2Apex.remark1 == null);
		System.assert(objJSON2Apex.remark2 == null);
		System.assert(objJSON2Apex.timeZoneSign == null);
		System.assert(objJSON2Apex.timeZoneValue == null);
		System.assert(objJSON2Apex.installDate == null);
		System.assert(objJSON2Apex.purchaseDate == null);
		System.assert(objJSON2Apex.paCoverage == null);
		System.assert(objJSON2Apex.creatorUnitNo == null);
		System.assert(objJSON2Apex.ssaCoverage == null);
		System.assert(objJSON2Apex.helperRequired == null);
		System.assert(objJSON2Apex.partPickupFlag == null);
		System.assert(objJSON2Apex.partPickupUnitNum == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		JSON2Apex.Customer objCustomer = new JSON2Apex.Customer(System.JSON.createParser(json));
		System.assert(objCustomer != null);
		System.assert(objCustomer.customerFirstName == null);
		System.assert(objCustomer.customerMiddleName == null);
		System.assert(objCustomer.customerLastName == null);
		System.assert(objCustomer.customerAddress == null);
		System.assert(objCustomer.customerCity == null);
		System.assert(objCustomer.customerState == null);
		System.assert(objCustomer.customerZipCode == null);
		System.assert(objCustomer.customerZipCodeExt == null);
		System.assert(objCustomer.customerLanguage == null);
	}
    /**
    * @description test case for Sears ReQuestWrapper
    * @author DYaste | 09-05-2021 
    * @return testMethod 
    **/
    static testMethod void testSearsAvailabilityQWrapper() {
		String json = '{'+
		'    \"dataFoundFlag\": \"Y\",'+
		'    \"serviceUnitNumber\": \"0007084\",'+
		'    \"capacityArea\": \"7084_J\",'+
		'    \"capacityNeeded\": 43,'+
		'    \"wrkAreaCd\": \"SRS\",'+
		'    \"mdsPriceTierCd\": \"E\",'+
		'    \"chargeCodeAmount\": 75,'+
		'    \"unitType\": \"SRS\",'+
		'    \"numberOfOccurences\": 5,'+
		'    \"minorFlag\": \"N\",'+
		'    \"allDayFlag\": \"N\",'+
		'    \"emergency\": \"N\",'+
		'    \"timeZoneValue\": \"01\",'+
		'    \"businessHours\": ['+
		'        {'+
		'            \"fromTime\": \"0000\",'+
		'            \"toTime\": \"0000\"'+
		'        },'+
		'        {'+
		'            \"fromTime\": \"0800\",'+
		'            \"toTime\": \"1700\"'+
		'        },'+
		'        {'+
		'            \"fromTime\": \"0800\",'+
		'            \"toTime\": \"1700\"'+
		'        },'+
		'        {'+
		'            \"fromTime\": \"0800\",'+
		'            \"toTime\": \"1700\"'+
		'        },'+
		'        {'+
		'            \"fromTime\": \"0800\",'+
		'            \"toTime\": \"1700\"'+
		'        },'+
		'        {'+
		'            \"fromTime\": \"0800\",'+
		'            \"toTime\": \"1700\"'+
		'        },'+
		'        {'+
		'            \"fromTime\": \"0800\",'+
		'            \"toTime\": \"1700\"'+
		'        }'+
		'    ],'+
		'    \"offerID\": \"000067892974\",'+
		'    \"holidayDate\": [],'+
		'    \"offers\": ['+
		'        {'+
		'            \"provider\": \"W2\",'+
		'            \"techId\": \"\",'+
		'            \"techDescription\": \"CLICK Sears\",'+
		'            \"availableDate\": \"2018-03-05\",'+
		'            \"timeWindowCd\": \"AD\",'+
		'            \"timeWindowStart\": \"08:00\",'+
		'            \"timeWindowEnd\": \"17:00\",'+
		'            \"availableDay\": \"MON\",'+
		'            \"priorityFlag\": \"4\"'+
		'        },'+
		'        {'+
		'            \"provider\": \"W2\",'+
		'            \"techId\": \"\",'+
		'            \"techDescription\": \"CLICK Sears\",'+
		'            \"availableDate\": \"2018-03-05\",'+
		'            \"timeWindowCd\": \"AM\",'+
		'            \"timeWindowStart\": \"08:00\",'+
		'            \"timeWindowEnd\": \"12:00\",'+
		'            \"availableDay\": \"MON\",'+
		'            \"priorityFlag\": \"4\"'+
		'        },'+
		'        {'+
		'            \"provider\": \"W2\",'+
		'            \"techId\": \"\",'+
		'            \"techDescription\": \"CLICK Sears\",'+
		'            \"availableDate\": \"2018-03-05\",'+
		'            \"timeWindowCd\": \"OR\",'+
		'            \"timeWindowStart\": \"15:00\",'+
		'            \"timeWindowEnd\": \"18:00\",'+
		'            \"availableDay\": \"MON\",'+
		'            \"priorityFlag\": \"4\"'+
		'        }'+
		'    ],'+
		'    \"messages\": null,'+
		'    \"CorrelationId\": \"3434\",'+
		'    \"ResponseCode\": \"00\",'+
		'    \"ResponseMessage\": \"SUCCESS\"'+
		'}';
		SearsAvailibilitySWrapper obj = SearsAvailibilitySWrapper.parse(json);
		System.assert(obj != null);
	}
}
