public class SendContractorCloseout {
    
    @InvocableMethod
    public static void triggerEmail(List<Contract> policies) {
        system.debug('SendContractorCloseout-triggerEmail');

        Set<Id> polIds = new Set<Id>();
        List<Case> caseUpdates = new List<Case>();
        List<Task> tsks = new List<Task>();
        for (Contract pol : policies) {
            polIds.add(pol.Id);
        }
        
        system.debug('polIds: ' + polIds);
        
        for(Case cs : [SELECT Id, Policy__c, CaseNumber, Contractor__r.Name, Contractor_Closeout_Email__c, Send_Contractor_Closeout_Email__c, Contractor_Close_Out_Email_Sent__c, Contractor__c, Contractor__r.Business_Email__c, CreatedDate
                       FROM Case
                       WHERE Policy__c IN :polIds AND
                       (CreatedDate = TODAY OR CreatedDate = LAST_N_DAYS:60) AND 
                       Contractor__r.Business_Email__c != NULL]) {
                           cs.Contractor_Closeout_Email__c = cs.Contractor__r.Business_Email__c;
                           cs.Send_Contractor_Closeout_Email__c = DateTime.now();
                           cs.Contractor_Close_Out_Email_Sent__c = DateTime.now();
                           caseUpdates.add(cs);
                           
                           //create a task to log the email that is sent - not using the api to send/log emails to reduce limit usage
                           Task tsk = new Task();
                           tsk.ActivityDate = Date.today();
                           tsk.WhatId = cs.Id;
                           tsk.From__c = 'claims@selecthomewarranty.com';
                           tsk.Subject = 'Email: Claim Closeout Claim#'+cs.CaseNumber;
                           tsk.Description = 'Policy was \'Cancelled\' so Contractor closeout email sent to: ' + cs.Contractor__r.Name + '('+cs.Contractor__r.Business_Email__c+')';
                           tsk.OwnerId = UserInfo.getUserId();
                           tsk.Status = 'Completed';
                           tsk.TaskSubtype = 'Email';
                           tsks.add(tsk);
                       }
        if (!caseUpdates.isEmpty()) {
            system.debug('caseUpdates: ' + caseUpdates);

            update caseUpdates;
            insert tsks;
        }
    }
}