@isTest
private class SendContractorCloseoutTest {
    private static DataFactory df = new DataFactory();

    @isTest static void caseTriggerHandlerTests() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        df.newPolicy.Status = 'Canceled';
        df.newPolicy.Cancelled_Date__c = Date.today();
        update df.newPolicy;

        SendContractorCloseout.triggerEmail(New Contract[]{df.newPolicy});

        Test.stopTest();
    }
}