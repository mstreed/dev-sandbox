public class ServiceOrderInformationController {
    public Id acctId;
    public Id caseId {get;set;}
    public Case caseRec {get;set;}
    
    public ServiceOrderInformationController(){
        caseRec = new Case();
        if(apexpages.currentpage().getparameters().get('id') != null && apexpages.currentpage().getparameters().get('id') != ''){
            caseId  = apexpages.currentpage().getparameters().get('id');
            for(Case cRecord : [SELECT Id,Policy_Name__c,Contractor__c,Customer_Phone_Number_New__c,Customer_Mobile_Phone__c,Property_Street__c,
                                Property_State__c,Property_City__c,Property_Zip__c,Case_and_Claim_Number_Text__c,Coverage_Items__c,
                                Service_Call_Fee_Final__c,Description FROM Case WHERE Id =: caseId LIMIT 1]){
                                    caseRec = cRecord;  
                                }
        }
    }
}