global class SetupSandbox implements SandboxPostCopy {
    global void runApexClass(SandboxContext context) {
        System.debug('Org ID: ' + context.organizationId());
        System.debug('Sandbox ID: ' + context.sandboxId());
        System.debug('Sandbox Name: ' + context.sandboxName());

        recordType rtAuthorize = [SELECT Id,Name,DeveloperName FROM RecordType WHERE SobjectType ='ChargentBase__Gateway__c' AND DeveloperName = 'Authorizenet' LIMIT 1];
		
        //create gateway
        ChargentBase__Gateway__c newGateway = new ChargentBase__Gateway__c (
            Name= 'Authorize.net Gateway (test)',
            ChargentBase__Merchant_ID__c= '4K56Lqa7Fy52',
            ChargentBase__Security_Key__c = '8Ht6C9ey3cV52W66',
            ChargentBase__Active__c = true,
            ChargentBase__Test_Mode__c = true,
            ChargentBase__Credit_Card_Data_Handling__c = 'Never Clear',
            ChargentBase__Available_Payment_Methods__c = 'Credit Card',
            ChargentBase__Available_Card_Types__c = 'Visa; Mastercard; American Express',
            recordTypeId = rtAuthorize.id
        );
        insert newGateway;
        
        //gateway assignment
        Gateway_Assignment__c ga = new Gateway_Assignment__c(
            State__c = 'Default',
        	Primary_Gateway__c = newGateway.Id
        );
        for (Gateway_Assignment__c existingGA : [SELECT Id FROM Gateway_Assignment__c WHERE State__c = 'Default']) { 
        	ga.Id = existingGA.Id;
        }
        upsert ga;
        
        //NSA Contractor
        List<Account> accList = new List<Account>();
        Account nsa = new Account(
            Name = 'National Service Alliance',
            Business_Email__c = 'test@test.com',
            Account_Status__c = 'Active',
            Services_Provided__c = 'Appliance'
        );
        nsa.RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Contractor_Account' LIMIT 1].Id;
        insert nsa;
        
        //Global Setting
        Global_Setting__c gs = [SELECT Site_Domain__c FROM Global_Setting__c LIMIT 1];
        if (context.organizationId() != '00D8A000000Cf9tUAC') {
            User usr = [SELECT Username FROM User WHERE IsActive = TRUE AND Profile.Name = 'SHW System Administrator' LIMIT 1];
            gs.Site_Domain__c = usr.Username.substringAfterLast('.') + Util.siteURL();
        }
        gs.Default_Gateway__c = newGateway.Id;
        gs.Deductible_Gateway__c = newGateway.Id;
        gs.NSA_Contractor_ID__c = nsa.Id;
        update gs;
        
        //Website Setting and pricing Record
        Website_Setting__c  ws = new Website_Setting__c();
        ws.Header_Image_URL__c = 'https://marketing.selecthomewarranty.com/assets/images/icons/icon-award-a76189ee.png';
        ws.Header_Text__c = '2019 EDITOR’S CHOICE BY HOMEWARRANTYREVIEWS.COM';
        ws.Plans_Header_Text_1__c = 'Shield Your Wallet';
        ws.Plans_Header_Text_2__c = 'From Expensive Home Repairs';
        ws.Plans_Subheader_Text_1__c = 'Pick a plan that works best for you, customize your add-ons.';
        ws.Quote_Header_Text_1__c = 'Go Ahead Get  A Quote, You\'ll Save Money';
        ws.Quote_Promo_Text_1__c = '$100 OFF ANY PLAN';
        ws.Quote_Promo_Text_2__c = 'FREE ROOF COVERAGE';
        ws.Quote_Promo_Text_3__c = '2 EXTRA MONTHS FREE';
        ws.Affiliate_Image_URL__c = 'https://selecthomewarranty--c.na73.content.force.com/servlet/servlet.ImageServer?id=0151I000006tEzU&oid=00D1I000000m05g&lastMod=1593185316000';
        ws.Affiliate_Header_Text_1__c = 'Summer Sale Going on Now , Lowest Rates Ever!';
        ws.Affiliate_Header_Text_2__c = 'Use Coupon Code "SAVE25" For Additional Savings!';
        ws.Affiliate_First_Message__c = 'Call Now & Save $100 Off + 2 Free Months + Free Roof Coverage';
        ws.Affiliate_Second_Message__c = 'AC, Heat, Appliance Coverage...Plans Starting Less Than 75 Cents  Per Day';
        ws.Countdown_Message__c = '$100 OFF Ends Soon..';
        ws.Countdown_To__c = DateTime.now().addDays(30);
        ws.Countdown_Type__c = 'Days';
        ws.Active__c = TRUE;
        ws.RecordTypeId = [SELECT Id,Name,DeveloperName FROM RecordType WHERE SobjectType ='Website_Setting__c' AND DeveloperName = 'Global_Setting' LIMIT 1].Id;
        insert ws;
        
        Pricing__c pricing = new Pricing__c(
        	//Effective_Date__c = Date.today().addDays(1),
            Website_Setting__c = ws.Id
        );
        insert pricing; 
        
        ws.Active_Pricing__c = pricing.Id;
        update ws;
        
        //discount codes
        List<Discount__c> dcList = new List<Discount__c>();
        Discount__c dc1 = new Discount__c(
        	Name = 'ANNUAL75',
            Plan__c = 'Both',
            Type__c = 'Standard',
            Starts__c = Date.today().addDays(-30),
            Ends__c = Date.today().addDays(-1),
            Dollar_Amount__c = 75
        );
        dcList.add(dc1);
        Discount__c dc2 = dc1.clone(false, true, false, false);
        dc2.Name = 'DISCOUNT100';
        dc2.Ends__c = NULL;
        dc2.Dollar_Amount__c = 100;
        dcList.add(dc2);
        Discount__c dc3 = dc2.clone(false, true, false, false);
        dc3.Name = 'DISCOUNT50';
        dc3.Dollar_Amount__c = NULL;
        dc3.Percentage_Amount__c = 50;
        dcList.add(dc3);
        Discount__c dc4 = dc2.clone(false, true, false, false);
        dc4.Name = 'MONTHLY100';
        dc4.Plan__c = 'Monthly';
        dcList.add(dc4);
        Discount__c dc5 = dc3.clone(false, true, false, false);
        dc5.Name = 'RENEWAL50';
        dc5.Type__c = 'Renewal';
        dcList.add(dc5);
        Discount__c dc6 = dc2.clone(false, true, false, false);
        dc6.Name = 'YEAR50';
        dc6.Plan__c = 'Annual';
        dc6.Dollar_Amount__c = 50;
        dcList.add(dc6);
        insert dcList;    
        
        //affiliate
        //create affiliate
        Account affiliate = new Account(
            Name = 'Test Affiliate',
            Account_Status__c = 'Active',
            Nickname__c = 'Testor',
            ShippingStreet = '120 2nd Ave',
            ShippingCity = 'New York',
            ShippingStateCode = 'NY',
            ShippingCountryCode = 'US',
            ShippingPostalCode = '10022',
            Business_Email__c = 'test@test.com',
            Phone = '1234567890',
            ShippingLatitude = 40.728004,
            ShippingLongitude = -73.987649,
            Affiliate_ID__c = '12345',
            Leads_Report__c = TRUE,
            Leads_Report_Type__c = 'Month-to-Date'
        );
        affiliate.RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Affiliate_Account' LIMIT 1].Id;
        insert affiliate;
        Contact newAffCon = new Contact(
            AccountId = affiliate.Id,
            Company__c = affiliate.Name,
            Email = affiliate.Business_Email__c,
            MailingStreet = affiliate.ShippingStreet,
            MailingCity = affiliate.ShippingCity,
            MailingStateCode = affiliate.ShippingStateCode,
            MailingPostalCode = affiliate.ShippingPostalCode,
            MailingCountryCode = affiliate.ShippingCountryCode,
            MobilePhone = affiliate.Phone,
            FirstName = 'Test',
            LastName = 'Affiliate'
        );
        insert newAffCon;
    }
}