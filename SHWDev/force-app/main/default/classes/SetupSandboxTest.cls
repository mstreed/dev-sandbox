@isTest
class SetupSandboxTest {
    private static DataFactory df = new DataFactory();

    @isTest
    static void test() {

        Test.startTest();
        df.createGlobalSetting();

        Test.testSandboxPostCopyScript(
            new SetupSandbox(), UserInfo.getOrganizationId(),
                UserInfo.getOrganizationId(), UserInfo.getOrganizationName());

        Test.stopTest();

        // Insert assert statements here to check that the records you created above have
        // the values you expect.
    }
}