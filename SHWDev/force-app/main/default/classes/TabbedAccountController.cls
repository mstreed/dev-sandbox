public class TabbedAccountController{
    public Account thisAcc {set; get;}
    public User currentUser {set;get;}
    public String currentRecordId {get;set;}
    public String recordType{get;set;}
    
    public TabbedAccountController( ApexPages.StandardController stdController){
        currentRecordId   = ApexPages.currentPage().getParameters().get('id');

        currentUser = [Select Id, Name, Profile.Name FROM User where Id = :userinfo.getUserId()];

         // get account record
        if ( String.isBlank( currentRecordId ) ) {
            // must have accountId , otherwise bail out
            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, 'No accountId ID provided.' ) );
            return;
        }
        this.thisAcc = (Account)stdController.getRecord();
        
        recordType = [SELECT 
                      Id,
                      Recordtype.Id,
                      Recordtype.DeveloperName 
                      FROM Account
                      WHERE Id = :currentRecordId
                     ].Recordtype.DeveloperName;
    }
    public PageReference redirectView() {
        PageReference returnURL;
        /*if(currentUser.Name == 'Select Home Warrantyyyy') { 
            return new PageReference('/' + thisAcc.Id + '?nooverride=1');
        } else*/ if( thisAcc != NULL && (recordtype == 'Contractor_Account' || recordtype == 'Affiliate_Account')) { 
            /*returnURL = Page.AccountEdit;
            returnURL.getParameters().put('id', currentRecordId);
            returnURL.getParameters().put('nooverride', '1');
            returnURL.setRedirect(true);
            return returnURL; */
            return new PageReference('/' + thisAcc.Id + '?nooverride=1');
        } else {
            if (thisAcc.Policy__c!=NULL) {                
                return new PageReference('/' + thisAcc.Policy__c);
            } else {
                return new PageReference('/apex/InsufficientAccess');
            }
        }
    }
    
    public PageReference redirectEdit() {
        PageReference returnURL;
        if( thisAcc != NULL && (recordtype == 'Contractor_Account' || recordtype == 'Affiliate_Account')) { 
            return new PageReference('/' + thisAcc.Id + '/e?retURL=%2F'+ thisAcc.Id +'&nooverride=1');
        } else {
            if (thisAcc.Policy__c!=NULL) {                
                return new PageReference('/' + thisAcc.Policy__c);
            } else {
                return new PageReference('/apex/InsufficientAccess');
            }
        }
    }
  
}