/*

    @Description    TabbedAccountController Class 
    @createdate     20 May 2018

*/
@isTest
private class TabbedAccountControllerTest {
    private static DataFactory df = new DataFactory();

    @isTest static void controllerTest() {
        df.createGlobalSetting();
        df.createPolicy();
        
        System.assertNotEquals(df.acc, NULL );
        Test.StartTest();

        PageReference pageRef = Page.CaseNew;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',df.acc.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(df.acc);
        TabbedAccountController extension = new TabbedAccountController(controller);
        System.assertEquals(df.acc.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        extension.thisAcc = df.acc;
        //extension.redirect();
        Test.stopTest();
    }
    @isTest static void testRedirect() {
        df.createGlobalSetting();
        
        Test.StartTest();
        ApexPages.currentPage().getParameters().put('id',df.contractor.id );
        ApexPages.StandardController controller = new ApexPages.StandardController( df.contractor );
        TabbedAccountController extension = new TabbedAccountController(controller);
        System.assertEquals( df.contractor.id, Apexpages.currentpage().getparameters().get( 'id' ) );
    }
}