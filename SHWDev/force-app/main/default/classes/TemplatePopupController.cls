public class TemplatePopupController {

    //List to store templates
    public List<EmailTemplate> lstTemplate {get; set;}
    //List to store folderName
    public List<SelectOption> templateFolderList { get; set; }
    //To store selected folder Id
    public String selectedTemplateFolderId { get; set; }
    
    // Constrcuor to initialize the default list of templates and folder
    public TemplatePopupController (){
        
        lstTemplate = new List<EmailTemplate>();
        templateFolderList = new List<SelectOption>();
        templateFolderList.add( new SelectOption( UserInfo.getOrganizationId(), 'Unfiled Public Email Templates' ) );
        templateFolderList.add( new SelectOption( UserInfo.getUserId(), 'Personal Email Template' ) );
        selectedTemplateFolderId = UserInfo.getOrganizationId();
        List<Folder> templateFolders = [ SELECT Id, Name FROM Folder WHERE Type = 'Email' ];
        for( Folder fol : templateFolders ) {
            templateFolderList.add( new SelectOption( fol.Id, fol.Name ) );
        }
    } 
	
	//For to refresh the list of folder
    public PageReference refreshTemplates() {
        getRecs();
        return NULL;
    }
    
    //Get all template from selected folder by user
    public List<EmailTemplate> getRecs() {
        lstTemplate = [ SELECT Id, Name, TemplateType, Description FROM EmailTemplate WHERE FolderId = :selectedTemplateFolderId ];
        return lstTemplate;
    }
}