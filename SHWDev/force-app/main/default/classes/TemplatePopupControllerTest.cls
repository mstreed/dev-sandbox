/*

    @Description    TemplatePopupController Class 
    @createdate     23 Jan 2018

*/
@isTest
private class TemplatePopupControllerTest{
     
     @isTest
     static void testMethod1() {
        Test.StartTest();
        TemplatePopupController controller = new TemplatePopupController();
        System.assertNotEquals( controller, NULL );
        controller.refreshTemplates();
        Test.StopTest();    
     }
}