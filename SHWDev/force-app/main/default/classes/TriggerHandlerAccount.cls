public class TriggerHandlerAccount extends TriggerHandler {
    
    @TestVisible private Boolean testException = FALSE;                         // used to induce exception coverage by test methods
    
    public TriggerHandlerAccount() {}
    public static Global_Setting__c gs = [Select Id, System_Administrator_User_ID__c, Affiliate_Lead_Access_Start_Date__c FROM Global_Setting__c LIMIT 1];
    
    /* context overrides */
    
    public override void beforeInsert() {
        generateAffiliateId(Trigger.new);
        
    }
    
    public override void beforeUpdate() {
        qualifiedForQuickDispatch(Trigger.new, Trigger.oldMap);
    }
    
    public override void beforeDelete() {}
    
    public override void afterInsert() {
        if(RecursiveTriggerHandler.runOnceAccount()){
            syncContactsQuickEmail(Trigger.new, Trigger.oldMap); //one time only
            createAffiliateRemoteSite(Trigger.new, Trigger.oldMap);
        }
    }
    
    public override void afterUpdate() {
        setOwnerOnLeadsForDupes(Trigger.new, Trigger.oldMap);
        if(RecursiveTriggerHandler.runOnceAccount()){
            pixelBatch(Trigger.new, Trigger.oldMap); //one time only
            syncContactsQuickEmail(Trigger.new, Trigger.oldMap); //one time only
            createAffiliateRemoteSite(Trigger.new, Trigger.oldMap);
        }
    }
    
    public override void afterDelete() {}
    
    public override void afterUndelete() {} 
    
    /* private methods */
    private void generateAffiliateId(List<sObject> newAccts){
        system.debug('generateAffiliateId');
        List<Account> newAccountList = (List<Account>) newAccts;
        Id affiliateRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Affiliate Account').getRecordTypeId();
        
        for(Account acct : newAccountList){
            if (acct.RecordTypeId == affiliateRecordTypeId && acct.Affiliate_ID__c == NULL) {
                Blob blobKey = crypto.generateAesKey(128);
                String key = EncodingUtil.convertToHex(blobKey);
                if (acct.Affiliate_ID__c == NULL) {
                    acct.Affiliate_ID__c = key.left(6);
                }
                
                acct.Affiliate_API_Token__c = key.left(40);
            }
        }
    }
    
    private void qualifiedForQuickDispatch(List<sObject> newAccts, Map<Id,sObject> oldAccountMap){
        system.debug('qualifiedForQuickDispatch');
        List<Account> newAccountList = (List<Account>) newAccts;
        List<Account> acctsQuickDispatch = new List<Account>();
        //List<Task> tsks = new List<Task>();
        
        if(oldAccountMap != NULL ){
            //Assign oldMap
            Map<Id,Account> oldMap = (Map<Id,Account>) oldAccountMap;
            
            for(Account acct : newAccountList){
                /*system.debug('acct.GL_Policy__c: ' + acct.GL_Policy__c);
                system.debug('acct.GL_Expiration__c: ' + acct.GL_Expiration__c);
                system.debug('acct.Crim__c: ' + acct.Crim__c);
                system.debug('acct.Average_Rating__c: ' + acct.Average_Rating__c);
                system.debug('acct.of_Ratings__c: ' + acct.of_Ratings__c);
                system.debug('acct.Average_Rating_Higher_Than_3_5_Since__c: ' + acct.Average_Rating_Higher_Than_3_5_Since__c);
                system.debug('acct.Do_Not_Qualify_for_Quick_Dispatch__c: ' + acct.Do_Not_Qualify_for_Quick_Dispatch__c);
                system.debug('acct.Qualified_for_Quick_Dispatch__c: ' + acct.Qualified_for_Quick_Dispatch__c);
                system.debug('acct.Approved_for_Quick_Dispatch__c: ' + acct.Approved_for_Quick_Dispatch__c);
                system.debug('acct.Quick_Dispatch_Goodbye_Triggered__c: ' + acct.Quick_Dispatch_Goodbye_Triggered__c);
*/                
                Contact con;
                
                if (acct.Average_Rating__c >= 3.5 && acct.Average_Rating_Higher_Than_3_5_Since__c == NULL) {
                    acct.Average_Rating_Higher_Than_3_5_Since__c = Date.today();
                } else if (acct.Average_Rating__c < 3.5) {
                    acct.Average_Rating_Higher_Than_3_5_Since__c = NULL;
                }
                
                if (acct.GL_Policy__c == TRUE && acct.GL_Expiration__c >= Date.today() &&
                    ((acct.Crim__c == 'Y' && acct.Average_Rating__c >= 3.5) || acct.Average_Rating__c >= 4) &&
                    acct.of_Ratings__c >= 5 &&
                    acct.Average_Rating_Higher_Than_3_5_Since__c <= Date.today().addDays(-14) &&
                    acct.Do_Not_Qualify_for_Quick_Dispatch__c == FALSE ) {
                        system.debug('qualified for quick-dispatch');
                        
                        if (acct.Qualified_for_Quick_Dispatch__c == FALSE) {
                            acct.Qualified_for_Quick_Dispatch__c = TRUE;
                            acct.Qualified_Unqualified_for_Quick_Dispatch__c = DateTime.now();
                            if (acct.Quick_Dispatch_Goodbye_Triggered__c == TRUE) {
                                acct.Quick_Dispatch_Goodbye_Triggered__c = FALSE;
                                acct.Quick_Dispatch_Goodbye_Trigger_Date_Time__c = NULL;
                            }
                            if ((acct.Quick_Dispatch_Last_Response_Date_Time__c == NULL || acct.Quick_Dispatch_Last_Response_Date_Time__c <= Date.today().addDays(-30)) &&
                                (acct.Quick_Dispatch_Goodbye_Trigger_Date_Time__c <= DateTime.now() || acct.Quick_Dispatch_Goodbye_Trigger_Date_Time__c == NULL)) {
                                    if (acct.Business_Email__c != NULL) {
                                        acctsQuickDispatch.add(acct);
                                    }
                                    acct.Quick_Dispatch_Goodbye_Trigger_Date_Time__c = NULL;
                                }
                        }
                    } else {
                        system.debug('unqualified for quick-dispatch');
                        if (acct.Qualified_for_Quick_Dispatch__c == TRUE) {
                            acct.Qualified_for_Quick_Dispatch__c = FALSE;
                            acct.Qualified_Unqualified_for_Quick_Dispatch__c = DateTime.now();
                        }
                        if (acct.Approved_for_Quick_Dispatch__c == TRUE) {
                            acct.Approved_for_Quick_Dispatch__c = FALSE;
                            acct.Quick_Dispatch_Goodbye_Trigger_Date_Time__c = DateTime.now().addDays(14);
                        }
                        if (acct.Quick_Dispatch_Goodbye_Triggered__c == TRUE) {
                            acct.Quick_Dispatch_Goodbye_Triggered__c = FALSE;
                            acct.Quick_Dispatch_Goodbye_Trigger_Date_Time__c = NULL;
                            acct.Quick_Dispatch_Goodbye_Email_Sent__c = DateTime.now();
                            
                            if (acct.Business_Email__c != NULL) {
                                con = [SELECT Id FROM Contact WHERE AccountId = :acct.Id LIMIT 1];
                                Boolean sendEmail =  Util.sendEmail(con.Id, acct.Business_Email__c, 'Quick_Dispatch_Goodbye', 'Admin Emails', acct.Id, 'SHW Claims', false);
                                system.debug('QD-Goobye: ' + sendEmail);
                            }
                            /*Task tsk = new Task();
                            tsk.ActivityDate = Date.today();
                            tsk.WhatId = acct.Id;
                            tsk.Subject = 'Email:  Select Home Warranty - Executive Dispatch Network';
                            tsk.Description = 'Contractor sent email that they no longer qualify for quick-dispatch.';
                            tsk.OwnerId = UserInfo.getUserId();
                            tsk.Status = 'Completed';
                            tsk.TaskSubtype = 'Email';
                            tsks.add(tsk);*/
                        }
                    }
                
                if (acct.Send_GL_Expiration_Email__c == TRUE) {
                    acct.Send_GL_Expiration_Email__c = FALSE;
                    if (con == NULL) {
                        con = [SELECT Id FROM Contact WHERE AccountId = :acct.Id LIMIT 1];
                    }
                    system.debug('con: ' + con);
                    system.debug('acct.Id: ' + acct.Id);
                    Boolean sendEmail =  Util.sendEmail(con.Id, acct.Business_Email__c, 'GL_Insurance_Expired', 'Admin Emails', acct.Id, 'SHW Claims', false);
                    system.debug('GL-Expire: ' + sendEmail);
                }
                
                if (acct.Quick_Dispatch_Reevaluate_Trigger__c == TRUE) {
                    acct.Quick_Dispatch_Reevaluate_Trigger__c = FALSE;
                }
            }
            
            if (!acctsQuickDispatch.isEmpty()) {
                Util.SendQuickDispatchRequest(acctsQuickDispatch);
            }
            
            /*if (!tsks.isEmpty()) {
                insert tsks;
            }*/
        }
    }
    
    private void setOwnerOnLeadsForDupes(List<sObject> newAccts , Map<Id,sObject> oldAccountMap){
        system.debug('setOwnerOnLeadsForDupes');
        List<Account> newAccountList = (List<Account>) newAccts;
        Set<Id> affiliatesToQueryAdd = new Set<Id>();
        Set<Id> affiliatesToQueryRemove = new Set<Id>();
        List<Lead> updateLeads = new List<Lead>();
        
        if(oldAccountMap != NULL ){
            //Assign oldMap
            Map<Id,Account> oldMap = (Map<Id,Account>) oldAccountMap;
            
            for(Account acct : newAccountList){
                if (acct.Affiliate_ID__c != NULL) {
                    if (acct.View_Duplicates__c != oldMap.get(acct.Id).View_Duplicates__c) {
                        if (acct.IsPartner) {
                            leadOwnership_batch lob1 = new leadOwnership_batch(acct.Id, acct.Partner_Login__c, acct.View_Duplicates__c); 
                            database.executeBatch(lob1,50);
                        } else {
                            leadOwnership_batch lob2 = new leadOwnership_batch(acct.Id, NULL, acct.View_Duplicates__c); 
                            database.executeBatch(lob2,50);
                        }
                    }
                }
            }
        }
    }
    
    private void syncContactsQuickEmail(List<sObject> newAccts, Map<Id,sObject> oldAccountMap){
        system.debug('syncContactsQuickEmail');
        
        List<Account> newAccountList = (List<Account>) newAccts;
        List<Contact> contacts = new List<Contact>();
        Map<Id,List<Contact>> acctConMap = new Map<Id,List<Contact>>();
        for(Account acct : newAccountList){
            //This is only for Contractor and Affiliate RT
            if (acct.Record_Type_Name__c == 'Contractor Account' || acct.Record_Type_Name__c == 'Affiliate Account') {
                acctConMap.put(acct.Id, new List<Contact>());
            }
        }
        
        
        if (!acctConMap.isEmpty()) {
            //List<Contact> extraCons = new List<Contact>(); //delete extra contacts -- only one!!!

            List<Contact> consToUpsert = new List<Contact>();
            for (Contact con :[SELECT Id, AccountId, FirstName, LastName FROM Contact WHERE AccountId IN :acctConMap.keySet()]) {
                acctConMap.get(con.AccountId).add(con);
            }
            
            for(Account acct : newAccountList){
                if (acct.Record_Type_Name__c == 'Contractor Account' || acct.Record_Type_Name__c == 'Affiliate Account') {
                    
                    //if Account does not have a contact, create one
                    if (acctConMap.get(acct.Id).isEmpty()) {
                        acctConMap.get(acct.Id).add(new Contact(AccountId = acct.Id));
                    }
                    
                    //now sync contact data                    
                    for (Contact con : acctConMap.get(acct.Id)) {
                        con.Company__c = acct.Name;
                        con.Phone = acct.Phone;
                        con.Email = acct.Business_Email__c;
                        con.MailingStreet = acct.ShippingStreet;
                        con.MailingCity = acct.ShippingCity;
                        con.MailingStateCode = acct.ShippingStateCode;
                        con.MailingPostalCode = acct.ShippingPostalCode;
                        con.MailingCountryCode = acct.ShippingCountryCode;
                        con.MobilePhone = acct.Pager_number__c;
                        
                        if (acct.Record_Type_Name__c == 'Contractor Account') {
                            if (acct.Contractor_Name__c == NULL) {
                                con.LastName = acct.Name.left(80);
                            } else {
                                con.FirstName = acct.Contractor_Name__c.left(acct.Contractor_Name__c.indexOf(' ')).left(80);
                                con.LastName = acct.Contractor_Name__c.mid(acct.Contractor_Name__c.indexOf(' '), 80);
                            }
                        }
                        
                        if (acct.Record_Type_Name__c == 'Affiliate Account') {
                            con.FirstName = acct.Affiliate_First_Name__c;
                            con.LastName = acct.Affiliate_Last_Name__c;
                        }
                        consToUpsert.add(con);
                    }
                }
            }
            
            if (!consToUpsert.isEmpty()) {
                UPSERT consToUpsert;
            }
            /*if (!extraCons.isEmpty()) {
                DELETE extraCons;
            }*/
        }
        
        List<Account> quickEmailAccounts = new List<Account>();
        for(Account acct : newAccountList){
            if (acct.Contractor_Quick_Email__c != NULL) {
                Boolean sendEmail = Util.sendEmail(acctConMap.get(acct.Id)[0].Id, String.valueOf(acct.Business_Email__c), acct.Contractor_Quick_Email__c, 'Contractor Quick Email', acct.Id, 'SHW Claims', false);
                quickEmailAccounts.add(new Account(Id = acct.Id, Contractor_Quick_Email__c = NULL));
            } else if (acct.Affiliate_Quick_Email__c != NULL) {
                Boolean sendEmail = Util.sendEmail(acctConMap.get(acct.Id)[0].Id, String.valueOf(acct.Business_Email__c), acct.Affiliate_Quick_Email__c, 'Affiliate Quick Email', acct.Id, 'SHW Info', true);
                quickEmailAccounts.add(new Account(Id = acct.Id, Affiliate_Quick_Email__c = NULL));
            }
        }
        if (!quickEmailAccounts.isEmpty()) {
            UPDATE quickEmailAccounts;
        }
    }
    
    private void createAffiliateRemoteSite(List<sObject> newAccts, Map<Id,sObject> oldAccountMap){
        system.debug('createAffiliateRemoteSite');
        
        List<Account> newAccountList = (List<Account>) newAccts;
        Map<Id,Account> oldMap = new Map<Id,Account>();
        
        if(oldAccountMap != NULL){
            //Assign oldMap
            oldMap = (Map<Id,Account>) oldAccountMap;
        }
        
        for(Account acct : newAccountList){
            if (acct.Record_Type_Name__c == 'Affiliate Account') {
                
                //if new and endpoint populated, create a remote site
                if (acct.Endpoint_URL__c != NULL) {
                    if(oldAccountMap == NULL){
                        CalloutUtil.createRemoteSiteSettings(acct.Endpoint_URL__c);
                    } else if (oldAccountMap != NULL && acct.Endpoint_URL__c != oldMap.get(acct.Id).Endpoint_URL__c) {
                        CalloutUtil.createRemoteSiteSettings(acct.Endpoint_URL__c);
                    }
                }
            }
        }
    }
    
    private void pixelBatch(List<sObject> newAccts , Map<Id,sObject> oldAccountMap){
        system.debug('pixelBatch');
        List<Account> newAccountList = (List<Account>) newAccts;
        List<Lead> updateLeads = new List<Lead>();
        
        if(oldAccountMap != NULL ){
            //Assign oldMap
            Map<Id,Account> oldMap = (Map<Id,Account>) oldAccountMap;
            
            for(Account acct : newAccountList){
                if (acct.Affiliate_ID__c != NULL) {
                    if (acct.Batch_Pixel_Trigger__c == TRUE && acct.Batch_Pixel_Trigger__c != oldMap.get(acct.Id).Batch_Pixel_Trigger__c &&
                        acct.Batch_Pixel_Start_Date__c != NULL && acct.Batch_Pixel_End_Date__c != NULL) {
                        sendLeadPixel_batch batch = new sendLeadPixel_batch(acct.Id, acct.Batch_Pixel_Start_Date__c, acct.Batch_Pixel_End_Date__c); 
                        database.executeBatch(batch,1);
                    }
                }
            }
        }
    }
}