public class TriggerHandlerCaseAssignmentRoundRobin extends TriggerHandler {
    // Constructor 
    public TriggerHandlerCaseAssignmentRoundRobin() {}
    
    /* Trigger context overrides */
    public override void beforeInsert() {}
    public override void beforeUpdate() {}
    //public override void beforeDelete() {}
    public override void afterInsert()  {}
    public override void afterUpdate()  {
        if(RecursiveTriggerHandler.runOnce()){
            redistributeCases(Trigger.new, Trigger.oldMap);
        }
    }
    //public override void afterDelete()  {}
    //public override void afterUndelete(){}
    
    //ASSUMPTION IS THAT THIS IS CALLED FOR ONE CARR AT A TIME - NOT OPTIMIZED FOR MULTIPLE RECORDS
    private void redistributeCases(List<sObject> newCarrs, Map<Id,sObject> oldCarrMap){
        system.debug('redistributeCases');
        List<Case_Assignment_Round_Robin__c> newCarrList = (List<Case_Assignment_Round_Robin__c>) newCarrs;
		List<Case> casesToUpdate = new List<Case>();
        Id usrToExlude;
        
        List<Dispatcher_History__c> newDispHistList = new List<Dispatcher_History__c>();

        if(oldCarrMap != NULL){
            Map<Id,Case_Assignment_Round_Robin__c> oldMap = (Map<Id,Case_Assignment_Round_Robin__c>) oldCarrMap ;
            
            Integer loopNum = 0;
            Case_Assignment_Round_Robin__c[] carrList;
            
            for(Case_Assignment_Round_Robin__c carr : newCarrList){                 
                /*Map<String,GroupMember> gmMap = new Map<String,GroupMember>();
                for (GroupMember gm : [SELECT GroupId, Group.Name, UserOrGroupId 
                                   FROM GroupMember 
                                   WHERE UserOrGroupId = :carr.User__c AND (Group.Name = 'Claims@' OR Group.Name = 'Contract@' OR Group.Name = 'Info@' OR Group.Name = 'Invoices@')]) {
                                       gmMap.put(carr.User__c+''+gm.Group.Name, gm);
                                   }*/
                Map<String,User> usrEmlMap = new Map<String,User>();
                for (User usr : [SELECT Id, Email2Case__c 
                                 FROM User 
                                 WHERE Email2Case__c != NULL]) {
                                     if (usr.Email2Case__c.contains('Claims@')) {
                                         usrEmlMap.put(usr.Id+'Claims@', usr);
                                     } 
                                     if (usr.Email2Case__c.contains('Contract@')) {
                                         usrEmlMap.put(usr.Id+'Contract@', usr);
                                     } 
                                     if (usr.Email2Case__c.contains('Info@')) {
                                         usrEmlMap.put(usr.Id+'Info@', usr);
                                     } 
                                     if (usr.Email2Case__c.contains('Invoices@')) {
                                         usrEmlMap.put(usr.Id+'Invoices@', usr);
                                     } 
                                     if (usr.Email2Case__c.contains('Faxes@')) {
                                         usrEmlMap.put(usr.Id+'Faxes@', usr);
                                     }
                                 }
                
                if (oldMap.get(carr.Id).Cases_Redistributed_On__c != carr.Cases_Redistributed_On__c) {   
                    carrList = [Select Id, Status__c, User__c, User__r.Name, Star_Priority__c, Away__c, Active__c, Last_Assigned__c, Apply_To__c
                                FROM Case_Assignment_Round_Robin__c 
                                WHERE Status__c = :carr.Status__c AND Active__c = TRUE AND Away__c = FALSE AND User__c != :carr.User__c
                                ORDER By Status__c, Last_Assigned__c, User__r.Full_Name__c];
                               
                    system.debug('carrList.size(): ' + carrList.size());
                    system.debug('carrList: ' + carrList);
                    if (carrList.size() > 0) {
                        for (Case cs : [Select Id, OwnerId, Status, Customer_Priority_Value__c, RecordType.Name, Initial_Queue__c FROM Case WHERE OwnerId = :carr.User__c AND Status = :carr.Status__c ORDER BY Customer_Priority_Value__c]) {
                            for (Integer i = loopNum; i < carrList.size(); i++) {

                                if (carrList[i].Star_Priority__c.contains(cs.Customer_Priority_Value__c) && 
                                    carrList[i].Apply_To__c.contains(cs.RecordType.Name)) {
                                        system.debug('usrEmlMap check: ' + carr.User__c + cs.Initial_Queue__c);

                                        if (cs.RecordType.Name == 'Claim' || (cs.RecordType.Name == 'Inquiry' && usrEmlMap.get(carr.User__c + cs.Initial_Queue__c) != NULL)) {
                                            system.debug('assign! ' + carrList[i].User__r.Name);
                                            
                                            cs.OwnerId = carrList[i].User__c;
                                            carrList[i].Last_Assigned__c = DateTime.now();
                                            
                                            if (cs.Status == 'Ready to Dispatch' || cs.Status == 'Redispatch') {
                                                Dispatcher_History__c dh = new Dispatcher_History__c(
                                                    case__c = cs.Id,
                                                    Type__c = 'Assigned',
                                                    User__c = cs.OwnerId,
                                                    Dispatch_Status__c = cs.Status
                                                );
                                                newDispHistList.add(dh);
                                            }
                                            
                                            if (loopNum < (carrList.size()-1)) {
                                                loopNum++;
                                            } else {
                                                loopNum = 0;
                                            }
                                            
                                            casesToUpdate.add(cs);
                                            
                                            break;
                                        }
                                }
                            }
                        }
                    }
                }
            }
            
            system.debug('casesToUpdate: ' + casesToUpdate);

            if (!casesToUpdate.isEmpty()) {
                update casesToUpdate;
                update carrList;
            }
            
            if (!newDispHistList.isEmpty()) {
                insert newDispHistList;
            }
        }
    }
}