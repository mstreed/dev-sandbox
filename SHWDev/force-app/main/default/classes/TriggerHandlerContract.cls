public class TriggerHandlerContract extends TriggerHandler {
    
    
    @TestVisible private Boolean testException = FALSE;                         // used to induce exception coverage by test methods
    
    public TriggerHandlerContract() {}
    public static Global_Setting__c gs = [Select Id, Gateway_Counter_CA__c, Gateway_Counter_CA_Max__c, System_Administrator_User_ID__c, Do_Not_Call_Task_User_ID__c FROM Global_Setting__c LIMIT 1];
    public static Profile[] currentUserProfile = [SELECT Id, Name FROM PROFILE WHERE Id = :UserInfo.getProfileId() LIMIT 1];

    /* context overrides */ 
    
    public override void beforeInsert() {
        before(Trigger.new);
    }
    
    public override void beforeUpdate() {
        setGateway(Trigger.new, Trigger.oldMap);
        before(Trigger.new);
        captureRepChanges(Trigger.new, Trigger.oldMap);
    }
    
    public override void beforeDelete() {}
    
    public override void afterInsert() {}
    
    public override void afterUpdate() {
        if(RecursiveTriggerHandler.runOnce2()){
            policyAfterUpdate(Trigger.new, Trigger.oldMap);
            contractAdditonalHistory(Trigger.new, Trigger.oldMap);
        }
        setTotalPriceManual(Trigger.new, Trigger.oldMap);
        sendPardotEmail(Trigger.new, Trigger.oldMap);
    }
    
    public override void afterDelete() {}
    
    public override void afterUndelete() {} 
    
    /* private methods */
    public void before(List<sObject> newPolicies){
        system.debug('before');
        List<Contract> newPolicyList = (List<Contract>) newPolicies;
        
        for(Contract pol : newPolicyList){
            Date formattedDate = Date.valueOf(pol.Converted_Date__c.format('yyyy-MM-dd',  UserInfo.getTimeZone().toString())); //Date.valueOf(String.valueOf(con.Converted_Date__c));
            if (formattedDate == Date.today()) { //check if change is happening same day
                if (pol.Service_Call_Fee_Dropdown__c == '45') {
                    pol.X45_SCF_Deduction__c = TRUE;
                } else {
                    pol.X45_SCF_Deduction__c = FALSE;
                }
                if (pol.X1st_Service_call_free__c == TRUE) {
                    pol.Free_SCF_Deduction__c = TRUE;
                } else {
                    pol.Free_SCF_Deduction__c = FALSE;
                }
            }
            
            if (pol.Months_Free__c != NULL) {
                pol.Free_Months__c = Integer.valueOf(pol.Months_Free__c.split(' ')[0]);
            } else {
                pol.Free_Months__c = 0;
            }
            
            if (pol.Contract_Term__c == NULL) {
                pol.ContractTerm = 1;
            } else if (pol.Contract_Term__c == '2 Year') {
                pol.ContractTerm = 24;
            } else if (pol.Contract_Term__c == '3 Year') {
                pol.ContractTerm = 36;
            } else if (pol.Contract_Term__c == '4 Year') {
                pol.ContractTerm = 48;
            } else if (pol.Contract_Term__c == '5 Year') {
                pol.ContractTerm = 60;
            } else if (pol.Contract_Term__c == '10 Year') {
                pol.ContractTerm = 120;
            } else {
                pol.ContractTerm = 12;
            }
        }
  	}
    
    private void captureRepChanges(List<sObject> newPolicies , Map<Id,sObject> oldPolicyMap){
        system.debug('captureRepChanges');
        List<Contract> newPolicyList = (List<Contract>) newPolicies;
        
        Map<Id,Contract> oldMap = (Map<Id,Contract>) oldPolicyMap;
        
        for(Contract pol : newPolicyList){
            if (pol.X1st_Service_Call_Free__c == TRUE && oldMap.get(pol.Id).X1st_Service_Call_Free__c == FALSE) {
                pol.X1st_Service_Call_Free_Rep__c = UserInfo.getUserId();
                pol.X1st_Service_Call_Free_Date_Time__c = DateTime.now();
            } else if (pol.X1st_Service_Call_Free__c == FALSE && oldMap.get(pol.Id).X1st_Service_Call_Free__c == TRUE) {
                pol.X1st_Service_Call_Free_Rep__c = NULL;
                pol.X1st_Service_Call_Free_Date_Time__c = NULL;
            }
            if (pol.No_Cancellation_Fee__c == TRUE && oldMap.get(pol.Id).No_Cancellation_Fee__c == FALSE) {
                pol.No_Cancellation_Fee_Rep__c = UserInfo.getUserId();
                pol.No_Cancellation_Fee_Date_Time__c = DateTime.now();
            } else if (pol.No_Cancellation_Fee__c == FALSE && oldMap.get(pol.Id).No_Cancellation_Fee__c == TRUE) {
                pol.No_Cancellation_Fee_Rep__c = NULL;
                pol.No_Cancellation_Fee_Date_Time__c = NULL;
            }
        }
    }
    
    private void policyAfterUpdate(List<sObject> newPolicies , Map<Id,sObject> oldPolicyMap){
        system.debug('policyAfterUpdate');
        List<Contract> newPolicyList = (List<Contract>) newPolicies;
        
        if(oldPolicyMap != NULL ){
            //Assign oldMap
            Map<Id,Contract> oldMap = (Map<Id,Contract>) oldPolicyMap;
            
            for(Contract pol : newPolicyList){
                system.debug('pol.Status: ' + pol.Status);
                system.debug('oldMap.get(pol.Id).Status: ' + oldMap.get(pol.Id).Status);
                
                system.debug('pol.Payment_Terms__c: ' + pol.Payment_Terms__c);
                system.debug('oldMap.get(pol.Id).Payment_Terms__c: ' + oldMap.get(pol.Id).Payment_Terms__c);
                
                if (pol.Payment_Terms__c != oldMap.get(pol.Id).Payment_Terms__c) {
                    Util.newTask(pol.Id, 'Billings', pol.Contract_Number__c + ' - # of Payments Modified to ' + pol.Payment_Terms__c, '# of Payments Modified. Please review policy.',  gs.System_Administrator_User_ID__c, 'Task', false);
                    system.debug('create billing task');
                }
                
                if (pol.Do_Not_Call__c == true && oldMap.get(pol.Id).Do_Not_Call__c == false) {
                    Util.newTask(pol.Id, '', pol.Contract_Number__c + ' - Do Not Call', '', gs.Do_Not_Call_Task_User_ID__c, 'Task', false);
                    system.debug('create do not call task');
                }
                
                if (pol.Status == 'Payment Declined' && oldMap.get(pol.Id).Status != 'Payment Declined') {
                    List<Contract> relPolUpdate = new List<Contract>();
                    for (Related_Policies__c rp : [SELECT
                                                   Id,
                                                   Current_Policy__c,
                                                   Related_Policy__c
                                                   FROM Related_Policies__c
                                                   WHERE Current_Policy__c = :pol.Id OR Related_Policy__c = :pol.Id 
                                                   ORDER BY CreatedDate ASC
                                                   LIMIT 50000]) {
                                                       if(rp.Related_Policy__c != pol.Id){
                                                           Contract con = new Contract(
                                                               Id = rp.Related_Policy__c,
                                                               Status = 'Payment Declined'
                                                           );
                                                           relPolUpdate.add(con);
                                                       } 
                                                       if(rp.Current_Policy__c != pol.Id) {
                                                           Contract con = new Contract(
                                                               Id = rp.Current_Policy__c,
                                                               Status = 'Payment Declined'
                                                           );
                                                           relPolUpdate.add(con);
                                                       }
                                                   }
                    if (relPolUpdate.size()>0) {
                        update relPolUpdate;
                    }
                    system.debug('set related policies to payment declined');
                }
                
                if (pol.BillingStateCode == 'CA' && oldMap.get(pol.Id).BillingStateCode != 'CA') {
                    if (gs.Gateway_Counter_CA__c >= gs.Gateway_Counter_CA_Max__c) { //SET THIS NUMBER for 
                        gs.Gateway_Counter_CA__c = 1;
                    } else {
                        gs.Gateway_Counter_CA__c++;
                    }
                    update gs;
                }
                
                if (pol.Status == 'Refuse SCF' && oldMap.get(pol.Id).Status != 'Refuse SCF') {
                    Boolean sendEmail = Util.sendEmail(pol.Contact_ID__c, pol.Email__c, 'Refuse_SCF', 'Admin Emails', pol.Id, 'SHW Sales', true);
                    system.debug('sendEmail: ' + sendEmail);
                }
            }
        }
    }
    
    private void contractAdditonalHistory(List<sObject> newPolicies , Map<Id,sObject> oldPolicyMap){
        system.debug('contractAdditonalHistory');
        List<Contract> newPolicyList = (List<Contract>) newPolicies;
        
        if(oldPolicyMap != NULL ){
            //Assign oldMap
            Map<Id,Contract> oldMap = (Map<Id,Contract>) oldPolicyMap;
            List<Additional_History__c> ahList = new List<Additional_History__c>();

            for(Contract con : newPolicyList){
                if (con.Central_Vacuum__c != oldMap.get(con.Id).Central_Vacuum__c) {
                    ahList.add(new Additional_History__c(
                        Policy__c = con.Id,
                        Date__c = dateTime.now(),
                        Field__c = Schema.getGlobalDescribe().get('Contract').getDescribe().fields.getMap().get('Central_Vacuum__c').getDescribe().getLabel(),
                        Old_Value__c = String.valueOf(oldMap.get(con.Id).Central_Vacuum__c),
                        New_Value__c = String.valueOf(con.Central_Vacuum__c)
                    ));
                }
                if (con.Freon__c != oldMap.get(con.Id).Freon__c) {
                    ahList.add(new Additional_History__c(
                        Policy__c = con.Id,
                        Date__c = dateTime.now(),
                        Field__c = Schema.getGlobalDescribe().get('Contract').getDescribe().fields.getMap().get('Freon__c').getDescribe().getLabel(),
                        Old_Value__c = String.valueOf(oldMap.get(con.Id).Freon__c),
                        New_Value__c = String.valueOf(con.Freon__c)
                    ));
                }
                if (con.Geo_Thermal__c != oldMap.get(con.Id).Geo_Thermal__c) {
                    ahList.add(new Additional_History__c(
                        Policy__c = con.Id,
                        Date__c = dateTime.now(),
                        Field__c = Schema.getGlobalDescribe().get('Contract').getDescribe().fields.getMap().get('Geo_Thermal__c').getDescribe().getLabel(),
                        Old_Value__c = String.valueOf(oldMap.get(con.Id).Geo_Thermal__c),
                        New_Value__c = String.valueOf(con.Geo_Thermal__c)
                    ));
                }
                if (con.Grinder_Pump__c != oldMap.get(con.Id).Grinder_Pump__c) {
                    ahList.add(new Additional_History__c(
                        Policy__c = con.Id,
                        Date__c = dateTime.now(),
                        Field__c = Schema.getGlobalDescribe().get('Contract').getDescribe().fields.getMap().get('Grinder_Pump__c').getDescribe().getLabel(),
                        Old_Value__c = String.valueOf(oldMap.get(con.Id).Grinder_Pump__c),
                        New_Value__c = String.valueOf(con.Grinder_Pump__c)
                    ));
                }
                if (con.Ice_Maker_In_Refrigerator__c != oldMap.get(con.Id).Ice_Maker_In_Refrigerator__c) {
                    ahList.add(new Additional_History__c(
                        Policy__c = con.Id,
                        Date__c = dateTime.now(),
                        Field__c = Schema.getGlobalDescribe().get('Contract').getDescribe().fields.getMap().get('Ice_Maker_In_Refrigerator__c').getDescribe().getLabel(),
                        Old_Value__c = String.valueOf(oldMap.get(con.Id).Ice_Maker_In_Refrigerator__c),
                        New_Value__c = String.valueOf(con.Ice_Maker_In_Refrigerator__c)
                    ));
                }
                if (con.Lawn_Sprinkler_System__c != oldMap.get(con.Id).Lawn_Sprinkler_System__c) {
                    ahList.add(new Additional_History__c(
                        Policy__c = con.Id,
                        Date__c = dateTime.now(),
                        Field__c = Schema.getGlobalDescribe().get('Contract').getDescribe().fields.getMap().get('Lawn_Sprinkler_System__c').getDescribe().getLabel(),
                        Old_Value__c = String.valueOf(oldMap.get(con.Id).Lawn_Sprinkler_System__c),
                        New_Value__c = String.valueOf(con.Lawn_Sprinkler_System__c)
                    ));
                }
                if (con.Lighting_Fixtures_Plumbing_Fixtures__c != oldMap.get(con.Id).Lighting_Fixtures_Plumbing_Fixtures__c) {
                    ahList.add(new Additional_History__c(
                        Policy__c = con.Id,
                        Date__c = dateTime.now(),
                        Field__c = Schema.getGlobalDescribe().get('Contract').getDescribe().fields.getMap().get('Lighting_Fixtures_Plumbing_Fixtures__c').getDescribe().getLabel(),
                        Old_Value__c = String.valueOf(oldMap.get(con.Id).Lighting_Fixtures_Plumbing_Fixtures__c),
                        New_Value__c = String.valueOf(con.Lighting_Fixtures_Plumbing_Fixtures__c)
                    ));
                }
                if (con.Oven__c != oldMap.get(con.Id).Oven__c) {
                    ahList.add(new Additional_History__c(
                        Policy__c = con.Id,
                        Date__c = dateTime.now(),
                        Field__c = Schema.getGlobalDescribe().get('Contract').getDescribe().fields.getMap().get('Oven__c').getDescribe().getLabel(),
                        Old_Value__c = String.valueOf(oldMap.get(con.Id).Oven__c),
                        New_Value__c = String.valueOf(con.Oven__c)
                    ));
                }
                if (con.Pool__c != oldMap.get(con.Id).Pool__c) {
                    ahList.add(new Additional_History__c(
                        Policy__c = con.Id,
                        Date__c = dateTime.now(),
                        Field__c = Schema.getGlobalDescribe().get('Contract').getDescribe().fields.getMap().get('Pool__c').getDescribe().getLabel(),
                        Old_Value__c = String.valueOf(oldMap.get(con.Id).Pool__c),
                        New_Value__c = String.valueOf(con.Pool__c)
                    ));
                }
                if (con.Roof_Leak__c != oldMap.get(con.Id).Roof_Leak__c) {
                    ahList.add(new Additional_History__c(
                        Policy__c = con.Id,
                        Date__c = dateTime.now(),
                        Field__c = Schema.getGlobalDescribe().get('Contract').getDescribe().fields.getMap().get('Roof_Leak__c').getDescribe().getLabel(),
                        Old_Value__c = String.valueOf(oldMap.get(con.Id).Roof_Leak__c),
                        New_Value__c = String.valueOf(con.Roof_Leak__c)
                    ));
                }
                if (con.Tankless_Water_Heater__c != oldMap.get(con.Id).Tankless_Water_Heater__c) {
                    ahList.add(new Additional_History__c(
                        Policy__c = con.Id,
                        Date__c = dateTime.now(),
                        Field__c = Schema.getGlobalDescribe().get('Contract').getDescribe().fields.getMap().get('Tankless_Water_Heater__c').getDescribe().getLabel(),
                        Old_Value__c = String.valueOf(oldMap.get(con.Id).Tankless_Water_Heater__c),
                        New_Value__c = String.valueOf(con.Tankless_Water_Heater__c)
                    ));
                }
                if (con.Salt_Water_Pool__c != oldMap.get(con.Id).Salt_Water_Pool__c) {
                    ahList.add(new Additional_History__c(
                        Policy__c = con.Id,
                        Date__c = dateTime.now(),
                        Field__c = Schema.getGlobalDescribe().get('Contract').getDescribe().fields.getMap().get('Salt_Water_Pool__c').getDescribe().getLabel(),
                        Old_Value__c = String.valueOf(oldMap.get(con.Id).Salt_Water_Pool__c),
                        New_Value__c = String.valueOf(con.Salt_Water_Pool__c)
                    ));
                }
                if (con.Second_Refrigerator__c != oldMap.get(con.Id).Second_Refrigerator__c) {
                    ahList.add(new Additional_History__c(
                        Policy__c = con.Id,
                        Date__c = dateTime.now(),
                        Field__c = Schema.getGlobalDescribe().get('Contract').getDescribe().fields.getMap().get('Second_Refrigerator__c').getDescribe().getLabel(),
                        Old_Value__c = String.valueOf(oldMap.get(con.Id).Second_Refrigerator__c),
                        New_Value__c = String.valueOf(con.Second_Refrigerator__c)
                    ));
                }
                if (con.Septic_System__c != oldMap.get(con.Id).Septic_System__c) {
                    ahList.add(new Additional_History__c(
                        Policy__c = con.Id,
                        Date__c = dateTime.now(),
                        Field__c = Schema.getGlobalDescribe().get('Contract').getDescribe().fields.getMap().get('Septic_System__c').getDescribe().getLabel(),
                        Old_Value__c = String.valueOf(oldMap.get(con.Id).Septic_System__c),
                        New_Value__c = String.valueOf(con.Septic_System__c)
                    ));
                }
                if (con.Spa__c != oldMap.get(con.Id).Spa__c) {
                    ahList.add(new Additional_History__c(
                        Policy__c = con.Id,
                        Date__c = dateTime.now(),
                        Field__c = Schema.getGlobalDescribe().get('Contract').getDescribe().fields.getMap().get('Spa__c').getDescribe().getLabel(),
                        Old_Value__c = String.valueOf(oldMap.get(con.Id).Spa__c),
                        New_Value__c = String.valueOf(con.Spa__c)
                    ));
                }
                if (con.Stand_Alone_Freezer__c != oldMap.get(con.Id).Stand_Alone_Freezer__c) {
                    ahList.add(new Additional_History__c(
                        Policy__c = con.Id,
                        Date__c = dateTime.now(),
                        Field__c = Schema.getGlobalDescribe().get('Contract').getDescribe().fields.getMap().get('Stand_Alone_Freezer__c').getDescribe().getLabel(),
                        Old_Value__c = String.valueOf(oldMap.get(con.Id).Stand_Alone_Freezer__c),
                        New_Value__c = String.valueOf(con.Stand_Alone_Freezer__c)
                    ));
                }
                if (con.Sump_Pump__c != oldMap.get(con.Id).Sump_Pump__c) {
                    ahList.add(new Additional_History__c(
                        Policy__c = con.Id,
                        Date__c = dateTime.now(),
                        Field__c = Schema.getGlobalDescribe().get('Contract').getDescribe().fields.getMap().get('Sump_Pump__c').getDescribe().getLabel(),
                        Old_Value__c = String.valueOf(oldMap.get(con.Id).Sump_Pump__c),
                        New_Value__c = String.valueOf(con.Sump_Pump__c)
                    ));
                }
                if (con.Water_Softener__c != oldMap.get(con.Id).Water_Softener__c) {
                    ahList.add(new Additional_History__c(
                        Policy__c = con.Id,
                        Date__c = dateTime.now(),
                        Field__c = Schema.getGlobalDescribe().get('Contract').getDescribe().fields.getMap().get('Water_Softener__c').getDescribe().getLabel(),
                        Old_Value__c = String.valueOf(oldMap.get(con.Id).Water_Softener__c),
                        New_Value__c = String.valueOf(con.Water_Softener__c)
                    ));
                }
                if (con.Well_Pump__c != oldMap.get(con.Id).Well_Pump__c) {
                    ahList.add(new Additional_History__c(
                        Policy__c = con.Id,
                        Date__c = dateTime.now(),
                        Field__c = Schema.getGlobalDescribe().get('Contract').getDescribe().fields.getMap().get('Well_Pump__c').getDescribe().getLabel(),
                        Old_Value__c = String.valueOf(oldMap.get(con.Id).Well_Pump__c),
                        New_Value__c = String.valueOf(con.Well_Pump__c)
                    ));
                }
                if (con.Additional_AC_Unit_each__c != oldMap.get(con.Id).Additional_AC_Unit_each__c) {
                    ahList.add(new Additional_History__c(
                        Policy__c = con.Id,
                        Date__c = dateTime.now(),
                        Field__c = Schema.getGlobalDescribe().get('Contract').getDescribe().fields.getMap().get('Additional_AC_Unit_each__c').getDescribe().getLabel(),
                        Old_Value__c = String.valueOf(oldMap.get(con.Id).Additional_AC_Unit_each__c),
                        New_Value__c = String.valueOf(con.Additional_AC_Unit_each__c)
                    ));
                }
                if (con.Additional_Heat_Furnace_each__c != oldMap.get(con.Id).Additional_Heat_Furnace_each__c) {
                    ahList.add(new Additional_History__c(
                        Policy__c = con.Id,
                        Date__c = dateTime.now(),
                        Field__c = Schema.getGlobalDescribe().get('Contract').getDescribe().fields.getMap().get('Additional_Heat_Furnace_each__c').getDescribe().getLabel(),
                        Old_Value__c = String.valueOf(oldMap.get(con.Id).Additional_Heat_Furnace_each__c),
                        New_Value__c = String.valueOf(con.Additional_Heat_Furnace_each__c)
                    ));
                }
                if (con.Additional_Water_Heater_each__c != oldMap.get(con.Id).Additional_Water_Heater_each__c) {
                    ahList.add(new Additional_History__c(
                        Policy__c = con.Id,
                        Date__c = dateTime.now(),
                        Field__c = Schema.getGlobalDescribe().get('Contract').getDescribe().fields.getMap().get('Additional_Water_Heater_each__c').getDescribe().getLabel(),
                        Old_Value__c = String.valueOf(oldMap.get(con.Id).Additional_Water_Heater_each__c),
                        New_Value__c = String.valueOf(con.Additional_Water_Heater_each__c)
                    ));
                }
                if (con.of_Ovens__c != oldMap.get(con.Id).of_Ovens__c) {
                    ahList.add(new Additional_History__c(
                        Policy__c = con.Id,
                        Date__c = dateTime.now(),
                        Field__c = Schema.getGlobalDescribe().get('Contract').getDescribe().fields.getMap().get('of_Ovens__c').getDescribe().getLabel(),
                        Old_Value__c = String.valueOf(oldMap.get(con.Id).of_Ovens__c),
                        New_Value__c = String.valueOf(con.of_Ovens__c)
                    ));
                }
                if (con.of_Refrigerators__c != oldMap.get(con.Id).of_Refrigerators__c) {
                    ahList.add(new Additional_History__c(
                        Policy__c = con.Id,
                        Date__c = dateTime.now(),
                        Field__c = Schema.getGlobalDescribe().get('Contract').getDescribe().fields.getMap().get('of_Refrigerators__c').getDescribe().getLabel(),
                        Old_Value__c = String.valueOf(oldMap.get(con.Id).of_Refrigerators__c),
                        New_Value__c = String.valueOf(con.of_Refrigerators__c)
                    ));
                }
                if (con.of_Stand_Alone_Freezers__c != oldMap.get(con.Id).of_Stand_Alone_Freezers__c) {
                    ahList.add(new Additional_History__c(
                        Policy__c = con.Id,
                        Date__c = dateTime.now(),
                        Field__c = Schema.getGlobalDescribe().get('Contract').getDescribe().fields.getMap().get('of_Stand_Alone_Freezers__c').getDescribe().getLabel(),
                        Old_Value__c = String.valueOf(oldMap.get(con.Id).of_Stand_Alone_Freezers__c),
                        New_Value__c = String.valueOf(con.of_Stand_Alone_Freezers__c)
                    ));
                }
                if (con.Package__c != oldMap.get(con.Id).Package__c) {
                    ahList.add(new Additional_History__c(
                        Policy__c = con.Id,
                        Date__c = dateTime.now(),
                        Field__c = Schema.getGlobalDescribe().get('Contract').getDescribe().fields.getMap().get('Package__c').getDescribe().getLabel(),
                        Old_Value__c = String.valueOf(oldMap.get(con.Id).Package__c),
                        New_Value__c = String.valueOf(con.Package__c)
                    ));
                }
            }
            if (!ahList.isEmpty()) {
            	insert ahList;
            }
        }
    }
    
    private void setTotalPriceManual(List<sObject> newPolicies , Map<Id,sObject> oldPolicyMap){
        system.debug('setTotalPriceManual');
        List<Contract> newPolicyList = (List<Contract>) newPolicies;

        if(oldPolicyMap != NULL && currentUserProfile.size() > 0){
            //Assign oldMap
            Map<Id,Contract> oldMap = (Map<Id,Contract>) oldPolicyMap;
            List<Contract> contractPricingUpdate = new List<Contract>();
            
            for(Contract con : newPolicyList){
                system.debug('total price calculated: ' + con.Subtotal_Calculated__c);
                system.debug('total price calculated-old: ' + oldMap.get(con.Id).Subtotal_Calculated__c );
                if (!currentUserProfile[0].Name.contains('Administrator')) {
                    if (con.Subtotal_Calculated__c != oldMap.get(con.Id).Subtotal_Calculated__c && ((con.Manager_Total_Discounted_Price_Override__c == oldMap.get(con.Id).Manager_Total_Discounted_Price_Override__c) || (con.Discount_Percentage__c != oldMap.get(con.Id).Discount_Percentage__c))) {
                        contractPricingUpdate.add(new Contract(
                            Id = con.Id,
                            Manager_Total_Discounted_Price_Override__c = con.Subtotal_Calculated__c
                        ));
                    }
                }
            }
            
            if (!contractPricingUpdate.isEmpty()) {
                update contractPricingUpdate;
            }
        }
    }
    
    private void setGateway(List<sObject> newPolicies , Map<Id,sObject> oldPolicyMap){
        system.debug('setGateway');
        List<Contract> newPolicyList = (List<Contract>) newPolicies;
        Set<String> websiteSettingsToQuery = new Set<String>();
        
        Set<Id> renewalIds = new Set<Id>();
        Map<Id, String> renewalMap = new Map<Id, String>();
        Map<String, Gateway_Assignment__c> gaMap = new Map<String, Gateway_Assignment__c>();
        
        for (Gateway_Assignment__c gaItem : [SELECT Id, State__c, State_Code__c, Primary_Gateway__c, Secondary_Gateway__c, Tertiary_Gateway__c, Alternate_Between_Primary_Every__c, Current_Count__c, X1_Year_Monthly_Policy_Gateway__c, Last_Gateway_Assigned__c, Last_Alternate_Gateway__c FROM Gateway_Assignment__c]) {
            gaMap.put(gaItem.State_Code__c, gaItem);
        }
        system.debug('gaMap: ' + gaMap);
        
        Map<Id,Contract> oldMap = new Map<Id,Contract>();

        if(oldPolicyMap != NULL){
            oldMap = (Map<Id,Contract>) oldPolicyMap;
            
            for(Contract pol : newPolicyList){
                if (pol.Current_Active_Renewal__c != NULL) {
                    renewalIds.add(pol.Current_Active_Renewal__c);
                }
            }
                
        }
        
        if (!renewalIds.isEmpty()) {
            for (Policy_Renewal__c pr : [SELECT Id, Contract_Term__c FROM Policy_Renewal__c WHERE Id IN :renewalIds]) {
                renewalMap.put(pr.Id, pr.Contract_Term__c);
            }
        }
        
        Boolean updateMap = FALSE;

        for(Contract pol : newPolicyList){
            Gateway_Assignment__c ga = new Gateway_Assignment__c();
            Set<Id> gwIds = new Set<Id>();

            if (pol.BillingStateCode == NULL || !gaMap.containsKey(pol.BillingStateCode)) { //defaul gateway assignment
                ga = gaMap.get('Default');
            } else {
                ga = gaMap.get(pol.BillingStateCode); //state gateway assignment
            }
            
            if (ga.Primary_Gateway__c != NULL) {
                gwIds.add(ga.Primary_Gateway__c);
            }
            if (ga.Secondary_Gateway__c != NULL) {
                gwIds.add(ga.Secondary_Gateway__c);
            }
            if (ga.Tertiary_Gateway__c != NULL) {
                gwIds.add(ga.Tertiary_Gateway__c);
            }
            if (ga.X1_Year_Monthly_Policy_Gateway__c != NULL) {
                gwIds.add(ga.X1_Year_Monthly_Policy_Gateway__c);
            }
            
            String newConTerm = pol.Contract_Term__c;
            if (pol.Current_Active_Renewal__c != NULL) {
                newConTerm = renewalMap.get(pol.Current_Active_Renewal__c);
            }
            String oldConTerm = oldMap.get(pol.Id).Current_Contract_Term__c;
                        
            if (oldMap == NULL || pol.Gateway__c == NULL || pol.BillingStateCode != oldMap.get(pol.Id).BillingStateCode || newConTerm != oldConTerm) {
                // if there is a 1 year/monthly default that applies, assign that gateway
                if (ga.X1_Year_Monthly_Policy_Gateway__c != NULL && (newConTerm == '1 Year' || newConTerm == 'Monthly') && newConTerm != oldConTerm) {
                    pol.Gateway__c = ga.X1_Year_Monthly_Policy_Gateway__c;
                    gaMap.get(ga.State_Code__c).Last_Gateway_Assigned__c = pol.Gateway__c;
                    updateMap = TRUE;
                    
                //not currently assigned to the assigned gateways then reassign
                } else if (!gwIds.contains(pol.Gateway__c)) {
                    // if there is no last gateway assigned OR there is no secondary or tertiary gateway, or alternate is 0, assign the primary
                    if ((ga.Secondary_Gateway__c == NULL && ga.Tertiary_Gateway__c == NULL) || ga.Last_Gateway_Assigned__c == NULL) {
                        pol.Gateway__c = ga.Primary_Gateway__c;
                        
                        // there is a secondary or tertiary gateway so go through the counter to determing which one to assign
                    } else if (ga.Secondary_Gateway__c != NULL || ga.Tertiary_Gateway__c != NULL) {
                        
                        if (ga.Secondary_Gateway__c != NULL && ga.Tertiary_Gateway__c == NULL) { //only secondary populated
                            //if counter matches assign the secondary gateway or 1 and last wasn't secondary, else assign primary
                            if ((ga.Alternate_Between_Primary_Every__c != 0 && ga.Current_Count__c == ga.Alternate_Between_Primary_Every__c) || 
                                (ga.Alternate_Between_Primary_Every__c == 0 && ga.Last_Gateway_Assigned__c != ga.Secondary_Gateway__c)) {
                                    pol.Gateway__c = ga.Secondary_Gateway__c;
                                    gaMap.get(ga.State_Code__c).Last_Alternate_Gateway__c = ga.Secondary_Gateway__c;
                                } else {
                                    pol.Gateway__c = ga.Primary_Gateway__c;
                                }
                            
                        } else if (ga.Secondary_Gateway__c == NULL && ga.Tertiary_Gateway__c != NULL) { //only tertiary populated
                            //if counter matches, assign the tertiary gateway or 1 and last wasn't tertiary, else assign primary
                            if ((ga.Alternate_Between_Primary_Every__c != 0 && ga.Current_Count__c == ga.Alternate_Between_Primary_Every__c) || 
                                (ga.Alternate_Between_Primary_Every__c == 0 && ga.Last_Gateway_Assigned__c != ga.Tertiary_Gateway__c)) {
                                    pol.Gateway__c = ga.Tertiary_Gateway__c;
                                    gaMap.get(ga.State_Code__c).Last_Alternate_Gateway__c = ga.Secondary_Gateway__c;
                                } else {
                                    pol.Gateway__c = ga.Primary_Gateway__c;
                                }
                            
                        } else { //both populated
                            //if counter matches and last alternate assignment wasn't secondary or 1 and last was primary, assign the secondary gateway
                            if ((ga.Alternate_Between_Primary_Every__c != 0 && ga.Current_Count__c == ga.Alternate_Between_Primary_Every__c && ga.Last_Alternate_Gateway__c != ga.Secondary_Gateway__c) ||
                                (ga.Alternate_Between_Primary_Every__c == 0 && ga.Last_Gateway_Assigned__c == ga.Primary_Gateway__c)) {
                                    pol.Gateway__c = ga.Secondary_Gateway__c;
                                    gaMap.get(ga.State_Code__c).Last_Alternate_Gateway__c = ga.Secondary_Gateway__c;
                                    //if counter matches and last alternate assignment wasn't tertiary or 1 and last was secondary, assign the tertiary gateway
                                } else if ((ga.Alternate_Between_Primary_Every__c != 0 && ga.Current_Count__c == ga.Alternate_Between_Primary_Every__c && ga.Last_Alternate_Gateway__c != ga.Primary_Gateway__c) || 
                                           (ga.Alternate_Between_Primary_Every__c == 0 && ga.Last_Gateway_Assigned__c == ga.Secondary_Gateway__c)) {
                                               pol.Gateway__c = ga.Tertiary_Gateway__c;
                                               gaMap.get(ga.State_Code__c).Last_Alternate_Gateway__c = ga.Tertiary_Gateway__c;
                                           } else {
                                               pol.Gateway__c = ga.Primary_Gateway__c;
                                           } 
                        }
                        
                        //update counter
                        if (ga.Current_Count__c >= ga.Alternate_Between_Primary_Every__c) {
                            gaMap.get(ga.State_Code__c).Current_Count__c = 0;
                        } else {
                            gaMap.get(ga.State_Code__c).Current_Count__c++;
                        }
                        gaMap.get(ga.State_Code__c).Last_Gateway_Assigned__c = pol.Gateway__c;
                        updateMap = TRUE;
                    }
                }
            }
        }
        
        if (updateMap) {
            system.debug('gateway changed');
            update gaMap.values();
        }
    }
    
    private void sendPardotEmail(List<sObject> newPolicies , Map<Id,sObject> oldPolicyMap){
        system.debug('sendPardotEmailTrigger');
        List<Contract> newPolList = (List<Contract>) newPolicies;
        
        if(oldPolicyMap != NULL ){
            //Assign oldMap
            Map<Id,Contract> oldMap = (Map<Id,Contract>) oldPolicyMap;
            
            for(Contract con : newPolList){
                if (con.Pardot_URL__c != NULL && con.Velocify_Owner__c != NULL && con.Pardot_Email__c != NULL &&
                    con.Pardot_Email__c != oldMap.get(con.Id).Pardot_Email__c && RecursiveTriggerHandler.isFirstTimePardotEmail) { //velocify email
                        RecursiveTriggerHandler.isFirstTimePardotEmail = FALSE;
                        Integer startIdx = con.Pardot_URL__c.indexOf( 'id=' ) + 3;
                        Integer endIdx = con.Pardot_URL__c.length();
                        String prospectId = con.Pardot_URL__c.substring(startIdx, endIdx );
                        System.debug(prospectId);
                        String emailId = Util.getPardotEmailId(con.Pardot_Email__c);
                        if (emailId != NULL) {
                            CalloutUtil.sendPardotEmail(prospectId,emailId,TRUE,con.Velocify_Owner_Name__c,con.Velocify_Owner_Email__c);
                        }
                    }
            }
        }
    }
}