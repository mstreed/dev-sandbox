public  class TriggerHandlerEmailMessage extends TriggerHandler {
	
	// Constructor 
	public TriggerHandlerEmailMessage() {}
	
	/* Trigger context overrides */
	public override void beforeInsert() {
        newInboundEmailMessage(Trigger.new);
    }
    public override void beforeUpdate() {}
	public override void beforeDelete() {}
	public override void afterInsert()  {
        newOutboundEmailMessage(Trigger.new);
    }
  	public override void afterUpdate()  {
        updateEmailMessage(Trigger.new, Trigger.oldMap);
    }
  	public override void afterDelete()  {}
  	public override void afterUndelete(){}
    
    private void newInboundEmailMessage(List<sObject> newEmails){
        system.debug('newInboundEmailMessage');
        //need to create a task for the inbound email
        List<EmailMessage> newEmailList = (List<EmailMessage>) newEmails;

        Map<Id,Task> tskMap = new Map<Id,Task>();
        
        for(EmailMessage eml : newEmailList){
            if (eml.Incoming) {
                Task tsk = new Task();
                tsk.WhatId = eml.ParentId;
                tsk.ActivityDate = Date.valueOf(eml.MessageDate.format('yyyy-MM-dd',  UserInfo.getTimeZone().toString()));
                tsk.CreatedById = eml.CreatedById;
                tsk.CreatedDate = eml.CreatedDate;
                if (eml.TextBody != NULL) {
                    tsk.Description = eml.TextBody.left(32000);
                }
                tsk.IsReminderSet = false;
                tsk.From__c = eml.FromAddress;
                tsk.Read__c = eml.Read__c;
                tsk.Inbound__c = eml.Incoming;
                tsk.Attachments__c = eml.HasAttachment;
                tsk.Status = 'Completed';
                if (eml.Subject != NULL) {
                    tsk.Subject = 'Email: ' + eml.Subject.left(243);
                    if (eml.Subject.length()>255) {
                        eml.Subject = eml.Subject.left(243);
                    }
                }
                //tsk.Email_Message_ID__c = eml.Id;
                tskMap.put(eml.Id,tsk);
            }
        }
        
        if (!tskMap.isEmpty()) {
            INSERT tskMap.values();
            
            for(EmailMessage eml : newEmailList){
                if (eml.Incoming) {
                    eml.ActivityId = tskMap.get(eml.Id).Id;
                }
            }
        }

    }
    
    private void newOutboundEmailMessage(List<sObject> newEmails){
        system.debug('newOutboundEmailMessage');
        //need to sync some data on outbound email
        List<EmailMessage> newEmailList = (List<EmailMessage>) newEmails;
        
        Map<Id,EmailMessage> emlMap = new Map<Id,EmailMessage>();
        
        for(EmailMessage eml : newEmailList){
            if (!eml.Incoming) {
                emlMap.put(eml.ActivityId, eml);
            }
        }
        
        if (!emlMap.isEmpty()) {
            List<Task> taskList = [SELECT Id, From__c, Read__c, Inbound__c, Attachments__c FROM Task WHERE Id IN :emlMap.keySet()];
            for (Task tsk :taskList) {
                tsk.From__c = emlMap.get(tsk.Id).FromAddress;
                tsk.Read__c = emlMap.get(tsk.Id).Read__c;
                tsk.Attachments__c = emlMap.get(tsk.Id).HasAttachment;
                //tsk.Email_Message_ID__c = emlMap.get(tsk.Id).Id;
            }
            
            UPDATE taskList;
        }
    }
    
    private void updateEmailMessage(List<sObject> updateEmails , Map<Id,sObject> oldEmailMap){
        system.debug('updateEmailMessage'); //need to sync some data on update emails
        List<EmailMessage> updateEmailMessage = (List<EmailMessage>) updateEmails;
        
        Map<Id,EmailMessage> emlMap = new Map<Id,EmailMessage>();
        
        if(oldEmailMap != NULL ){
            Map<Id,EmailMessage> oldMap = (Map<Id,EmailMessage>) oldEmailMap;
            for(EmailMessage eml : updateEmailMessage){
                if (eml.Read__c != oldMap.get(eml.Id).Read__c) {
                    emlMap.put(eml.ActivityId, eml);
                }
            }
            
            if (!emlMap.isEmpty()) {
                List<Task> taskList = [SELECT Id, From__c, Read__c, Inbound__c, Attachments__c FROM Task WHERE Id IN :emlMap.keySet()];
                for (Task tsk :taskList) {
                    tsk.Read__c = emlMap.get(tsk.Id).Read__c;
                }
                
                UPDATE taskList;
            }
        }
    }
}