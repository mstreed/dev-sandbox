public class TriggerHandlerGatewayAssignment extends TriggerHandler {
    
    @TestVisible private Boolean testException = FALSE;                         // used to induce exception coverage by test methods
    
    public TriggerHandlerGatewayAssignment() {}

    /* context overrides */
    
    public override void beforeInsert() {
        setUniqueId(Trigger.new);
    }
    
    public override void beforeUpdate() {
        setUniqueId(Trigger.new);
    }
    
    public override void beforeDelete() {
        //stopDelete(Trigger.old);
    }
    public override void afterInsert() {}
    
    public override void afterUpdate() {}
    
    public override void afterDelete() {}
    
    public override void afterUndelete() {} 
    
    /* private methods */
    
    private void setUniqueId(List<sObject> newGAs){
        system.debug('setUniqueId');
        List<Gateway_Assignment__c> newGAList = (List<Gateway_Assignment__c>) newGAs;
        
        for(Gateway_Assignment__c ga : newGAList){
            ga.Unique_ID__c = ga.State__c;
        }
        
    }
}