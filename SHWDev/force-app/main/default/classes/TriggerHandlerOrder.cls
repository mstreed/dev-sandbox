public class TriggerHandlerOrder extends TriggerHandler {
    
    @TestVisible private Boolean testException = FALSE;                         // used to induce exception coverage by test methods
    
    public TriggerHandlerOrder() {}
    
    /* context overrides */
    
    public override void beforeInsert() {}
    
    public override void beforeUpdate() {}
    
    public override void beforeDelete() {}
    
    public override void afterInsert() {
        createPaymentMethod(Trigger.new);
        policyRollup(Trigger.new);
    }
    
    public override void afterUpdate() {
        policyRollup(Trigger.new);
    }
    
    public override void afterDelete() {
        policyRollup(Trigger.old);
    }
    
    public override void afterUndelete() {
        policyRollup(Trigger.new);
    } 
    
    /* private methods */
    
    private void createPaymentMethod(List<sObject> newOrders){
        system.debug('createPaymentMethod');
        List<ChargentOrders__ChargentOrder__c> newOrderList = (List<ChargentOrders__ChargentOrder__c>) newOrders;
        List<Payment_Method__c> newPaymentMethods = new List<Payment_Method__c>();
        List<ChargentOrders__ChargentOrder__c> ordersToUpdate = new List<ChargentOrders__ChargentOrder__c>();
        List<Lead> leadsToUpdate = new List<Lead>();
        
        for(ChargentOrders__ChargentOrder__c ord : newOrderList){
            //only run on web orders
            if (ord.Create_from_Web__c) {
                Payment_Method__c newPayMethod = new Payment_Method__c();

                //need to requery to get parent record - if exists
                ChargentOrders__ChargentOrder__c ordToUpdate = [SELECT Id, Lead__c, Lead__r.Parent_Lead__c, Policy__c, Lead__r.Parent_Policy__c, ChargentOrders__Payment_Frequency__c FROM ChargentOrders__ChargentOrder__c WHERE Id = :ord.Id LIMIT 1];
                
                system.debug('payFrequency: ' + ordToUpdate.ChargentOrders__Payment_Frequency__c);
                if (ordToUpdate.Lead__r.Parent_Lead__c != null) {
                    system.debug('parent lead');

                    Lead updLead = updateParentLeadGAQ(ordToUpdate.Lead__c, ordToUpdate.Lead__r.Parent_Lead__c);
                    updLead.Payment_Option__c = getPaymentMethod(ordToUpdate.ChargentOrders__Payment_Frequency__c);
                    system.debug('updLead.Payment_Option__c1: ' + updLead.Payment_Option__c);
                    
                    leadsToUpdate.add(updLead);
                    ordToUpdate.Lead__c = ordToUpdate.Lead__r.Parent_Lead__c;
                    newPayMethod.Lead_Name__c = ordToUpdate.Lead__r.Parent_Lead__c;
                } else if (ordToUpdate.Lead__r.Parent_Policy__c != null) {
                    system.debug('parent policy');

                    ordToUpdate.Policy__c = ordToUpdate.Lead__r.Parent_Policy__c;
                    newPayMethod.Policy_Name__c = ordToUpdate.Lead__r.Parent_Policy__c;
                } else if (ordToUpdate.Lead__c != null) {
                    system.debug('lead');

                    newPayMethod.Lead_Name__c = ordToUpdate.Lead__c;
                    
                    Lead updLead = new Lead(
                        Id = ordToUpdate.Lead__c,
                        Status = 'GAQ Order'
                    );
                    updLead.Payment_Option__c = getPaymentMethod(ordToUpdate.ChargentOrders__Payment_Frequency__c);
                    system.debug('updLead.Payment_Option__c2: ' + updLead.Payment_Option__c);

                    leadsToUpdate.add(updLead);
                } else if (ordToUpdate.Policy__c != null) {
                    system.debug('policy');

                    newPayMethod.Policy_Name__c = ordToUpdate.Policy__c;
                }
                
                
                
                newPayMethod.Card_Number_Encrypted__c = ord.ChargentOrders__Card_Number__c;
                newPayMethod.Month__c = Decimal.valueOf(ord.ChargentOrders__Card_Expiration_Month__c);
                newPayMethod.Year__c = Decimal.valueOf(ord.ChargentOrders__Card_Expiration_Year__c);
                newPayMethod.Payment_Method__c = 'Credit Card';
                newPayMethod.Set_as_default__c = true;
                
                
                newPaymentMethods.add(newPayMethod);
                
                ordersToUpdate.add(ordToUpdate);
            }
        }
        
        if (!newPaymentMethods.isEmpty()) {
            insert newPaymentMethods;
        }
        if (!ordersToUpdate.isEmpty()) {
            update ordersToUpdate;
        }
        if (!leadsToUpdate.isEmpty()) {
            update leadsToUpdate;
        }
    }
    
    private lead updateParentLeadGAQ(Id childId, Id parentId) {
        system.debug('updateParentLeadGAQ');
        Lead childLead = [SELECT Id, FirstName, LastName, Package__c,
                           Pool__c, Central_Vacuum__c, Second_Refrigerator__c, Sump_Pump__c, Well_Pump__c, Roof_Leak__c, Stand_Alone_Freezer__c, 
                           Lawn_Sprinkler_System__c, Septic_System__c, Ice_Maker_In_Refrigerator__c, Lighting_Fixtures_Plumbing_Fixtures__c, Spa__c, Additional_AC_Unit_each__c, 
                           Additional_Heat_Furnace_each__c, Additional_Water_Heater_each__c, 
                           Discount__c, Discount_Code__c, Discount_Dollars__c, Discount_Dropdown__c, Discount_Percentage__c,
                           Street, City, PostalCode, StateCode, Payment_Option__c, Total_Price_Manual__c,
                           Duplicate_Lead_Quote_Numbers__c
                           FROM Lead 
                           WHERE Id = :childId LIMIT 1];
                           
        /*Lead parentLead = [SELECT Id, FirstName, LastName, Package__c,
                           Pool__c, Central_Vacuum__c, Second_Refrigerator__c, Sump_Pump__c, Well_Pump__c, Roof_Leak__c, Stand_Alone_Freezer__c, 
                           Lawn_Sprinkler_System__c, Septic_System__c, Ice_Maker_In_Refrigerator__c, Lighting_Fixtures_Plumbing_Fixtures__c, Spa__c, Additional_AC_Unit_each__c, 
                           Additional_Heat_Furnace_each__c, Additional_Water_Heater_each__c, 
                           Discount__c, Discount_Code__c, Discount_Dollars__c, Discount_Dropdown__c, Discount_Percentage__c,
                           Street, City, PostalCode, StateCode, Payment_Option__c, Total_Price_Manual__c,
                           Duplicate_Lead_Quote_Numbers__c
                           FROM Lead 
                           WHERE Id = :parentId LIMIT 1];*/
        
        Lead parentLead = new Lead();
        parentLead.Id = parentId;
        parentLead.Status = 'GAQ Order';
        parentLead.Package__c = childLead.Package__c;
        parentLead.Pool__c = childLead.Pool__c;
        parentLead.Central_Vacuum__c = childLead.Central_Vacuum__c;
        parentLead.Second_Refrigerator__c = childLead.Second_Refrigerator__c;
        parentLead.Sump_Pump__c = childLead.Sump_Pump__c;
        parentLead.Well_Pump__c = childLead.Well_Pump__c;
        parentLead.Roof_Leak__c = childLead.Roof_Leak__c;
        parentLead.Stand_Alone_Freezer__c = childLead.Stand_Alone_Freezer__c;
        parentLead.Lawn_Sprinkler_System__c = childLead.Lawn_Sprinkler_System__c;
        parentLead.Septic_System__c = childLead.Septic_System__c;
        parentLead.Ice_Maker_In_Refrigerator__c = childLead.Ice_Maker_In_Refrigerator__c;
        parentLead.Lighting_Fixtures_Plumbing_Fixtures__c = childLead.Lighting_Fixtures_Plumbing_Fixtures__c;
        parentLead.Spa__c = childLead.Spa__c;
        parentLead.Additional_AC_Unit_each__c = childLead.Additional_AC_Unit_each__c;
        parentLead.Additional_Heat_Furnace_each__c = childLead.Additional_Heat_Furnace_each__c;
        parentLead.Additional_Water_Heater_each__c = childLead.Additional_Water_Heater_each__c;
        parentLead.Discount__c = childLead.Discount__c;
        parentLead.Discount_Code__c = childLead.Discount_Code__c;
        parentLead.Discount_Dollars__c = childLead.Discount_Dollars__c;
        parentLead.Discount_Dropdown__c = childLead.Discount_Dropdown__c;
        parentLead.Discount_Percentage__c = childLead.Discount_Percentage__c;
        parentLead.Street = childLead.Street;
        parentLead.City = childLead.City;
        parentLead.PostalCode = childLead.PostalCode;
        parentLead.StateCode = childLead.StateCode;
        parentLead.Payment_Option__c = childLead.Payment_Option__c;
        parentLead.Total_Price_Manual__c = childLead.Total_Price_Manual__c;
        
        return parentLead;
    }
    
    private String getPaymentMethod(string ordPayMethod) {
        if (ordPayMethod == 'Monthly') {
            return 'Monthly';
        } else {
            return 'Single Payment';
        }
    }
    
    
    private void policyRollup(List<sObject> orders){
        system.debug('policyRollup');
        List<ChargentOrders__ChargentOrder__c> orderList = (List<ChargentOrders__ChargentOrder__c>) orders;
        Set<Id> policyIds = new Set<Id>();
        List<Contract> policiesToUpdate = new List<Contract>();
        
        for(ChargentOrders__ChargentOrder__c ord : orderList){
            //only run orders with policies
            if (ord.Policy__c != NULL) {
                policyIds.add(ord.Policy__c);
            }
        }
        
        if (!policyIds.isEmpty()) {
            for (Contract con : [SELECT Id, of_Active_Subscriptions__c, of_Complete_Subscriptions__c, of_Error_Subscriptions__c, of_Stopped_Subscriptions__c, Total_of_Non_Subscriptions__c, of_Subscriptions_Without_Status__c,
                                 (Select Id, ChargentOrders__Payment_Status__c, Created_via_Transaction__c, Transaction_Total_Collected__c FROM Chargent_Orders__r) 
                                 FROM Contract WHERE Id = :policyIds]) {
                                     con.of_Active_Subscriptions__c = 0;
                                     con.of_Complete_Subscriptions__c = 0;
                                     con.of_Error_Subscriptions__c = 0;
                                     con.of_Stopped_Subscriptions__c = 0;
                                     con.Total_of_Non_Subscriptions__c = 0;
                                     con.of_Subscriptions_Without_Status__c = 0;
                                     con.Total_Collected__c = 0;
                                     
                                     for (ChargentOrders__ChargentOrder__c co : con.Chargent_Orders__r) {
                                     //system.debug('co.Created_via_Transaction__c: ' + co.Created_via_Transaction__c);

                                         if (co.Created_via_Transaction__c == true) {
                                             con.Total_of_Non_Subscriptions__c += 1;
                                         } else if (co.ChargentOrders__Payment_Status__c == 'Recurring') {
                                             con.of_Active_Subscriptions__c += 1;
                                         } else if (co.ChargentOrders__Payment_Status__c == 'Complete') {
                                             con.of_Complete_Subscriptions__c += 1;
                                         } else if (co.ChargentOrders__Payment_Status__c == 'Error') {
                                             con.of_Error_Subscriptions__c += 1;
                                         } else if (co.ChargentOrders__Payment_Status__c == 'Stopped') {
                                             con.of_Stopped_Subscriptions__c += 1;
                                         } else {
                                             con.of_Subscriptions_Without_Status__c += 1;
                                         }
                                         con.Total_Collected__c+= co.Transaction_Total_Collected__c;
                                     }
                                     policiesToUpdate.add(con);
            }

            update policiesToUpdate;
        }
    }
}