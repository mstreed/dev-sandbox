/*
* 	PURPOSE:       This class will handle trigger behaviour and will perform task accordingly
*	INTERFACES:     
*   CHANGE LOG:    12-6-2019  	Intial release 
*/
public  class TriggerHandlerPaymentMethod extends TriggerHandler {
	
	// Constructor 
	public TriggerHandlerPaymentMethod() {}
	
	/* Trigger context overrides */
	public override void beforeInsert() {}
	public override void beforeUpdate() {
        clearErrorDeclined(Trigger.new, Trigger.oldMap);
    }
	public override void beforeDelete() {}
	public override void afterInsert()  {
		updateDefaultPaymentMethod(Trigger.new);
	}
  	public override void afterUpdate()  {
		updateDefaultPaymentMethod(Trigger.new);
    }
  	public override void afterDelete()  {}
  	public override void afterUndelete(){}

    private void updateDefaultPaymentMethod(List<sObject> newPMs){
        system.debug('updateDefaultPaymentMethod');

        List<Payment_Method__c> newPMList = (List<Payment_Method__c>) newPMs;
        Set<Payment_Method__c> defaultPMs = new Set<Payment_Method__c>();
        Map<Id,Payment_Method__c> ldPolPMMap = new Map<Id,Payment_Method__c>();
        Set<Id> ldPolIds = new Set<Id>();

        List<Payment_Method__c> updatedPMList = new List<Payment_Method__c>();
        List<ChargentOrders__ChargentOrder__c> updatedCOList = new List<ChargentOrders__ChargentOrder__c>();
        
        for(Payment_Method__c pm : newPMList){
            if(pm.Set_as_default__c) {
                defaultPMs.add(pm);
                
                if (pm.Lead_Name__c != NULL && pm.Policy_Name__c == NULL) {
                    ldPolIds.add(pm.Lead_Name__c);
                    ldPolPMMap.put(pm.Lead_Name__c, pm);
                } else if (pm.Policy_Name__c != NULL) {
                    ldPolIds.add(pm.Policy_Name__c);
                    ldPolPMMap.put(pm.Policy_Name__c, pm);
                } 
            }
        }
        
        if (defaultPMs.size()>0) {
            for( Payment_Method__c p: [SELECT
                                       Id,
                                       Set_as_default__c
                                       FROM Payment_Method__c
                                       WHERE Id != :defaultPMs AND 
                                       (Policy_Name__c = :ldPolIds OR Lead_Name__c = :ldPolIds)
                                       ORDER BY CreatedDate DESC
                                       LIMIT 50000
                                      ] ) {
                                          p.Set_as_default__c= false;
                                          updatedPMList.add(p);
                                      }  
            
            if(!updatedPMList.isEmpty()) {
                Database.update(updatedPMList);
            }
            
            for(ChargentOrders__ChargentOrder__c co: [SELECT 
                                                      Id, 
                                                      ChargentOrders__Payment_Method__c,
                                                      ChargentOrders__Charge_Amount__c,
                                                      ChargentOrders__Gateway__c,
                                                      Payment_Terms_New__c,
                                                      Lead__c,
                                                      Policy__c
                                                      FROM ChargentOrders__ChargentOrder__c
                                                      WHERE (Policy__c = :ldPolIds OR Lead__c = :ldPolIds) AND Created_Via_Transaction__c = false
                                                     ]){
                                                         system.debug('co: ' + co.Id);
                                                         if (co.Lead__c != NULL && co.Policy__c == NULL) {
                                                             co.ChargentOrders__Payment_Method__c  = ldPolPMMap.get(co.Lead__c).Payment_Method__c;
                                                             co.ChargentOrders__Card_Number__c = ldPolPMMap.get(co.Lead__c).Card_Number_Encrypted__c;
                                                             co.ChargentOrders__Card_Expiration_Month__c =  String.valueOf(Integer.valueOf(ldPolPMMap.get(co.Lead__c).Month__c));
                                                             co.ChargentOrders__Card_Expiration_Year__c = String.valueOf(Integer.valueOf(ldPolPMMap.get(co.Lead__c).Year__c));
                                                         } else if (co.Policy__c != NULL) {
                                                             co.ChargentOrders__Payment_Method__c  = ldPolPMMap.get(co.Policy__c).Payment_Method__c;
                                                             co.ChargentOrders__Card_Number__c = ldPolPMMap.get(co.Policy__c).Card_Number_Encrypted__c;
                                                             co.ChargentOrders__Card_Expiration_Month__c =  String.valueOf(Integer.valueOf(ldPolPMMap.get(co.Policy__c).Month__c));
                                                             co.ChargentOrders__Card_Expiration_Year__c = String.valueOf(Integer.valueOf(ldPolPMMap.get(co.Policy__c).Year__c));
                                                         }
                                                        updatedCOList.add(co);
                                                     }
            if(!updatedCOList.isEmpty()) {
                Database.update(updatedCOList);
            }
        }
    }
    
    private void clearErrorDeclined(List<sObject> newPMs, Map<Id,sObject> oldPMMap){
        system.debug('clearErrorDeclinedFlag');
        
        List<Payment_Method__c> newPMList = (List<Payment_Method__c>) newPMs;
        Map<Id,Payment_Method__c> oldMap = (Map<Id,Payment_Method__c>) oldPMMap; 
        
        for(Payment_Method__c pm : newPMList){
            if(pm.Error_Declined__c != NULL &&
               (pm.Card_Number_Encrypted__c != oldMap.get(pm.Id).Card_Number_Encrypted__c ||
                pm.Month__c != oldMap.get(pm.Id).Month__c ||
                pm.Year__c != oldMap.get(pm.Id).Year__c)) {
                pm.Error_Declined__c = NULL;
            }
        }
    }
}