public class TriggerHandlerPricing extends TriggerHandler {
    
    @TestVisible private Boolean testException = FALSE;                         // used to induce exception coverage by test methods
    
    public TriggerHandlerPricing() {}
    //public static Global_Setting__c gs = [Select Id, Gateway_Counter_CA__c, Gateway_Counter_CA_Max__c, System_Administrator_User_ID__c, Do_Not_Call_Task_User_ID__c, Affiliate_Lead_Access_Start_Date__c FROM Global_Setting__c LIMIT 1];

    /* context overrides */
    
    public override void beforeInsert() {
        setUniqueId(Trigger.new);
    }
    
    public override void beforeUpdate() {
        setUniqueId(Trigger.new);
    }
    
    public override void beforeDelete() {
        stopDelete(Trigger.old);
    }
    public override void afterInsert() {}
    
    public override void afterUpdate() {}
    
    public override void afterDelete() {}
    
    public override void afterUndelete() {} 
    
    /* private methods */
    
    private void setUniqueId(List<sObject> newPricings){
        system.debug('setUniqueId');
        List<Pricing__c> newPricingList = (List<Pricing__c>) newPricings;
        
        for(Pricing__c pr : newPricingList){
            pr.Unique_ID__c = pr.Website_Setting__c + String.valueOf(pr.Effective_Date_Time__c);
        }
        
    }
    
    private void stopDelete(List<sObject> newPricings){
        system.debug('stopDelete');
        List<Pricing__c> newPricingList = (List<Pricing__c>) newPricings;
        
        for(Pricing__c pr : newPricingList){
            if (pr.Effective_Date_Time__c <= Date.today() || pr.Active__c == TRUE) {
                pr.AddError('Cannot delete pricing record that is active or in the past.');
            }
        }
    }
}