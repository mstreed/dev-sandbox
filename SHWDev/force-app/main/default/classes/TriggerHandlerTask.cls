public  class TriggerHandlerTask extends TriggerHandler {
	
	// Constructor 
	public TriggerHandlerTask() {}
	
	/* Trigger context overrides */
	public override void beforeInsert() {
        newCallTasks(Trigger.new);
        newEmailTasks(Trigger.new);
    }
    public override void beforeUpdate() {}
	public override void beforeDelete() {}
	public override void afterInsert()  {
        
        
    }
  	public override void afterUpdate()  {}
  	public override void afterDelete()  {}
  	public override void afterUndelete(){}
    
    private void newCallTasks(List<sObject> newTasks){
        system.debug('newCallTasks');
        List<Task> newTaskList = (List<Task>) newTasks;
        Id five9TaskId = [SELECT Id,Name,DeveloperName FROM RecordType WHERE SobjectType ='Task' AND DeveloperName = 'Five9' LIMIT 1].Id;
        
        for(Task tsk : newTaskList){
            if (tsk.Subject != NULL) {
                if (tsk.Subject.Contains('Call')) {
                    tsk.TaskSubtype = 'Call';
                }
            }
            if (tsk.Five9__Five9SessionId__c != NULL) {
                tsk.recordTypeId = five9TaskId;
            }
        }
    }

    private void newEmailTasks(List<sObject> newTasks){
        system.debug('newEmailTasks');
        List<Task> newTaskList = (List<Task>) newTasks;
        
        Set<Id> leadIds = new Set<Id>();
        Set<Id> contactIds = new Set<Id>();
        Set<Id> shipIds = new Set<Id>();
        Map<Id,String> customerMap = new Map<Id,String>();
        Map<Id,Id> conPolMap = new Map<Id,Id>();
        
        for(Task tsk : newTaskList){
            if (tsk.Subject == 'Buy It Now Clicked' && String.valueOf(tsk.WhoId).startsWith('00Q')) {
                leadIds.add(tsk.WhoId);
            }
            
            if (tsk.Subject == 'Buy It Now Clicked' && String.valueOf(tsk.WhoId).startsWith('003')) {
                contactIds.add(tsk.WhoId);
            }
            
            if (tsk.WhatId != NULL) {
                system.debug('tsk.WhatId: ' + tsk.WhatId);
                system.debug('prefix: ' + Schema.getGlobalDescribe().get('Shipping_Alert__c').getDescribe().getKeyPrefix());
                
                if (String.valueOf(tsk.WhatId).startsWith(Schema.getGlobalDescribe().get('Shipping_Alert__c').getDescribe().getKeyPrefix())) {
                    shipIds.add(tsk.WhatId);
                }
            }
            
            system.debug('shipIds: ' + shipIds);
                        
            if (tsk.From__c == NULL) {
                if (tsk.Subject != NULL && tsk.Subject.left(18) == 'Pardot Misc Email:') {                    
                    if (tsk.Subject == 'Pardot Misc Email: Welcome to Select Home Warranty!') {
                        tsk.From__c = 'belinda@selecthomewarranty.com';
                    } else if (tsk.Subject.right(32) == 'Select Home Warranty Price Quote') {
                        tsk.From__c = 'noreply@selecthomewarranty.com';
                    }
                } else if ((tsk.Description != NULL && tsk.Description.left(10) == 'Buy it now') || 
                           (tsk.Description != NULL && tsk.Description.left(41) == 'Customer sent link to policy renewal page') ||
                           (tsk.Description != NULL && tsk.Description.left(39) == 'Customer re-sent link to policy renewal') ||
                           (tsk.Description != NULL && tsk.Description.left(28) == 'Customer sent discount email') ||
                           (tsk.Description != NULL && tsk.Description.left(44) == 'Customer sent thank you email for responding') ||
                           (tsk.Subject != NULL && tsk.Subject.left(38) == 'Email: [Action Required] - Credit Card') ||
                           tsk.Subject == 'Email: SHW Closing Date Confirmation' ||
                           tsk.Subject == 'Email: SHW Follow up' ||
                           tsk.Subject == 'Email: SHW Invoice Confirmation') {
                               tsk.From__c = 'sales@selecthomewarranty.com';
                           } else if (tsk.Subject != NULL && tsk.Subject.left(38) == 'Email: Welcome to Select Home Warranty') {
                               tsk.From__c = 'info@selecthomewarranty.com';
                           } else if (tsk.Subject != NULL && tsk.Subject.left(16) == 'Email: Thank You' ||
                                      (tsk.Description != NULL && tsk.Description.left(16) == 'SHW Leads Report')) {
                                          tsk.From__c = 'noreply@selecthomewarranty.com';
                                      } else if ((tsk.Description != NULL && tsk.Description.left(36) == 'Customer sent link to payment method') || 
                                                 (tsk.Description != NULL && tsk.Description.left(25) == 'There was a Payment Error') || 
                                                 (tsk.Subject != NULL && tsk.Subject.left(18) == 'Email: Your Policy')) {
                                                     tsk.From__c = 'subs@selecthomewarranty.com';
                                                 } else if ((tsk.WhatId != NULL && String.valueOf(tsk.WhatId).left(3) == '500') ||
                                                            (tsk.WhatId != NULL && String.valueOf(tsk.WhatId).left(3) == 'a0r') ||
                                                            (tsk.WhatId != NULL && String.valueOf(tsk.WhatId).left(3) == 'a01') ||
                                                            (tsk.WhatId != NULL && String.valueOf(tsk.WhatId).left(3) == 'a0y') ||
                                                            tsk.Description == 'Dispatch Follow Up Email' ||
                                                            tsk.Description == 'Initial Dispatch Email to Customer' ||
                                                            (tsk.Description != NULL && tsk.Description.left(37) == 'You have been assigned a job request!') ||
                                                            (tsk.Description != NULL && tsk.Description.left(38) == 'Contractor sent link to quick-dispatch') ||
                                                            tsk.Subject == 'Email: HVAC Price Guide' ||
                                                            tsk.Subject == 'Email: Plumbing Price Guide' ||
                                                            tsk.Subject == 'Email: Select Home Warranty - Executive Dispatch Network' ||
                                                            (tsk.Subject != NULL && tsk.Subject.left(5) == 'Email' && tsk.Subject.right(16) == 'Invoice Rejected') ||
                                                            (tsk.Subject != NULL && tsk.Subject.left(24) == 'Email: Diagnosis Request')) {
                                                                tsk.From__c = 'claims@selecthomewarranty.com';
                                                            }
            }
        }
        
        if (!leadIds.isEmpty() || !contactIds.isEmpty()) {
            //setup the data for updating the tasks
            if (!leadIds.isEmpty()) {
                for (Lead ld : [SELECT Id, Quote_Number_Text__c FROM Lead WHERE Id IN :leadIds]) {
                    customerMap.put(ld.Id, ld.Quote_Number_Text__c);
                }
            }
            
            if (!contactIds.isEmpty()) {
                for (Contact con : [Select Id, Account.Policy__c, Account.Policy__r.Contract_Number__c FROM Contact WHERE Id IN :contactIds AND Account.Policy__c != NULL]) {
                    customerMap.put(con.Id, con.Account.Policy__r.Contract_Number__c);
                    conPolMap.put(con.Id, con.Account.Policy__c);
                }
            }
            
            for(Task tsk : newTaskList){
                //update tasks subject
                if (tsk.Subject == 'Buy It Now Clicked') {
                    if (customerMap.get(tsk.WhoId) != NULL) {
                        tsk.Subject = customerMap.get(tsk.WhoId) + ' - Buy It Now Clicked';
                    }
                    if (String.valueOf(tsk.WhoId).startsWith('003')) {
                        if (conPolMap.get(tsk.WhoId) != NULL) {
                            tsk.WhatId = conPolMap.get(tsk.WhoId);
                        }
                    }
                    tsk.Department__c = 'Sales';
                }
            }
        }
        
        if (!shipIds.isEmpty()) {
            Map<Id,Shipping_Alert__c> shipToAuthMap = new Map<Id,Shipping_Alert__c>();
            for (Shipping_Alert__c sa : [SELECT Id, Authorization_Form__r.Claim__c, Authorization_Form__c FROM Shipping_Alert__c WHERE Id IN :shipIds]) {
                shipToAuthMap.put(sa.Id, sa);
            }
            
            system.debug('shipToAuthMap: ' + shipToAuthMap);
            
            for(Task tsk : newTaskList){
                if (tsk.WhatId != NULL && shipIds.contains(tsk.WhatId)) {
                    //tsk.WhatId = shipToAuthMap.get(tsk.WhatId).Authorization_Form__r.Claim__c;
                    tsk.Authorization_Form__c = shipToAuthMap.get(tsk.WhatId).Authorization_Form__c;
                }
                system.debug('tsk.Authorization_Form__c: ' + tsk.Authorization_Form__c);
            }
        }
    }
}