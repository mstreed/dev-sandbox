@isTest
private class TriggerHandlerTest {
    private static DataFactory df = new DataFactory();

    @isTest static void caseTriggerHandlerTests1() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        
        df.newCase.Customer_Reimbursement_Amount__c = 10 ; 
        //caseRec.Claims_Paid2__c = 100 ;
        //caseRec.Customer_Paid2__c = 1000 ; 
        update df.newCase ;
        
        df.newCase.Status = 'App Email Sent';
        df.newCase.App_Email_Triggered__c = TRUE;
        update df.newCase;
        
        Case billingCase = new Case(
            Policy__c = df.newPolicy.id,
            AccountId = df.acc.Id,
            ContactId = df.conId,
            Subject='subject',
            Status = 'Not Set',
            Description='description',
            Reason = 'Billing Ticket'
        );
        insert billingCase;
        
        Test.StopTest();
    }
    
    @isTest static void caseTriggerHandlerTests2() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        
        RecursiveTriggerHandler.isFirstTime = true;
        RecursiveTriggerHandler.isFirstTime2 = true;
        
        df.newCase.Status = 'Contractor Accepted Appointment Set';
        df.newCase.Customer_Reimbursement_Amount__c = 100 ;
        df.newCase.Reason = 'Heating System';
        update df.newCase;
        
        RecursiveTriggerHandler.isFirstTime = true;
        RecursiveTriggerHandler.isFirstTime2 = true;

        df.newCase.Status = 'Contractor Accepted Appointment Set';
        df.newCase.Customer_Reimbursement_Amount__c = 100;
        df.newCase.Reason = 'Heating System';
        df.newCase.Resolution_Picklist__c = 'Not Effective';
        update df.newCase;
        
        Case notEffCase = new Case(
            Policy__c = df.newPolicy.id,
            AccountId = df.acc.Id,
            ContactId = df.conId,
            Subject='subject',
            Status = 'Not Set',
            Description='description',
            Reason = 'Heating System'
        );
        insert notEffCase;
        
        df.newCase.Status = 'Converted from Inquiry';
        df.newCase.Reason = 'Heating System';
        df.newCase.Resolution_Picklist__c = NULL;
        update df.newCase;
        
        Test.StopTest();
    }
    
    @isTest static void caseTriggerHandlerTests3() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createPolicy();
        df.createInquiry();
        
        RecursiveTriggerHandler.isFirstTime = true;
        RecursiveTriggerHandler.isFirstTime2 = true;

        df.newInquiry.Status = 'Closed';
        update df.newInquiry;
        
        df.newInquiry.Status = 'In Progress';
        update df.newInquiry;
        Test.StopTest();
    }
    
    @isTest static void caseTriggerWebTests1() {
        Test.StartTest();
        df.createGlobalSetting();
        
        Case webCase = new Case(
            SuppliedName = 'Test Test',
            Origin = 'Web',
            Web_Policy__c = 'SHW1123',
            SuppliedPhone = '123456789',
            Web_Issue_Type__c = 'Test',
            Customer_Reimbursement_Amount__c = 100
        );
        Database.insert(webCase);
        webCase.Policy__c = contract.Id;
        Database.update(webCase);
        
        Case webCase2 = new Case(
            SuppliedName = 'Test Test',
            Origin = 'Web',
            Web_Policy__c = 'SHW123',
            SuppliedPhone = '123456789',
            Web_Issue_Type__c = 'Test'
        );
        Database.insert(webCase2);
        Test.StopTest();
    }
    
    @isTest static void caseTriggerWebTests2() {
        Test.StartTest();
        df.createGlobalSetting();
        
        Case webCase3 = new Case(
            SuppliedName = 'Amy Adams',
            SuppliedEmail = 'AmyAdams@test.com',
            Origin = 'Web',
            Web_Policy__c = 'SHW1233456',
            SuppliedPhone = '45666645',
            Web_Issue_Type__c = 'Test'
        );
        Database.insert(webCase3);
        
        Case webCase4 = new Case(
            SuppliedEmail = 'AmyAdams@test.com',
            Origin = 'Web',
            Web_Policy__c = 'SHW1233456',
            SuppliedPhone = '45666645',
            Web_Issue_Type__c = 'Test'
        );
        Database.insert(webCase4);
        
        Test.StopTest();
    }
    
    @isTest static void caseTriggerWebTests3() {
        Test.StartTest();
        df.createGlobalSetting();
        
        Case webCase5 = new Case(
            SuppliedName = 'Jane',
            SuppliedEmail = 'JaneTree@test.com',
            Origin = 'Web',
            Web_Policy__c = 'SHW664545',
            SuppliedPhone = '775656',
            Web_Issue_Type__c = 'Test'
        );
        Database.insert(webCase5);
        
        Test.StopTest();
    }
    
    @isTest static void caseAssignmentRoundRobinHandlerTests() {
        System.assertNotEquals(NULL,carrList);
    
        Test.StartTest();
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        
        List<Case> caseList = new List<Case>();
        Case cs2 = df.newCase.clone(false, true, false, false);
        caseList.add(cs2);
        Case cs3 = df.newCase.clone(false, true, false, false);
        caseList.add(cs3);
        insert caseList;
        
        caseList = new List<Case>();
        for (Case cs : [SELECT Id, Status, OwnerId FROM Case]) {
            cs.Status = 'Autho Review';
            cs.OwnerId = carrList[4].User__c;
            caseList.add(cs);
        }
        update caseList;
        
        RecursiveTriggerHandler.isFirstTime = true;
        RecursiveTriggerHandler.isFirstTime2 = true;

        carrList[4].Cases_Redistributed_On__c = DateTime.now();
        update carrList;

        Test.StopTest();

    }
    
    @isTest
    static void leadOrderTriggerHandlerTests() {
        df.createGlobalSetting();
        System.assertNotEquals( newlead, NULL );
        
        Test.StartTest();
        Lead dupeLead1 = newlead.clone(false, true, false, false);
        insert dupeLead1;
        
        Lead dupeLead2 = newlead.clone(false, true, false, false);
        dupeLead2.Additional_AC_Unit_each__c = '3';
        dupeLead2.Additional_Heat_Furnace_each__c = '3';
        dupeLead2.Additional_Water_Heater_each__c = '3';        
        insert dupeLead2;
        
        ChargentOrders__ChargentOrder__c order = new ChargentOrders__ChargentOrder__c();
        order.ChargentOrders__Card_Number__c = '4175551111111111';
        order.ChargentOrders__Card_Expiration_Month__c = '04';
        order.ChargentOrders__Card_Expiration_Year__c = '2024';
        order.Create_from_Web__c = true;
        order.Lead__c = dupeLead1.Id;
        insert order;
        
        ApexPages.currentPage().getParameters().put('id',dupeLead1.id );
        ApexPages.StandardController controller = new ApexPages.StandardController(dupeLead1);
        LeadTabController extension = new LeadTabController(controller);
        extension.redirectLead();
        
        ApexPages.currentPage().getParameters().put('id',newlead.id );
        controller = new ApexPages.StandardController(newlead);
        extension = new LeadTabController(controller);
        extension.removeDupeId = dupeLead1.Id;
        extension.removeDupe();
        
        delete dupeLead2;
        undelete dupeLead2;
        
        Test.StopTest();
    }
    
    @isTest
    static void leadOrderTriggerHandlerTests2() {
        df.createGlobalSetting();
        df.createLead();
        Lead ldUpdate = new Lead(Id = df.newLead.Id);

        Test.StartTest();
        RecursiveTriggerHandler.isFirstTime = true;
        RecursiveTriggerHandler.isFirstTime2 = true;

        ldUpdate.Contract_Term__c = '2 Year';
        ldUpdate.Central_Vacuum__c = TRUE;
        ldUpdate.Freon__c = TRUE;
        ldUpdate.Geo_Thermal__c = TRUE;
        ldUpdate.Grinder_Pump__c = TRUE;
        ldUpdate.Ice_Maker_In_Refrigerator__c = TRUE;
        ldUpdate.Lawn_Sprinkler_System__c = TRUE;
        ldUpdate.Lighting_Fixtures_Plumbing_Fixtures__c = TRUE;
        ldUpdate.Pool__c = TRUE;
        ldUpdate.Roof_Leak__c = TRUE;
        ldUpdate.Tankless_Water_Heater__c = TRUE;
        ldUpdate.Salt_Water_Pool__c  = TRUE;
        ldUpdate.Second_Refrigerator__c = TRUE;
        ldUpdate.Septic_System__c = TRUE;
        ldUpdate.Spa__c = TRUE;  
        ldUpdate.Stand_Alone_Freezer__c = TRUE;
        ldUpdate.Sump_Pump__c = TRUE;
        ldUpdate.Water_Softener__c = TRUE;
        ldUpdate.Well_Pump__c = TRUE;
        ldUpdate.Additional_AC_Unit_each__c = '1';
        ldUpdate.Additional_Heat_Furnace_each__c = '1';
        ldUpdate.Additional_Water_Heater_each__c = '1'; 
        ldUpdate.Oven__c = TRUE;
        ldUpdate.of_Ovens__c = '1';
        ldUpdate.of_Refrigerators__c = '1';
        ldUpdate.of_Stand_Alone_Freezers__c = '1';
        ldUpdate.Package__c = 'Gold';
            
        RecursiveTriggerHandler.isFirstTime = TRUE;
        RecursiveTriggerHandler.isFirstTime2 = TRUE;
        update ldUpdate;
        Test.StopTest();
    }
    
    @isTest
    static void pricingDeleteTest() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createWebsiteSettings();
        try {
            delete df.pricing;
        } catch (Exception e) {
        }   
        Test.StopTest();
    }
    
    @isTest
    static void taskTests() {
        df.createGlobalSetting();
        df.createLead();
        df.createPolicy();
        
        Test.StartTest();
        List<Task> tskList = new List<Task>();
        Task tsk1 = new Task(
            Subject = 'Buy It Now Clicked',
            Description = 'Buy It Now Clicked',
            WhoId = df.newlead.Id
        );
        Task tsk2 = new Task(
            Subject = 'Buy It Now Clicked',
            Description = 'Buy It Now Clicked',
            WhoId = df.conId
        );
        
        tskList.add(tsk1);
        tskList.add(tsk2);
        
        insert tskList;
        Test.StopTest();
    }
    
    @isTest 
    static void workOrderTriggerHandlerTests() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        df.createWorkOrder();
        //System.assertNotEquals( caseRec, NULL );
        //System.assertNotEquals( workOrder, NULL );

        Test.StartTest();
        PageReference pageRef = Page.CaseEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',df.newCase.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(df.newCase);
        CaseEditController extension = new CaseEditController(controller);
        System.assertEquals(df.newCase.id, Apexpages.currentpage().getparameters().get( 'id' ) );
        extension.thisCase.Status = 'Ready to Dispatch';
        extension.thisCase.Contractor__c = df.contractor.Id;
        extension.workOrderList[0].Contractor__c = df.contractor.Id;
        extension.workOrderList[0].Date_Scheduled__c = Date.today();
        extension.workOrderList[0].Date_Rescheduled__c = Date.today();
        extension.updateRecord();
        extension.workOrderList[0].Rating__c = 5;
        extension.workOrderRecords();
        delete extension.workOrderList[0];
        undelete extension.workOrderList[0];
        
        Test.stopTest();
    }
    
    @isTest 
    static void workOrderTriggerHandlerTests2() {
        Test.StartTest();

        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        df.createWorkOrder();
        //System.assertNotEquals( caseRec, NULL );
        //System.assertNotEquals( workOrder, NULL );
        
        df.workOrder.Date_Scheduled__c = Date.today();
        df.workOrder.Date_Rescheduled__c = Date.today();
        df.workOrder.Rating__c = 1;
        update df.workOrder;
        Test.stopTest();
    }
    
    @isTest 
    static void TestAffiliateLead() {
        df.createGlobalSetting();
        df.createAffiliate();
        df.createLead();
        
        Test.StartTest();
        df.affiliate.View_Duplicates__c = true;
        update df.affiliate;
        Test.stopTest();
    }
    
    @isTest 
    static void TestWebsiteSetting() {
        df.createGlobalSetting();
        df.createWebsiteSettings();
    }
    
    @isTest
    static void transactionTriggerHandlerTests() {
        df.createGlobalSetting();
        System.assertNotEquals( order, NULL );
        System.assertNotEquals( tran, NULL );
        
        Test.StartTest();
        
        ChargentOrders__Transaction__c tran1 = tran.clone(false, true, false, false);
        tran1.ChargentOrders__Response_Status__c = 'Declined';
        insert tran1;
        ChargentOrders__Transaction__c tran2 = tran1.clone(false, true, false, false);
        insert tran2;
        ChargentOrders__Transaction__c tran3 = tran1.clone(false, true, false, false);
        insert tran3;
        
        Test.StopTest();
    }
    
    /***************************************************************************************************
                          Initialize Test Data
    **************************************************************************************************/
    
    private static Lead newlead{
        get {
            if ( newlead == NULL ) {
                
                newlead = new Lead(
                    FirstName ='test',
                    LastName = 'testLast',
                    Email = 'test@test.com',
                    MobilePhone = '3242',
                    CountryCode = 'US',
                    Street = 'testStreet',
                    City = 'testCity',
                    PostalCode = '32423',
                    status = 'Warranty Sold',
                    Months_Free__c = '1 Month',
                    Customer_Id__c = '123',
                    Total_Price_Manual__c = 1
                );
                insert newlead;
            }
            return newlead;
        }
        private set;
    }
    
    private static Account account{
        get {
            if ( account == NULL ) {
                account = new Account(
                    FirstName ='John',
                    LastName = 'Test',
                    PersonEmail = 'test@test.com'
                );
                account.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'PersonAccount'].Id;
                insert account;
            }
            return account;
        }
        private set;
    }
     private static Contract contract{
        get {
            if ( contract == NULL ) {
                contract = new Contract(
                   Name='Test PolicyTab',
                   accountid=account.id,
                   Primary_Policy__c = true,
                   Email_with_Multiple_Address__c = 'test@test.com',
                   Email__c = 'test@test.com',
                   Priority_Override__c = '1',
                    Converted_Date__c = DateTime.now()
                );
                insert contract;
            }
            return contract;
        }
        private set;
    }
    
    private static ChargentOrders__ChargentOrder__c order{
        get {
            if (order == NULL ) {
                order = new ChargentOrders__ChargentOrder__c(
                    ChargentOrders__Card_Number__c = '4175551111111111',
                    ChargentOrders__Card_Expiration_Month__c = '04',
                    ChargentOrders__Card_Expiration_Year__c = '2024',
                    ChargentOrders__Payment_Status__c = 'Once',
                    Payment_Terms_New__c = '2 Payments',
                    Payment_Method__c = 'One Time',
                    ChargentOrders__Charge_Amount__c = 20.00,
                    Policy__c = contract.Id
                );
                insert order;
            }
            return order;
        }
        private set;
    }
    
    private static ChargentOrders__Transaction__c tran{
        get {
            if ( tran== NULL ) {
                
                tran = new ChargentOrders__Transaction__c (
                    ChargentOrders__Order__c = order.id,
                    ChargentOrders__Type__c = 'Refund',
                    ChargentOrders__Gateway_Date__c = Date.today(),
                    ChargentOrders__Response_Message__c = 'This transaction has been approved.',
                    ChargentOrders__Gateway_ID__c = '60102691449',
                    ChargentOrders__Response_Status__c = 'Approved',
                    ChargentOrders__Account__c = account.id,
                    ChargentOrders__Amount__c = 20.00,
                    ChargentOrders__Amount_available_for_Refund__c = 10.00,
                    ChargentOrders__Authorization__c = '60RKMH',
                    ChargentOrders__Reason_Code__c = 1,
                    ChargentOrders__Reason_Text__c = 'This transaction has been approved.',
                    ChargentOrders__Card_Last_4__c = '1111',
                    ChargentOrders__Payment_Method__c = 'Credit Card',
                    ChargentOrders__Credit_Card_Name__c = 'test',
                    ChargentOrders__Credit_Card_Type__c = 'Visa',
                    ChargentOrders__Billing_Last__c = 'test',
                    ChargentOrders__Gateway_Response__c = 'test'
                    
                );
                insert tran;
            }
            return tran;
        }
        private set;
    }
    
    private static List<Case_Assignment_Round_Robin__c> carrList {
        get {
            if ( carrList == NULL ) {
                User[] randUsers = [Select Id FROM User WHERE IsActive = TRUE AND UserType = 'Standard' AND Id != :UserInfo.getUserId() LIMIT 3];

                carrList = new List<Case_Assignment_Round_Robin__c>{
                    new Case_Assignment_Round_Robin__c(
                        User__c = randUsers[0].Id,
                        Status__c = 'Open',
                        Star_Priority__c = '1;2;3',
                        Apply_To__c = 'Inquiry'
                    ),
                        new Case_Assignment_Round_Robin__c(
                        User__c = randUsers[1].Id,
                        Status__c = 'Open',
                        Star_Priority__c = '1;2;3',
                        Apply_To__c = 'Inquiry'
                    ),
                        new Case_Assignment_Round_Robin__c(
                        User__c = randUsers[0].Id,
                        Status__c = 'In Progress',
                        Star_Priority__c = '1;2;3',
                        Apply_To__c = 'Inquiry'
                    ),
                        new Case_Assignment_Round_Robin__c(
                        User__c = randUsers[0].Id,
                        Status__c = 'Autho Review',
                        Star_Priority__c = '1;2;3',
                        Apply_To__c = 'Claim'
                    ),
                        new Case_Assignment_Round_Robin__c(
                        User__c = randUsers[1].Id,
                        Status__c = 'Autho Review',
                        Star_Priority__c = '1;2;3',
                        Apply_To__c = 'Claim'
                    ),
                        new Case_Assignment_Round_Robin__c(
                            User__c = randUsers[2].Id,
                            Status__c = 'Autho Review',
                            Star_Priority__c = '1;2;3',
                            Apply_To__c = 'Claim'
                        )};
                            insert carrList;
            }
            return carrList;
        }
        private set;
    }
}