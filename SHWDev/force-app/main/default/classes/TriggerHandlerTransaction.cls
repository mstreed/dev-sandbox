public class TriggerHandlerTransaction extends TriggerHandler {
    
    @TestVisible private Boolean testException = FALSE;                         // used to induce exception coverage by test methods
    
    public TriggerHandlerTransaction() {}
    public static Global_Setting__c gs = [Select Id, Payment_Declined_3X_Email_User_ID__c, Payment_Declined_3X_Task_User_ID__c, Special_Request_Task_User_Id__c, System_Administrator_User_ID__c FROM Global_Setting__c LIMIT 1];

    /* context overrides */
    
    public override void beforeInsert() {}
    
    public override void beforeUpdate() {}
    
    public override void beforeDelete() {}
    
    public override void afterInsert() {
        checkDeclineTransaction(Trigger.new);
    }
    
    public override void afterUpdate() {
        caseRollup(Trigger.new, Trigger.oldMap);
    }
    
    public override void afterDelete() {}
    
    public override void afterUndelete() {} 
    
    /* private methods */
    
    private void checkDeclineTransaction(List<sObject> newTrans){
        system.debug('checkDeclineTransaction');
        List<ChargentOrders__Transaction__c> newTransactionList = (List<ChargentOrders__Transaction__c>) newTrans;
        Set<Id> ordersToCheck = new Set<Id>();
        Map<Id,String> policiesToUpdateCard = new Map<Id,String>();
        List<ChargentOrders__ChargentOrder__c> ordersToUpdate = new List<ChargentOrders__ChargentOrder__c>();
        List<Contract> policiesToUpdate = new List<Contract>();
        List<Messaging.SingleEmailMessage> customerEmails = new List<Messaging.SingleEmailMessage>();
        List<Messaging.SingleEmailMessage> userEmails = new List<Messaging.SingleEmailMessage>();
        EmailTemplate cusTemplateId = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'Declined_Three_Times' LIMIT 1];
        OrgWideEmailAddress owa = [SELECT Id, DisplayName, Address FROM OrgWideEmailAddress WHERE DisplayName = 'SHW Subs' LIMIT 1];
        String siteFullUrl = Util.siteURL();
        List<Payment_Method__c> paymentMethods = new List<Payment_Method__c>();
        List<Task> tasks = new List<Task>();
        
        //If trans is declined/error, add to check list (only if recurring)
        for(ChargentOrders__Transaction__c tran : newTransactionList){
            if (tran.ChargentOrders__Recurring__c &&
                (tran.ChargentOrders__Response_Status__c == 'Declined' || tran.ChargentOrders__Response_Status__c == 'Error')) {
                    ordersToCheck.add(tran.ChargentOrders__Order__c);
                    
                    //send payment error/declined message to customer if code or specific message matches
                    if (tran.ChargentOrders__Reason_Code__c != 30) {
                        policiesToUpdateCard.put(tran.Lead_Policy_ID__c,tran.ChargentOrders__Response_Message__c);
                    }
                }
        }
        
        //check orders
        for(ChargentOrders__ChargentOrder__c ord : [SELECT Id, Name, Policy__c, Policy__r.Contract_Number__c, Policy__r.Current_Sales_Agent__c, Policy__r.Contact_ID__c, Policy__r.Account.PersonContact.EmailBouncedDate, ChargentOrders__Payment_Status__c, of_Transactions_Recurring__c, CreatedById, (SELECT Id, Name, ChargentOrders__Response_Status__c, CreatedDate FROM ChargentOrders__Transactions__r ORDER BY CreatedDate DESC) 
                                                    FROM ChargentOrders__ChargentOrder__c
                                                    WHERE Id IN :ordersToCheck]){
                                                        Integer totalErrorCounter = 0; //counter for errors
                                                        
                                                        //check if the first 3 transactions has a payment declined or error, if yes, send something out to sales agent and task to sharon
                                                        system.debug('ord.of_Transactions_Recurring__c: ' + ord.of_Transactions_Recurring__c);
                                                        if (ord.of_Transactions_Recurring__c < 3) {
                                                            for(ChargentOrders__Transaction__c tran : newTransactionList){
                                                                if (ord.Id == tran.ChargentOrders__Order__c &&
                                                                    tran.ChargentOrders__Recurring__c &&
                                                                    (tran.ChargentOrders__Response_Status__c == 'Declined' || tran.ChargentOrders__Response_Status__c == 'Error')) {
                                                                        Util.newTask(ord.Policy__c, 'Billings', ord.Policy__r.Contract_Number__c + ' - Payment Error on Subscription(' + ord.Name + ')', 'There was a Payment Error for Policy#'+ ord.Policy__r.Contract_Number__c + ' on Subscription ' + ord.Name +' for Transaction ' + tran.Name, gs.Payment_Declined_3X_Task_User_ID__c, 'Task', false);
                                                                        
                                                                        Messaging.SingleEmailMessage eml = new Messaging.SingleEmailMessage();
                                                                        if (ord.Policy__r.Current_Sales_Agent__c == gs.System_Administrator_User_ID__c.left(15)) {
                                                                            eml.setTargetObjectId(gs.Payment_Declined_3X_Email_User_ID__c); 
                                                                        } else {
                                                                            eml.setTargetObjectId(ord.Policy__r.Current_Sales_Agent__c); 
                                                                        }
                                                                        eml.setOrgWideEmailAddressId(owa.id);
                                                                        eml.setWhatId(ord.Policy__c);
                                                                        eml.setSubject(ord.Policy__r.Contract_Number__c + ' - Payment Error on Subscription(' + ord.Name + ')');
                                                                        eml.setPlainTextBody('There was a Payment Error for Policy#'+ ord.Policy__r.Contract_Number__c + ' on Subscription ' + ord.Name +' for Transaction ' + tran.Name);
                                                                        eml.setSaveAsActivity(false); //must be false when sending to user
                                                                        userEmails.add(eml);

                                                                        Task emlTask = new Task(
                                                                            OwnerId = eml.getTargetObjectId(),
                                                                            Subject = 'Email: ' + eml.getSubject(),
                                                                            WhatId = eml.getWhatId(),
                                                                            ActivityDate = Date.today(),
                                                                            Description = eml.getPlainTextBody(), 
                                                                            Status = 'Completed',
                                                                            TaskSubtype = 'Email'
                                                                        );
                                                                        system.debug('emlTask: ' + emlTask);
                                                                        tasks.add(emlTask);
                                                                    }
                                                            }
                                                        }
                                                        
                                                        //check trans if equal or more than 3 records than check if the last 3 transactions has an error
                                                        if (ord.ChargentOrders__Transactions__r.size() >= 3) {
                                                            for(ChargentOrders__Transaction__c tran : ord.ChargentOrders__Transactions__r){
                                                                
                                                                //if order has 3 consecutive declines, set order to Stopped. otherwise, skip
                                                                if (tran.ChargentOrders__Response_Status__c == 'Declined' || tran.ChargentOrders__Response_Status__c == 'Error') {
                                                                    totalErrorCounter++;
                                                                    system.debug('totalErrorCounter: ' + totalErrorCounter);
                                                                    
                                                                    if (totalErrorCounter == 3) {
                                                                        ord.ChargentOrders__Payment_Status__c = 'Stopped';
                                                                        ordersToUpdate.add(ord);
                                                                        if (ord.Policy__c != NULL) {
                                                                            policiesToUpdate.add(new Contract(Id = ord.Policy__c, Status = 'Lapsed'));
                                                                        }
                                                                        
                                                                        if (ord.Policy__r.Account.PersonContact.EmailBouncedDate == NULL) {
                                                                            Messaging.SingleEmailMessage eml = new Messaging.SingleEmailMessage();
                                                                            eml.setTargetObjectId(ord.Policy__r.Contact_ID__c); 
                                                                            eml.setTemplateID(cusTemplateId.Id); 
                                                                            eml.setSaveAsActivity(true);
                                                                            eml.setOrgWideEmailAddressId(owa.id);
                                                                            //eml.setToAddresses(toAddresses);
                                                                            eml.setWhatId(ord.Policy__c);
                                                                            system.debug('cusEml: ' + eml);
                                                                            customerEmails.add(eml);
                                                                        }
                                                                        
                                                                        break;
                                                                    }
                                                                } else {
                                                                    totalErrorCounter = 0;
                                                                    break;
                                                                }
                                                            }
                                                        }
        }
        
        for (Payment_Method__c pm : [SELECT Id, Policy_Name__c, Policy_Name__r.Account.PersonContactId, Policy_Name__r.Account.PersonEmail, Policy_Name__r.Contract_Number__c, Customer_Email__c, Payment_Method_URL__c, Last_Reminder_Sent__c
                                     FROM Payment_Method__c WHERE Policy_Name__c = :policiesToUpdateCard.keyset() AND Set_as_default__c = TRUE]) {
                                         String paymentMethodURL = siteFullUrl;
                                         paymentMethodURL += '/pm?token=' + pm.Id; 
                                         
                                         if (pm.Customer_Email__c != pm.Policy_Name__r.Account.PersonEmail) {
                                             pm.Customer_Email__c = pm.Policy_Name__r.Account.PersonEmail;
                                         }
                                         
                                         Blob blobKey = crypto.generateAesKey(128);
                                         String key = EncodingUtil.convertToHex(blobKey);
                                         
                                         pm.Customer_Access_Code__c = key.substring(0,10);
                                         pm.Payment_Method_URL__c = paymentMethodURL;
                                         pm.Error_Declined__c = DateTime.now(); //Changing this field will trigger WF email alert
                                         pm.Error_Declined_Message__c = policiesToUpdateCard.get(pm.Policy_Name__c);
                                         paymentMethods.add(pm);
                                         
                                         //create a task to log the email that is sent - not using the api to send/log emails to reduce limit usage
                                         Task tsk = new Task();
                                         tsk.ActivityDate = Date.today();
                                         tsk.WhoId = pm.Policy_Name__r.Account.PersonContactId;
                                         tsk.WhatId = pm.Id;
                                         tsk.From__c = 'subs@selecthomewarranty.com';
                                         tsk.Department__c = 'Sales';
                                         tsk.Subject = 'Email: [Action Required] - Policy #'+pm.Policy_Name__r.Contract_Number__c+' - Problem With Credit Card';
                                         tsk.Description = 'Customer sent link to payment method page to update credit card: ' + paymentMethodURL;
                                         tsk.OwnerId = UserInfo.getUserId();
                                         tsk.Status = 'Completed';
                                         tsk.TaskSubtype = 'Email';
                                         tasks.add(tsk);
                                     }
        
            
        
        if (!ordersToUpdate.isEmpty()) {
            update ordersToUpdate;
        }
        if (!policiesToUpdate.isEmpty()) {
            update policiesToUpdate;
            Messaging.SendEmailResult[] r = Messaging.sendEmail(customerEmails);
        }
        if (!userEmails.isEmpty()) {
            Messaging.SendEmailResult[] r = Messaging.sendEmail(userEmails);
        }
        if (!paymentMethods.isEmpty()) {
            update paymentMethods;
        }
        if (!tasks.isEmpty()) {
            insert tasks;
        }
    }
    
    private void caseRollup(List<sObject> newTrans, Map<Id,sObject> oldTransMap){
        system.debug('caseRollup');
        List<ChargentOrders__Transaction__c> newTransList = (List<ChargentOrders__Transaction__c>) newTrans;
        Set<Id> caseIds = new Set<Id>();
        
        if(oldTransMap != NULL ){
            //Assign oldMap
            Map<Id,ChargentOrders__Transaction__c> oldMap = (Map<Id,ChargentOrders__Transaction__c>) oldTransMap;
            
            //get caseIds to update rollup
            for(ChargentOrders__Transaction__c tran : newTransList){
                if (tran.Case__c != NULL || oldMap.get(tran.Id).Case__c != NULL) {
                    caseIds.add(tran.Case__c);
                    if (oldMap.get(tran.Id).Case__c != NULL) {
                        caseIds.add(oldMap.get(tran.Id).Case__c);
                    }
                }
            }            
        }
        
        if (!caseIds.isEmpty()) {
            List<Case> csToUpdate = new List<Case>();
            for (Case cs : [SELECT Id, Paid_Deductible__c, (SELECT Id, ChargentOrders__Type__c, Amount_for_Summary__c FROM Transactions__r) FROM Case WHERE Id IN :caseIds]) {
                cs.Paid_Deductible__c = 0;
                for (ChargentOrders__Transaction__c tran : cs.Transactions__r) {
                    if (tran.ChargentOrders__Type__c == 'Charge' || tran.ChargentOrders__Type__c == 'Refund') {
                        cs.Paid_Deductible__c+= tran.Amount_for_Summary__c;
                    }
                }
                csToUpdate.add(cs);
            }
            UPDATE csToUpdate;
        }
    }
}