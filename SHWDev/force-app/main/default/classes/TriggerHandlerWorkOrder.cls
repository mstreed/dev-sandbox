/*
* 	PURPOSE:       This class will handle trigger behaviour and will perform task accordingly
*	INTERFACES:     
*   CHANGE LOG:    27-8-2018  	Intial release 
*/
public  class TriggerHandlerWorkOrder extends TriggerHandler {
	
	// Constructor 
	public TriggerHandlerWorkOrder() {}
	
	/* Trigger context overrides */
	public override void beforeInsert() {}
    public override void beforeUpdate() {
        //workOrderBeforeUpdate(Trigger.new , Trigger.oldMap);
    }
	public override void beforeDelete() {}
	public override void afterInsert()  {}
  	public override void afterUpdate()  {
        if(RecursiveTriggerHandler.runOnceWorkOrder()){
            workOrderAfterUpdateOnce(Trigger.new, Trigger.oldMap);
        }
        workOrderAfterUpdate(Trigger.new, Trigger.oldMap);
        
    }
  	public override void afterDelete()  {}
  	public override void afterUndelete(){}

    /*private void workOrderBeforeUpdate(List<sObject> newWOs , Map<Id,sObject> oldWOMap){
        system.debug('workOrderBeforeUpdate');
        List<Work_Order__c> newWOList = (List<Work_Order__c>) newWOs;
        
        if(oldWOMap != NULL ){
            //Assign oldMap
            Map<Id,Work_Order__c> oldMap = (Map<Id,Work_Order__c>) oldWOMap;
            
            for(Work_Order__c wo : newWOList){
                if (wo.Customer_StateCode__c == 'VA') {
                    DateTime dT;
                    
                    if (wo.Dispatch_Date_Scheduled__c != oldMap.get(wo.Id).Dispatch_Date_Scheduled__c) {
                        if (wo.Dispatch_Date_Scheduled__c == NULL) {
                            wo.Date_Scheduled__c = NULL;
                        } else {
                            dT = wo.Dispatch_Date_Scheduled__c;
                            wo.Date_Scheduled__c = date.newinstance(dT.year(), dT.month(), dT.day());
                        }
                    }
                    
                    if (wo.Dispatch_Date_Rescheduled__c != oldMap.get(wo.Id).Dispatch_Date_Rescheduled__c) {
                        if (wo.Dispatch_Date_Rescheduled__c == NULL) {
                            wo.Date_Rescheduled__c = NULL;
                        } else {
                            dT = wo.Dispatch_Date_Rescheduled__c;
                            wo.Date_Rescheduled__c = date.newinstance(dT.year(), dT.month(), dT.day());
                        }
                    }
                }
            }
        }
    }*/
    
    private void workOrderAfterUpdate(List<sObject> newWOs , Map<Id,sObject> oldWOMap){
        system.debug('workOrderAfterUpdate');
        List<Work_Order__c> newWOList = (List<Work_Order__c>) newWOs;
        
        if(oldWOMap != NULL ){
            //Assign oldMap
            Map<Id,Work_Order__c> oldMap = (Map<Id,Work_Order__c>) oldWOMap;
            Set<Id> contractorsToRecalcRating = new Set<Id>();
            List<Account> contractorsToUPDATE = new List<Account>();

            for(Work_Order__c wo : newWOList){
                //if (!wo.Customer_Dispatchable__c) {
                    
                    /*if (wo.Date_Scheduled__c != null && oldMap.get(wo.Id).Date_Scheduled__c == null) {
                        Boolean sendEmail = Util.sendEmail(wo.Customer_Contact_Id__c, wo.Customer_Email__c, 'Contractor_Scheduled', null, wo.Id, 'SHW Claims', true);
                        system.debug('Contractor_Scheduled email result: ' + sendEmail);
                    }
                    
                    if (wo.Date_Rescheduled__c != null && oldMap.get(wo.Id).Date_Rescheduled__c == null) {
                        Boolean sendEmail = Util.sendEmail(wo.Customer_Contact_Id__c, wo.Customer_Email__c, 'Contractor_Rescheduled', null, wo.Id, 'SHW Claims', true);
                        system.debug('Contractor_Scheduled email result: ' + sendEmail);
                    }*/
                    
                    if (wo.Rating__c != oldMap.get(wo.Id).Rating__c) {
                        contractorsToRecalcRating.add(wo.Contractor__c);
                    }
                    
                    if (wo.Contractor__c != oldMap.get(wo.Id).Contractor__c && wo.Rating__c != NULL) {
                        contractorsToRecalcRating.add(wo.Contractor__c);                        
                        contractorsToRecalcRating.add(oldMap.get(wo.Id).Contractor__c);
                    }

                //}
            }
            
            if (!contractorsToRecalcRating.isEmpty()){
                for(Account con : [Select Id, Average_Rating__c, of_Ratings__c, Average_Rating_Higher_Than_3_5_Since__c, (Select Id, Rating__c FROM Work_Orders__r) FROM Account WHERE Id = :contractorsToRecalcRating]){
                    con.of_Ratings__c = 0;
                    Double total = 0;
                    for (Work_Order__c wo : con.Work_Orders__r) {
                        if (wo.Rating__c != NULL) {
                            con.of_Ratings__c++;
                            total+= wo.Rating__c;
                        }
                    }
                    system.debug('con.of_Ratings__c: ' + con.of_Ratings__c);
                    system.debug('total: ' + total);
                    if (con.of_Ratings__c == 0) {
                        con.Average_Rating__c = NULL;
                    } else {
                        con.Average_Rating__c = total/con.of_Ratings__c;
                    }
                    /*if (con.Average_Rating__c >= 3.5 && con.Average_Rating_Higher_Than_3_5_Since__c == NULL) {
                        con.Average_Rating_Higher_Than_3_5_Since__c = Date.today();
                    } else {
                        con.Average_Rating_Higher_Than_3_5_Since__c = NULL;
                    }*/
                    contractorsToUpdate.add(con);
                }
                UPDATE contractorsToUpdate;
            }
        }
    }
    
    private void workOrderAfterUpdateOnce(List<sObject> newWOs , Map<Id,sObject> oldWOMap){
        system.debug('workOrderAfterUpdateOnce');
        List<Work_Order__c> newWOList = (List<Work_Order__c>) newWOs;
        
        if(oldWOMap != NULL ){
            //Assign oldMap
            Map<Id,Work_Order__c> oldMap = (Map<Id,Work_Order__c>) oldWOMap;
            List<Case> casesToUPDATE = new List<Case>();
            List<Task> tasksToCreate = new List<Task>();

            for(Work_Order__c wo : newWOList){
                if (wo.Rating__c >= 4 && wo.Rating__c != oldMap.get(wo.Id).Rating__c && wo.BBB_Review_Sent__c == FALSE) {
                    casesToUpdate.add(new Case(Id = wo.Claim__c, Automated_BBB_Review_Email_Sent__c = DateTime.now()));
                    //create a task to log the email that is sent - not using the api to send/log emails to reduce limit usage
                    Task tsk = new Task();
                    tsk.ActivityDate = Date.today();
                    tsk.WhoId = wo.Customer_Contact_ID__c;
                    tsk.From__c = 'bob@selecthomewarranty.com';
                    tsk.WhatId = wo.Claim__c;
                    tsk.Department__c = 'Claims';
                    tsk.Subject = 'Email:  Select Home Warranty Claim';
                    tsk.Description = 'Customer sent BBB Review email for work order rating of '+wo.Rating__c;
                    tsk.OwnerId = UserInfo.getUserId();
                    tsk.Status = 'Completed';
                    tsk.TaskSubtype = 'Email';
                    tasksToCreate.add(tsk);
                }
            }
            
            if (!casesToUpdate.isEmpty()) {
                UPDATE casesToUpdate;
                INSERT tasksToCreate;
            }
        }
    }
}