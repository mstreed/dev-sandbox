global without sharing class UnsubscribeController {
    public String email {get;set;}
    public Boolean renderResub {get;set;}

    public UnsubscribeController() {
        email = ApexPages.currentPage().getParameters().get('email');
        
        renderResub = FALSE;
    }
    
    public void unsubscribe() {
        system.debug('unsubscribe');
        List<sObject> unsubList = new List<sObject>();
        
        for (Lead ld : [SELECT Id, HasOptedOutOfEmail
                        FROM Lead 
                        WHERE RecordType.DeveloperName = 'Queue_Lead' AND IsConverted = FALSE AND Email = :email]) {
                            ld.HasOptedOutOfEmail = TRUE;
                            unsubList.add(ld);
                        }
        
        for (Account acct : [SELECT Id, PersonHasOptedOutOfEmail
                             FROM Account 
                             WHERE PersonEmail = :email]) {
                                 acct.PersonHasOptedOutOfEmail  = TRUE;
                                 unsubList.add(acct);
                             }
        
        UPDATE unsubList;
    }
    
    public void resubscribe() {
        system.debug('resubscribe');
        List<sObject> resubList = new List<sObject>();
        
        for (Lead ld : [SELECT Id, HasOptedOutOfEmail
                        FROM Lead 
                        WHERE RecordType.DeveloperName = 'Queue_Lead' AND IsConverted = FALSE AND Email = :email]) {
                            ld.HasOptedOutOfEmail = FALSE;
                            resubList.add(ld);
                        }
        
        for (Account acct : [SELECT Id, PersonHasOptedOutOfEmail
                             FROM Account 
                             WHERE PersonEmail = :email]) {
                                 acct.PersonHasOptedOutOfEmail = FALSE;
                                 resubList.add(acct);
                             }
        
        UPDATE resubList;
        renderResub = TRUE;
    }
}