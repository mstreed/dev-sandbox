@isTest
private class UnsubscribeTest {
    private static DataFactory df = new DataFactory();

    @isTest
    static void testRenewalResponse() {
        df.createGlobalSetting();
        df.createLead();
        df.createPolicy();
        
        Test.StartTest();
        
        PageReference pageRef = Page.Unsubscribe;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('email',df.newLead.Email);
        UnsubscribeController controller = new UnsubscribeController();
        controller.unsubscribe();
        controller.resubscribe();
        
        PageReference pageRef2 = Page.Unsubscribe;
        Test.setCurrentPage(pageRef2);
        ApexPages.currentPage().getParameters().put('email',df.newPolicy.Email__c);
        UnsubscribeController controller2 = new UnsubscribeController();
        controller2.unsubscribe();
        controller2.resubscribe();
    }
}