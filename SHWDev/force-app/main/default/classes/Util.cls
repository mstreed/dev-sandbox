global without sharing class Util {
    
    public static String siteURL() {
        //Need to create the URL this way because in sandboxes, domain formats are domain-sandbox format whereas site urls are sandbox-domain format. Latter does not have proper methods.
        Organization org = [SELECT InstanceName,Name, IsSandbox, OrganizationType FROM Organization];
        String siteFullUrl = 'http://';
        if(org.IsSandbox == true) {
            siteFullUrl += UserInfo.getUserName().substringAfterLast('.')+'-';
        }
        siteFullUrl += 'selecthomewarranty.';
        siteFullUrl += (org.IsSandbox || org.OrganizationType == 'Developer Edition' ? (org.InstanceName.toLowerCase() + '.') : '') + 'force.com';
        return siteFullUrl;
    }
    
    public static Id suggestContractor(String CusLat, String CusLong, String caseServiceProvided) {
        if (CusLat != null && CusLong != null && CusLat != '' && CusLong != '') {
            Id suggestedContractorId;
            Id contractorRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Contractor Account').getRecordTypeId();
            
            system.debug('CusLat: ' + CusLat);
            system.debug('CusLong: ' + CusLong);
            
            List<Account> contractors = new List<Account>();
            
            // SOQL query to get Accounts 
            String Inactive = 'Inactive';
            String caseServiceProvidedQuery;
            
            system.debug('caseServiceProvided:' + caseServiceProvided);
            if (caseServiceProvided != null) {
                if (caseServiceProvided.contains(';')) {
                    for (String sp : caseServiceProvided.split(';')) {
                        if (caseServiceProvidedQuery == null) {
                            caseServiceProvidedQuery = '\'' + sp + '\'';
                        } else {
                            caseServiceProvidedQuery = caseServiceProvidedQuery + ',\'' + sp + '\'';
                        }
                    }
                } else {
                    caseServiceProvidedQuery = '\'' + caseServiceProvided + '\'';
                }
            } else {
                caseServiceProvidedQuery = '\'\'';
            }
            
            system.debug('caseServiceProvidedQuery:' + caseServiceProvidedQuery);
            
            String queryString1 = '';
            queryString1 = 
                'SELECT Id, Name, Type, Services_Provided__c, Business_Email__c, Account_Status__c, First_Hour_Labor_Rate__c , Description, Rating__c, DISTANCE(ShippingAddress, GEOLOCATION('+CusLat+','+CusLong+'), \'mi\') distance, Phone, ShippingLatitude, ShippingLongitude, ShippingStreet, ShippingCity, ShippingState, Approved_for_Quick_Dispatch__c, Maximum_Distance_mi_for_Quick_Dispatch__c '+ 
                'FROM Account ' +
                'WHERE Services_Provided__c includes ( '+ caseServiceProvidedQuery +' ) AND Account_Status__c != :Inactive AND RecordTypeId = :contractorRTId AND ShippingLatitude !=null AND ShippingLongitude !=null AND Approved_for_Quick_Dispatch__c = TRUE ' + 
                //'WHERE Services_Provided__c includes ( '+ caseServiceProvidedQuery +' ) AND Account_Status__c != :Inactive AND RecordTypeId = :contractorRTId AND ShippingLatitude !=null AND ShippingLongitude !=null AND DISTANCE(ShippingAddress, GEOLOCATION('+lat+','+lon+'), \'mi\')<50 AND Approved_for_Quick_Dispatch__c = TRUE ' + 
                'ORDER BY DISTANCE(ShippingAddress, GEOLOCATION('+CusLat+','+CusLong+'), \'mi\') NULLS LAST ' +
                'LIMIT 150';
            system.debug('query1');
            
            // run query
            system.debug(queryString1);
            contractors = database.Query(queryString1);
            system.debug(contractors);
            
            for (Account con : contractors) {
                Object distance = con.get('distance');
                system.debug('con.Maximum_Distance_mi_for_Quick_Dispatch__c: ' + con.Maximum_Distance_mi_for_Quick_Dispatch__c);
                system.debug('distance: ' + distance);
                if (con.Maximum_Distance_mi_for_Quick_Dispatch__c >= (Decimal)distance) {
                    system.debug('ok');
                    suggestedContractorId = con.Id;
                    break;
                }
            }
            
            return suggestedContractorId;
        } else {
            return null;
        }
    }
    
    public static ChargentOrders__ChargentOrder__c newSubscription(Contract policy, Decimal chargeAmount, String payTerms, Date startDate) {
        ChargentOrders__ChargentOrder__c billingOrder = new ChargentOrders__ChargentOrder__c();
        for(Payment_Method__c pm : [SELECT Id, Card_Number_Encrypted__c, Month__c, Year__c FROM Payment_Method__c WHERE Policy_Name__c = :policy.Id AND Set_as_default__c = true LIMIT 1]){
            billingOrder.ChargentOrders__Card_Number__c = pm.Card_Number_Encrypted__c;
            billingOrder.ChargentOrders__Card_Expiration_Month__c = String.valueOf(pm.Month__c);
            billingOrder.ChargentOrders__Card_Expiration_Year__c = String.valueOf(pm.Year__c) ;
        }
        billingOrder.ChargentOrders__Payment_Frequency__c = 'Monthly';
        billingOrder.ChargentOrders__Payment_Status__c = 'Recurring';
        billingOrder.ChargentOrders__Account__c = policy.AccountId;
        billingOrder.ChargentOrders__Billing_First_Name__c= policy.First_Name__c;
        billingOrder.ChargentOrders__Billing_Last_Name__c= policy.Last_Name__c;
        billingOrder.Override__c= true;
        billingOrder.ChargentOrders__Manual_Charge__c = true;
        billingOrder.Policy__c = policy.Id;
        billingOrder.ChargentOrders__Charge_Amount__c = chargeAmount;
        billingOrder.ChargentOrders__Subtotal__c = chargeAmount; 
        billingOrder.Payment_Terms_New__c = payTerms;
        billingOrder.ChargentOrders__Payment_Start_Date__c = startDate;
        billingOrder.ChargentOrders__Gateway__c = policy.Gateway__c;

        insert billingOrder; 
        
        return billingOrder;
    }
    
    public static void newTask(Id parentId, String dept, String subject, String body, Id ownerId, String subType, Boolean completed) {
        system.debug('parentId: ' + parentId);
        system.debug('dept: ' + dept);
        system.debug('subject: ' + subject);
        system.debug('body: ' + body);
        system.debug('ownerId: ' + ownerId);
        system.debug('subType: ' + subType);
        system.debug('completed: ' + completed);
        Task tsk = new Task();
        tsk.ActivityDate = Date.today();
        if (String.valueOf(parentId).startsWith('00Q')) {
            tsk.WhoId = parentId;
        } else {
            tsk.WhatId = parentId;
        }
        tsk.Department__c = dept;
        tsk.Subject = subject;
        tsk.Description = body;
        tsk.OwnerId = ownerId;
        tsk.TaskSubtype = subType;
        if (completed) {
            tsk.Status = 'Completed';
        }
       
        insert tsk; 
    }
    
    public static boolean sendEmail(Id targetObjId, String emailAddress, String templateName, String templateFolder, Id currentRecordId, String sendFrom, Boolean sendToContact){
        system.debug('targetObjId: ' + targetObjId);
        system.debug('emailAddress: ' + emailAddress);
        system.debug('templateName: ' + templateName);
        system.debug('templateFolder: ' + templateFolder);
        system.debug('currentRecordId: ' + currentRecordId);
        system.debug('sendFrom: ' + sendFrom);
        system.debug('sendToContact: ' + sendToContact);
        
        if (emailAddress != null) {
            OrgWideEmailAddress owa = [SELECT Id, DisplayName, Address FROM OrgWideEmailAddress WHERE DisplayName = :sendFrom LIMIT 1];
            String templateQuery = 'SELECT Id FROM EmailTemplate WHERE';
            if (templateFolder != null) {
                if (templateFolder.contains('Quick Email')) {
                    templateQuery = templateQuery + ' Name = :templateName AND Folder.Name = :templateFolder LIMIT 1';
                } else {
                    templateQuery = templateQuery + ' DeveloperName = :templateName AND Folder.Name = :templateFolder LIMIT 1';
                }
            } else {
                templateQuery = templateQuery + ' DeveloperName = :templateName LIMIT 1';
            }
            system.debug('templateQuery: ' + templateQuery);
            EmailTemplate templateId = database.Query(templateQuery);
            String[] toAddresses = new String[] {emailAddress};
                
                Messaging.SingleEmailMessage eml = new Messaging.SingleEmailMessage();
            eml.setTargetObjectId(targetObjId); 
            eml.setTemplateID(templateId.Id);
            if (templateName == 'Need to Talk Retention') {
                eml.setReplyTo(UserInfo.getUserEmail());
            } else {
                eml.setOrgWideEmailAddressId(owa.id);
            }
            eml.setToAddresses(toAddresses);
            if (!String.valueOf(currentRecordId).startsWith('00Q')) {
                eml.setWhatId(currentRecordId);
            } 
            eml.setTreatTargetObjectAsRecipient(sendToContact);
            eml.setSaveAsActivity(sendToContact); //set this to the same as sendToContact so that email whoId is captured/not captured
            system.debug('eml: ' + eml);
            try {
                Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { eml });
                //log task when sendToContact is false
                if (sendToContact == false) {
                    Task emlTask = new Task(
                        OwnerId = UserInfo.getUserId(),
                        Subject = 'Email: ' + eml.getSubject(),
                        WhatId = eml.getWhatId(),
                        ActivityDate = Date.today(),
                        Description = eml.getPlainTextBody(),
                        Status = 'Completed',
                        TaskSubtype = 'Email'
                    );
                    insert emlTask;
                }
                system.debug('eml-result: ' + r);

                return r[0].isSuccess();
            } catch(Exception ex) {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }
    
    public static Decimal calculateTotalAmtCollected(Id policyId) {
        Decimal totalAmtCollected = 0;
        Set<Id> policyIds = relatedPolicies(policyId);
        for (ChargentOrders__Transaction__c trans : [SELECT Id, ChargentOrders__Amount__c,ChargentOrders__Order__c,ChargentOrders__Order__r.Policy__c,ChargentOrders__Type__c,ChargentOrders__Response_Status__c,
                                                     Charge_Reason__c, Parent_Charge_Reason__c
                                                     FROM ChargentOrders__Transaction__c 
                                                     WHERE (ChargentOrders__Type__c = 'Charge' OR ChargentOrders__Type__c = 'Refund') 
                                                     AND Charge_Reason__c != 'Deductible' AND Parent_Charge_Reason__c != 'Deductible'
                                                     AND ChargentOrders__Response_Status__c = 'Approved' 
                                                     AND ChargentOrders__Order__r.Policy__c = :policyIds]) {
                                                         if (trans.ChargentOrders__Amount__c != null) {
                                                             if(trans.ChargentOrders__Type__c == 'Charge'){
                                                                 totalAmtCollected += trans.ChargentOrders__Amount__c;
                                                             } else {
                                                                 if (trans.ChargentOrders__Amount__c > 0) {
                                                                     totalAmtCollected -= trans.ChargentOrders__Amount__c;
                                                                 } else {
                                                                     totalAmtCollected += trans.ChargentOrders__Amount__c;
                                                                 }
                                                                 
                                                             }
                                                         }
                                                     }
        return totalAmtCollected;
    }
    
    public static Decimal calculateTotalAmtPaidOut(Id policyId) {
        Decimal totalAmtPaidOut = 0;
        Set<Id> policyIds = relatedPolicies(policyId);
        for (Case cs : [SELECT Id, Policy__c, Customer_Reimbursement_Amount__c, Contractor_Reimbursement_Amount__c ,
                           Customer_Authorized_Amount__c, Contractor_Authorized_Amount__c, Total_Cost_of_Parts_Ordered__c
                           FROM Case 
                           WHERE (Contractor_Reimbursement_Amount__c != null OR Customer_Reimbursement_Amount__c != null 
                                  OR Customer_Authorized_Amount__c != null OR Contractor_Authorized_Amount__c != null) 
                           AND Policy__c = :policyIds]) {
                               if (cs.Customer_Reimbursement_Amount__c != null) {
                                   totalAmtPaidOut += cs.Customer_Reimbursement_Amount__c;
                               } else if (cs.Customer_Authorized_Amount__c != null) {
                                   totalAmtPaidOut += cs.Customer_Authorized_Amount__c;
                               }
                               
                               if (cs.Contractor_Reimbursement_Amount__c != null) {
                                   totalAmtPaidOut += cs.Contractor_Reimbursement_Amount__c;
                               } else if (cs.Contractor_Authorized_Amount__c != null) {
                                   totalAmtPaidOut += cs.Contractor_Authorized_Amount__c;
                               }
                               
                               totalAmtPaidOut += cs.Total_Cost_of_Parts_Ordered__c;
                           }
        return totalAmtPaidOut;
    }
    
    private static Set<Id> relatedPolicies(Id policyId){
        Set<Id> policyIds = new Set<Id>();
        policyIds.add(policyId);
        for (Related_Policies__c rp : [SELECT Id,Current_Policy__c,Related_Policy__c FROM Related_Policies__c WHERE Current_Policy__c = :policyId OR Related_Policy__c = :policyId]) {
            policyIds.add(rp.Current_Policy__c);
            policyIds.add(rp.Related_Policy__c);
        }
        return policyIds;
    }
    
    public static void logView(String prefix, Id recordId) {
        View__c logView = new View__c();
        if (prefix == '00Q') { //Lead
            logView.Lead__c = recordId;
        } else if (prefix == '800') { //Policy
            logView.Policy__c = recordId;
        } else if (prefix == '500') { //Case
            logView.Case__c = recordId;
        } else if (prefix == '001') { //Account
            logView.Contractor__c = recordId;
        }
        logView.Viewed_By__c = UserInfo.getUserId();
        logView.Viewed_On__c = DateTime.now();
        insert logView;
    }
    
    /*public static Boolean updateOkay(Id recordId, Id userToCheck, DateTime dateToCheck) {
        String prefix = String.valueOf(recordId).left(3); 
        Id lastModifiedBy;
        DateTime lastModifiedDate;
        if (prefix == '00Q') { //Lead
            for (Lead rec : [SELECT LastModifiedById, LastModifiedDate FROM Lead WHERE Id = :recordId LIMIT 1]) {
                lastModifiedBy = rec.LastModifiedById;
                lastModifiedDate = rec.LastModifiedDate;
            }
        } else if (prefix == '800') { //Policy
            for (Contract rec : [SELECT LastModifiedById, LastModifiedDate FROM Contract WHERE Id = :recordId LIMIT 1]) {
                lastModifiedBy = rec.LastModifiedById;
                lastModifiedDate = rec.LastModifiedDate;
            }
        } else if (prefix == '500') { //Case
            for (Case rec : [SELECT LastModifiedById, LastModifiedDate FROM Case WHERE Id = :recordId LIMIT 1]) {
                lastModifiedBy = rec.LastModifiedById;
                lastModifiedDate = rec.LastModifiedDate;
            }
        } else if (prefix == '001') { //Account
            for (Account rec : [SELECT LastModifiedById, LastModifiedDate FROM Account WHERE Id = :recordId LIMIT 1]) {
                lastModifiedBy = rec.LastModifiedById;
                lastModifiedDate = rec.LastModifiedDate;
            }
        }
        if (lastModifiedBy == userToCheck && lastModifiedDate == dateToCheck) {
            return true;
        } else {
            return false;
        }
    }*/
    
    public static boolean checkCovered(Case cs){
        system.debug('cs.reason: ' + cs.reason);
        Boolean covered = false;
        
        if (cs.Id != NULL) {
            if (cs.Policy__r.Package__c == 'Platinum Care' &&
                cs.Reason == 'Ceiling Fan' || cs.Reason == 'Garage Door Opener' || cs.Reason == 'Plumbing Stoppage') {
                    covered = true;
                } else if ((cs.Policy__r.Package__c == 'Platinum Care' || cs.Policy__r.Package__c == 'Gold Care') &&
                           cs.Reason == 'A/C, Cooling' || cs.Reason == 'Ductwork' || cs.Reason == 'Electrical System' || 
                           cs.Reason == 'Heating System' || cs.Reason == 'Plumbing System' || cs.Reason == 'Water Heater') {
                               covered = true;
                           } else if ((cs.Policy__r.Package__c == 'Platinum Care' || cs.Policy__r.Package__c == 'Bronze Care') &&
                                      cs.Reason == 'Clothes Dryer' || cs.Reason == 'Clothes Washer' || cs.Reason == 'Cooktop' || 
                                      cs.Reason == 'Dishwasher' || cs.Reason == 'Garbage Disposal' || cs.Reason == 'Microwave Oven (Built In)' || 
                                      cs.Reason == 'Refrigerator' || cs.Reason == 'Stove/Oven') {
                                          covered = true;
                                      } else if ((cs.Policy__r.Additional_AC_Unit_each__c != NULL && cs.Reason == 'Additional AC Unit(each)') ||
                                                 (cs.Policy__r.Additional_Heat_Furnace_each__c != NULL && cs.Reason == 'Additional Heat/Furnace(each)') ||
                                                 (cs.Policy__r.Additional_Water_Heater_each__c != NULL && cs.Reason == 'Additional Water Heater (each)') ||
                                                 (cs.Policy__r.Central_Vacuum__c == TRUE && cs.Reason == 'Central Vacuum') ||
                                                 (cs.Policy__r.Freon__c == TRUE && cs.Reason == 'Freon') ||
                                                 (cs.Policy__r.Geo_Thermal__c == TRUE && cs.Reason == 'Geo Thermal') ||
                                                 (cs.Policy__r.Ice_Maker_In_Refrigerator__c == TRUE && cs.Reason == 'Ice Maker (In Refrigerator)') ||
                                                 (cs.Policy__r.Lawn_Sprinkler_System__c == TRUE && cs.Reason == 'Lawn Sprinkler System') ||
                                                 (cs.Policy__r.Lighting_Fixtures_Plumbing_Fixtures__c == TRUE && cs.Reason == 'Lighting Fixtures/Plumbing Fixtures') ||
                                                 (cs.Policy__r.Pool__c == TRUE && cs.Reason == 'Pool') ||
                                                 (cs.Policy__r.Roof_Leak__c == TRUE && cs.Reason == 'Roof Leak') || 
                                                 (cs.Policy__r.Salt_Water_Pool__c == TRUE && cs.Reason == 'Salt Water Pool') || 
                                                 (cs.Policy__r.Second_Refrigerator__c == TRUE && cs.Reason == 'Second Refrigerator') || 
                                                 (cs.Policy__r.Septic_System__c == TRUE && cs.Reason == 'Septic System') || 
                                                 (cs.Policy__r.Spa__c == TRUE && cs.Reason == 'Spa') || 
                                                 (cs.Policy__r.Stand_Alone_Freezer__c == TRUE && cs.Reason == 'Stand Alone Freezer') || 
                                                 (cs.Policy__r.Sump_Pump__c == TRUE && cs.Reason == 'Sump Pump') || 
                                                 (cs.Policy__r.Tankless_Water_Heater__c == TRUE && cs.Reason == 'Tankless Water Heater') || 
                                                 (cs.Policy__r.Water_Softener__c == TRUE && cs.Reason == 'Water Softener') || 
                                                 (cs.Policy__r.Well_Pump__c == TRUE && cs.Reason == 'Well Pump')) {
                                                     covered = true;
                                                 }
        } else if (cs.Policy__c != NULL) {
            Contract policy = [SELECT
                               Id,
                               Package__c,
                               Additional_AC_Unit_each__c,
                               Additional_Heat_Furnace_each__c,
                               Additional_Water_Heater_each__c,
                               Central_Vacuum__c,
                               Freon__c,
                               Geo_Thermal__c,
                               Ice_Maker_In_Refrigerator__c,
                               Lawn_Sprinkler_System__c,
                               Lighting_Fixtures_Plumbing_Fixtures__c,
                               Pool__c,
                               Roof_Leak__c,
                               Salt_Water_Pool__c,
                               Second_Refrigerator__c,
                               Septic_System__c,
                               Spa__c,
                               Stand_Alone_Freezer__c,
                               Sump_Pump__c,
                               Tankless_Water_Heater__c,
                               Water_Softener__c,
                               Well_Pump__c
                               FROM Contract
                               WHERE Id = :cs.Policy__c LIMIT 1];
            
            if (policy.Package__c == 'Platinum Care' &&
                cs.Reason == 'Ceiling Fan' || cs.Reason == 'Garage Door Opener' || cs.Reason == 'Plumbing Stoppage') {
                    covered = true;
                } else if ((policy.Package__c == 'Platinum Care' || policy.Package__c == 'Gold Care') &&
                           cs.Reason == 'A/C, Cooling' || cs.Reason == 'Ductwork' || cs.Reason == 'Electrical System' || 
                           cs.Reason == 'Heating System' || cs.Reason == 'Plumbing System' || cs.Reason == 'Water Heater') {
                               covered = true;
                           } else if ((policy.Package__c == 'Platinum Care' || policy.Package__c == 'Bronze Care') &&
                                      cs.Reason == 'Clothes Dryer' || cs.Reason == 'Clothes Washer' || cs.Reason == 'Cooktop' || 
                                      cs.Reason == 'Dishwasher' || cs.Reason == 'Garbage Disposal' || cs.Reason == 'Microwave Oven (Built In)' || 
                                      cs.Reason == 'Refrigerator' || cs.Reason == 'Stove/Oven') {
                                          covered = true;
                                      } else if ((policy.Additional_AC_Unit_each__c != NULL && cs.Reason == 'Additional AC Unit(each)') ||
                                                 (policy.Additional_Heat_Furnace_each__c != NULL && cs.Reason == 'Additional Heat/Furnace(each)') ||
                                                 (policy.Additional_Water_Heater_each__c != NULL && cs.Reason == 'Additional Water Heater (each)') ||
                                                 (policy.Central_Vacuum__c == TRUE && cs.Reason == 'Central Vacuum') ||
                                                 (policy.Freon__c == TRUE && cs.Reason == 'Freon') ||
                                                 (policy.Geo_Thermal__c == TRUE && cs.Reason == 'Geo Thermal') ||
                                                 (policy.Ice_Maker_In_Refrigerator__c == TRUE && cs.Reason == 'Ice Maker (In Refrigerator)') ||
                                                 (policy.Lawn_Sprinkler_System__c == TRUE && cs.Reason == 'Lawn Sprinkler System') ||
                                                 (policy.Lighting_Fixtures_Plumbing_Fixtures__c == TRUE && cs.Reason == 'Lighting Fixtures/Plumbing Fixtures') ||
                                                 (policy.Pool__c == TRUE && cs.Reason == 'Pool') ||
                                                 (policy.Roof_Leak__c == TRUE && cs.Reason == 'Roof Leak') || 
                                                 (policy.Salt_Water_Pool__c == TRUE && cs.Reason == 'Salt Water Pool') || 
                                                 (policy.Second_Refrigerator__c == TRUE && cs.Reason == 'Second Refrigerator') || 
                                                 (policy.Septic_System__c == TRUE && cs.Reason == 'Septic System') || 
                                                 (policy.Spa__c == TRUE && cs.Reason == 'Spa') || 
                                                 (policy.Stand_Alone_Freezer__c == TRUE && cs.Reason == 'Stand Alone Freezer') || 
                                                 (policy.Sump_Pump__c == TRUE && cs.Reason == 'Sump Pump') || 
                                                 (policy.Tankless_Water_Heater__c == TRUE && cs.Reason == 'Tankless Water Heater') || 
                                                 (policy.Water_Softener__c == TRUE && cs.Reason == 'Water Softener') || 
                                                 (policy.Well_Pump__c == TRUE && cs.Reason == 'Well Pump')) {
                                                     covered = true;
                                                 }
        }
        
        
        
        return covered;
    }

    public static void SendQuickDispatchRequest(List<Account> accounts) {
        system.debug('SendQuickDispatchRequest');

        List<Quick_Dispatch_Request__c> QDRs = new List<Quick_Dispatch_Request__c>();
        List<Task> tsks = new List<Task>();
        for (Account acct : accounts) {
            //create qdr
            Quick_Dispatch_Request__c qdr = new Quick_Dispatch_Request__c();
            qdr.Account__c = acct.Id;
            qdr.Contractor_Email__c = acct.Business_Email__c;
            QDRs.add(qdr);
        }
        
        if (!QDRs.isEmpty()) {
            insert QDRs;
            
            String siteURL = Util.siteURL();
            
            for (Quick_Dispatch_Request__c qdr : QDRs) {
                qdr.Request_URL__c = siteURL + '/qdr/?token=' + qdr.Id;
                //create a task to log the email that is sent - not using the api to send/log emails to reduce limit usage
                
                Task tsk = new Task();
                tsk.ActivityDate = Date.today();
                tsk.WhatId = qdr.Account__c;
                tsk.From__c = 'claims@selecthomewarranty.com';
                tsk.Subject = 'Email: Join our Executive Dispatch network today!';
                tsk.Description = 'Contractor sent link to quick-dispatch request page: ' + qdr.Request_URL__c;
                tsk.OwnerId = UserInfo.getUserId();
                tsk.Status = 'Completed';
                tsk.TaskSubtype = 'Email';
                tsks.add(tsk);
            }
            
            update QDRs;
            insert tsks;
        }
    }
    
    public static Boolean duringBusinessHours(Time tm) {
        system.debug('duringBusinessHours');
        
        system.debug('Time: ' + tm);
        Time strTm = Time.newInstance(8, 30, 0, 0); //8:30AM
        Time endTm = Time.newInstance(21, 0, 0, 0); //9:00PM
        
        if (tm >= strTm && tm <= endTm) {
            return TRUE;
        } else {
            return FALSE;
        }
        
    }
    
    public static Boolean duplicateClaimReason(String reason) {
        if (reason == 'A/C, cooling' ||
            reason == 'Additional AC Unit(each)' ||
            reason == 'Additional Heat/Furnace(each)' ||
            reason == 'Ceiling Fan' ||
            reason == 'Clothes Dryer' ||
            reason == 'Clothes Washer' ||
            reason == 'Cooktop' ||
            reason == 'Dishwasher' ||
            reason == 'Ductwork ' ||
            reason == 'Garbage Disposal' ||
            reason == 'Heating System' ||
            reason == 'Ice Maker (In Refrigerator)' ||
            reason == 'Microwave Oven (Built In)' ||
            reason == 'Refrigerator' ||
            reason == 'Stove/Oven') {
                return TRUE;
            } else {
                return FALSE;
            }
    }
    
    public static String getPardotEmailId(String pardotEmail) {
        String pardotEmailTemplateId = NULL;
        if (pardotEmail == 'Welcome Realtor') {
            return '49615';
        } else if (pardotEmail == 'Sample Policy') {
            return '62560';
        }
        return pardotEmailTemplateId;
    }
    
    public static Map<String, List<String>> getDependentPicklistValues(Schema.sObjectField dependToken) {
        Schema.DescribeFieldResult depend = dependToken.getDescribe();
        Schema.sObjectField controlToken = depend.getController();
        if (controlToken == null) {
            return new Map<String, List<String>>();
        }
        
        Schema.DescribeFieldResult control = controlToken.getDescribe();
        List<Schema.PicklistEntry> controlEntries;
        if(control.getType() != Schema.DisplayType.Boolean) {
            controlEntries = control.getPicklistValues();
        }
        
        String base64map = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
        Map<String,List<String>> dependentPicklistValues = new Map<String,List<String>>();
        for (Schema.PicklistEntry entry : depend.getPicklistValues()) {
            if (entry.isActive() && String.isNotEmpty(String.valueOf(((Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(entry))).get('validFor')))) {
                List<String> base64chars =
                    String.valueOf(((Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(entry))).get('validFor')).split('');
                for (Integer index = 0; index < (controlEntries != null ? controlEntries.size() : 2); index++) {
                    Object controlValue =
                        (controlEntries == null
                         ?   (Object) (index == 1)
                         :   (Object) (controlEntries[index].isActive() ? controlEntries[index].getLabel() : null)
                        );
                    Integer bitIndex = index / 6;
                    if (bitIndex > base64chars.size() - 1) {
                        break;
                    }
                    Integer bitShift = 5 - Math.mod(index, 6);
                    if  (controlValue == null || (base64map.indexOf( base64chars[ bitIndex ] ) & (1 << bitShift)) == 0)
                        continue;
                    if (!dependentPicklistValues.containsKey((String) controlValue)) {
                        dependentPicklistValues.put((String) controlValue, new List<String>());
                    }
                    dependentPicklistValues.get((String) controlValue).add(entry.getLabel());
                }
            }
        }
        return dependentPicklistValues;
    }
    
    /*
    @future(callout=true)
    public static void createRemoteSiteSettings(String name, String url) {   
        MetadataService.MetadataPort service = createService();
        MetadataService.RemoteSiteSetting remoteSiteSettings = new MetadataService.RemoteSiteSetting();
        remoteSiteSettings.fullName = name;
        remoteSiteSettings.url = url;
        remoteSiteSettings.isActive=true;
        remoteSiteSettings.disableProtocolSecurity=false;
        MetadataService.AsyncResult[] results = service.create(new List<MetadataService.Metadata> { remoteSiteSettings });
        MetadataService.AsyncResult[] checkResults = service.checkStatus(new List<string> {string.ValueOf(results[0].Id)});
        system.debug('chk' + checkResults );
    }
    
    public static MetadataService.MetadataPort createService() {
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = UserInfo.getSessionId();
        return service;
    }*/
}