@isTest
private class ViewsCleanupTest {
    private static DataFactory df = new DataFactory();
    
    @isTest 
    static void testBatch() {
        df.createGlobalSetting();
        df.createLead();
        df.createPolicy();
        df.createCase();
       	        
        List<View__c> views = new List<View__c>();
        View__c v1 = new View__c(
        	Contractor__c = df.acc.Id,
            CreatedDate = DateTime.now().addDays(-60),
            Viewed_On__c = DateTime.now().addDays(-60)
        );
        views.add(v1);
        View__c v2 = new View__c(
        	Lead__c = df.newLead.Id,
            CreatedDate = DateTime.now().addDays(-60),
            Viewed_On__c = DateTime.now().addDays(-60)
        );
        views.add(v2);
        View__c v3 = new View__c(
        	Case__c = df.newCase.Id,
            CreatedDate = DateTime.now().addDays(-60),
            Viewed_On__c = DateTime.now().addDays(-60)
        );
        views.add(v3);
        View__c v4 = new View__c(
        	Policy__c = df.newPolicy.Id,
            CreatedDate = DateTime.now().addDays(-60),
            Viewed_On__c = DateTime.now().addDays(-60)
        );
        views.add(v4);
        INSERT views;
        
        Test.StartTest();
        ViewsCleanup_schedule sh1 = new ViewsCleanup_schedule();
        String sch = '0 0 23 * * ?'; system.schedule('Test', sch, sh1);
        ViewsCleanup_batch batch = new ViewsCleanup_batch();
        ID batchprocessid = Database.executeBatch(batch,50);
        Test.StopTest();
    }
}