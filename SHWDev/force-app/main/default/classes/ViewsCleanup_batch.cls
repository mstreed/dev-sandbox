global class ViewsCleanup_batch implements Database.Batchable<SObject> {
    global Database.QueryLocator start(Database.BatchableContext Bc){
        return Database.getQueryLocator([SELECT Id, Contractor__c, Lead__c, Case__c, Policy__c, Type__c, Viewed_On__c FROM View__c]);
    }

    global void execute(Database.BatchableContext Bc, List<View__c> scope){
        Savepoint sp = Database.setSavepoint();
        
        List<View__c> deleteViews = new List<View__c>();
        
        try {
            for (View__c rec : scope) {
                if (rec.Contractor__c != null) {
                    deleteViews.add(rec);
                } else if (rec.Lead__c != null && rec.Viewed_On__c <= DateTime.now().addDays(-30)) {
                    deleteViews.add(rec);
                } else if (rec.Case__c != null && rec.Viewed_On__c <= DateTime.now().addDays(-30)) {
                    deleteViews.add(rec);
                } else if (rec.Policy__c != null && rec.Viewed_On__c <= DateTime.now().addDays(-30)) {
                    deleteViews.add(rec);
                }
            }
            
            if (!deleteViews.isEmpty()) {
                Database.delete(deleteViews);
            }
            
        } catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
            Database.rollback(sp);
        }
    }

    global void finish(Database.BatchableContext Bc){

    }

}