global class ViewsCleanup_schedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        ViewsCleanup_batch batchRun = new ViewsCleanup_batch(); 
        ID batchId = Database.executeBatch(batchRun,5000);
    }
}