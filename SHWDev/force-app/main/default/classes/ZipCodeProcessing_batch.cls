global class ZipCodeProcessing_batch implements Database.Batchable<sObject>, Database.Stateful {
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([SELECT Id, City__c, State__c, County__c, AreaCode__c, CityType__c, CityAliasAbbreviation__c, CityAliasName__c, Geolocation__Latitude__s, Geolocation__Longitude__s, 
                                         TimeZone__c, Elevation__c, CountyFIPS__c, DayLightSaving__c, PreferredLastLineKey__c, ClassificationCode__c, MultiCounty__c, StateFIPS__c, CityStateKey__c,
                                         CityAliasCode__c, PrimaryRecord__c, CityMixedCase__c, CityAliasMixedCase__c, StateANSI__c, CountyANSI__c, FacilityCode__c, CityDeliveryIndicator__c, CarrierRouteRateSortation__c,
                                         FinanceNumber__c, UniqueZIPName__c, CountyMixedCase__c, ZipCode__c 
                                         FROM Zip_Code_Staging__c]);
    }

    global void execute(Database.BatchableContext BC, List<Zip_Code_Staging__c> scope) {
        List<Zip_Code__c> zipCodeList = new List<Zip_Code__c>();
        
        for (Zip_Code_Staging__c rec : scope) {
            if (rec.PrimaryRecord__c == 'P') {
                zipCodeList.add(new Zip_Code__C(
                    Name = rec.ZipCode__c,
                    City__c = rec.City__c, 
                    State__c = rec.State__c,
                    County__c = rec.County__c,
                    AreaCode__c = rec.AreaCode__c,
                    CityType__c = rec.CityType__c,
                    CityAliasAbbreviation__c = rec.CityAliasAbbreviation__c,
                    CityAliasName__c = rec.CityAliasName__c,
                    Geolocation__Latitude__s = rec.Geolocation__Latitude__s,
                    Geolocation__Longitude__s = rec.Geolocation__Longitude__s, 
                    TimeZone__c = rec.TimeZone__c, 
                    Elevation__c = rec.Elevation__c,
                    CountyFIPS__c = rec.CountyFIPS__c,
                    DayLightSaving__c = rec.DayLightSaving__c,
                    PreferredLastLineKey__c = rec.PreferredLastLineKey__c,
                    ClassificationCode__c = rec.ClassificationCode__c,
                    MultiCounty__c = rec.MultiCounty__c,
                    StateFIPS__c = rec.StateFIPS__c,
                    CityStateKey__c = rec.CityStateKey__c,
                    CityAliasCode__c = rec.CityAliasCode__c,
                    CityMixedCase__c = rec.CityMixedCase__c,
                    CityAliasMixedCase__c = rec.CityAliasMixedCase__c,
                    StateANSI__c = rec.StateANSI__c,
                    CountyANSI__c = rec.CountyANSI__c,
                    FacilityCode__c = rec.FacilityCode__c,
                    CityDeliveryIndicator__c = rec.CityDeliveryIndicator__c,
                    CarrierRouteRateSortation__c = rec.CarrierRouteRateSortation__c,
                    FinanceNumber__c = rec.FinanceNumber__c,
                    UniqueZIPName__c = rec.UniqueZIPName__c,
                    CountyMixedCase__c = rec.CountyMixedCase__c,
                    UID__c = rec.ZipCode__c + rec.PreferredLastLineKey__c + rec.CityStateKey__c
                ));
            }
        }
        
        if (!zipCodeList.isEmpty()) {
            UPSERT zipCodeList UID__c;
        }
        
        DELETE scope;
    }   

    global void finish(Database.BatchableContext BC) {
        
    }
}