global class ZipCodeProcessing_schedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        ZipCodeProcessing_batch batchRun = new ZipCodeProcessing_batch(); 
        ID batchId = Database.executeBatch(batchRun,1000);
    }
}