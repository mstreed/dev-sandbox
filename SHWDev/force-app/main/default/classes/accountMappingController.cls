global without sharing class accountMappingController {
    private static Global_Setting__c gs = [Select Id, NSA_Contractor_ID__c,Sears_Contractor_ID__c, GE_Contractor_Id__c FROM Global_Setting__c LIMIT 1];
    
    public Map<String, Double> userLocation { get; private set; }  
    
    public List<Account> accounts { get; set; } 
    public List<Account> accountsToMap { get; set; } 
    public String googleURL { get; set; }
    public String autoDeployto { get; set; }
    public String nsaURL { get; set; }
    public String nsaLink { get; set; }
    public String street{ get; set; }
    public String suite{ get; set; }
    public String city{ get; set; }
    public String state{ get; set; }
    public String zipCode {get;set;}
    public String country {get;set;}
    public string CusLat {get;set;}
    public string CusLong {get;set;}
    public string CaseAccountId {get;set;}
    public String caseServiceProvided;
    //public Work_Order__c workOrders;
    public Work_Order__c workOrder {get;set;}
    public boolean isMapShow{get;set;}
    public Id accId;
    public Id caseId {get;set;}
    public String nsaRecall {get;set;}
    public String previousCons {get;set;}
    public Set<String> geZips = new Set<String>();
    public Case cs {get;set;}
    public boolean showPopup {get;set;}
    public String glPolicy;
    public List<AccountWrapper> conWrappers { get; private set; }
    public Id accountRecordTypeId;
    public Set<String> contractIds;
    //public Decimal customersservicecallcharge{get;set;}
    private String service;
    public Set<String> dispatchTo = new Set<String>();
    global String sortValue{get;set;}
    
    public String searchvalue{get;set;}
    public List<String> listOfaccountName{get;set;}
    
    public String serializedList {get;set;}
    public Enum SORT_DIRECTION {
        Ascending,
            Descending
            }
    public static SORT_DIRECTION sortDirection{get;set;}
    
    global String sortField{get;set;}
    public Enum SORT_FIELD {
        Score,
            Distance
            }
    
    /*QuickDispatch,
Distance,
Priority*/
    public static SORT_FIELD sortFieldEnum{get;set;}
    
    public accountMappingController (){
        conWrappers = new List<AccountWrapper>();
        contractIds = new Set<String>();
        previousCons = '';
        searchvalue = '';
        showPopup = false;
        listOfaccountName = new List<String>();
        isMapShow = false;   
        sortDirection = SORT_DIRECTION.Descending;
        sortValue = 'Descending';
        sortField = 'Score'; //QuickDispatch
        //workOrders = new Work_Order__c();
        accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Contractor Account').getRecordTypeId();
        Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=10');
        List<String>  coverageItem = new List<String> ();
        Set<String> coverageContractors = new Set<String> ();
        if(apexpages.currentpage().getparameters().get('id') != null && apexpages.currentpage().getparameters().get('id') != ''){
            caseId  = apexpages.currentpage().getparameters().get('id');
            nsaRecall  = apexpages.currentpage().getparameters().get('nsaRecall');
            
            cs = [SELECT Id,Contractor__r.Name, Status, CaseNumber, Dispatch_To__c, NSA_Link_Open__c, Reason, Service_Call_Fee_Final__c, Trip_Charge__c, 
                  Coverage_Trades__c, Brand__c, Coverage_Items__c,Service_Provided__c, Policy__r.BillingLatitude, Policy__r.BillingLongitude, Policy__r.BillingStreet, 
                  Policy__r.BillingCountry, Policy__r.BillingCity, Policy__r.BillingState, Policy__r.BillingPostalCode, Property_Suite__c, Contractor__c,
                 (SELECT Id, Status__c, Contractor__c FROM Work_Orders__r) 
                  FROM Case WHERE Id = :caseId LIMIT 1];
            if(cs.Coverage_Items__c != Null){
               coverageItem = cs.Coverage_Items__c.split(';');
            }
            
            
            System.debug('cs.Coverage_Items__c  : '+ cs.Coverage_Items__c);
            for(Coverage_Item__c convItem : [SELECT Id,Contractor__c, Trade__c, Item__c FROM Coverage_Item__c
                                Where Item__c IN: coverageItem]){
                coverageContractors.add(convItem.Contractor__c);
            }
            System.debug('coverageContractors  : '+ coverageContractors);
            // Find the contractors zip code area where they provide services
            for (Coverage_Area__c ca :[SELECT Zip_Code__r.Name,Contractor__c 
                                       FROM Coverage_Area__c 
                                       WHERE Active__c = TRUE AND Contractor__c IN : coverageContractors
                                       ])
            {
                if(ca.Zip_Code__r.Name == cs.Policy__r.BillingPostalCode){
                    contractIds.add(ca.Contractor__c);
                     geZips.add(ca.Zip_Code__r.Name);
                }
            }
            System.debug('contractIds  : '+ contractIds);
            cs = [SELECT Id, Status, CaseNumber, Dispatch_To__c, NSA_Link_Open__c, Reason, Service_Call_Fee_Final__c, Trip_Charge__c, 
                  Coverage_Trades__c, Brand__c, Service_Provided__c, Policy__r.BillingLatitude, Policy__r.BillingLongitude, Policy__r.BillingStreet, Policy__r.BillingCountry, Policy__r.BillingCity, Policy__r.BillingState, Policy__r.BillingPostalCode, Property_Suite__c, Contractor__c, (SELECT Id, Status__c, Contractor__c FROM Work_Orders__r) 
                  FROM Case WHERE Id = :caseId LIMIT 1];
            
            for (Work_Order__c wo : cs.Work_Orders__r) {
                previousCons += wo.Contractor__c;
            }
            System.debug('geZips ::  ' + geZips);
            nsaLink = cs.NSA_Link_Open__c;
            street = cs.Policy__r.BillingStreet;
            suite = cs.Property_Suite__c;
            city = cs.Policy__r.BillingCity;
            state = cs.Policy__r.BillingState;
            zipCode = cs.Policy__r.BillingPostalCode;
            country = cs.Policy__r.BillingCountry;
            service = cs.Service_Provided__c;
            dispatchTo.add(cs.Dispatch_To__c);

            if (cs.Reason == 'Garbage Disposal' || cs.Reason == 'Lighting Fixtures/Plumbing Fixtures' || cs.Reason == 'Septic System' || cs.Reason == 'Water Heater') {
                service = 'Plumber';
            }
            
            if (cs.Service_Provided__c != null) {
                system.debug('cs.Service_Provided__c:' + cs.Service_Provided__c);
                if (cs.Service_Provided__c.contains(';')) {
                    for (String sp : cs.Service_Provided__c.split(';')) {
                        if (caseServiceProvided == null) {
                            caseServiceProvided = '\'' + sp + '\'';
                        } else {
                            caseServiceProvided = caseServiceProvided + ',\'' + sp + '\'';
                        }
                    }
                } else {
                    caseServiceProvided = '\'' + cs.Service_Provided__c + '\'';
                }
            } else {
                caseServiceProvided = '\'\'';
            }
            
            system.debug('caseServiceProvided1: ' + caseServiceProvided);
            
            //customersservicecallcharge = cs.Service_Call_Fee_Final__c;
        }
    }
    
    public String currentPosition {
        get {
            if(String.isBlank(street) && String.isBlank(city) && String.isBlank(state) && String.isBlank(zipCode)){
                if (String.isBlank(currentPosition)) {
                    currentPosition = '0,0';
                }
            }
            return currentPosition;
        }
        set;
    }
    
    public Boolean resultsAvailable {
        get {
            if(accounts == NULL) {
                return false;
            }
            return true;
        }
        
        set;
    }
    public void closePopup(){
        showPopup = false;
    }
    public PageReference dispatchWorkOrder() {
        system.debug('dispatchWorkOrder');
        
        accId = apexpages.currentpage().getparameters().get('accId');
        System.debug('accId^^' + accId);
        System.debug('gs.Sears_Contractor_ID__c^^' + gs.Sears_Contractor_ID__c);
        glPolicy = apexpages.currentpage().getparameters().get('glPolicy');
        System.debug('glPolicy^^' + glPolicy);
        if(accId != null){
            if(accId == gs.Sears_Contractor_ID__c){
                showPopup = true;    
            }
            try{
                workOrder = new Work_Order__c();
                system.debug('caseId: ' + caseId);
                system.debug('accId: ' + accId);
                if (accId == gs.NSA_Contractor_ID__c) {
                    return dispatchToNSA();
                } else {
                    workOrder = quickDispatch(caseId, accId);
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Contractor assigned successfully to ' + [SELECT Name FROM Work_Order__c WHERE Id = :workOrder.Id LIMIT 1].Name));
                    if (glPolicy == 'FALSE') {
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Contractor does not have a GL Policy. Please reach out to contractor to submit the policy.'));
                    }
                    
                    return NULL;
                }
            } catch(Exception e) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
            }
        }
        return NULL; 
    }
    
    public PageReference dispatchToNSA() {
        system.debug('dispatchToNSA');
        system.debug('caseId: ' + caseId);
        system.debug('nsaURL: ' + nsaURL);
        workOrder = quickDispatch(caseId, gs.NSA_Contractor_ID__c);
        nsaURL+= '&caseNumber=' + cs.CaseNumber + '-' + workOrder.Sequence__c;
        nsaURL+= '&transactionID=' + cs.CaseNumber + '-' + workOrder.Sequence__c;
        system.debug('cs.CaseNumber-workOrder.Sequence__c: ' + cs.CaseNumber + '-' + workOrder.Sequence__c);
        system.debug('nsaRecall: ' + nsaRecall);
        if (nsaRecall == 'y') {
            nsaURL+= '&suspectedRecall=1';
        }
        system.debug('nsaURL2: ' + nsaURL);
        PageReference pageRef = new PageReference(nsaURL);
        return pageRef;
    } 
    
    public PageReference dispatchToGE() {
        system.debug('dispatchToGE');
        workOrder = quickDispatch(caseId, gs.GE_Contractor_ID__c);
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'GE assigned successfully to WorkOrder.'));
        
        PageReference pageRef = new PageReference('/' + caseId); 
        pageRef.setRedirect(true); 
        return pageRef;
    } 
    
    public void findNearby() {
        system.debug('findNearby');
        
        isMapShow = true;
        String lat, lon;
        
        List<String> latlon;
        
        latlon = currentPosition.split(',');
        lat = latlon[0].trim();
        lon = latlon[1].trim(); 
        userLocation = new Map<String, Double>
        {
          'latitude' =>  decimal.valueOf(lat),
           'longitude' =>  decimal.valueOf(lon)
         };
                    
                    
        accounts = new List<Account>(); 
        accountsToMap = new List<Account>();
        
        //Limited to 99 Results, UPDATE LIMIT code Lines to change results ratio
        System.debug('lat :: ' + lat  + '  lon :: ' + lon);
        System.debug('userLocation :: ' + userLocation);
        String tempInput ='\'%' + String.escapeSingleQuotes(searchvalue)  + '%\'';
        String whereClause = ''; 
        if (searchvalue != null && searchvalue != '') {
            whereClause = 'Name Like '+ tempInput+ ' AND ';
        }
        
        // SOQL query to get Accounts
        String queryString1 = 
            'SELECT Id,NSA__c,Ranked__c, Dispatch_Method_New__c, Priority_New__c, Name,(SELECT Id,Overall_Score__c FROM Contractor_Rankings__r WHERE Service__c =\''+service+'\'), Type,Services_Provided__c, Business_Email__c,Account_Status__c,First_Hour_Labor_Rate__c ,Description, Rating__c,DISTANCE(ShippingAddress, GEOLOCATION('+lat+','+lon+'), \'mi\') distance, Phone, ShippingLatitude,ShippingLongitude,ShippingStreet, ShippingCity, ShippingState, Approved_for_Quick_Dispatch__c, Average_Rating__c, New__c, GL_Policy__c ' +
            'FROM Account ' +
            'WHERE '+whereClause+' Services_Provided__c includes ( '+ caseServiceProvided +' ) AND Account_Status__c != \'Inactive\' AND Account_Status__c != \'Provider Declined SHW\' AND RecordTypeId = :accountRecordTypeId AND ';
        if (cs.Dispatch_To__c == 'NSA') {
            contractIds.add(gs.NSA_Contractor_ID__c);
            queryString1+= '((Id IN : contractIds) OR (ShippingLatitude !=null AND ShippingLongitude !=null AND DISTANCE(ShippingAddress, GEOLOCATION('+lat+','+lon+'), \'mi\')<50)) ';                
        } else if(contractIds.size() > 0){
            queryString1+= '((Id IN : contractIds) OR (ShippingLatitude !=null AND ShippingLongitude !=null AND DISTANCE(ShippingAddress, GEOLOCATION('+lat+','+lon+'), \'mi\')<50)) ';
            System.debug('((Id IN : contractIds) OR (ShippingLatitude !=null AND ShippingLongitude !=null AND DISTANCE(ShippingAddress, GEOLOCATION('+lat+','+lon+'), \'mi\')<50)) ');
              
        } else {
            queryString1+= '(ShippingLatitude !=null AND ShippingLongitude !=null AND DISTANCE(ShippingAddress, GEOLOCATION('+lat+','+lon+'), \'mi\')<50) ';
        }
        system.debug('queryString1b: ' + queryString1);
        
        queryString1+= 'ORDER BY Ranked__c DESC, DISTANCE(ShippingAddress, GEOLOCATION('+lat+','+lon+'), \'mi\') NULLS LAST LIMIT 150';
        
        system.debug('queryString1c: ' + queryString1);
        // run query
        accounts = database.Query(queryString1);
        for (Account record : accounts){
            if (record.Id != gs.NSA_Contractor_ID__c) {
                accountsToMap.add(record);
            }
            Decimal score = 0;
            for (Contractor_Ranking__c cr : record.Contractor_Rankings__r) {
                score = cr.Overall_Score__c;
            }
            System.debug('score^^' + score);
                if(score >= 400){
                conWrappers.add(new AccountWrapper(record,score));    
                }
            if(!listOfaccountName.contains(record.Name) )
                listOfaccountName.add(record.Name);
        }
        
        serializedList = JSON.serialize(listOfaccountName );
        
        if(sortValue.contains('Ascending')){
            sortDirection = SORT_DIRECTION.Ascending;
        }else{
            sortDirection = SORT_DIRECTION.Descending;
        }
        
        /*if(sortValue.contains('Priority')){
        sortFieldEnum = SORT_FIELD.Priority;
        }else if(sortValue.contains('QuickDispatch')){
        sortFieldEnum = SORT_FIELD.QuickDispatch;
        }else */if(sortValue.contains('Score')){
            sortFieldEnum = SORT_FIELD.Score;
        }else{
            sortFieldEnum = SORT_FIELD.Distance;
        }
        
        conWrappers.sort();
        if (0 == accounts.size()) 
            System.debug('No results. Query: ' + queryString1);
        
        setAutoDeployFlag();
    }
    
    //This method is a copy of findNearby() method with the only difference that current
    //location is calculated on the basis of street, city, and state field on the UI
    public void findbyNewCurrentLocation() {
        system.debug('findbyNewCurrentLocation');
        System.debug('');
        if(CusLat != '' && CusLong != '' && CusLong != null && CusLat != null)
            isMapShow = true;
        
        try{
            //check if NSA or GE
            setAutoDeployFlag();

            String lat, lon;
            
            accounts = new List<Account>();
            accountsToMap = new List<Account>();
            system.debug('CusLat: ' + CusLat);
            system.debug('CusLong: ' + CusLong);
            
            currentPosition = CusLat +','+ CusLong;
            lat = CusLat.trim();
            lon = CusLong.trim();
            
            userLocation = new Map<String, Double>
            {
                'latitude' =>  decimal.valueOf(lat),
                'longitude' =>  decimal.valueOf(lon)
             };
            String tempInput ='\'%' + String.escapeSingleQuotes(searchvalue)  + '%\'';
            String whereClause = ''; 
            if(searchvalue != null && searchvalue != '') {
                whereClause = ' Name Like '+ tempInput+ ' AND ';
            } 
            
            system.debug('caseServiceProvided2:' + caseServiceProvided);
             
            
            String queryString1 = '';
            //if(workOrderId != null){
            queryString1 = 
                'SELECT Id,NSA__c,Ranked__c,Dispatch_Method_New__c, Priority_New__c, Name,(SELECT Id,Overall_Score__c FROM Contractor_Rankings__r WHERE Service__c =\''+service+'\'), Type,Services_Provided__c,Business_Email__c,Account_Status__c,First_Hour_Labor_Rate__c ,Description, Rating__c,DISTANCE(ShippingAddress, GEOLOCATION('+lat+','+lon+'), \'mi\') distance, Phone,ShippingLatitude,ShippingLongitude,ShippingStreet,ShippingPostalCode, ShippingCity, ShippingState, Approved_for_Quick_Dispatch__c, Average_Rating__c, New__c, GL_Policy__c '+ 
                'FROM Account ' +
                'WHERE '+whereClause+' Services_Provided__c includes ( '+ caseServiceProvided +' )  AND Account_Status__c != \'Inactive\' AND Account_Status__c != \'Provider Declined SHW\' AND RecordTypeId = :accountRecordTypeId AND ';
            
            if (autoDeployto == 'NSA') {
                contractIds.add(gs.NSA_Contractor_ID__c);
                queryString1+= '(Id IN : contractIds)';
                System.debug('(Id = \''+gs.NSA_Contractor_ID__c+'\') ');
            } else if (autoDeployto == 'GE') {
                contractIds.add(gs.GE_Contractor_ID__c);
                queryString1+= '(Id IN : contractIds)';
                System.debug('Id IN : contractIds');
            } else if (dispatchTo.contains('NSA') && dispatchTo.contains('GE')) {
                contractIds.add(gs.NSA_Contractor_ID__c);
                contractIds.add(gs.GE_Contractor_ID__c);
                queryString1+= '((Id IN : contractIds) OR (ShippingLatitude !=null AND ShippingLongitude !=null AND DISTANCE(ShippingAddress, GEOLOCATION('+lat+','+lon+'), \'mi\')<50)) ';
                System.debug('((Id IN : contractIds) OR (Id = \''+gs.NSA_Contractor_ID__c+'\') OR (ShippingLatitude !=null AND ShippingLongitude !=null AND DISTANCE(ShippingAddress, GEOLOCATION('+lat+','+lon+'), \'mi\')<50)) ');
            } else if (dispatchTo.contains('NSA')) {
                contractIds.add(gs.NSA_Contractor_ID__c);
                queryString1+= '((Id IN : contractIds) OR (ShippingLatitude !=null AND ShippingLongitude !=null AND DISTANCE(ShippingAddress, GEOLOCATION('+lat+','+lon+'), \'mi\')<50)) ';
                System.debug('((Id IN : contractIds) OR (ShippingLatitude !=null AND ShippingLongitude !=null AND DISTANCE(ShippingAddress, GEOLOCATION('+lat+','+lon+'), \'mi\')<50)) ');
            } else if (dispatchTo.contains('GE')) {
                contractIds.add(gs.GE_Contractor_ID__c);
                queryString1+= '((Id IN : contractIds) OR (ShippingLatitude !=null AND ShippingLongitude !=null AND DISTANCE(ShippingAddress, GEOLOCATION('+lat+','+lon+'), \'mi\')<50)) ';
                System.debug('((Id IN : contractIds) OR (ShippingLatitude !=null AND ShippingLongitude !=null AND DISTANCE(ShippingAddress, GEOLOCATION('+lat+','+lon+'), \'mi\')<50)) ');
            } else if(contractIds.size() > 0){
                queryString1+= '((Id IN : contractIds) OR (ShippingLatitude !=null AND ShippingLongitude !=null AND DISTANCE(ShippingAddress, GEOLOCATION('+lat+','+lon+'), \'mi\')<50)) ';
                System.debug('((Id IN : contractIds) OR (ShippingLatitude !=null AND ShippingLongitude !=null AND DISTANCE(ShippingAddress, GEOLOCATION('+lat+','+lon+'), \'mi\')<50)) '); 
            } else {
                queryString1+= '(ShippingLatitude !=null AND ShippingLongitude !=null AND DISTANCE(ShippingAddress, GEOLOCATION('+lat+','+lon+'), \'mi\')<50) ';
                System.debug('(ShippingLatitude !=null AND ShippingLongitude !=null AND DISTANCE(ShippingAddress, GEOLOCATION('+lat+','+lon+'), \'mi\')<50) ');
            }
           
            queryString1+= 'ORDER BY Ranked__c DESC NULLS LAST, DISTANCE(ShippingAddress, GEOLOCATION('+lat+','+lon+'), \'mi\') NULLS LAST LIMIT 150';
            system.debug('query1');
            
            // run query
            system.debug(queryString1);
            accounts = database.Query(queryString1);
            System.debug('accounts ::  ' + accounts);
            // for retundancy prevent
            conWrappers = new List<AccountWrapper>();
            for (Account record : accounts)
            {
                // ignore Sears a/c if they don't trade 
                if(record.Id == gs.Sears_Contractor_ID__c && !contractIds.contains(record.Id) ){
                    continue;
                }
                // Ignore case of NSA 
                if(record.Id == gs.NSA_Contractor_ID__c && !contractIds.contains(record.Id) ){
                    continue;
                }
                accountsToMap.add(record);
                
                system.debug('record.Services_Provided__c:' + record.Services_Provided__c);
                Decimal score = 0;
                for (Contractor_Ranking__c cr : record.Contractor_Rankings__r) {
                    score = cr.Overall_Score__c;
                }
                System.debug('score^^' + score);
                if(score >= 400){
                conWrappers.add(new AccountWrapper(record,score));    
                }
                if(!listOfaccountName.contains(record.Name) )
                    listOfaccountName.add(record.Name); 
            }
            
            serializedList = JSON.serialize(listOfaccountName );
            if(sortValue.contains('Ascending')){
                sortDirection = SORT_DIRECTION.Ascending;
            }else{
                sortDirection = SORT_DIRECTION.Descending;
            }
            
            /*if(sortField.contains('Priority')){
sortFieldEnum = SORT_FIELD.Priority;
}else if(sortField.contains('QuickDispatch')){
sortFieldEnum = SORT_FIELD.QuickDispatch;
}else */if(sortField.contains('Score')){
    sortFieldEnum = SORT_FIELD.Score;
}else{
    sortFieldEnum = SORT_FIELD.Distance;
}
            
            system.debug('sortValue: ' + sortValue);
            system.debug('sortDirection: ' + sortDirection);
            system.debug('SORT_DIRECTION.Descending: ' + SORT_DIRECTION.Descending);
            system.debug('sortFieldEnum: ' + sortFieldEnum);
            system.debug('SORT_FIELD.Score: ' + SORT_FIELD.Score);
            
            conWrappers.sort();
            if (0 == accounts.size()) 
                System.debug('No results. Query: ' + queryString1); 
            
            //recheck
            setAutoDeployFlag();
        }
        catch(exception e){ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'NotFound')); system.debug('Error'+ e);return;}
    }
    
    //returns location data on the condition of whether street field is empty or not on UI
    public PageReference redirectByLocation(){
        system.debug('street: ' + street);
        system.debug('city: ' + city);
        system.debug('state: ' + state);
        system.debug('zipCode: ' + zipCode);
        system.debug('country: ' + country);
        
        conWrappers = new List<AccountWrapper>();
        if(!String.IsBlank(street) && !String.IsBlank(city) && !String.IsBlank(state) && !String.IsBlank(zipCode) && !String.IsBlank(country)){
            findbyNewCurrentLocation();
        }
        else{
            system.debug('Yes I am in nearby');
            findNearby();
        }
        
        return NULL;
    }
    
    public void setAutoDeployFlag() {
        autoDeployto = NULL;
        system.debug('geZips: ' + geZips);
        system.debug('zipCode: ' + zipCode);
                
        if (nsaRecall == 'y') {
            system.debug('nsaRecall');
            autoDeployto = 'NSA';
            dispatchTo.add('NSA');
        }
        System.debug('cs.Coverage_Trades__c :: ' + cs.Coverage_Trades__c);
        System.debug('geZips :: ' + geZips);
        if (cs.Coverage_Trades__c != null && cs.Coverage_Trades__c.contains('Appliances') && geZips != null &&  geZips.contains(zipCode) &&
            (cs.Brand__c == 'GE (General Electric)' || cs.Brand__c == 'Ge (General Electric) Monogram' || cs.Brand__c == 'Haier' || cs.Brand__c == 'Hot Point')) {
                system.debug('ge1');
                system.debug('cs.Status: ' + cs.Status);
                system.debug('autoDeployto: ' + autoDeployto);

                if (cs.Status == 'Ready to Dispatch' && autoDeployto == null) {
                    system.debug('ge2');
                    autoDeployto = 'GE';
                }
            dispatchTo.add('GE');
        }
        
        if ((!conWrappers.isEmpty() && conWrappers[0].record.Id == gs.NSA_Contractor_ID__c)) {
            system.debug('NSA');

            if (cs.Status == 'Ready to Dispatch' && autoDeployto == null) {
                autoDeployto = 'NSA';
            }
            dispatchTo.add('NSA');
        }
        
        system.debug('autoDeployto: ' + autoDeployto);
        

    }
    
    global class AccountWrapper implements Comparable {
        public Account record { get; private set; }
        public Boolean quickDispatchBool { get; private set; }
        public Decimal distance { get; private set; }
        public Decimal score { get; private set; }
        public Integer priority { get; private set; }
        public AccountWrapper(Account record, Decimal score)
        {
            this.record = record;
            system.debug('Account: ' + record.Name);
            this.quickDispatchBool = record.Approved_for_Quick_Dispatch__c;
            this.distance = (Decimal)record.get('distance');            
            this.score = score;
            this.priority = record.Priority_New__c == TRUE ? 1 : 0;
        }
        global Integer compareTo(Object compareTo) {
            AccountWrapper compareToEmp = (AccountWrapper)compareTo;
            /*if(sortFieldEnum == SORT_FIELD.QuickDispatch) {
system.debug('quickDispatchBool' + quickDispatchBool);
if (compareToEmp.quickDispatchBool == false) {
if (distance == compareToEmp.distance) {
return -2;
}
if (distance > compareToEmp.distance  && sortDirection == SORT_DIRECTION.Ascending) {
return -1;
}
if (distance < compareToEmp.distance  && sortDirection == SORT_DIRECTION.Descending) {
return -1;
}
return -3;
}
if (compareToEmp.quickDispatchBool == true) {
if (distance == compareToEmp.distance) {
return 1;
}
if (distance > compareToEmp.distance  && sortDirection == SORT_DIRECTION.Ascending) {
return 2;
}
if (distance < compareToEmp.distance  && sortDirection == SORT_DIRECTION.Descending) {
return 2;
}
return 0;
}
return 0;
} else */if (sortFieldEnum == SORT_FIELD.Distance) {
    if (distance == compareToEmp.distance) { //closest
        return 0;
    }
    if (distance > compareToEmp.distance  && sortDirection == SORT_DIRECTION.Ascending) {
        return 1;
    }
    if (distance < compareToEmp.distance  && sortDirection == SORT_DIRECTION.Descending) {
        return 1;
    }
    return -1;
} else if(sortFieldEnum == SORT_FIELD.Score) {
    /*if (score == NULL) { //Doesn't have score, just order by distance
system.debug('no score');
if (distance == compareToEmp.distance) {
return -2;
}
if (distance > compareToEmp.distance  && sortDirection == SORT_DIRECTION.Ascending) {
return -1;
}
if (distance < compareToEmp.distance  && sortDirection == SORT_DIRECTION.Descending) {
return -1;
}
return -3;
}*/
    if (score == compareToEmp.score) {
        //return 1;
        if (distance == compareToEmp.distance) {
            return -2;
        }
        if (distance > compareToEmp.distance  && sortDirection == SORT_DIRECTION.Ascending) {
            return -1;
        }
        if (distance < compareToEmp.distance  && sortDirection == SORT_DIRECTION.Descending) {
            return -1;
        }
        return -3;
    }
    if (score > compareToEmp.score  && sortDirection == SORT_DIRECTION.Ascending) {
        return 2;
    }
    if (score < compareToEmp.score  && sortDirection == SORT_DIRECTION.Descending) {
        return 2;
    }
    return 0;
} /*else if (sortFieldEnum == SORT_FIELD.Priority) {
if (priority == compareToEmp.priority) {
return 0;
}
if (priority > compareToEmp.priority  && sortDirection == SORT_DIRECTION.Ascending) {
return 1;
}
if (priority < compareToEmp.priority  && sortDirection == SORT_DIRECTION.Descending) {
return 1;
}
return -1;
}*/
            else return NULL;
        }
    }
    
    public static Work_Order__c quickDispatch(Id caseId, Id dispatchToId) {
        system.debug('quickDispatch');
        system.debug('dispatchToId: ' + dispatchToId);
        if(dispatchToId != null){
            Account con = [SELECT Id, Business_Email__c, Dispatch_Me__c, Dispatch_Method__c, Not_To_Exceed__c FROM Account WHERE Id =: dispatchToId LIMIT 1];
            
            Case claim = [SELECT Id, Last_Work_Order__c, of_Work_Orders__c, First_Dispatcher__c, Second_Dispatcher__c, Description,Service_Call_Fee_Final__c, Trip_Charge__c, Contractor__c, NSA_Redispatched__c, 
                          ContactId, ContactEmail, Reason, Status, Policy__r.BillingStateCode, Policy__r.of_Free_Service_Calls__c, AccountId, (SELECT Id, Status__c, Contractor__c FROM Work_Orders__r ORDER BY CreatedDate DESC LIMIT 1)
                          FROM Case WHERE Id =: caseId LIMIT 1];
            
            Dispatcher_History__c dh = new Dispatcher_History__c(
                case__c = claim.Id,
                Type__c = 'Dispatched',
                User__c = UserInfo.getUserId(),
                Dispatch_Status__c = claim.Status
            );
            INSERT dh;
            
            if (claim.Contractor__c == gs.NSA_Contractor_ID__c && con.Id != gs.NSA_Contractor_ID__c) {
                claim.NSA_Redispatched__c = TRUE;
            }
            claim.Contractor__c = con.Id;
            if (claim.Policy__r.of_Free_Service_Calls__c > 0) {
                claim.Policy__r.of_Free_Service_Calls__c--;
                claim.Service_Call_Fee_Override__c = 0;
                UPDATE claim.Policy__r;
            }
            
            //claim.Last_Dispatcher__c = UserInfo.getUserId();
            if (claim.First_Dispatcher__c == NULL) {
                claim.First_Dispatcher__c = UserInfo.getUserId();
            } else if (claim.Second_Dispatcher__c == NULL) {
                claim.Second_Dispatcher__c = UserInfo.getUserId();
            }
            
            if (dispatchToId == gs.NSA_Contractor_ID__c || dispatchToId == gs.GE_Contractor_ID__c) {
                con.Dispatch_Me__c = false;
            } else {
                con.Dispatch_Me__c = true;
            }
            
            if (dispatchToId != gs.NSA_Contractor_ID__c) {
                claim.Status = 'Contractor Accepted';
            }
            con.New__c = FALSE;
            UPDATE con;
            
            Work_Order__c workOrder = new Work_Order__c();
            workOrder.Claim__c = claim.Id;
            workOrder.Description__c = claim.Description;
            workOrder.Service_Charge__c = claim.Service_Call_Fee_Final__c;
            workOrder.Trip_Charge__c = claim.Trip_Charge__c;
            workOrder.Contractor__c = con.Id;
            workOrder.Contractor_Email__c = con.Business_Email__c;
            workOrder.Not_To_Exceed__c = con.Not_To_Exceed__c;
            if (dispatchToId == gs.NSA_Contractor_ID__c) {
                workOrder.Status__c = 'New';
            } else {
                workOrder.Status__c = 'Accepted';
            }
            workOrder.Dispatched_Date__c = Date.today();
            if (dispatchToId == gs.GE_Contractor_ID__c) {
                workOrder.Date_Scheduled__c = Date.today();
            }
            workOrder.Sequence__c = claim.of_Work_Orders__c;
            //workOrder.Last_Dispatched_By__c = UserInfo.getUserId();
            if (workOrder.First_Dispatch_By__c == NULL) {
                workOrder.First_Dispatch_By__c = UserInfo.getUserId();
            } /*else if (workOrder.Second_Dispatch_By__c == NULL) {
workOrder.Second_Dispatch_By__c = UserInfo.getUserId();
}*/
            INSERT workOrder;
            
            claim.Last_Work_Order__c = workOrder.Id;
            UPDATE claim;
            
            con.Recent_Work_Order__c = workOrder.Id;
            UPDATE con;
            //system.debug('sendDispatchEmails-InDM: ' + claim.Policy__r.Dispatchable__c);
            
            /*if (claim.Policy__r.Dispatchable__c == FALSE) { //these emails are only sent in SF if not going through dispatch.me
                //Send Emails
                //Send Contractor Email
                Boolean contractorEmail = Util.sendEmail(claim.ContactId, con.Business_Email__c, 'Contractor_Dispatch_Email', 'Admin Emails', workOrder.Id, 'SHW Claims', false);
                system.debug('contractor email result: ' + contractorEmail);
                
                //Send Customer Email
                Boolean customerEmail = false;
                if (claim.Reason == 'A/C, Cooling' || claim.Reason == 'Heating System') {
                    customerEmail = Util.sendEmail(claim.ContactId, claim.ContactEmail, 'Customer_Work_Order_Notification_w_PicsRecs', null, workOrder.Id, 'SHW Claims', true);
                } else {
                    customerEmail = Util.sendEmail(claim.ContactId, claim.ContactEmail, 'Customer_Work_Order_Notification', null, workOrder.Id, 'SHW Claims', true);
                }
                system.debug('customer email result: ' + customerEmail);
            }*/
            
            
            return workOrder;
        } else {
            return NULL;
        }
    }
    
}