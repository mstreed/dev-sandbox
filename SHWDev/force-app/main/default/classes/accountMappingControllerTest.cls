/*

@Description    accountMappingController Class 
@createdate     16 Feb 2018

*/
@isTest
private class accountMappingControllerTest {
    private static DataFactory df = new DataFactory();
    private static final RecordType contractorRT = [SELECT Id FROM RecordType WHERE Name = 'Contractor Account' and SObjectType = 'Account'];
    
    @isTest
    static void testMethod1() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        //df.createWorkOrder();
        df.newCase.Reason = 'Garbage Disposal';
        UPDATE df.newCase;
        
        df.gs.GE_Contractor_ID__c = df.contractor.id;
        UPDATE df.gs;
        
        Test.StartTest();
        Apexpages.currentpage().getparameters().put( 'id',df.newCase.id );
        accountMappingController controller = new  accountMappingController();
        system.debug(controller.nsaURL);
        controller.CusLat ='40.728004';
        controller.CusLong ='-73.987649';
        controller.googleURL = 'testURL';
        controller.CaseAccountId = 'testId';
        controller.findbyNewCurrentLocation();
        controller.redirectByLocation();
        Apexpages.currentpage().getparameters().put('accId',df.contractor.id );
        controller.accId = df.contractor.id;
        controller.caseId = df.newCase.id;
        controller.nsaURL = 'www.google.ca';
        //controller.workOrderId = df.workOrder.id;
        controller.dispatchWorkOrder();
        controller.dispatchToGE();
        System.assertNotEquals( controller.resultsAvailable, NULL ) ;
        System.assertNotEquals( controller.currentPosition, NULL ) ;
        controller.closePopup();
        Test.StopTest();
    }
    
    @isTest
    static void testMethod2() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        //df.createWorkOrder();
        df.newCase.Reason = 'Garbage Disposal';
        UPDATE df.newCase;

        Account con2 = df.Contractor.clone(false, true, false, false);
        insert con2;
        
        Test.StartTest();
        Apexpages.currentpage().getparameters().put( 'id',df.newCase.id );
        
        System.assertEquals( df.newCase.id, Apexpages.currentpage().getparameters().get('id') );
        accountMappingController controller = new  accountMappingController();
        //system.debug(controller.nsaURL);
        controller.CusLat ='40.728004';
        controller.CusLong ='-73.987649';
        controller.googleURL = 'testURL';
        controller.CaseAccountId = df.acc.Id;
        controller.findbyNewCurrentLocation();
        controller.redirectByLocation();
        controller.caseId = df.newCase.id;
        controller.nsaURL = 'www.google.ca';
        controller.accId = con2.id;
        
        controller.dispatchWorkOrder();
        System.assertNotEquals( controller.resultsAvailable, NULL ) ;
        System.assertNotEquals( controller.currentPosition, NULL ) ;
        Test.StopTest();
    }
    
    @isTest
    static void testMethod3() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createCase();
        //df.createWorkOrder();
        
        Test.StartTest();
        Apexpages.currentpage().getparameters().put( 'id',df.newCase.id );
        System.assertEquals( df.newCase.id, Apexpages.currentpage().getparameters().get('id') );
        accountMappingController controller = new  accountMappingController();
        Apexpages.currentpage().getparameters().put( 'accId',df.contractor.id );
        System.assertEquals( df.contractor.id, Apexpages.currentpage().getparameters().get('accId') );
        controller.dispatchWorkOrder();
        controller.searchvalue = 'Test';
        controller.currentPosition = '40.728004,-73.987649';
        controller.CusLat ='40.728004';
        controller.CusLong ='-73.987649';
        controller.findNearby();
        controller.findbyNewCurrentLocation();
        System.assertNotEquals( controller.resultsAvailable, NULL ) ;
        System.assertNotEquals( controller.currentPosition, NULL ) ;
        controller.street = NULL;
        controller.city = NULL;
        controller.state = NULL;
        controller.country = NULL;
        controller.currentPosition = NULL;
        System.assertEquals( controller.currentPosition, '0,0') ;
        try {accountMappingController.AccountWrapper wrap = new  accountMappingController.AccountWrapper( df.contractor, 50 );}catch(exception e){}
        
        Test.StopTest();
    }
}