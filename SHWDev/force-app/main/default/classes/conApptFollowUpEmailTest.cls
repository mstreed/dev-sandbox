@isTest 
public class conApptFollowUpEmailTest {
    private static DataFactory df = new DataFactory();

    public static testMethod void testBatch() {
        df.createGlobalSetting();
        df.createPolicy();
		df.createCase();
		df.createWorkOrder();
        
        Account contractor = new Account(
            Name ='John Carpenter',
            Business_Email__c = 'test@test.com'
        );
        Database.insert(contractor);
        
        df.newCase.Status = 'Ready to Dispatch';
		update df.newCase;
        df.newCase.Ready_to_Dispatch_Redispatch_Status_Date__c = Date.today().addDays(-10);
        df.newCase.Status = 'Contractor Accepted';
        
        Work_Order__c wo = [Select Id, Dispatched_Date__c, Contractor__c, Contractor_Email__c FROM Work_Order__c WHERE Claim__c = :df.newCase.Id LIMIT 1];
        wo.Dispatched_Date__c = Date.today().addDays(-3);
        wo.Contractor__c = contractor.Id;
        wo.Contractor_Email__c = contractor.Business_Email__c;
        update wo;
        
        df.newCase.Last_Work_Order__c = wo.Id;
        update df.newCase;
        
        Test.StartTest();
        conApptFollowUpEmail_schedule sh1 = new conApptFollowUpEmail_schedule();
        String sch = '0 0 23 * * ?'; system.schedule('Test Territory Check', sch, sh1);
        conApptFollowUpEmail_batch batch = new conApptFollowUpEmail_batch();
        ID batchprocessid = Database.executeBatch(batch);
        Test.StopTest();
    }
}