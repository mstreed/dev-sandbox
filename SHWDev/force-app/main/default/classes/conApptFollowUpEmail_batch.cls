global class conApptFollowUpEmail_batch implements Database.Batchable<SObject> {
    global Database.QueryLocator start(Database.BatchableContext Bc){
        //return Database.getQueryLocator([SELECT Id, Customer_Contact_ID__c, Customer_Email__c, Contractor_Email__c, Claim__r.Contact.EmailBouncedDate, Claim__r.Status, Claim__r.Contractor_Name__c, Dispatched_Date__c, Date_Scheduled__c FROM Work_Order__c WHERE Claim__r.Status = 'Contractor Accepted' AND Dispatched_Date__c >= :Date.Today().addDays(-3) AND Dispatched_Date__c < :Date.Today().addDays(-1) AND Date_Scheduled__c = null AND Dispatched_Date__c != null AND Claim__r.Contractor_Name__c != 'National Service Alliance']);
        return Database.getQueryLocator([SELECT Id, ContactId, ContactEmail, Last_Work_Order__c, Last_Work_Order__r.Contractor_Email__c, Contact.EmailBouncedDate, Status, Last_Work_Order__r.Contractor__c, Contractor__c, Contractor_Name__c, Last_Work_Order__r.Dispatched_Date__c, Last_Work_Order__r.Date_Scheduled__c 
                                         FROM Case 
                                         WHERE Status = 'Contractor Accepted' AND Last_Work_Order__r.Dispatched_Date__c >= :Date.Today().addDays(-3) AND Last_Work_Order__r.Dispatched_Date__c < :Date.Today().addDays(-1) AND Last_Work_Order__r.Date_Scheduled__c = null AND Last_Work_Order__r.Dispatched_Date__c != null AND Last_Work_Order__r.Contractor_Name__c != 'National Service Alliance' AND Last_Work_Order__c != NULL]);
    }

    global void execute(Database.BatchableContext Bc, List<Case> scope){
        Savepoint sp = Database.setSavepoint();
        
        OrgWideEmailAddress owa = [SELECT Id, DisplayName, Address FROM OrgWideEmailAddress WHERE DisplayName = 'SHW Claims' LIMIT 1];
        EmailTemplate conTemplateId = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'Contractor_Dispatched_Reminder' LIMIT 1];
        EmailTemplate cusTemplateId = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'Customer_Dispatched_Reminder' LIMIT 1];
        List<Messaging.SingleEmailMessage> contractorEmails = new List<Messaging.SingleEmailMessage>();
        List<Messaging.SingleEmailMessage> customerEmails = new List<Messaging.SingleEmailMessage>();
        //List<Task> conEmlTasks = new List<Task>();
        
        //random contactId for contractor Emails
        Id contactRecordId = [Select Id FROM Contact WHERE email != null AND EmailBouncedDate = null AND EmailBouncedReason = null LIMIT 1].Id;
        
        try {
            for (Case cs : scope) {
                if (cs.ContactId != null) {
                    //contractorEmails
                    if (cs.Last_Work_Order__r.Contractor_Email__c != null)  {
                        String[] toAddresses = new String[] {cs.Last_Work_Order__r.Contractor_Email__c};
                            
                            Messaging.SingleEmailMessage eml = new Messaging.SingleEmailMessage();
                        eml.setTargetObjectId(contactRecordId); 
                        eml.setTemplateID(conTemplateId.Id); 
                        eml.setSaveAsActivity(true);
                        eml.setOrgWideEmailAddressId(owa.id);
                        eml.setToAddresses(toAddresses);
                        eml.setWhatId(cs.Last_Work_Order__c);
                        eml.setTreatTargetObjectAsRecipient(false);
                        system.debug('conEml: ' + eml);
                        contractorEmails.add(eml);
                    }
                    
                    //customerEmails - can't change to mass email because task needs to be merged to the work order, can't change to wf email alerts because contact comes from case
                    if (cs.Contact.EmailBouncedDate == null && cs.ContactEmail != null)  {
                        String[] toAddresses = new String[] {cs.ContactEmail};
                            
                            Messaging.SingleEmailMessage eml = new Messaging.SingleEmailMessage();
                        eml.setTargetObjectId(cs.ContactId); 
                        eml.setTemplateID(cusTemplateId.Id); 
                        eml.setSaveAsActivity(true);
                        eml.setOrgWideEmailAddressId(owa.id);
                        eml.setToAddresses(toAddresses);
                        eml.setWhatId(cs.Last_Work_Order__c);
                        system.debug('cusEml: ' + eml);
                        customerEmails.add(eml);
                    }
                }
            }
            
            //send contractor emails
            if (contractorEmails.size()>0) {
                Messaging.SendEmailResult[] r = Messaging.sendEmail(contractorEmails);
            }
            
            //send customer emails
            if (customerEmails.size()>0) {
                Messaging.SendEmailResult[] r = Messaging.sendEmail(customerEmails);
            }
            
        } catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
            Database.rollback(sp);
        }
    }
    
    global void finish(Database.BatchableContext Bc){

    }

}