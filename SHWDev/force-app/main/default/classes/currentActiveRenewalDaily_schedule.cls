global class currentActiveRenewalDaily_schedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        currentActiveRenewalDaily_batch batchRun = new currentActiveRenewalDaily_batch(); 
        ID batchId = Database.executeBatch(batchRun,25);
    }
}