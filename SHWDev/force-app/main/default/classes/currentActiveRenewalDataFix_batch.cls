global class currentActiveRenewalDataFix_batch implements Database.Batchable<SObject> {
    global Database.QueryLocator start(Database.BatchableContext Bc){
        return Database.getQueryLocator([SELECT Id, Current_Active_Renewal__c, (SELECT Id, Renewal_Status__c, Renewal_Start_Date__c FROM Policy_Renewals__r WHERE Renewal_Status__c = 'Active' ORDER BY Renewal_Start_Date__c DESC) 
                                         FROM Contract 
                                         WHERE Id IN (SELECT Policy__c FROM Policy_Renewal__c WHERE Renewal_Status__c = 'Active') AND Current_Active_Renewal__c = NULL]);
    }

    global void execute(Database.BatchableContext Bc, List<Contract> scope){
        Savepoint sp = Database.setSavepoint();
        List<Contract> consToUpdate = new List<Contract>();
                
        try {
            for (Contract con : scope) {
                system.debug('con: ' + con);

                for (Policy_Renewal__c pr : con.Policy_Renewals__r) {
                    system.debug('pr: ' + pr);
                    if (pr.Renewal_Start_Date__c == NULL || Date.today() >= pr.Renewal_Start_Date__c) {
                        con.Current_Active_Renewal__c = pr.Id;
                        break;
                    }
                }
            }
			update scope;
        } catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
            Database.rollback(sp);
        }
    }

    global void finish(Database.BatchableContext Bc){

    }

}