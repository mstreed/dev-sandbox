@isTest
private class currentActiveRenewalTest {
    private static DataFactory df = new DataFactory();

    @isTest
    static void testBatches() {
        df.createGlobalSetting();
        df.createPolicy();
        df.createRenewal();
        df.policyRenewal.Renewal_Start_Date__c = Date.today();
        update df.policyRenewal;
        
        df.newPolicy.Current_Active_Renewal__c = NULL;
        update df.newPolicy;
        
        Test.StartTest();
        currentActiveRenewalDataFix_batch batch1 = new currentActiveRenewalDataFix_batch();
        ID batchprocessid1 = Database.executeBatch(batch1);
        
        currentActiveRenewalDaily_schedule sched = new currentActiveRenewalDaily_schedule();
        String sch = '0 0 23 * * ?'; system.schedule('Test', sch, sched);
        currentActiveRenewalDaily_batch batch2 = new currentActiveRenewalDaily_batch();
        ID batchprocessid2 = Database.executeBatch(batch2);
        Test.StopTest();
    }
}