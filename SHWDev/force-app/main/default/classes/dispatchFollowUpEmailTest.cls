@isTest
public class dispatchFollowUpEmailTest {
    private static DataFactory df = new DataFactory();

    public static testMethod void testBatch() {
        df.createGlobalSetting();
        df.createPolicy();
		df.createCase();

        df.newCase.Status = 'Ready to Dispatch';
        update df.newCase;
        df.newCase.Ready_to_Dispatch_Redispatch_Status_Date__c = Date.today().addDays(-10);            
        update df.newCase;

        //Case[] csList = [SELECT Id, CaseNumber, ContactId, ContactEmail, Contact.EmailBouncedDate, Ready_to_Dispatch_Redispatch_Status_Date__c, Status FROM Case WHERE Id = :df.newCase.Id];
        //system.debug('csList: ' + csList);
        
        Test.StartTest();
        dispatchFollowUpEmail_schedule sh1 = new dispatchFollowUpEmail_schedule();
        String sch = '0 0 23 * * ?'; system.schedule('Test Dispatch Follow Up', sch, sh1);
        dispatchFollowUpEmail_batch batch = new dispatchFollowUpEmail_batch();
        ID batchprocessid = Database.executeBatch(batch);
        Test.StopTest();
    }
    
}