global class dispatchFollowUpEmail_batch implements Database.Batchable<SObject> {
    global Database.QueryLocator start(Database.BatchableContext Bc){
        return Database.getQueryLocator([SELECT Id, CaseNumber, ContactId, ContactEmail, Contact.EmailBouncedDate, Contractor_Name__c FROM Case WHERE Ready_to_Dispatch_Redispatch_Status_Date__c < :Date.today() AND (Status = 'Ready to Dispatch' OR Status = 'Redispatch') AND Contact.EmailBouncedDate = null AND Contractor_Name__c != 'National Service Alliance']);
    }

    global void execute(Database.BatchableContext Bc, List<Case> scope){
        Savepoint sp = Database.setSavepoint();
        
        //OrgWideEmailAddress owa = [SELECT Id, DisplayName, Address FROM OrgWideEmailAddress WHERE DisplayName = 'SHW Claims' LIMIT 1];
        //EmailTemplate templateId = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'Dispatch_Status_Follow_Up_Email' LIMIT 1];
        //List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        //List<Id> contactIds = new List<Id>();
        //List<Id> caseIds = new List<Id>();
        List<Case> cases = new List<Case>();
        List<Task> tasks = new List<Task>();
        
        try {
            for (Case cs : scope) {
                if (cs.ContactId != null && cs.ContactEmail != null)  {
                    //contactIds.add(cs.ContactId);
                    //caseIds.add(cs.Id);
                    cases.add(new Case(Id = cs.Id, Dispatch_Follow_Up_Email__c = DateTime.now()));
                    
                    /*String[] toAddresses = new String[] {cs.ContactEmail};
                        
                    Messaging.SingleEmailMessage eml = new Messaging.SingleEmailMessage();
                    eml.setTargetObjectId(cs.ContactId); 
                    eml.setTemplateID(templateId.Id); 
                    eml.setSaveAsActivity(true);
                    eml.setOrgWideEmailAddressId(owa.id);
                    eml.setToAddresses(toAddresses);
                    eml.setWhatId(cs.Id);
                    system.debug('eml: ' + eml);
                    emails.add(eml);*/
                    
                    Task tsk = new Task();
                    tsk.ActivityDate = Date.today();
                    tsk.WhoId = cs.ContactId;
                    tsk.WhatId = cs.Id;
                    tsk.From__c = 'claims@selecthomewarranty.com';
                    tsk.Department__c = 'Claims';
                    tsk.Subject = 'Email: Re: Claim #' + cs.CaseNumber;
                    tsk.Description = 'Dispatch Follow Up Email';
                    tsk.OwnerId = UserInfo.getUserId();
                    tsk.Status = 'Completed';
                    tsk.TaskSubtype = 'Email';
                    tasks.add(tsk);
                }
            }
            
            if (cases.size()>0) {                
                /*Messaging.MassEmailMessage massMail = new Messaging.MassEmailMessage();
                massMail.setTargetObjectIds(contactids);
                massMail.setTemplateId(templateId.Id);
                //massMail.setSaveAsActivity(true);
                massMail.setSenderDisplayName('SHW Claims');
                massMail.setReplyTo('claims@selecthomewarranty.com');
                massMail.setWhatIds(caseIds);
                massMail.setUseSignature(false);
                Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.MassEmailMessage[] { massMail });*/
                
                update cases; //update case so the wf email alert is triggered
                insert tasks;
            }
        } catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
            Database.rollback(sp);
        }
    }
    
    global void finish(Database.BatchableContext Bc){

    }

}