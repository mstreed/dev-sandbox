global class dispatchFollowUpEmail_schedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        dispatchFollowUpEmail_batch batchRun = new dispatchFollowUpEmail_batch(); 
        ID batchId = Database.executeBatch(batchRun,25);
        
    }
}