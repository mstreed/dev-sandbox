public class objectLookupController {
    
    public String searchText {get; set;}
    public String objectName {get; set;}
    public List<Sobject> Sobj {get; set;}
    public List<sObjectWrapper> sObjWrapList {get; set;}
    public String fieldName { get; set; }
    public String nameField { get; set; }
  
    public objectLookupController (){
        sObjWrapList = new List<sObjectWrapper>();
    } 
    
    public String getNameField(SObjectType sObjectType) {
        
        DescribeSObjectResult objDescribe = sobjectType.getDescribe();
        Map<String, SObjectField> fieldsMap = objDescribe.fields.getMap();
        
        for ( SObjectField fieldToken : fieldsMap.values() ) {
            DescribeFieldResult fieldDescribe = fieldToken.getDescribe();
            if ( fieldDescribe.isNameField() ) {
                nameField = fieldDescribe.getLabel();
                return fieldDescribe.getName();
            }      
        }
        return null;
    }
    
    public PageReference runQuery(){
        sObjWrapList.clear();
        String searchVal='\'%' + String.escapeSingleQuotes(searchText.trim()) + '%\'';
        String query = 'SELECT id, ' + 
                            (fieldName == 'to' ? 'Name' : getNameField(schema.getGlobalDescribe().get(objectName)) ) + 
                        ' FROM '+ (fieldName == 'to' ? ( objectName == 'Lead' ? 'Lead' : 'Contact' ) : objectName) + 
                        ' WHERE ' + (fieldName == 'to' ? 'Name' : getNameField(schema.getGlobalDescribe().get(objectName)) ) + 
                        ' LIKE ' + searchVal;

        Sobj = Database.query( query );
        for(sObject sobj: Sobj) {
            sObjectWrapper sobjWrap = new sObjectWrapper();
            sObjWrap.id = sObj.get('id')+'';
            sObjWrap.name = ( fieldName == 'to' ? sObj.get('Name')+'' : sObj.get(getNameField(schema.getGlobalDescribe().get(objectName)))+'' );
            sObjWrapList.add(sObjWrap);
        }

        if( fieldName == 'to' )
            getNameField( schema.getGlobalDescribe().get(objectName == 'Lead' ? 'Lead' : 'Contact'));

        return null;
    }
    
    public void setObjectName(String name) {
        objectName = name;
    }
    
    //Inner class because of we would be interested in common field of all sobjects
    public class sObjectWrapper {
        public string id{get;set;}
        public string name{get;set;}
    }
}