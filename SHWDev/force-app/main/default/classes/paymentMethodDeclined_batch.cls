global class paymentMethodDeclined_batch implements Database.Batchable<SObject> {
    global Database.QueryLocator start(Database.BatchableContext Bc){
        return Database.getQueryLocator([SELECT Id, Policy_Name__c, Policy_Name__r.Account.PersonContactId, Policy_Name__r.Account.PersonEmail, Policy_Name__r.Contract_Number__c, Customer_Email__c, Payment_Method_URL__c, Last_Reminder_Sent__c
                                         FROM Payment_Method__c WHERE Resend_Error_Declined_Email__c = TRUE]);
    }
    
    global void execute(Database.BatchableContext Bc, List<Payment_Method__c> scope){
        Savepoint sp = Database.setSavepoint();
        
        List<Payment_Method__c> paymentMethods = new List<Payment_Method__c>();
        List<Task> tasks = new List<Task>();
        
        try {
            for (Payment_Method__c pm : scope) {
                pm.Customer_Email__c = pm.Policy_Name__r.Account.PersonEmail;
                pm.Error_Declined_Resent__c = DateTime.now(); //this will trigger the wf to resend the email
                paymentMethods.add(pm);
                
                //create a task to log the email that is sent - not using the api to send/log emails to reduce limit usage
                Task tsk = new Task();
                tsk.ActivityDate = Date.today();
                tsk.WhoId = pm.Policy_Name__r.Account.PersonContactId;
                tsk.WhatId = pm.Id;
                tsk.From__c = 'subs@selecthomewarranty.com';
                tsk.Department__c = 'Sales';
                tsk.Subject = 'Email: [Action Required] - Policy #'+pm.Policy_Name__r.Contract_Number__c+' - Problem With Credit Card';
                tsk.Description = 'Customer resent link to payment method page to update credit card: ' + pm.Payment_Method_URL__c;
                tsk.OwnerId = UserInfo.getUserId();
                tsk.Status = 'Completed';
                tsk.TaskSubtype = 'Email';
                tasks.add(tsk);
            }
            
            if (!paymentMethods.isEmpty()) {
                UPDATE paymentMethods;
                INSERT tasks;
            }
        } catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
            Database.rollback(sp);
        }
    }

    global void finish(Database.BatchableContext Bc){

    }

}