global class paymentMethodDeclined_schedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        paymentMethodDeclined_batch batchRun = new paymentMethodDeclined_batch(); 
        ID batchId = Database.executeBatch(batchRun,5);
    }
}