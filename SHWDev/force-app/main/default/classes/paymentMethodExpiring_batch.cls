global class paymentMethodExpiring_batch implements Database.Batchable<SObject> {
    global Database.QueryLocator start(Database.BatchableContext Bc){
        return Database.getQueryLocator([SELECT Id, Policy_Name__r.Account.PersonContactId, Policy_Name__r.Email__c, Policy_Name__r.Contract_Number__c, Customer_Email__c, Payment_Method_URL__c, Last_Reminder_Sent__c, Send_Expiration_Reminder__c
                                         FROM Payment_Method__c WHERE Send_Expiration_Reminder__c = TRUE]);
    }

    global void execute(Database.BatchableContext Bc, List<Payment_Method__c> scope){
        Savepoint sp = Database.setSavepoint();
        
        List<Payment_Method__c> paymentMethods = new List<Payment_Method__c>();
        List<Task> tasks = new List<Task>();
        
        String siteFullUrl = Util.siteURL();

        try {
            for (Payment_Method__c pm : scope) {
                String paymentMethodURL = siteFullUrl;
                paymentMethodURL += '/pm?token=' + pm.Id; 
                
                if (pm.Customer_Email__c != pm.Policy_Name__r.Email__c) {
                    pm.Customer_Email__c = pm.Policy_Name__r.Email__c;
                }

                Blob blobKey = crypto.generateAesKey(128);
                String key = EncodingUtil.convertToHex(blobKey);
                
                pm.Customer_Access_Code__c = key.substring(0,10);
                pm.Payment_Method_URL__c = paymentMethodURL;
                pm.Last_Reminder_Sent__c = DateTime.now(); //Changing this field will trigger WF email alert
                paymentMethods.add(pm);
                
                //create a task to log the email that is sent - not using the api to send/log emails to reduce limit usage
                Task tsk = new Task();
                tsk.ActivityDate = Date.today();
                tsk.WhoId = pm.Policy_Name__r.Account.PersonContactId;
                tsk.WhatId = pm.Id;
                tsk.From__c = 'subs@selecthomewarranty.com';
                tsk.Department__c = 'Sales';
                tsk.Subject = 'Email: [Action Required] - Payment Method on Policy #'+pm.Policy_Name__r.Contract_Number__c+' - Expiring Soon';
                tsk.Description = 'Customer sent link to payment method page to update credit card: ' + paymentMethodURL;
                tsk.OwnerId = UserInfo.getUserId();
                tsk.Status = 'Completed';
                tsk.TaskSubtype = 'Email';
                tasks.add(tsk);
            }
            
            if (!paymentMethods.isEmpty()) {
                update paymentMethods;
                insert tasks;
            }
        } catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
            Database.rollback(sp);
        }
    }

    global void finish(Database.BatchableContext Bc){

    }

}