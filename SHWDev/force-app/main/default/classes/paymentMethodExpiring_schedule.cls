global class paymentMethodExpiring_schedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        paymentMethodExpiring_batch batchRun = new paymentMethodExpiring_batch(); 
        ID batchId = Database.executeBatch(batchRun,5);
    }
}