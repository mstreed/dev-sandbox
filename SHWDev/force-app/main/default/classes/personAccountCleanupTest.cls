@isTest
private class personAccountCleanupTest {
    private static DataFactory df = new DataFactory();

    @isTest
    static void testBatch1() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createWebsiteSettings();
        
        Contact conContact = new Contact(
            AccountId = df.contractor.Id,
            FirstName = 'Test',
            LastName = 'Test',
            Email = 'test@test.com'
        );
        insert conContact;
        
        RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Person Account' and SObjectType = 'Account' LIMIT 1];
        
        Account newPersonAccount = new Account();
        newPersonAccount.FirstName = '[unknown]';                          
        newPersonAccount.LastName = '[unknown]';
        newPersonAccount.PersonEmail = 'test@test.com';
        newPersonAccount.RecordType = personAccountRecordType;
        insert newPersonAccount;
        
        Task newTsk = new Task();
        newTsk.WhoId = [select Id, PersonContactId from Account where Id = :newPersonAccount.Id LIMIT 1].PersonContactId;
        insert newTsk;
        
        Case newCase = new Case(
            AccountId = newPersonAccount.Id,
            ContactId = newTsk.WhoId,
            Subject='subject',
            Status = 'Not Set',
            Description='description'
        );
        insert newCase;
        
        personAccountCleanup_schedule sh1 = new personAccountCleanup_schedule();
        String sch = '0 0 23 * * ?'; system.schedule('Test', sch, sh1);
        personAccountCleanup_Batch batch = new personAccountCleanup_Batch();
        ID batchprocessid = Database.executeBatch(batch);
        
        Test.StopTest();
    }
    
    @isTest
    static void testBatch2() {
        Test.StartTest();
        df.createGlobalSetting();
        df.createWebsiteSettings();
        df.createPolicy();
        
        Contact conContact = new Contact(
            AccountId = df.contractor.Id,
            FirstName = 'Test',
            LastName = 'Test',
            Email = 'test@test.com'
        );
        insert conContact;
        
        RecordType personAccountRecordType = [SELECT Id FROM RecordType WHERE Name = 'Person Account' and SObjectType = 'Account' LIMIT 1];
        
        Account newPersonAccount = new Account();
        newPersonAccount.FirstName = '[unknown]';                          
        newPersonAccount.LastName = '[unknown]';
        newPersonAccount.PersonEmail = 'test@test.com';
        newPersonAccount.RecordType = personAccountRecordType;
        insert newPersonAccount;
        
        Task newTsk = new Task();
        newTsk.WhoId = [select Id, PersonContactId from Account where Id = :newPersonAccount.Id LIMIT 1].PersonContactId;
        insert newTsk;
        
        Case newCase = new Case(
            AccountId = newPersonAccount.Id,
            ContactId = newTsk.WhoId,
            Subject='subject',
            Status = 'Not Set',
            Description='description'
        );
        insert newCase;
        
        personAccountCleanup_Batch batch = new personAccountCleanup_Batch();
        ID batchprocessid = Database.executeBatch(batch);
        
        Test.StopTest();
    }
}