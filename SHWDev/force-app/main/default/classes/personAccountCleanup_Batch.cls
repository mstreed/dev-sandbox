global class personAccountCleanup_Batch implements Database.Batchable<SObject> {
    global Database.QueryLocator start(Database.BatchableContext Bc){
        return Database.getQueryLocator([SELECT Id, Email, Account.RecordType.Name FROM Contact WHERE Account.Policy__c = NULL AND Account.RecordType.Name = 'Person Account']);
    }

    global void execute(Database.BatchableContext Bc, List<Contact> scope){
        Savepoint sp = Database.setSavepoint();
        
        try {
            List<Task> tsksToUpdate = new List<Task>();
            List<Case> casesToUpdate = new List<Case>();
            List<Account> accountToDelete = new List<Account>();
            
            Boolean match = false;
            for (Contact rec : scope) {
                system.debug('rec: ' + rec);

                //match on contract
                String queryString1 = 'Select Id, AccountId, Account.PersonContactId, Email_with_Multiple_Address__c FROM Contract WHERE Email_with_Multiple_Address__c LIKE \'%' + rec.Email + '%\' LIMIT 1';                
                Contract[] matchPol = database.Query(queryString1);              
                
                if (matchPol.size() > 0) {
                    match = true;
                    accountToDelete.add(new Account(Id = rec.AccountId));
                    for (Task tsk : [SELECT Id, WhoId FROM Task WHERE WhoId = :rec.Id]) {
                        tsk.WhoId = matchPol[0].Account.PersonContactId;
                        tsksToUpdate.add(tsk);
                    }
                    
                    for (Case cs : [SELECT Id, AccountId, ContactId FROM Case WHERE ContactId = :rec.Id]) {
                        cs.AccountId = matchPol[0].AccountId;
                        cs.ContactId = matchPol[0].Account.PersonContactId;
                        casesToUpdate.add(cs);
                    }
                }
                
                if (!match) {
                    //Logic to match via email to Contact of contractor or affiliate
                    String queryString2 = 'SELECT Id, AccountId, Email, Record_Type__c FROM Contact WHERE (Record_Type__c = \'Contractor\' OR Record_Type__c = \'Affiliate\') AND Id != \'' + rec.Id+ '\' AND Email = \'' + rec.Email + '\' LIMIT 1';                
                    Contact[] matchCon = database.Query(queryString2);
                    
                    if (matchCon.size() > 0) {
                        match = true;
                        accountToDelete.add(new Account(Id = rec.AccountId));
                        for (Task tsk : [SELECT Id, WhoId FROM Task WHERE WhoId = :rec.Id]) {
                            tsk.WhoId = matchCon[0].Id;
                            tsksToUpdate.add(tsk);
                        }
                        
                        for (Case cs : [SELECT Id, AccountId, ContactId FROM Case WHERE ContactId = :rec.Id]) {
                            cs.AccountId = matchCon[0].AccountId;
                            cs.ContactId = matchCon[0].Id;
                            casesToUpdate.add(cs);
                        }
                    }
                }
            }
            system.debug('match: ' + match);
            
            if (match) {
                if (!tsksToUpdate.isEmpty()) {
                    update tsksToUpdate;
                }
                if (!casesToUpdate.isEmpty()) {
                    update casesToUpdate;
                }
                delete accountToDelete;
            }
            
        } catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
            Database.rollback(sp);
        }
    }

    global void finish(Database.BatchableContext Bc){

    }

}