global class personAccountCleanup_schedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        personAccountCleanup_Batch batchRun = new personAccountCleanup_Batch(); 
        ID batchId = Database.executeBatch(batchRun,1);
    }
}