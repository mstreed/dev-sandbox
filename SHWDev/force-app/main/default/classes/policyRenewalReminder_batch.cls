global class policyRenewalReminder_batch implements Database.Batchable<SObject> {
    global Database.QueryLocator start(Database.BatchableContext Bc){
        return Database.getQueryLocator([SELECT Id, Policy__r.Account.PersonContactId, Policy__r.Email__c, Policy__r.Contract_Number__c, Customer_Email__c, Renewal_Response_URL__c, Last_Reminder_Sent__c, Send_Reminder__c
                                         FROM Renewal_Response__c WHERE Send_Reminder__c = TRUE]);
    }

    global void execute(Database.BatchableContext Bc, List<Renewal_Response__c> scope){
        Savepoint sp = Database.setSavepoint();
        
        List<Renewal_Response__c> responseRenewals = new List<Renewal_Response__c>();
        List<Task> tasks = new List<Task>();
        
        try {
            for (Renewal_Response__c rr : scope) {
                if (rr.Customer_Email__c != rr.Policy__r.Email__c) {
                    rr.Customer_Email__c = rr.Policy__r.Email__c;
                }
                
                rr.Last_Reminder_Sent__c = DateTime.now(); //Changing this field will trigger WF email alert
                responseRenewals.add(rr);
                
                //create a task to log the email that is sent - not using the api to send/log emails to reduce limit usage
                Task tsk = new Task();
                tsk.ActivityDate = Date.today();
                tsk.WhoId = rr.Policy__r.Account.PersonContactId;
                tsk.WhatId = rr.Id;
                tsk.From__c = 'sales@selecthomewarranty.com';
                tsk.Department__c = 'Sales';
                tsk.Subject = 'Email: [Action Required] - Policy #'+rr.Policy__r.Contract_Number__c+' - Expiring Soon';
                tsk.Description = 'Customer re-sent link to policy renewal page because no response has been received yet: ' + rr.Renewal_Response_URL__c;
                tsk.OwnerId = UserInfo.getUserId();
                tsk.Status = 'Completed';
                tsk.TaskSubtype = 'Email';
                tasks.add(tsk);
            }
            
            if (!responseRenewals.isEmpty()) {
                update responseRenewals;
                insert tasks;

            }
        } catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
            Database.rollback(sp);
        }
    }

    global void finish(Database.BatchableContext Bc){

    }

}