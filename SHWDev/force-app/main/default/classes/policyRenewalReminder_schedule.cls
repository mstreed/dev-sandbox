global class policyRenewalReminder_schedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        policyRenewalReminder_batch batchRun = new policyRenewalReminder_batch(); 
        ID batchId = Database.executeBatch(batchRun,25);
    }
}