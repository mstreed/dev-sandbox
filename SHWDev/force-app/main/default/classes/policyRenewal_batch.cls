global class policyRenewal_batch implements Database.Batchable<SObject> {
    global Database.QueryLocator start(Database.BatchableContext Bc){
        //***for velocify***/
        /*return Database.getQueryLocator([SELECT Id, Account.PersonContactId, Email__c, Name, Contract_Number__c, Property_Address__c, Contract_Term__c, Months_Free__c, Service_Call_Fee__c, Payment_Terms__c, Manager_Total_Discounted_Price_Override__c, Amount_Per_Payment__c, Original_Policy_End_Date__c, Total_List_Price_Calculated__c,Total_Price__c,Send_Renewal_Response__c,Velocify_Renewal__c,(SELECT Id, Contract_Term__c, Free_Months_Formatted__c, Service_Call_Fee__c, of_Payments__c, Renewal_Amount__c, Amount_Per_Payment__c, Renewal_Amount_Total_Price__c FROM Policy_Renewals__r WHERE Renewal_Status__c = 'Active' ORDER BY CreatedDate DESC) 
                                         FROM Contract WHERE Send_Renewal_Response__c = TRUE OR Velocify_Renewal__c = TRUE]);*/
        
        return Database.getQueryLocator([SELECT Id, Account.PersonContactId, Email__c, Name, Contract_Number__c, Property_Address__c, Contract_Term__c, Months_Free__c, Service_Call_Fee__c, Payment_Terms__c, Manager_Total_Discounted_Price_Override__c, Amount_Per_Payment__c, Original_Policy_End_Date__c, Total_List_Price_Calculated__c,Total_Price__c,Send_Renewal_Response__c,(SELECT Id, Contract_Term__c, Free_Months_Formatted__c, Service_Call_Fee__c, of_Payments__c, Renewal_Amount__c, Amount_Per_Payment__c, Renewal_Amount_Total_Price__c FROM Policy_Renewals__r WHERE Renewal_Status__c = 'Active' ORDER BY CreatedDate DESC) 
                                         FROM Contract WHERE Send_Renewal_Response__c = TRUE]);
    }

    global void execute(Database.BatchableContext Bc, List<Contract> scope){
        Savepoint sp = Database.setSavepoint();
        List<Renewal_Response__c> responseRenewals = new List<Renewal_Response__c>();
        List<Task> tasks = new List<Task>();
        Map<Id, Contract> contractMap = new Map<Id, Contract>();
        
        String siteFullUrl = Util.siteURL();
        
        try {
            for (Contract con : scope) {
                contractMap.put(con.Id, con);
                
                //60 days out, SF sends a renewal response
                if (con.Send_Renewal_Response__c) {
                    Renewal_Response__c newRR = new Renewal_Response__c (
                        Policy__c = con.Id,
                        Policy_End_Date__c = con.Original_Policy_End_Date__c,
                        Customer_Email__c = con.Email__c,
                        Total_List_Price__c = con.Total_List_Price_Calculated__c
                    );
                    system.debug('con.Policy_Renewals__r: ' + con.Policy_Renewals__r);
                    
                    Boolean hasRenewal = false;
                    for (Policy_Renewal__c pr : con.Policy_Renewals__r) {
                        hasRenewal = true;
                        
                        newRR.Contract_Term__c = pr.Contract_Term__c;
                        newRR.Free_Months__c = pr.Free_Months_Formatted__c; 
                        newRR.Payment_Terms__c = pr.of_Payments__c;
                        newRR.Service_Call_Fee__c = pr.Service_Call_Fee__c;
                        newRR.Total_Price__c = pr.Renewal_Amount_Total_Price__c;
                        newRR.Source_Policy_Renewal__c = pr.Id;
                        
                        break;
                    }
                    
                    if (hasRenewal == false && con.Contract_Term__c != 'Monthly') {
                        newRR.Contract_Term__c = con.Contract_Term__c;
                        newRR.Free_Months__c = con.Months_Free__c;
                        newRR.Payment_Terms__c = con.Payment_Terms__c;
                        newRR.Service_Call_Fee__c = con.Service_Call_Fee__c;
                        newRR.Total_Price__c = con.Total_Price__c;
                    } else if (hasRenewal == false && con.Contract_Term__c == 'Monthly') { //skip monthly contracts
                        break;
                    }
                    
                    responseRenewals.add(newRR);
                }
                
                //***for velocify***/
                //45 days out, update policy details for velocify
                /*if (con.Velocify_Renewal__c) {
                    con.Velocify_Status__c = 'Renewal - New	';
                }*/
            }
            
            if (!responseRenewals.isEmpty()) {
                insert responseRenewals;
                
                for (Renewal_Response__c rr : responseRenewals) {
                    String renewalURL = siteFullUrl;
                    renewalURL += '/pr?token=' + rr.Id; 
                    rr.Renewal_Response_URL__c = renewalURL; //setting the response url will trigger the WF email
                    
                    //create a task to log the email that is sent - not using the api to send/log emails to reduce limit usage
                    Task tsk = new Task();
                    tsk.ActivityDate = Date.today();
                    tsk.WhoId = contractMap.get(rr.Policy__c).Account.PersonContactId;
                    tsk.From__c = 'sales@selecthomewarranty.com';
                    tsk.WhatId = rr.Id;
                    tsk.Department__c = 'Sales';
                    tsk.Subject = 'Email: [Action Required] - Policy #'+contractMap.get(rr.Policy__c).Contract_Number__c+' - Expiring Soon';
                    tsk.Description = 'Customer sent link to policy renewal page: ' + renewalURL;
                    tsk.OwnerId = UserInfo.getUserId();
                    tsk.Status = 'Completed';
                    tsk.TaskSubtype = 'Email';
                    tasks.add(tsk);
                }
                
                update responseRenewals;
                insert tasks;
            }
            
            UPDATE scope;
            system.debug('responseRenewals: ' + responseRenewals);
        } catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
            Database.rollback(sp);
        }
    }

    global void finish(Database.BatchableContext Bc){

    }

}