global class policyRenewal_schedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        policyRenewal_batch batchRun = new policyRenewal_batch(); 
        ID batchId = Database.executeBatch(batchRun,25);
    }
}