@isTest
public class proformaFollowUpEmailTest {
    private static DataFactory df = new DataFactory();

    public static testMethod void testBatch() {
        df.createGlobalSetting();
        df.createLead();
        df.newLead.Status = 'Proforma';
        df.newLead.Proforma_Closing_Date__c = Date.today().addDays(7);
        df.newLead.Realtor_s_Email__c = 'test@test.com';
        update df.newLead;
        
        Test.StartTest();
        proformaFollowUpEmail_schedule sh1 = new proformaFollowUpEmail_schedule();
        String sch = '0 0 23 * * ?'; system.schedule('Test', sch, sh1);
        proformaFollupUpEmail_batch batch = new proformaFollupUpEmail_batch();
        ID batchprocessid = Database.executeBatch(batch);
        Test.StopTest();
    }
    
}