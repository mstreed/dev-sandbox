global class proformaFollowUpEmail_schedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        proformaFollupUpEmail_batch batchRun = new proformaFollupUpEmail_batch(); 
        ID batchId = Database.executeBatch(batchRun,25);
        
    }
}