global class proformaFollupUpEmail_batch implements Database.Batchable<SObject> {
    global Database.QueryLocator start(Database.BatchableContext Bc){
        return Database.getQueryLocator([SELECT Id, Name, Email, Realtor_s_Email__c, of_Days_Before_Proforma_Closing__c FROM Lead WHERE (of_Days_Before_Proforma_Closing__c = 7 OR of_Days_Before_Proforma_Closing__c = -14) AND Status = 'Proforma' AND IsConverted = FALSE AND Lead.EmailBouncedDate = null]);
    }

    global void execute(Database.BatchableContext Bc, List<Lead> scope){
        Savepoint sp = Database.setSavepoint();
        
        OrgWideEmailAddress owa = [SELECT Id, DisplayName, Address FROM OrgWideEmailAddress WHERE DisplayName = 'SHW Sales' LIMIT 1];
        EmailTemplate oneWeekTemplate = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'Proforma_1_Week_Email' LIMIT 1];
        EmailTemplate twoWeekTemplate = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'Proforma_2_Week_Email' LIMIT 1];
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
                
        try {
            for (Lead ld : scope) {
                
                String[] toAddresses = new String[] {ld.Email};
                    if (ld.Realtor_s_Email__c != NULL) {
                        toAddresses.add(ld.Realtor_s_Email__c);
                    }
                
                Messaging.SingleEmailMessage eml = new Messaging.SingleEmailMessage();
                eml.setTargetObjectId(ld.Id); 
                eml.setSaveAsActivity(true);
                eml.setOrgWideEmailAddressId(owa.id);
                eml.setToAddresses(toAddresses);
                eml.setTreatTargetObjectAsRecipient(true);
                
                if (ld.of_Days_Before_Proforma_Closing__c == 7) {
                    eml.setTemplateID(oneWeekTemplate.Id); 
                } else if (ld.of_Days_Before_Proforma_Closing__c == -14) {
                    eml.setTemplateID(twoWeekTemplate.Id); 
                }
                
                system.debug('ldEml: ' + eml);
                emails.add(eml);
                
            }
            
            //send emails
            if (emails.size()>0) {
                Messaging.SendEmailResult[] r = Messaging.sendEmail(emails);
            }
            
        } catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
            Database.rollback(sp);
        }
    }
    
    global void finish(Database.BatchableContext Bc){

    }

}