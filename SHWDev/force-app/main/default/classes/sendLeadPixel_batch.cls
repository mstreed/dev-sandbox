global class sendLeadPixel_batch implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {

    private Id affId;
    private DateTime stDate;
    private DateTime edDate;

    public sendLeadPixel_batch(Id affiliteId, DateTime startDate, DateTime endDate) {
        affId = affiliteId;
        stDate = startDate;
        edDate = endDate;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([SELECT Id, Customer_ID__c, Status, OwnerId, Affiliate_Account__c, Affiliate_Endpoint_URL__c, Affiliate_Request_Type__c, Sub_ID__c, Sub_ID2__c, CreatedDate FROM Lead WHERE Affiliate_Account__c = :affId AND CreatedDate >= :stDate AND CreatedDate <= :edDate]);

    }

    global void execute(Database.BatchableContext BC, List<Lead> scope) {
        system.debug('==========check string====='+affId);    
        system.debug('==========check string====='+stDate);    
        system.debug('==========check string====='+edDate);    
        
        for (Lead ld : scope) {
            system.debug('ld: ' + ld);
            sendLeadPixel(ld.Affiliate_Endpoint_URL__c, ld.Affiliate_Request_Type__c, ld.Customer_ID__c, ld.Sub_ID__c, ld.Sub_ID2__c);
        }
    }   

    global void finish(Database.BatchableContext BC) {
    }
    
    private static void sendLeadPixel(String endpoint, String reqType, String custId, String subId, String subId2){
        system.debug('sendLeadPixel');
        system.debug('endpoint: ' + endpoint);
        system.debug('reqType: ' + reqType);
        system.debug('custId: ' + custId);
        system.debug('subId: ' + subId);
        system.debug('subId2: ' + subId2);
        
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        if (custId != NULL) {
            endpoint = endpoint.replace('#{custId}', custId);
        }
        if (subId != NULL) {
            endpoint = endpoint.replace('#{subId}', subId);
        }
        if (subId2 != NULL) {
            endpoint = endpoint.replace('#{subId2}', subId2);
        }
        endpoint = endpoint.replace('#{dateTimeNow}', DateTime.now().format('yyyy-MM-dd HH:mm:ss', UserInfo.getTimeZone().toString()));
        system.debug('endpoint: ' + endpoint);
                       
        req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod(reqType);
        //req.setBody( 'email=' + gs.Pardot_Email__c + '&password=' + Crypto.decryptWithManagedIV('AES256', EncodingUtil.base64Decode(gs.AES256_Key__c), EncodingUtil.base64Decode(gs.Pardot_Password_Encrypted__c)).toString() + '&user_key=' + gs.Pardot_User_Key__c);
        //req.setHeader('Authorization', 'Pardot user_key=' + gs.Pardot_User_Key__c + ', api_key=' + gs.Pardot_API_Key__c);
        System.debug('req-sendLeadPixel: ' + req );
        //System.debug('reqBody-sendLeadPixel: ' + req.getBody());

        if (!Test.isRunningTest()) {
            res = new Http().send( req );
            
            System.debug('res-sendLeadPixel: ' + res );
            System.debug('res.getStatus()-sendLeadPixel: ' + res.getStatus());
            System.debug('res.getBody()-sendLeadPixel: ' + res.getBody());
        }
    }
}