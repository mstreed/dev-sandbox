/*
* 	PURPOSE:        Establishes a trigger framework for the Case object.
                	Extension of TriggerHandler.class (virtual class) methods occurs
                	in TriggerHandlerCase.class
*	INTERFACES:     See specific method descriptions TriggerHandlerCase.class.
*/
trigger CaseAssignmentRoundRobinTrigger on Case_Assignment_Round_Robin__c (
    before insert,
    before update,
    after insert,
    after update) {

	new TriggerHandlerCaseAssignmentRoundRobin().run();
}