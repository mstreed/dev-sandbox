/*
* 	PURPOSE:        Establishes a trigger framework for the Contract object.
                	Extension of TriggerHandler.class (virtual class) methods occurs
                	in TriggerHandlerContract.class
*	INTERFACES:     See specific method descriptions TriggerHandlerContract.class.
*/
trigger ContractTrigger on Contract ( before insert,
                                     before update,
                                     before delete,
                                     after insert,
                                     after update,
                                     after delete,
                                     after undelete ) {
                                         new TriggerHandlerContract().run();
                                     }



/*trigger ContractTrigger on Contract (before insert) {
    if(trigger.isBefore && trigger.isInsert){
        map<String,String> mapAddonCheckboxes = new map<String, String>{'5' => 'Central_Vacuum__c',
                                                                        '28' => 'Ice_Maker_In_Refrigerator__c',
                                                                        '24' => 'Lawn_Sprinkler_System__c',
                                                                        '29' => 'Lighting_Fixtures_Plumbing_Fixtures__c',
                                                                        '4' => 'Pool__c',
                                                                        '13' => 'Roof_Leak__c',
                                                                        '10' => 'Second_Refrigerator__c',
                                                                        '25' => 'Septic_System__c',
                                                                        '36' => 'Spa__c',
                                                                        '23' => 'Stand_Alone_Freezer__c',
                                                                        '11' => 'Sump_Pump__c',
                                                                        '12' => 'Well_Pump__c'};
        
        map<String, String> mapAddonPicklists = new map<String, String>{'30' => 'Additional_AC_Unit_each__c',
                                                                        '34' => 'Additional_Heat_Furnace_each__c',
                                                                        '35' => 'Additional_Water_Heater_each__c'};
                                                                            
        for(Contract l : trigger.new){
            if(String.isNotBlank(l.Coverage_Addon__c)){
                List<String> addonNumbers = l.Coverage_Addon__c.trim().Split(',');
                for(String i : addonNumbers){
                    if(mapAddonCheckboxes.containsKey(i))
                        l.put(mapAddonCheckboxes.get(i), true);
                    else if(mapAddonPicklists.containsKey(i))
                        l.put(mapAddonPicklists.get(i), '1'); // Assuming 1 as default value.
                }
            }
        }
    }
}*/