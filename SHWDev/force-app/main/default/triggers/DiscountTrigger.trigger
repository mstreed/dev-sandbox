trigger DiscountTrigger on Discount__c ( 
    before insert,
    before update,
    //before delete,
    ///fter update,
    //after delete,
    //after undelete 
    after insert) {
        new TriggerHandlerDiscount().run();
    }