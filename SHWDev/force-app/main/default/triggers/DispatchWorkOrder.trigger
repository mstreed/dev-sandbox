trigger DispatchWorkOrder on Work_Order__c (after update, after insert) {
    
    // ---------------------------------------------------------------------------
    // Dispatch
    // ---------------------------------------------------------------------------
    //if (Trigger.isAfter) {
        if (DispatchTriggerHandler.triggersEnabled()) {
            DispatchTriggerHandler.disableTriggers();
            DispatchTriggerHandler.DispatchJobToDispatch(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap);
            DispatchTriggerHandler.enableTriggers();
        }
    //}
    // ---------------------------------------------------------------------------
}