trigger EmailPolicyCongaRedirect on Attachment (before insert) {
	system.debug('Trigger.new: ' + Trigger.new);
    for(Attachment att: Trigger.new){
        system.debug('att.ParentId.left(3): '+ string.valueOf(att.ParentId).left(3));
        system.debug('Contract.SObjectType.getDescribe().getKeyPrefix(): '+ Contract.SObjectType.getDescribe().getKeyPrefix());
        if (string.valueOf(att.ParentId).left(3) == Contract.SObjectType.getDescribe().getKeyPrefix() && att.Name.startsWith('Select Home Warranty Policy')) {
            Task[] eml = [Select Id FROM Task WHERE Status = 'Completed' AND TaskSubtype = 'Email' AND WhatId = :att.ParentId ORDER BY CreatedDate DESC LIMIT 1];
            att.ParentId = eml[0].Id;
        }
    }
}