trigger GatewayAssignmentTrigger on Gateway_Assignment__c ( 
    before insert,
    before update,
    before delete,
    after insert,
    after update,
    after delete,
    after undelete ) {
        new TriggerHandlerGatewayAssignment().run();
    }