trigger NoteTrigger on ContentDocument (before delete, before update) {
    for (ContentDocument c : Trigger.old) {
        if (c.FileType == 'SNOTE') {
            if (Trigger.isDelete) {
                c.addError('You cannot delete a Note');
            }
        }
    }
    
    for (ContentDocument c : Trigger.new) {
        if (c.FileType == 'SNOTE') {
            if (Trigger.isUpdate && Date.newInstance(c.CreatedDate.year(), c.CreatedDate.month(), c.CreatedDate.day()) != Date.today()) {
                c.addError('You cannot edit a Note');
            }
        }
    }
}