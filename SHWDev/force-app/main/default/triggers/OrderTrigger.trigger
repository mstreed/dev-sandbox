trigger OrderTrigger on ChargentOrders__ChargentOrder__c ( 
    before insert,
    before update,
    before delete,
    after insert,
    after update,
    after delete,
    after undelete ) {
        new TriggerHandlerOrder().run();
    }