/*
*   PURPOSE:        Establishes a trigger framework for the Payment_Method__c object.
                    Extension of TriggerHandler.class (virtual class) methods occurs
                    in TriggerHandlerPaymentMethod.class
*   INTERFACES:     See specific method descriptions TriggerHandlerPaymentMethod.class.
*/
trigger PaymentMethodTrigger on Payment_Method__c ( before insert,
                              before update,
                              before delete,
                              after insert,
                              after update,
                              after delete,
                              after undelete ) {

    new TriggerHandlerPaymentMethod().run();
}