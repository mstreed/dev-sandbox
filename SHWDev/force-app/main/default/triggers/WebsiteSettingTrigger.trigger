trigger WebsiteSettingTrigger on Website_Setting__c (after insert, after update) {
    for(Website_Setting__c ws : trigger.new){
        system.debug('ws: ' + ws);
        system.debug('ws.Record_Type__c: ' + ws.Record_Type__c);
        if (ws.Active__c && ws.Record_Type__c == 'Global Setting') {
            CalloutUtil.rebuildWebsite();
        }
    }
}