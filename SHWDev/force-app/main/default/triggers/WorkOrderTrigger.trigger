/*
* 	PURPOSE:        Establishes a trigger framework for the Case object.
                	Extension of TriggerHandler.class (virtual class) methods occurs
                	in TriggerHandlerWorkOrder.class
*	INTERFACES:     See specific method descriptions TriggerHandlerWorkOrder.class.
*/
trigger WorkOrderTrigger on Work_Order__c ( before insert,
							  before update,
							  before delete,
							  after insert,
							  after update,
							  after delete,
							  after undelete ) {

	new TriggerHandlerWorkOrder().run();
}